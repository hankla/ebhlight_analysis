import matplotlib.pyplot as plt
import sys
import numpy as np
sys.path.append('modules/')
import hdf5_to_dict as io
from simulation_plotter import *
from post_processor import *
import plotting_utils as pu
sims = [
    "test_torus_isothermal",
    # "test_negQcoul",
    # "test_noNegQcoul"
]
#
sigma_cut = 1.
dumps = {}
headers = {}
geoms = {}
base_path = {}
for sim in sims:
    filename = "/mnt/c/Users/liaha/research/projects/ebhlight_code/ebhlight_ecool/prob/" + sim + "/dumps/dump_00000304.h5"
    base_path[sim] = "/mnt/c/Users/liaha/research/projects/ebhlight_code/ebhlight_ecool/prob/" + sim + "/dumps/"
    dumps[sim] = io.load_dump(filename)
    headers[sim] = io.load_hdr(filename)
    geoms[sim] = io.load_geom(headers[sim])

Te = dumps[sims[0]]['Te']
Tp = dumps[sims[0]]['Tp']
TpTe = dumps[sims[0]]['TpTe']
sigma_mask = dumps[sims[0]]['sigma'] < sigma_cut
# Always true
TpTemask = TpTe < headers[sims[0]]['tptemax']
# Always false
TpTemask = np.isclose(TpTe, headers[sims[0]]['tptemax'])
TpTe_minmask = np.isclose(TpTe, headers[sims[0]]['tptemin'], atol=1.e-6)
TpTe_maxmask = np.isclose(TpTe, headers[sims[0]]['tptemax'], atol=1.e-6)
Temask = np.isclose(Te, headers[sims[0]]['Tel_target'])
# print(TpTemask == Temask)
print("{:d} cells are at the target temperature".format(Temask.sum()))
if TpTe_minmask.any():
    print("The TpTe minimum is being hit for {:d} cells!".format(TpTe_minmask.sum()))
else:
    print("The TpTe minimum is NOT being hit.")
if TpTe_maxmask.any():
    print("The TpTe maximum is being hit for {:d} cells!".format(TpTe_maxmask.sum()))
else:
    print("The TpTe maximum is NOT being hit.")
print("Total cells: {:d}. TpTe floor + Target: {:d}".format(Te.size, TpTe_maxmask.sum() + TpTe_minmask.sum() + Temask.sum()))
# print((np.isclose(Te*TpTemask, headers[sims[0]]['Tel_target'])).sum())
# print(np.allclose(dumps[sims[0]]['Te'], headers[sims[0]]['Tel_target'])E
fig, axs = plt.subplots(1, 3, sharex=True, sharey=True)
rcyl, z, mesh, sc = pu.plot_xz(axs[0], geoms[sims[0]], Te, headers[sims[0]], cmap='RdBu_r', vmin=1.e8, vmax=1.e10)
con_data1 = plt.contour(rcyl, z, dumps[sims[0]]['sigma'][:, :, 0], levels=np.array([1.0]), colors=sc)
plt.colorbar(mesh, ax=axs[0], label=r"$T_e$")
rcyl, z, mesh, sc = pu.plot_xz(axs[1], geoms[sims[0]], TpTe, headers[sims[0]], cmap='RdBu_r', vmin=headers[sims[0]]['tptemin'], vmax=headers[sims[0]]['tptemax'])
con_data2 = plt.contour(rcyl, z, dumps[sims[0]]['sigma'][:, :, 0], levels=np.array([1.0]), colors=sc)
plt.colorbar(mesh, ax=axs[1], label=r"$T_p/T_e$")
rcyl, z, mesh, sc = pu.plot_xz(axs[2], geoms[sims[0]], Tp, headers[sims[0]], cmap='RdBu_r', vmin=1.e8, vmax=1.e10)
plt.colorbar(mesh, ax=axs[2], label=r"$T_p$")
plt.xlim([0., 100.])
plt.ylim([-100., 100.])
plt.tight_layout()

plt.figure()
diag = io.load_diag(base_path[sims[0]])
print((diag['num_super'] > 0).any())
plt.plot(diag['t'], diag['num_super'])
plt.show()
exit()
print((dumps[sims[0]]['RHO'] == dumps[sims[1]]['RHO']).all())
print((dumps[sims[0]]['Qcoul'] == dumps[sims[1]]['Qcoul']).all())

print((dumps[sims[0]]['Qcoul'] < 0.0).any())
print((dumps[sims[1]]['Qcoul'] < 0.0).any())
print(dumps[sims[0]]['Qcoul'].min())
print(dumps[sims[1]]['Qcoul'].min())
print(dumps[sims[0]]['Qcoul'].max())
print(dumps[sims[1]]['Qcoul'].max())
exit()
# sim_name = "test_torus_isothermal"
# setup = "lia_hp"
# kwargs = {
    # "path_to_raw_data":"/mnt/c/Users/liaha/research/projects/ebhlight_code/ebhlight_ecool/prob/test_torus_isothermal/dumps/",
    # "path_to_reduced_data":"/mnt/c/Users/liaha/research/projects/ebhlight_code/ebhlight_ecool/prob/test_torus_isothermal/reduced/",
    # "path_to_properties":"/mnt/c/Users/liaha/research/projects/ebhlight_code/ebhlight_ecool/prob/test_torus_isothermal/reduced/properties.p",
    # "path_to_figures":"/mnt/c/Users/liaha/research/projects/ebhlight_code/ebhlight_ecool/prob/test_torus_isothermal/figures/",
# }
# sim_postprocess = post_processor(sim_name, setup, **kwargs)
# sim_postprocess.reduce_data()
# kwargs["tstart_shells"] = 29
# kwargs["tend_shells"] = 10
# kwargs["time_average_shells"] = False
# shell_quantities_to_plot = [
    # "Qvisc_e", "Qvisc_p",
    # "coulViscRatioPost",
    # "coolViscRatioPost",
    # "coolTotalRatioPost",
    # "Thetae", "Thetap", "sigma", "RHO", "beta",
    # "Te", "Tp", "TpTe",
    # "RHO", "ue", "up", "UU",
    # "KEL", "KTOT",
    # "Qcool",
    # "Qcoul",
    # "H_over_R"
# ]
# kwargs["shell_quantities_to_plot"] = shell_quantities_to_plot
# sim_plotter = simulation_plotter(sim_name, setup, **kwargs)
# sim_plotter.plot_shell_averages(**kwargs)
# exit()
# sims = ["dumps"]
# for dumppath in sims:
    # filename = "/mnt/c/Users/liaha/research/projects/ebhlight_code/ebhlight_ecool/prob/test_torus_isothermal/" + dumppath + "/dump_00000012.h5"
    # dumps[dumppath] = io.load_dump(filename)
    # headers[dumppath] = io.load_hdr(filename)
Te = dumps[sims[0]]['Te']
Tp = dumps[sims[0]]['Tp']
Tp_over_Te = Tp/Te
KEL = dumps[sims[0]]['KEL']
KTOT = dumps[sims[0]]['KTOT']
rho = dumps[sims[0]]['RHO']
Qcool = dumps[sims[0]]['Qcool']
Qcoul = dumps[sims[0]]['Qcoul']
ue = dumps[sims[0]]['ue']
hdr = headers[sims[0]]
gam = hdr["gam"]
game = hdr["game"]
gamp = hdr["gamp"]
tptemin = hdr["tptemin"]
tptemax = hdr["tptemax"]
kelmax = KTOT*rho**(gam-game)/(tptemin*(gam-1.)/(gamp-1.) + (gam-1.)/(game-1.))
kelmin = KTOT*rho**(gam-game)/(tptemax*(gam-1.)/(gamp-1.) + (gam-1.)/(game-1.))

print(np.isinf(rho).any())
print(np.isinf(ue).any())
print(np.isnan(Qcool).any())
print(np.isinf(Qcoul).any())
print(np.allclose(Qcoul, 1e-30))
print(np.where(np.isinf(Qcoul)))
# plt.imshow(Qcoul)
# plt.show()
# exit()
Te_min = Te.min()
Te_min_loc = np.argwhere(Te == Te_min)
print(hdr.keys())
print(np.allclose(Te, 1.0e9))
print(Te.shape)
print("{:.9e}".format(Te.max()))
print("{:.9e}".format(Te_min))
# print(Te_min_loc)
# print(Te[31][0][0])
# print(np.min(Te[:-1][:][:]))
print(np.allclose(Te[:-1][:][:], 1.0e9))
plt.plot(KEL[:, 0])
plt.plot(kelmax[:, 0], ls='--', color='black')
plt.plot(kelmin[:, 0], ls='--', color='black')
plt.ylabel(r"$\kappa_e$")
plt.figure()
plt.plot(Tp_over_Te[:, 0])
plt.plot(np.ones(kelmax[:,0].shape)*tptemin, ls='--', color='black')
plt.plot(np.ones(kelmax[:,0].shape)*tptemax, ls='--', color='black')
plt.yscale('log')
plt.ylabel(r"$T_p/T_e$")
plt.figure()
plt.plot(Tp[:,0])
plt.yscale('log')
plt.ylabel(r"$T_p$")
plt.figure()
plt.plot(Qcoul[:,10], label=r"$Q_{\rm coul}$")
plt.plot(Qcool[:,10], label=r"$Q_{\rm cool}$")
plt.title("Mass unit: {:.2e}".format(hdr['M_unit']))
plt.yscale('log')
plt.legend()
print(tptemax)
print(tptemin)
print(Tp_over_Te.max())
plt.show()
# sims = ["dumps", "dumps_noQv", "dumps_Qv1em6"]
# for dumppath in sims:
    # filename = "/mnt/c/Users/liaha/research/projects/ebhlight_code/ebhlight_ecool/prob/template_torus_ecool/" + dumppath + "/dump_00000001.h5"
    # dumps[dumppath] = io.load_dump(filename)

# print(dumps[sims[0]]['Qvisc_e'][0][2][0])
# print(dumps[sims[1]]['Qvisc_e'][0][2][0])
# print(dumps[sims[2]]['Qvisc_e'][0][2][0])
# print(dumps[sims[2]]['Qvisc_e'])
# print((dumps[sims[0]]['RHO'] == dumps[sims[1]]['RHO']).all())
# print((dumps[sims[0]]['KEL'] == dumps[sims[1]]['KEL']).all())
# print((dumps[sims[0]]['KTOT'] == dumps[sims[1]]['KTOT']).all())
# print((dumps[sims[0]]['UU'] == dumps[sims[1]]['UU']).all())
# print((dumps[sims[0]]['Qvisc_e'] == dumps[sims[1]]['Qvisc_e']).all())
# print("Three-velocities:")
# print((dumps[sims[0]]['U1'] == dumps[sims[1]]['U1']).all())
