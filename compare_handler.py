import numpy as np
import os
import scipy.integrate as integrate
import matplotlib.pyplot as plt
import sys
sys.path.append('modules/')
from plotting_utils import *
import hdf5_to_dict as io
from simulation_plotter import *
from comparison_list import set_categories
import nice_plots as mp
from units import cgs


class comparison:
    # TODO: make sim analyzer data more easily accessible
    def __init__(self, category, setup="lia_hp", **kwargs):
        self.setup = setup
        self.category = category
        # -----------------------------------
        #      Add default setup information here
        # -----------------------------------
        if setup == "lia_hp":
            path_to_figures = "/mnt/c/Users/liaha/research/projects/"\
                "implementing_ebhlight_ecool/figures/comparison/" + category + "/"
            path_to_reduced_data = "/mnt/c/Users/liaha/research/projects/"\
                "implementing_ebhlight_ecool/reduced_data/comparison/"
        elif setup == "seagate_tests":
            path_to_figures = "/mnt/e/ebhlight_ecool_tests/figures/comparison/"\
                + category + "/"
            path_to_reduced_data = "/mnt/e/ebhlight_ecool_tests/reduced_data/comparison/"

        # -----------------------------------
        # If paths are non-standard, set them here using kwargs
        # Otherwise, use the defaults from above
        # -----------------------------------
        self.path_to_figures = kwargs.get("path_to_figures", path_to_figures)
        self.path_to_reduced_data = kwargs.get("path_to_reduced_data",
                                               path_to_reduced_data)

        self._initialize(**kwargs)
        self.get_labels()
        self.get_category_values()
        self.get_list_of_quantities()

        # -----------------------------------
        # Load simulations
        # -----------------------------------
        self.simulations = {}
        self.last_times = {}
        self.rEHs = {}
        self.rISCOs = {}
        if "quiet" not in kwargs:
            kwargs["quiet"] = True
        for sim_name in self.sim_names:
            self.simulations[sim_name] = simulation_plotter(sim_name, self.setup, **kwargs)
            dump_nums, dump_rgcs = self.simulations[sim_name].load_dump_times("shells")
            self.last_times[sim_name] = np.max(dump_rgcs)
            self.rEHs[sim_name] = self.simulations[sim_name].rEH
            self.rISCOs[sim_name] = self.simulations[sim_name].rISCO
        self.last_common_time = np.min(list(self.last_times.values()))
        if len(set(list(self.rEHs.values()))) == 1:
            self.rEH = list(self.rEHs.values())[0]
        if len(set(list(self.rISCOs.values()))) == 1:
            self.rISCO = list(self.rISCOs.values())[0]
        return

    def _initialize(self, **kwargsOuter):
        print("Initializing comparison " + self.category)
        # -----------------------------------
        # Create directories if need be
        # -----------------------------------
        if not os.path.exists(self.path_to_figures):
            os.makedirs(self.path_to_figures)
        if not os.path.exists(self.path_to_reduced_data):
            os.makedirs(self.path_to_reduced_data)

        self.get_category_values()
        self.get_list_of_quantities()
        self.get_labels()
        self.rISCO = None
        self.rEH = None

    def update_path_to_figures(self, new_path):
        self.path_to_figures = new_path
        return

    def update_path_to_reduced_data(self, new_path):
        self.path_to_reduced_data = new_path
        return

    def get_category_values(self):
        category = self.category
        value = set_categories(category, self)
        return value

    def get_list_of_quantities(self):
        # Standard quantities can just be shell-averaged with nothing changed from the file
        self.list_of_standard_quantities = get_list_of_standard_quantities()
        self.list_of_raw_calculated_quantities = get_list_of_raw_calculated_quantities()
        self.list_of_reduced_calculated_quantities = get_list_of_reduced_calculated_quantities()
        return

    def get_labels(self):
        self.labels = get_list_of_labels()
        self.filenames = {}
        self.limits = {}
        return

    def set_rEH(self, rEH_in_rg):
        self.rEH = rEH_in_rg
        return

    def set_rISCO(self, rISCO_in_rg):
        self.rISCO = rISCO_in_rg
        return

    def set_last_common_time(self, last_common_time):
        self.last_common_time = last_common_time
        return

    def get_target_temp(self):
        targets = {}
        for sim_name in self.sim_names:
            sim = self.simulations[sim_name]
            r_vals = sim.r_grid[:, 0, 0]
            T_target = sim.header["Tel_target"]*r_vals**(-sim.header["Tel_rslope"])
            targets[sim_name] = T_target
        return targets

    def plot_diag_quantities_over_time(self, **kwargs):
        print("Plotting diagonal quantities over time.")
        quantities = kwargs.get("diag_quantities_to_plot", ["mdot_eh"])
        figdir = self.path_to_figures + "1d_over_time/"
        if not os.path.exists(figdir): os.makedirs(figdir)

        for q in quantities:
            plt.figure()
            for i, sim_name in enumerate(self.sim_names):
                sim = self.simulations[sim_name]
                diag = io.load_diag(sim.path_to_reduced_data, sim.header)
                diag_data = sim.retrieve_diag_quantity(diag, q)
                # if i == 2: fs = 'none'
                # else: fs = 'full'
                fs = 'full'
                if diag_data is not None:
                    plt.plot(diag['t'], diag_data, label=self.sim_labels[sim_name], ls='none', marker='o', fillstyle=fs)

            plt.xlabel(r"$tc/r_g$")
            plt.ylabel(self.labels[q])
            plt.yscale('log')
            if q == "mdot_eh":
                plt.ylim([1e-8, 1e0])
            elif q == "num_super":
                plt.ylim([1.0, 1e5])
            elif q == "lum_super":
                plt.ylim([1e-7, 1e0])
            elif q == "phi":
                plt.ylim([1e-3, 1e4])
            elif q == "Qcool":
                plt.ylim([1e-5, 1e0])
            elif q == "Qcoul":
                plt.ylim([1e-8, 1e0])
            # plt.xlim([0, 8000])
            plt.legend()
            plt.tight_layout()
            figname = q + ".png"
            print("Saving figure " + figdir + figname)
            plt.savefig(figdir + figname)
        return

    def plot_shell_over_time(self, radius, **kwargs):
        """
        radius in rg.
        """
        print("Plotting shell averages over time at r={:.2f}rg".format(radius))
        tstart_rgc = 0
        tend_rgc = -1
        kwargs["tstart_shells"] = tstart_rgc
        kwargs["tend_shells"] = tend_rgc
        dpi = kwargs.get("dpi", 300)
        in_cgs = kwargs.get("in_cgs", True)
        fix_shell_clims = kwargs.get("fix_shell_clims", {})

        sim0 = list(self.simulations.values())[0]
        quantities = kwargs.get("shell_quantities_to_plot", get_list_of_all_quantities())
        quantities = clean_reduced_quantities_list(quantities, sim0.header)
        q_to_retrieve = get_quantities_to_retrieve(quantities)
        kwargs["shell_quantities"] = q_to_retrieve
        # For quantity over time, don't give error if tstarts don't match up (restarts)
        kwargs["over_time"] = True

        tstart_rgc0, tend_rgc0, shell_avgs0 = sim0.retrieve_shell_averages(**kwargs)
        figdir = self.path_to_figures + "shell_averages_over_time/r{:.2f}/".format(radius)
        if not os.path.exists(figdir): os.makedirs(figdir)

        if tend_rgc > self.last_common_time:
            tend_rgc = self.last_common_time

        tit_str = "Density-weighted shell averages at $r={:.2f}r_g$\n ".format(radius)
        target_temps = self.get_target_temp()
        # Determine whether to do different colors for targets
        targets_same = check_same_targets(target_temps)

        for quantity in quantities:
            plt.figure()
            plt.title(tit_str)
            figname = quantity
            for i, sim_name in enumerate(self.sim_names):
                sim = self.simulations[sim_name]
                r_vals = sim.r_grid[:, 0, 0]
                dump_nums, dump_rgcs = sim.load_dump_times("shells")
                tstart_rgc_sim, tend_rgc_sim, shell_avgs = sim.retrieve_shell_averages(**kwargs)
                r_index = (np.abs(r_vals - radius)).argmin()
                # Indices of tstart and tend
                tstart_ind = (np.abs(tstart_rgc_sim - dump_rgcs)).argmin()
                tend_ind = (np.abs(tend_rgc_sim - dump_rgcs)).argmin()
                times = dump_rgcs[tstart_ind:tend_ind + 1]
                if quantity in get_list_of_reduced_calculated_quantities():
                    shell_data = calculate_reduced_quantity(quantity, shell_avgs, r_vals, r_index, sim_units=sim.header)
                elif quantity in shell_avgs:
                    shell_data = shell_avgs[quantity][:, r_index]
                else:
                    print("Unknown quantity " + quantity)
                    continue

                if shell_data is None:
                    continue
                # Convert to cgs
                if in_cgs and quantity in sim.get_list_of_cgs_quantities():
                    shell_data = shell_data*sim.get_list_of_cgs_quantities()[quantity]

                if quantity in ["vrr_bl", "vrr", "ucon_bl1"] and shell_data is not None:
                    shell_data = np.abs(shell_data)
                plt.plot(times, shell_data, label=self.sim_labels[sim_name], ls=self.line_styles[sim_name], color=self.sim_colors[sim_name])

                # --------------------------------------
                if sum(list(targets_same.values())) == 1:
                    target_color = 'k'
                else:
                    target_color = self.sim_colors[sim_name]
                if quantity in get_list_of_target_quantities() and targets_same[sim_name]:
                    T_target = target_temps[sim_name][r_index]
                    if quantity == "Thetae":
                        target = get_thetae(T_target)
                        leg_label = r"Target $\theta_e$"
                    elif quantity == "Thetap":
                        target = get_thetap(T_target)
                        leg_label = r"Target $\theta_p$"
                    elif "_over_R" in quantity:
                        target = get_Hr_from_targetT(T_target, radius, sim.header['gam'])
                        leg_label = r"Target $H/r$"
                    else:
                        target = T_target
                        leg_label = r"Target $T$"
                    plt.gca().axhline(target, color=target_color, ls=':', label=leg_label)
            if quantity == "TpTe":
                plt.gca().axhline(sim.header["tptemin"], color='k', ls=':')
                plt.gca().axhline(sim.header["tptemax"], color='k', ls=':')
                plt.gca().axhline([1.0], color='k', ls=':')
            if quantity == "tCoulomb":
                tdyn = (radius**1.5 + sim.header["a"])
                plt.gca().axhline([tdyn], color='k', ls=':', label=r"$t_{\rm dyn}=1/\Omega$")
            if quantity == "ucon_bl1":
                uR = np.sqrt(2./(3.*self.rISCO))*(self.rISCO/radius - 1.)**1.5
                plt.gca().axhline(uR, color='k', ls=':', label=r"Geodesic")
            if quantity == "tinfall":
                dense_r = np.linspace(self.rEH, self.rISCO, 1000, endpoint = False)
                uR = np.sqrt(2./(3.*self.rISCO))*(self.rISCO/dense_r - 1.)**1.5
                freefall_time = np.hstack([0, integrate.cumtrapz(1.0/uR, dense_r)])
                plt.gca().axhline(freefall_time[r_index], color='k', ls=':', label=r"$t_{\rm freefall}c/r_g$")
            plt.legend(frameon=False)
            if quantity in get_log_scales() and get_log_scales()[quantity]:
                if quantity in fix_shell_clims and fix_shell_clims[quantity][0] < 0:
                    plt.yscale('symlog')
                else:
                    plt.yscale('log')
                if "Ratio" in quantity:
                    plt.gca().axhline([1.0], ls=':', color='black')
            else:
                plt.ylim([0.0, None])
                if "Ratio" in quantity:
                    plt.gca().axhline([1.0], ls=':', color='black')
            plt.xlabel(r"$tc/r_g$")
            # plt.xlim([5000, 6000])
            # plt.xscale('log')
            if quantity in self.labels:
                label_str = self.labels[quantity]
            else:
                label_str = quantity
            plt.ylabel(label_str)
            if quantity in fix_shell_clims:
                plt.ylim(fix_shell_clims[quantity])
                figname = "ylim_" + figname
            if in_cgs and quantity in sim.get_list_of_cgs_quantities():
                figname += "_cgs"
            figname += ".png"
            plt.tight_layout()
            print("Saving figure " + figdir + figname)
            plt.savefig(figdir + figname, bbox_inches='tight', dpi=dpi)
            plt.close()

    def plot_integrals(self, **kwargs):
        """
        NOTE:
        """
        xlog = kwargs.get("xlog", True)
        norm_EH = kwargs.get("norm_EH", None)
        norm_ISCO = kwargs.get("norm_ISCO", None)
        to_time_average = kwargs.get("time_average_integrals", True)
        fix_integral_clims = kwargs.get("fix_integral_clims", {})
        fix_integral_clims_norm = kwargs.get("fix_integral_clims_norm", {})
        tstart_rgc = kwargs.get("tstart_integrals", 0.0) # in rg/c
        if to_time_average:
            tend_rgc = kwargs.get("tend_integrals", -1)
            if self.last_common_time is not None:
                if tend_rgc == -1 or tend_rgc > self.last_common_time:
                    tend_rgc = self.last_common_time
            # Catch errors where I forget to change tstart so it's larger than tend
            if tstart_rgc > tend_rgc:
                print("tstart was greater than tend. Resetting to tstart = tend - 50 rg/c.")
                tstart_rgc = tend_rgc - 50.
        else:
            tend_rgc = tstart_rgc
            kwargs["tend_integrals"] = tend_rgc
        sim0 = list(self.simulations.values())[0]
        figdir_base = self.path_to_figures + "radial_integrals/"
        quantities = kwargs.get("integral_quantities_to_plot", get_list_of_integral_quantities())
        if norm_EH is None:
            norm_EH = {}
            for q in quantities: norm_EH[q] = False
        if norm_ISCO is None:
            norm_ISCO = {}
            for q in quantities: norm_ISCO[q] = False
        # -------------------------
        for quantity in quantities:
            q_to_retrieve = get_quantities_to_retrieve([quantity])
            q_to_retrieve.append("vrr_bl")
            kwargs["integral_quantities"] = q_to_retrieve
            if to_time_average:
                tit_str = "Time-averaged from t={:.2f} - {:.2f} $GM/c^3$".format(tstart_rgc, tend_rgc)
                figdir = figdir_base + "tAvg{:.0f}-{:.0f}rgc/".format(tstart_rgc, tend_rgc)
                figname = quantity
                if quantity in self.labels:
                    label_str = self.labels[quantity]
                else:
                    label_str = quantity
                if norm_EH[quantity]:
                    figname += "_normEH"
                    label_str += r"$/$" + self.labels[quantity] + r"$(r_{\rm EH})$"
                elif norm_ISCO[quantity]:
                    figname += "_normISCO"
                    label_str += r"$/$" + self.labels[quantity] + r"$(r_{\rm ISCO})$"
            else:
                tit_str = "Snapshot at t={:.2f} $GM/c^3$".format(tstart_rgc)
                figdir = figdir_base + "snapshot/" + quantity + "/"
                figname = quantity + "_t{:.0f}rgc".format(tstart_rgc)
            if not os.path.exists(figdir): os.makedirs(figdir)

            plt.figure()
            for sim_name in self.sim_names:
                sim = self.simulations[sim_name]
                tstart_rgc, tend_rgc, integrals = sim.retrieve_radial_integrals(**kwargs)
                dump_nums, dump_rgcs = sim.load_dump_times("integrals")

                # Indices of tstart and tend
                tstart_ind = (np.abs(tstart_rgc - dump_rgcs)).argmin()
                tend_ind = (np.abs(tend_rgc - dump_rgcs)).argmin()
                # Code values of tstart and tend
                tstart_dump_rgc = dump_rgcs[tstart_ind]
                tend_dump_rgc = dump_rgcs[tend_ind]

                r_vals = sim.r_grid[:, 0, 0]
                EH_ind = (np.abs(sim.rEH - r_vals)).argmin()
                ISCO_ind = (np.abs(sim.rISCO - r_vals)).argmin()

                if quantity in integrals:
                    if quantity == "magnetic_flux":
                        integral_array = integrals[quantity]/np.sqrt(integrals["mass_flux"])
                    else:
                        integral_array = integrals[quantity]
                    if to_time_average and integral_array.ndim > 1:
                        integral_data = np.mean(integral_array, axis=0)
                    else:
                        integral_data = integral_array
                else:
                    continue

                if norm_EH[quantity]:
                    integral_data = integral_data/integral_data[EH_ind]
                elif norm_ISCO[quantity]:
                    integral_data = integral_data/integral_data[ISCO_ind]
                if integral_data is not None:
                    line0 = plt.plot(r_vals, integral_data, label=self.sim_labels[sim_name], ls=self.line_styles[sim_name])
                    # vrr_data = np.abs(np.mean(integral_avgs["vrr_bl"], axis=0))
                    # req_ind_start = (np.abs(vrr_data*tstart_rgc - r_vals)).argmin()
                    # plt.gca().axvline(r_vals[req_ind_start], color=line0[0].get_color(), ls=':')

            plt.title(tit_str)
            plt.legend()
            if xlog:
                plt.xscale('log')
                figname = "xlog_" + figname
            # plt.xlim([None, 20])
            # plt.xlim([self.rEH, None])
            # plt.xlim([self.rEH, 50])
            plt.xlim([self.rEH, 10])
            # plt.gca().axvline([self.rEH], color='black', ls=':')
            plt.gca().axvline([self.rISCO], color='black', ls=':')
            if "Ratio" in quantity:
                plt.gca().axhline([1.0], ls=':', color='black')
            if quantity in get_log_scales() and get_log_scales()[quantity]:
                plt.yscale('log')
            plt.xlabel(r"$r/r_g$")
            # if quantity in self.labels:
                # label_str = self.labels[quantity]
            # else:
                # label_str = quantity
            if norm_EH[quantity] or norm_ISCO[quantity]:
                if quantity in fix_integral_clims_norm:
                    plt.ylim(fix_integral_clims_norm[quantity])
                    figname = "ylim_" + figname
            elif quantity in fix_integral_clims:
                plt.ylim(fix_integral_clims[quantity])
                figname = "ylim_" + figname
            figname += ".png"
            plt.ylabel(label_str)
            plt.tight_layout()
            print("Saving figure " + figdir + figname)
            plt.savefig(figdir + figname, bbox_inches='tight')
            plt.close()

    def plot_vertical_slices(self, **kwargs):
        """
        NOTE:
        """
        phi_ind = kwargs.get("phi_ind", "Avg")
        azimuthal_average = False
        if phi_ind == "Avg":
            azimuthal_average = True
        kwargs["phi_ind"] = phi_ind
        tstart_rgc = kwargs.get("tstart_slices", 0)
        cbar = kwargs.get("cbar_slices", True)
        ticks = kwargs.get("ticks_slices", None)
        fix_slice_clims = kwargs.get("fix_slice_clims", {})
        cmaps = kwargs.get('cmaps', {})
        save_pdf = kwargs.get("save_pdf", False)
        dpi = kwargs.get("dpi", 100)

        # Determine tstart/tend
        to_time_average = False
        tend_rgc = kwargs.get("tend_slices", tstart_rgc)
        if tend_rgc != tstart_rgc:
            to_time_average = True

        if to_time_average:
            if self.last_common_time is not None:
                if tend_rgc == -1 or tend_rgc > self.last_common_time:
                    tend_rgc = self.last_common_time
            # Catch errors where I forget to change tstart so it's larger than tend
            if tstart_rgc > tend_rgc:
                print("tstart was greater than tend. Resetting to tstart = tend - 50 rg/c.")
                tstart_rgc = tend_rgc - 50.
        else:
            tend_rgc = tstart_rgc
            kwargs["tend_slices"] = tend_rgc

        # Determine quantities to load. Always load RHO/sigma/beta for contours
        sim0 = list(self.simulations.values())[0]
        quantities = kwargs.get("slice_quantities_to_plot", self.list_of_standard_quantities)
        quantities = clean_reduced_quantities_list(quantities, sim0.header)
        q_to_retrieve = get_quantities_to_retrieve(quantities)
        if "sigma" not in q_to_retrieve:
            q_to_retrieve.append("sigma")
        if "RHO" not in q_to_retrieve:
            q_to_retrieve.append("RHO")
        if "beta" not in q_to_retrieve:
            q_to_retrieve.append("beta")
        kwargs["slice_quantities"] = q_to_retrieve


        # Do all the loading for rho/sigma up front.
        sim_rho_data = {}
        sim_sigma_data = {}
        sim_beta_data = {}
        for j, sim_name in enumerate(self.sim_names):
            sim = self.simulations[sim_name]
            if sim.geom is None:
                sim.load_geom()
            # If to_time_average, check for a data file that has already done
            # the time-averaging (most expensive part of plot)
            sigma_data = sim.check_for_time_averaged_slice("sigma", **kwargs)
            rho_data = sim.check_for_time_averaged_slice("RHO", **kwargs)
            beta_data = sim.check_for_time_averaged_slice("beta", **kwargs)
            # rho_data = sim.retrieve_quantity_slice("RHO", **kwargs)
            # beta_data = sim.retrieve_quantity_slice("beta", **kwargs)

            if sigma_data.ndim > 2:
                sigma_data = flatten_xz(sigma_data, self.header, azi_avg=azimuthal_average)
                rho_data = flatten_xz(rho_data, self.header, azi_avg=azimuthal_average)
                beta_data = flatten_xz(beta_data, self.header, azi_avg=azimuthal_average)
            elif sigma_data.ndim == 3:
                sigma_data = sigma_data[:, :, 0]
                rho_data = rho_data[:, :, 0]
                beta_data = beta_data[:, :, 0]
            sim_rho_data[sim_name] = rho_data
            sim_sigma_data[sim_name] = sigma_data
            sim_beta_data[sim_name] = beta_data

        # -------------------------
        for quantity in quantities:
            if quantity in cmaps:
                cmap = cmaps[quantity]
            else:
                cmap = None
            if quantity in fix_slice_clims:
                vmin, vmax = fix_slice_clims[quantity]
                individual_cbars = False
            else:
                vmin = None; vmax = None
                individual_cbars = True
            if to_time_average:
                tit_str = "Time-averaged from t={:.2f} - {:.2f} $GM/c^3$".format(tstart_rgc, tend_rgc)
            else:
                tit_str = "Snapshot at t={:.2f} $GM/c^3$".format(tstart_rgc)

            ncol = len(com.sim_names)
            width_to_height = 8.0/12.0
            fig_width = ncol*3.0 + 3.0
            fig_height = fig_width*width_to_height
            fig, axs = plt.subplots(1, ncol, sharex=True, sharey=True)
            for j, sim_name in enumerate(self.sim_names):
                sim = self.simulations[sim_name]
                if sim.geom is None:
                    sim.load_geom()
                # Time averaging is included in retrieve function
                # slice_data = sim.retrieve_quantity_slice(quantity, **kwargs)
                slice_data = sim.check_for_time_averaged_slice(quantity, **kwargs)

                ax = axs[j]
                ax.set_title(self.sim_labels[sim_name])

                plt.axes(ax)
                fa = False
                if j == 0: fa = True
                rcyl, z, mesh, sigma_colors = plot_xz(ax, sim.geom, slice_data, sim.header, vmin=vmin, vmax=vmax, xlim=None, ylim=None, name=quantity, cmap=cmap, azi_avg=azimuthal_average, first_axis=fa)
                plt.contour(rcyl, z, sim_sigma_data[sim_name], levels=np.array([1.0]), colors=sigma_colors, linewidths=0.5)
                plt.contour(rcyl, z, sim_beta_data[sim_name], levels=np.array([1.0]), colors=sigma_colors, linewidths=0.5, linestyles='dotted')
                plt.contour(rcyl, z, sim_rho_data[sim_name], levels=np.array([1.0, 10.0]), colors=sigma_colors, linewidths=0.5, linestyles='dashed')

                if cbar and individual_cbars:
                    from mpl_toolkits.axes_grid1 import make_axes_locatable
                    divider = make_axes_locatable(ax)
                    cax = divider.append_axes("right", size="5%", pad=0.05)
                    plt.colorbar(mesh, cax=cax, label=self.labels[quantity], ticks=ticks)

            # plt.suptitle(tit_str)
            if cbar and not individual_cbars:
                fig.colorbar(mesh, ax=axs.ravel().tolist(), label=self.labels[quantity], ticks=ticks, shrink=0.65)
            # plt.tight_layout()

            # Run through the slice views and save
            slice_views = get_slice_view_lims()
            for slice_view in slice_views:
                xlim, ylim = slice_views[slice_view]
                ax.set_xlim([0, xlim])
                if ylim is not None:
                    ax.set_ylim([-ylim, ylim])
                figdir = self.path_to_figures + "vertical_slices/" + slice_view + "_view/"
                if azimuthal_average:
                    figdir += "azimuthally_averaged/"
                else:
                    figdir += "slice_phi{:d}/".format(phi_ind)
                if to_time_average:
                    figdir += "tAvg{:.0f}-{:.0f}rgc/".format(tstart, tend)
                    figname = quantity
                else:
                    figdir += "snapshot/" + quantity + "/"
                    figname = "t{:.0f}rgc".format(tstart_rgc)
                if not os.path.exists(figdir): os.makedirs(figdir)
                if quantity in fix_slice_clims:
                    figname = "clim_" + figname
                figname += ".png"
                print("Saving figure " + figdir + figname)
                plt.savefig(figdir + figname, bbox_inches='tight', dpi=dpi)
                if save_pdf:
                    if not os.path.exists(figdir + "pdfs/"): os.makedirs(figdir + "pdfs/")
                    plt.savefig(figdir + "pdfs/" + figname.replace(".png", ".pdf"), bbox_inches='tight', dpi=dpi)
            plt.close()
        return

    def plot_shell_averages(self, **kwargs):
        """
        NOTE:
        """
        to_time_average = kwargs.get("time_average_shells", True)
        tstart_rgc = kwargs.get("tstart_shells", 0.0) # in rg/c
        fix_shell_clims = kwargs.get("fix_shell_clims", {})
        fix_cgs_shell_clims = kwargs.get("fix_cgs_shell_clims", {})
        in_cgs = kwargs.get("in_cgs", True)
        norm = kwargs.get("norm_rho", False)
        norm_hr = kwargs.get("norm_H_over_R", False)
        if to_time_average:
            tend_rgc = kwargs.get("tend_shells", -1)
            if self.last_common_time is not None:
                if tend_rgc == -1 or tend_rgc > self.last_common_time:
                    tend_rgc = self.last_common_time
            # Catch errors where I forget to change tstart so it's larger than tend
            if tstart_rgc > tend_rgc:
                print("tstart was greater than tend. Resetting to tstart = tend - 50 rg/c.")
                tstart_rgc = tend_rgc - 50.
        else:
            tend_rgc = tstart
            kwargs["tend_shells"] = tend_rgc
        sim0 = list(self.simulations.values())[0]
        figdir_base = self.path_to_figures + "shell_averages/"
        quantities = kwargs.get("shell_quantities_to_plot", self.list_of_standard_quantities)
        quantities = clean_reduced_quantities_list(quantities, sim0.header)
        tit_str = "Density-weighted shell averages"

        target_temps = self.get_target_temp()
        # Determine whether to do different colors for targets
        targets_same = check_same_targets(target_temps)
        # -------------------------
        for quantity in quantities:
            q_to_retrieve = get_quantities_to_retrieve([quantity])
            q_to_retrieve.append("vrr_bl")
            kwargs["shell_quantities"] = q_to_retrieve
            if quantity in self.labels:
                label_str = self.labels[quantity]
            else:
                label_str = quantity
            if to_time_average:
                tit_str = "Time-averaged from t={:.2f} - {:.2f} $GM/c^3$".format(tstart_rgc, tend_rgc)
                figdir = figdir_base + "tAvg{:.0f}-{:.0f}rgc/".format(tstart_rgc, tend_rgc)
                figname = quantity
                if quantity == "H_over_R" and norm_hr:
                    figname += "_norm"
                    label_str += r"$/(H/r)(r_{\rm ISCO})$"
                if quantity == "RHO" and norm:
                    figname += "_norm"
                    label_str += r"$/\rho(r_{\rm ISCO})$"
            else:
                tit_str = "Snapshot at t={:.2f} $GM/c^3$".format(tstart_rgc)
                figdir = figdir_base + "snapshot/" + quantity + "/"
                figname = quantity + "_t{:.0f}rgc".format(tstart_rgc)
            if not os.path.exists(figdir): os.makedirs(figdir)

            plt.figure()
            for sim_name in self.sim_names:
                sim = self.simulations[sim_name]
                tstart_rgc_sim, tend_rgc_sim, shell_avgs = sim.retrieve_shell_averages(**kwargs)
                dump_nums, dump_rgcs = sim.load_dump_times("shells")

                # Indices of tstart and tend
                tstart_ind = (np.abs(tstart_rgc_sim - dump_rgcs)).argmin()
                tend_ind = (np.abs(tend_rgc_sim - dump_rgcs)).argmin()
                # Code values of tstart and tend
                tstart_dump_rgc = dump_rgcs[tstart_ind]
                tend_dump_rgc = dump_rgcs[tend_ind]

                if np.abs(tstart_dump_rgc - tstart_rgc) > 50.0:
                    # Error output already in base simulation analyzer retrieve shell averages.
                    # print("WARNING: the actual start time for " + sim.sim_name)
                    # print("will be {:.2f} instead of {:.2f}".format(tstart_dump_rgc, tstart_rgc))
                    shell_data = None

                r_vals = sim.r_grid[:, 0, 0]
                EH_ind = (np.abs(sim.rEH - r_vals)).argmin()
                ISCO_ind = (np.abs(sim.rISCO - r_vals)).argmin()

                if quantity in get_list_of_reduced_calculated_quantities():
                    if quantity == "theat":
                        shell_data = calculate_reduced_quantity(quantity, shell_avgs, r_vals, smooth=True, neighbors=10, EH_ind=EH_ind, sim_units=sim.header)
                    else:
                        shell_data = calculate_reduced_quantity(quantity, shell_avgs, r_vals, EH_ind=EH_ind, sim_units=sim.header)
                    if quantity == "tCool_e":
                        tcool_data = np.mean(shell_avgs["Tcool"], axis=0)
                elif quantity in shell_avgs:
                    shell_array = shell_avgs[quantity]
                    number_of_nans = np.sum(np.isnan(shell_array))
                    if number_of_nans != 0.0:
                        print(sim_name)
                        print(quantity)
                        print("NOTE: " + quantity + " has {:d} NaNs!".format(number_of_nans))
                    if to_time_average and shell_array.ndim > 1:
                        shell_data = np.mean(shell_array, axis=0)
                    else:
                        shell_data = shell_array
                else:
                    shell_data = None
                if quantity == "RHO" and norm:
                    shell_data = shell_data/shell_data[ISCO_ind]
                if quantity == "H_over_R" and norm_hr:
                    shell_data = shell_data/shell_data[ISCO_ind]

                if shell_data is not None:
                    if quantity in ["vrr_bl", "vrr", "ucon_bl1"]:
                        shell_data = np.abs(shell_data)
                    # Convert to cgs
                    if in_cgs and quantity in sim.get_list_of_cgs_quantities():
                        shell_data = shell_data*sim.get_list_of_cgs_quantities()[quantity]
                    if r_vals.shape != shell_data.shape:
                        line0 = plt.plot(r_vals[EH_ind:], shell_data, label=self.sim_labels[sim_name], ls=self.line_styles[sim_name])
                        # line0 = plt.plot(r_vals[EH_ind:], shell_data, ls=self.line_styles[sim_name])
                    else:
                        line0 = plt.plot(r_vals, shell_data, label=self.sim_labels[sim_name], ls=self.line_styles[sim_name])
                        # line0 = plt.plot(r_vals, shell_data, ls=self.line_styles[sim_name])
                    if "vrr_bl" in shell_avgs:
                        vrr_data = np.abs(np.mean(shell_avgs["vrr_bl"], axis=0))
                        # req_ind_start = (np.abs(vrr_data*tstart_rgc - r_vals)).argmin()
                        req_ind_start = np.argsort(np.abs(vrr_data*tstart_rgc_sim - r_vals))[1]
                        plt.gca().axvline(r_vals[req_ind_start], color=line0[0].get_color(), ls=':')
                # --------------------------------------
                if sum(list(targets_same.values())) == 1:
                    target_color = 'k'
                else:
                    target_color = self.sim_colors[sim_name]
                    if quantity in get_list_of_target_quantities() and targets_same[sim_name]:
                        T_target = target_temps[sim_name]
                        if quantity == "Thetae":
                            target = get_thetae(T_target)
                            leg_label = r"Target $\theta_e$"
                        elif quantity == "Thetap":
                            target = get_thetap(T_target)
                            leg_label = r"Target $\theta_p$"
                        elif "_over_R" in quantity:
                            target = get_Hr_from_targetT(T_target, r_vals, sim.header['gam'])
                            leg_label = r"Target $H/r$"
                        else:
                            target = T_target
                            leg_label = r"Target $T$"
                        plt.plot(r_vals, target, color=target_color, ls='--', label=leg_label)
            if sum(list(targets_same.values())) == 1 and quantity in get_list_of_target_quantities():
                T_target = target_temps[sim_name]
                if quantity == "Thetae":
                    target = get_thetae(T_target)
                    leg_label = r"Target $\theta_e$"
                elif quantity == "Thetap":
                    target = get_thetap(T_target)
                    leg_label = r"Target $\theta_p$"
                elif "_over_R" in quantity:
                    target = get_Hr_from_targetT(T_target, r_vals, sim.header['gam'])
                    leg_label = r"Target $H/r$"
                else:
                    target = T_target
                    leg_label = r"Target $T$"
                plt.plot(r_vals, target, color=target_color, ls='--', label=leg_label)

                # print(sim.sim_name)
                # if shell_data is not None:
                    # print(shell_data[:20])
            if quantity == "Te" or quantity == "Tp" and shell_data is not None:
                virial_const = 6./15.*cgs['MP']*cgs['CL']**2/cgs['KBOL']
                virial_temp = virial_const/r_vals
                plt.plot(r_vals, virial_temp, color='k', ls='--', label=r'$1/r$')
            if quantity == "vphir_bl":
                keplerian_vphi = 1.0/(sim.header["a"] + r_vals**1.5)
                plt.plot(r_vals, keplerian_vphi, color='red', ls='--', label=r"Keplerian $v^\phi$")
            # if quantity == "TpTe":
                # plt.gca().axhline(sim.header["tptemin"], color='k', ls=':')
                # plt.gca().axhline(sim.header["tptemax"], color='k', ls=':')
                # plt.gca().axhline(1.0, color='k', ls='--')
            if quantity == "Tcool":
                analytic = (r_vals**1.5 + 0.9375)
                plt.plot(r_vals, analytic, color='k', ls=':', label="Analytic")
            if quantity == "ucon_bl1":
                dense_r = np.linspace(self.rEH, self.rISCO, 1000, endpoint = False)
                uR = np.sqrt(2./(3.*self.rISCO))*(self.rISCO/dense_r - 1.)**1.5
                plt.plot(dense_r, uR, label=r"Geodesic", ls='--', color='red')
            if quantity == "tinfall":
                dense_r = np.linspace(self.rEH, self.rISCO, 1000, endpoint = False)
                uR = np.sqrt(2./(3.*self.rISCO))*(self.rISCO/dense_r - 1.)**1.5
                freefall_time = np.hstack([0, integrate.cumtrapz(1.0/uR, dense_r)])
                plt.plot(dense_r, freefall_time, label=r"$t_{\rm freefall}c/r_g$", ls='--', color='red')
            if quantity == "coulombQuality":
                plt.gca().axhline([1.0], color='k', ls='-', lw=1)
            if quantity == "tCool_e":
                plt.plot(r_vals, tcool_data, color='k', ls='--')

            tit_str = "Density-weighted shell averages\n " + tit_str
            plt.title(tit_str)
            # plt.legend(frameon=False, ncol=1, loc='upper right')
            # plt.legend(frameon=False, ncol=1)
            plt.xscale('log')
            # plt.xlim([None, 20])
            # plt.xlim([self.rEH, None])
            plt.xlim([self.rEH, 50])
            # plt.xlim([None, 50])
            # plt.gca().axvline([self.rEH], color='black', ls=':')
            plt.gca().axvline([self.rISCO], color='black', ls=':')
            if "Ratio" in quantity:
                plt.gca().axhline([1.0], color='k', ls='-', lw=1)
            if quantity in get_log_scales() and get_log_scales()[quantity]:
                if quantity in fix_shell_clims and fix_shell_clims[quantity][0] < 0:
                    plt.yscale('symlog')
                    plt.gca().axhline([0.0], color='k', ls='-', lw=1)
                else:
                    plt.yscale('log')
            plt.xlabel(r"$r/r_g$")
            # print(r_vals[20:40])
            # print(shell_data[20:40])
            # if quantity in self.labels:
                # label_str = self.labels[quantity]
            # else:
                # label_str = quantity
            if quantity in fix_shell_clims and not in_cgs:
                plt.ylim(fix_shell_clims[quantity])
                figname = "ylim_" + figname
            elif quantity in fix_cgs_shell_clims and in_cgs:
                plt.ylim(fix_cgs_shell_clims[quantity])
                figname = "ylim_" + figname
            if in_cgs and quantity in sim.get_list_of_cgs_quantities():
                figname += "_cgs"
            figname += ".png"
            plt.ylabel(label_str)
            plt.tight_layout()
            print("Saving figure " + figdir + figname)
            plt.savefig(figdir + figname, bbox_inches='tight')
            plt.close()

    def plot_grid_shell_averages(self, **kwargs):
        """
        NOTE:
        """
        results = kwargs.get("results", True) # other option: diagnostics
        dpi = kwargs.get("dpi", 300)
        if results:
            figname = "results"
            quantities = ["Te", "TpTe", "H_over_R", "coulViscRatioPost"]
            ylabels = [r"$T_e$", r"$T_p/T_e$", r"$H/r$", r"$\langle Q_{\rm coul}\rangle/\langle Q_{\rm visc}^e\rangle$"]
            sim0 = self.simulations[self.sim_names[0]]
            tptemin = sim0.header["tptemin"]
            tptemax = sim0.header["tptemax"]
            # ylims = [(1e8, 1e11), (tptemin*0.9, tptemax*1.1), (1.e-2, 1.0), (1e-5, 1e5)]
            ylims = [(1e8, 1e11), (0.1, tptemax*1.1), (1.e-2, 1.0), (1e-5, 1e5)]
        else:
            figname = "diagnostics"
            # quantities = ["coulombQuality", "coolCoulRatioPost", "RHO", "Qmri_theta"]
            # ylabels = [r"$(u_e/Q_{\rm coul})/\Delta t$", r"$\langle Q_{\rm cool}\rangle/\langle Q_{\rm coul}\rangle$", r"$\rho$", r"$Q_\theta^{\rm MRI}$"]
            # ylims = [(1.e0, 1.e8), (0.05, 1e6), (1e-1, 1e5), (1, 55)]
            quantities = ["coulombQuality", "coolCoulRatioPost", "Qmri_phi", "Qmri_theta"]
            ylabels = [r"$(u_e/Q_{\rm coul})/\Delta t$", r"$\langle Q_{\rm cool}\rangle/\langle Q_{\rm coul}\rangle$", r"$Q_\phi^{\rm MRI}$", r"$Q_\theta^{\rm MRI}$"]
            ylims = [(1.e0, 1.e8), (0.05, 1e6), (1, 55), (1, 55)]

        to_time_average = kwargs.get("time_average_shells", True)
        tstart_rgc = kwargs.get("tstart_shells", 0.0) # in rg/c
        if to_time_average:
            tend_rgc = kwargs.get("tend_shells", -1)
            if self.last_common_time is not None:
                if tend_rgc == -1 or tend_rgc > self.last_common_time:
                    tend_rgc = self.last_common_time
            # Catch errors where I forget to change tstart so it's larger than tend
            if tstart_rgc > tend_rgc:
                print("tstart was greater than tend. Resetting to tstart = tend - 50 rg/c.")
                tstart_rgc = tend_rgc - 50.
        else:
            tend_rgc = tstart
            kwargs["tend_shells"] = tend_rgc
        figdir_base = self.path_to_figures + "shell_averages/"
        if to_time_average:
            tit_str = "Time-averaged from t={:.2f} - {:.2f} $GM/c^3$".format(tstart_rgc, tend_rgc)
            figdir = figdir_base + "tAvg{:.0f}-{:.0f}rgc/grid/".format(tstart_rgc, tend_rgc)
            if not os.path.exists(figdir): os.makedirs(figdir)
        else:
            tit_str = "Snapshot at t={:.2f} $GM/c^3$".format(tstart_rgc)
            figdir = figdir_base + "snapshot/grid/"
            figname += "_t{:.0f}rgc".format(tstart_rgc)
            if not os.path.exists(figdir): os.makedirs(figdir)

        fig, axs = plt.subplots(2, 2, sharex=True, figsize=(10, 8))
        target_temps = self.get_target_temp()
        # Determine whether to do different colors for targets
        targets_same = check_same_targets(target_temps)
        # -------------------------
        for j, quantity in enumerate(quantities):
            q_to_retrieve = get_quantities_to_retrieve([quantity, "vrr_bl"])
            kwargs["shell_quantities"] = q_to_retrieve
            ax = axs[int(j/2), j % 2]

            for sim_name in self.sim_names:
                sim = self.simulations[sim_name]
                tstart_rgc_sim, tend_rgc_sim, shell_avgs = sim.retrieve_shell_averages(**kwargs)
                dump_nums, dump_rgcs = sim.load_dump_times("shells")
                r_vals = sim.r_grid[:, 0, 0]
                EH_ind = (np.abs(sim.rEH - r_vals)).argmin()

                if quantity in get_list_of_reduced_calculated_quantities():
                    shell_data = calculate_reduced_quantity(quantity, shell_avgs, r_vals, sim_units=sim.header)
                elif quantity in shell_avgs:
                    shell_array = shell_avgs[quantity]
                    number_of_nans = np.sum(np.isnan(shell_array))
                    if number_of_nans != 0.0:
                        print(sim_name)
                        print(quantity)
                        print("NOTE: " + quantity + " has {:d} NaNs!".format(number_of_nans))
                    if to_time_average and shell_array.ndim > 1:
                        shell_data = np.mean(shell_array, axis=0)
                    else:
                        shell_data = shell_array
                        if quantity == "vrr_bl":
                            shell_data = shell_data[0, :]
                else:
                    shell_data = None

                if shell_data is not None:
                    if quantity == "vrr_bl":
                        shell_data = np.abs(shell_data)
                    line0 = ax.plot(r_vals, shell_data, label=self.sim_labels[sim_name], ls=self.line_styles[sim_name])
                    if "vrr_bl" in shell_avgs:
                        if to_time_average:
                            vrr_data = np.abs(np.mean(shell_avgs["vrr_bl"], axis=0))
                        else:
                            vrr_data = np.abs(shell_avgs["vrr_bl"])
                        req_ind_start = (np.abs(vrr_data*tstart_rgc_sim - r_vals)).argmin()
                        ax.axvline(r_vals[req_ind_start], color=line0[0].get_color(), ls=':')

                if sum(list(targets_same.values())) == 1:
                    target_color = 'k'
                else:
                    target_color = self.sim_colors[sim_name]
                if quantity in get_list_of_target_quantities() and targets_same[sim_name]:
                    T_target = target_temps[sim_name]
                    if quantity == "Thetae":
                        target = get_thetae(T_target)
                        label_str = r"Target $\theta_e$"
                    elif quantity == "Thetap":
                        target = get_thetap(T_target)
                        label_str = r"Target $\theta_p$"
                    elif "_over_R" in quantity:
                        target = get_Hr_from_targetT(T_target, r_vals, sim.header['gam'])
                        leg_label = r"Target $H/r$"
                    else:
                        target = T_target
                        label_str = ""
                    ax.plot(r_vals, target, color=target_color, ls=':', label=label_str)
            if quantity == "Te" or quantity == "Tp" and shell_data is not None:
                virial_const = 6./15.*cgs['MP']*cgs['CL']**2/cgs['KBOL']
                virial_temp = virial_const/r_vals
                plt.plot(r_vals, virial_temp, color='k', ls='--', label=r'$1/r$')
            if quantity == "TpTe":
                ax.axhline([1.0], color='k', ls='-', lw=1)
                ax.axhline(sim.header["tptemin"], color='k', ls=':')
                ax.axhline(sim.header["tptemax"], color='k', ls=':')
            if quantity == "coulombQuality":
                ax.axhline([1.0], color='k', ls='-', lw=1)

            ax.set_xscale('log')
            # ax.set_xlim([self.rEH, None])
            ax.set_xlim([self.rEH, 50])
            ax.set_ylim(ylims[j])
            # plt.gca().axvline([self.rEH], color='black', ls=':')
            ax.axvline([self.rISCO], color='black', ls=':')
            if "Ratio" in quantity:
                ax.axhline([1.0], color='k', ls='-', lw=1)
            if ylims[j][0] > 0:
                # if quantity in get_log_scales() and get_log_scales()[quantity]:
                ax.set_yscale('log')
            if int(j/2) == 1:
                ax.set_xlabel(r"$r/r_g$")
            ax.set_ylabel(ylabels[j])
            # plt.tight_layout()
        tit_str = "Density-weighted shell averages\n " + tit_str
        fig.suptitle(tit_str)
        axs[0][0].legend(
        # plt.legend(
            loc='best',
            # loc='upper center',
            # bbox_to_anchor=(-0.5, 2.02, 1., 0.502),
            # bbox_to_anchor=(1.0, 0.5),
            # ncol=1,
            # bbox_to_anchor=(0.5, 2.0),
            # ncol=int(len(self.sim_names)/2),
            # mode='expand',
            # ncol=int(len(self.sim_names)),
            frameon=False,
        )

        plt.tight_layout()
        figname += ".png"
        print("Saving figure " + figdir + figname)
        plt.savefig(figdir + figname, bbox_inches='tight', dpi=dpi)
        if not os.path.exists(figdir + "pdfs/"): os.makedirs(figdir + "pdfs/")
        fig.suptitle('')
        plt.savefig(figdir + "pdfs/" + figname.replace(".png", ".pdf"), bbox_inches='tight', dpi=dpi)
        plt.close()


if __name__ == '__main__':
    plt.style.use('lia.mplstyle')
    # setup = "lia_hp"
    setup = "seagate_tests"
    category = "compare_2D_ieCoulomb_targetTemp1e10"
    category = "compare_2D_constHr"
    category = "compare_3D_ieCoulomb_Mdot"
    # category = "compare_3D_constHr"
    # category = "compare_3D_implicitCoulomb_floors"
    # category = "compare_3D_T1e10_Mdot"
    # category = "compare_3D_implicitCoulomb_Mdot"
    # category = "compare_3D_ieCoulomb_Mdot"
    # category = "compare_3D_ieCoulomb_targetTemp_Mu25"
    category = "compare_3D_high-res"
    com = comparison(category, setup)

    # for sim_name in com.sim_names:
        # sim = com.simulations[sim_name]
        # sim.clean_times_and_quantities_file("shells")

    kwargs = {}
    kwargs["dpi"] = 400
    # ----------------------------------------------
    # Plot diagonal quantities
    # ----------------------------------------------
    diag_quantities_to_plot = [
        "mdot_eh", "num_super", "lum_super", "phi",
        "Qcool", "Qcoul", "Qvisc",
        "visc_efficiency", "cool_efficiency"
    ]
    kwargs["diag_quantities_to_plot"] = diag_quantities_to_plot

    # com.plot_diag_quantities_over_time(**kwargs)
    # ----------------------------------------------
    # Plot shell averages
    # ----------------------------------------------
    overwrite_shell_avgs = False
    time_average_shells = False
    tstart = 4500.0; tend = tstart
    # Times to average over, in rg/c
    time_average_shells = True
    tend = 5000.0
    # tstart = 9000.0
    # tend = 10000.0
    tstart = 11000.0
    tend = 12000.0
    shell_quantities_to_plot = [
        # "coulombQuality",
        "TpTe",
        # "Te", "Tp",
        # "RHO",
        # "H_over_R",
        # "H_over_R0",
        "HT_over_R",
        # "Hmean",
        "coulViscRatioPost",
        # "coolViscRatioPost",
        # "coolTotalRatioPost",
        # "coolCoulRatioPost",
        # "Qcool",
        # "Qcoul",
        # "Qcool_corona",
        # "Qcoul_corona",
        # "Qvisc_e",
        # "coolTotalRatio",
        # "coolViscRatio",
        # "coolCoronaPost",
        # "coulCoronaPost",
        # "coolCoulCoronaRatioPost",
        # "Thetae",
        # "Thetap",
        # "Tcool",
        # "tCoulomb_p",
        # "tCoulomb_e",
        # "theat",
        # "theat_e",
        # "theat_p",
        # "tCool_e",
        # "sigma",
        # "beta",
        # "KEL", "KTOT",
        # "Qmri_theta",
        # "ueRatio",
        # "ue", "up",
        # "UU",
        # "Qmri_phi",
        # "tinfall",
        # "vrr_bl",
        # "vphir_bl",
        # "ucon_bl0",
        # "ucon_bl1",
        # "ucon_bl2",
        # "ucon_bl3",
    ]

    # -------------------------------------------------------
    kwargs["overwrite_shell_avgs"] = overwrite_shell_avgs
    kwargs["shell_quantities_to_plot"] = shell_quantities_to_plot
    kwargs["tstart_shells"] = tstart
    # kwargs["norm_H_over_R"] = True
    kwargs["tend_shells"] = tend
    kwargs["time_average_shells"] = time_average_shells
    kwargs["fix_shell_clims"] = {
        "coolViscRatioPost":[1e-1, 1e2],
        # "coulViscRatioPost":[2e-4, 2.e2],
        "coulViscRatioPost":[1e-5, 1.e4],
        "coolViscRatio":[1e-2, 1e2], "coolTotalRatio":[1e-2, 10],
        "coolCoulRatioPost":[1e-1, 1e5],
        "coolCoronaPost":[1.e-3, 1.],
        "coulCoronaPost":[1.e-3, 1.],
        "coolCoulCoronaRatioPost":[1.e-1, 1.e5],
        "Thetae":[1e-2, 10.0], "Thetap": [8e-5, 1],
        "Te":[1e8, 5e10],
        "Tp": [5e8, 5e12],
        "TpTe": [0.5, 3e3],
        "Qvisc_e":[1e-6, 1e+1],
        # "Qcool":[1e-7, 1e-1],
        "Qcool":[1e-7, 1e+1],
        "Qcool_corona":[1e-7, 1e+1],
        # "Qcoul":[1e-11, 1e-3],
        "Qcoul":[1e-9, 1e+1],
        "Qcoul_corona":[1e-9, 1e+1],
        "Qmri_theta":[1., 1.e3],
        "Qmri_phi":[1., 1.e3],
        "coolTotalRatioPost":[1e-4, 10],
        "RHO":[1e-2, 1.e4],
        "sigma":[1e-4, 1],
        "ue":[1e-4, 1e2],
        "up":[1e-4, 1e2],
        "UU":[1e-3, 1e0],
        # "Tcool":[1e-1, 10],
        # "tinfall":[0.1, 1.e3],
        "tinfall":[0.1, 1.e5],
        "tCoulomb_e":[1.e-1, 1.e6],
        "tCoulomb_p":[1.e-1, 1.e6],
        "tCool_e":[1.e-1, 1.e6],
        "theat_e":[1.e-1, 1.e6],
        "theat_p":[1.e-1, 1.e6],
        "theat":[1.0, 1.e4],
        "coulombQuality":[1.e0, 1.e8],
        # "vrr_bl":[-0.1, 0.0],
        "vrr_bl":[1.e-3, 1.],
        "vphir_bl":[1.e-3, 1.],
        "ucon_bl0":[1., 6],
        "ucon_bl1":[1e-3, 1.],
        # "ucon_bl1":[-0.3, 0],
        "ucon_bl3":[0., 2],
        "H_over_R":[0.01, 0.7],
        "H_over_R0":[0.01, 0.7],
        "HT_over_R":[0.01, 0.7],
        # "H_over_R":[0.04, 1.0],
        "beta":[1.e0, 1.e3],
        "Hmean":[-1., 1.],
    }
    kwargs["fix_cgs_shell_clims"] = {
        "Qvisc_e":[1e-3, 1e3],
        # "Qcool":[1e-6, 1e-2],
        # "Qcool":[1e-7, 1e-3],
        "Qcoul":[1e-8, 1e2],
        "RHO":[1e-18, 1e-13],
    }

    kwargs["in_cgs"] = False
    kwargs["norm_rho"] = False
    # Gridded shell averages, both results and diagnostics.
    # com.plot_grid_shell_averages(**kwargs)
    # kwargs["results"] = False
    # com.plot_grid_shell_averages(**kwargs)
    com.plot_shell_averages(**kwargs)
    # ----------------------------------------------
    # Plot timescales and heating
    # ----------------------------------------------
    # tstart = 13000.0
    # tend = 15000.0
    figdir_base = com.path_to_figures
    figdir = figdir_base + "shell_averages/"
    if time_average_shells and tstart != tend:
        dirstr = "tAvg{:.0f}-{:.0f}rgc".format(tstart, tend)
        figstr = ""
    else:
        dirstr = "snapshot"
        figstr = "_t{:.0f}rgc".format(tstart)
    figdir += dirstr + "/grid/"
    kwargs["figdir"] = figdir
    kwargs["tstart_rgc"] = tstart
    kwargs["tend_rgc"] = tend
    norms = ["infall", "dynamical", "none"]
    for norm in norms:
        kwargs["normalization"] = norm

        figname = "gas_timescales_norm-"
        figname += norm + figstr + ".png"
        kwargs["figname"] = figname
        mp.plot_gas_timescales_panel(com, **kwargs)
        # figname = "electron-proton_timescales-"
        # figname += norm + figstr + ".png"
        # kwargs["figname"] = figname
        # mp.plot_timescales_panel(com, **kwargs)
        # figname = "timescales_and_heating_norm-"
        # figname += norm + figstr + ".png"
        # kwargs["figname"] = figname
        # mp.plot_timescales_and_heating_panel(com, **kwargs)

    # ----------------------------------------------
    # Plot shells over time
    # ----------------------------------------------
    kwargs["shell_quantities_to_plot"] = [
        # "coulombQuality",
        "H_over_R",
        # "Qvisc_e",
        "Te", "Tp",
        "TpTe",
        "coolCoulRatio",
        "coolViscRatio",
        "coolTotalRatio",
        "coulViscRatio",
        # "Qcoul",
        # "Qcool",
        # "RHO",
        # "tCoulomb_e",
        # "tCoulomb_p",
        # "theat",
        # "tinfall",
        # "sigma",
        # "UU", "ue", "up",
        # "beta",
        # "ueRatio",
    ]
    kwargs["time_average_shells"] = False
    # for j in np.arange(450, 501)[::1]:
        # # for j in np.arange(400, com.last_common_time)[::1]:
        # kwargs["tstart_shells"] = int(j)
        # # print("Shell {:d}".format(j))
        # com.plot_shell_averages(**kwargs)

    rISCO = com.simulations[com.sim_names[0]].rISCO
    rEH = com.simulations[com.sim_names[0]].rEH + 0.1
    radii_to_plot = [5.0, rISCO, rEH]
    radii_to_plot = [10.0, 5.0, rISCO, rEH]
    # radii_to_plot = [4.84]# 7.0, 8.0]
    # for r in radii_to_plot:
        # com.plot_shell_over_time(r, **kwargs)

    # ----------------------------------------------
    # Plot integrals
    # ----------------------------------------------
    kwargs["tstart_integrals"] = tstart
    kwargs["tend_integrals"] = tend
    kwargs["time_average_integrals"] = True
    kwargs["quiet"] = False
    kwargs["fix_integral_clims"] = {
        "surface_density":[1e-1, 1e2],
        "mass_flux":[1e-1, 1e1],
    }
    kwargs["fix_integral_clims_norm"] = {
        "surface_density":[1e-1, 1e2],
        "mass_flux":[1e-1, 1e1],
    }
    kwargs["norm_EH"] = {"surface_density":True, "mass_flux":True, "magnetic_flux":False}
    # com.plot_integrals(**kwargs)
    kwargs["norm_EH"] = None
    kwargs["norm_ISCO"] = {"surface_density":True, "mass_flux":True, "magnetic_flux":False}
    # com.plot_integrals(**kwargs)
    kwargs["tstart_integrals"] = 7000
    kwargs["tend_integrals"] = 8000
    # com.plot_integrals(**kwargs)
    kwargs["norm"] = False
    # com.plot_integrals(**kwargs)

    # ----------------------------------------------
    # Plot radial profiles
    # ----------------------------------------------
    # tstart = 400
    # tend = 501
    kwargs["tstart_rprofs"] = tstart
    kwargs["tend_rprofs"] = tend
    kwargs["time_average_rprofile"] = True
    kwargs["fix_rprof_clims"] = {
        "coolViscRatioPost":[1e-2, 10], "coulViscRatioPost":[1e-5, 10],
        "Thetae":[1e-2, 10.0], "Thetap": [8e-5, 1],
        "Te":[6e8, 5e10], "Tp": [1e9, 1e12],
        "Qvisc_e":[1e-6, 1e-3],
        "Qcool":[1e-6, 1e0],
        "Qcoul":[1e-9, 1e0],
        "coolTotalRatioPost":[1e-1, 10],
        "RHO":[1e-2, 1],
        "sigma":[1e-4, 1]
    }
    kwargs["rprofile_quantities_to_plot"] = [
        "RHO", "Thetae", "Te", "ue",
        "coulViscRatioPost", "coolViscRatioPost", "coolTotalRatioPost"
    ]
    kwargs["rprofile_quantities_to_plot"] = ["Te", "ue"]
    kwargs["azi_average_rprofile"] = True
    kwargs["azi_average_rprofile"] = False
    # com.plot_radial_profiles(**kwargs)

    kwargs["time_average_rprofile"] = False
    # for j in np.arange(300, 501)[::10]:
        # kwargs["tstart_rprofs"] = int(j)
        # com.plot_radial_profiles(**kwargs)

    # ----------------------------------------------
    # Plot vertical slices
    # ----------------------------------------------
    # tstart = 7000
    # tend = 14000
    # tstart = 14000
    # tstart = 14990
    tstart = 14000
    tend = 14995
    # tend = 15000
    kwargs["tstart_slices"] = tstart
    kwargs["tend_slices"] = tend
    kwargs["phi_ind"] = 0
    kwargs["phi_ind"] = "Avg"
    kwargs["slice_quantities_to_plot"] = [
        "RHO",
        "Te",
        "Tp",
        "TpTe",
        "beta",
        # "Qcool",
        # "Qcoul",
        # "Qvisc_e",
        "coulViscRatioPost",
        "coolViscRatioPost",
        "coolCoulRatioPost",
        "coolTotalRatioPost",
        # "Qcool_corona",
        # "Qcoul_corona",
    ]
    kwargs["cmaps"] = {
        # "TpTe":"magma"
        "Te":"RdBu_r",
        "RHO":"viridis",
        "beta":"PuOr_r",
        # "Tp":"RdBu_r",
        "TpTe":"RdBu_r",
        # "Qcoul":"RdBu_r",
        "Qcoul":"viridis",
        "Qvisc_e":"RdBu_r",
        "coulViscRatioPost":"RdBu_r",
        "coolViscRatioPost":"RdBu_r",
        "coolCoulRatioPost":"RdBu_r",
        "coolTotalRatioPost":"RdBu_r",
    }
    fix_slice_clims = {
        # "RHO":[1.e-2, 1],
        "RHO":[1.e-2, 1.e2],
        "Te":[1.e8, 1.e10],
        # "Te":[5.e7, 5.e9],
        "Tp":[1.e9, 1.e12],
        # "Tp":[5.e8, 5.e9],
        "Qcool":[1.e-8, 1.e+0],
        # "Qcoul":[-1.e-2, 1.e-2],
        "Qcoul":[1.e-8, 1.e+0],
        "Qvisc_e":[-1.e-4, 1.e-4],
        "TpTe":[1.e-3, 1.e3],
        # "TpTe":[5.e0, 5.e2],
        "beta":[1.e-2, 1.e2],
        # "coulViscRatioPost":[-1.e3, 1.e3],
        "coulViscRatioPost":[1.e-2, 1.e2],
        "coolViscRatioPost":[1.e-2, 1.e2],
        "coolCoulRatioPost":[1.e-2, 1.e2],
        "coolTotalRatioPost":[1.e-1, 1.e1],
    }
    kwargs["fix_slice_clims"] = fix_slice_clims
    # kwargs["save_pdf"] = True
    kwargs["dpi"] = 200
    # com.plot_vertical_slices(**kwargs)
    # -----------------------------
    # Snapshots
    # -----------------------------
    # dump_nums, dump_rgcs = com.simulations[com.sim_names[0]].load_dump_times("slices")
    # times_rgc = np.sort(dump_rgcs)[::-1]
    # times_rgc = np.arange(tstart, tend, 5)[::-1]
    # times_rgc = np.arange(tstart, tend, 10)[::-1]
    # tstart = 7000
    tstart = 14000
    tend = 15000
    # times_rgc = np.arange(tstart, tend, 50)
    # for tstart_rgc in times_rgc:
        # kwargs["tstart_slices"] = tstart_rgc
        # kwargs["tend_slices"] = tstart_rgc
        # kwargs["fix_slice_clims"] = fix_slice_clims
        # com.plot_vertical_slices(**kwargs)
        # kwargs["fix_slice_clims"] = {}
        # com.plot_vertical_slices(**kwargs)
