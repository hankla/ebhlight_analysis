import numpy as np
import scipy.integrate as integrate
import os
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.pylab as pl
import sys
sys.path.append('modules/')
from plotting_utils import *
from ebhlight_tools import *
import hdf5_to_dict as io
from default_filepaths import get_default_filepaths
from base_simulation_analyzer import base_simulation_analyzer
import units
units = units.get_cgs()


# The simulation plotter does NOT have access to the raw data.
# It takes the output of post_processor to make figures.
# i.e. run post processor on supercomputer and plotter on laptop.
class simulation_plotter(base_simulation_analyzer):
    def __init__(self, sim_name, setup="seagate_tests", **kwargs):
        # Inherit all of base simulation analyzer methods
        super().__init__(sim_name, setup, **kwargs)

        # ------------------------------
        # Initialization process
        # ------------------------------
        self._initialize_plotter(**kwargs)
        return

    def _initialize_plotter(self, **kwargs):
        """
        Initialize the simulation plotter.
            - make necessary filesystems/check existence
            - load properties file
        """
        if not self.quiet:
            print("Initializing...")
        # Check for path existence
        if not os.path.exists(self.path_to_reduced_data):
            print("---------------------------------------")
            print("            ERROR")
            print("The path to the reduced data does not exist!")
            print("For the plotter, this means death. Please find the path: ")
            print(self.path_to_reduced_data)
            print("Exiting.....")
            print("---------------------------------------")
            exit()
        if not os.path.exists(self.path_to_figures):
            os.makedirs(self.path_to_figures)

        # Load various information that doesn't change.
        # Must load from properties file since plotter does
        # NOT have access to raw data path.
        if not os.path.exists(self.path_to_properties):
            print("----------------------------------------------------------")
            print("            ERROR")
            print("The path to the properties file does not exist!")
            print("For the plotter, this means death. Please find the path: ")
            print(self.path_to_properties)
            print("Exiting.....")
            print("----------------------------------------------------------")
            exit()

        self.load_properties()

        return

    def plot_shell_differences(self, **kwargs):
        """
        tstart and tend: put in rg/c, NOT dump number.
        """
        print("----------------------------------------------------------")
        print("Plotting shell differences")
        to_time_average = kwargs.get("time_average_shells", True)
        tstart_rgc = kwargs.get("tstart_shells", 0.)
        if to_time_average:
            tend_rgc = kwargs.get("tend_shells", -1)
        else:
            tend_rgc = tstart
            kwargs["tend_shells"] = tend_rgc

        quantities = ["ue", "Te", "RHO"]
        q_to_retrieve = get_quantities_to_retrieve(quantities)

        kwargs["shell_quantities"] = q_to_retrieve
        tstart_rgc, tend_rgc, shell_avgs = self.retrieve_shell_averages(**kwargs)
        figdir_base = self.path_to_figures + "shell_averages/"

        dump_nums, dump_rgcs = self.load_dump_times("shells")

        # Indices of tstart and tend
        tstart_ind = (np.abs(tstart_rgc - dump_rgcs)).argmin()
        tend_ind = (np.abs(tend_rgc - dump_rgcs)).argmin()
        # Code values of tstart and tend
        tstart_dump_rgc = dump_rgcs[tstart_ind]
        tend_dump_rgc = dump_rgcs[tend_ind]

        r_vals = self.r_grid[:, 0, 0]

        for quantity in quantities:
            if to_time_average:
                tit_str = "Time-averaged from $t={:.2f}$ --- ${:.2f}~GM/c^3$".format(tstart_dump_rgc, tend_dump_rgc)
                figdir = figdir_base + "tAvg{:.0f}-{:.0f}rgc/".format(tstart_dump_rgc, tend_dump_rgc - 1)
                if not os.path.exists(figdir): os.makedirs(figdir)
                figname = "difference_" + quantity
            else:
                tit_str = "Snapshot at t={:.2f} $GM/c^3$".format(tstart_dump_rgc)
                figdir = figdir_base + "snapshot/" + quantity + "/"
                if not os.path.exists(figdir): os.makedirs(figdir)
                figname = "difference_" + quantity + "_t{:.0f}rgc".format(tstart_dump_rgc)

            if quantity in get_list_of_reduced_calculated_quantities():
                shell_data = calculate_reduced_quantity(quantity, shell_avgs, r_vals)
            elif quantity in shell_avgs:
                # With tend = tstart + 1, can average even if not to_time_average
                shell_array = shell_avgs[quantity]
                shell_data = np.mean(shell_array, axis=0)

            # Skip shell data is None. Error message output in calculate_reduced_quantity
            if shell_data is None:
                continue
            tit_str = "Density-weighted shell averages\n " + tit_str
            plt.figure()
            plt.title(tit_str)
            if quantity in ["Thetae", "Thetap", "Te", "Tp"] and "Tel_target" in self.header:
                lthresh = 1e7
                ylims = [-2e9, 2e9]
                T_target = self.header["Tel_target"]*r_vals**(-self.header["Tel_rslope"])
                if quantity == "Thetae":
                    target = get_thetae(T_target)
                    label_str = r"Target $\theta_e=$" + format_exp(get_thetae(self.header["Tel_target"]))
                elif quantity == "Thetap":
                    target = get_thetap(T_target)
                    label_str = r"Target $\theta_p=$" + format_exp(get_thetap(self.header["Tel_target"]))
                else:
                    target = T_target
                    label_str = r"Target $T_e=$" + format_exp(self.header["Tel_target"])
                label_str += r"$(r/r_g)^{{-{:.1f}}}$ K".format(self.header["Tel_rslope"])
            if quantity == "ue" and "Tel_target" in self.header:
                T_target = self.header["Tel_target"]
                rho_data = np.mean(shell_avgs["RHO"], axis=0)
                gammae = self.header["game"]
                target = rho_data*units['KBOL']*T_target/(units['MP']*(gammae - 1.0)*units['CL']**2)
                label_str = r"Target $T_e=$" + format_exp(self.header["Tel_target"])
            lthresh = 1e-4
            ylims = [-1e-2, 1e-2]
            # lthresh = 1e-5
            # ylims = [-1e-3, 1e-3]
            plt.yscale('symlog', linthresh=lthresh)
            plt.ylim(ylims)
            plt.plot(r_vals, shell_data - target)
            plt.xlabel(r"$r/r_g$")
            plt.xscale('log')
            # plt.xlim([None, 10])
            plt.xlim([self.rEH, 50])
            # plt.xlim([self.rEH, None])
            # plt.gca().axvline([self.rEH], color='black', ls=':')
            plt.gca().axvline([self.rISCO], color='black', ls=':')
            # plt.gca().relim()
            # plt.gca().autoscale_view(); plt.draw()
            if quantity in self.labels:
                label_str = self.labels[quantity] + " minus target"
            else:
                label_str = quantity
            plt.ylabel(label_str)
            figname += ".png"
            if plt.gca().get_ylim()[0] < 0:
                plt.gca().axhline([0.0], color='black', ls='--')
            plt.tight_layout()
            print("Saving figure " + figdir + figname)
            plt.savefig(figdir + figname, bbox_inches='tight')
            plt.close()
        # plt.show()

    def plot_shell_timescales(self, **kwargs):
        """
        tstart and tend: put in rg/c, NOT dump number.
        Normalization options:
            - "infall": normalize to infall time
            - "dynamical": normalize to 1/Omega_K
            - "none": no normalization, units of rg/c
        """
        species = kwargs.get("species", "electron") # options: gas, protons, electrons
        normalization = kwargs.get("normalization", "none")
        to_time_average = kwargs.get("time_average_shells", True)
        fix_shell_clims = kwargs.get("fix_shell_clims", {})
        tstart_rgc = kwargs.get("tstart_shells", 0.0) # in rg/c
        if to_time_average:
            tend_rgc = kwargs.get("tend_shells", -1)
        else:
            tend_rgc = tstart_rgc
            kwargs["tend_shells"] = tend_rgc

        if species == "electron":
            quantities = ["theat_e", "tCoulomb_e", "tCool_e"]
        elif species == "proton":
            quantities = ["theat_p", "tCoulomb_p", "Tcool"]
        else:
            quantities = ["theat", "Tcool"]

        quantities.append("tinfall")
        if normalization == "infall":
            ylabel = r"$t/t_{\rm infall}$"
        elif normalization == "dynamical":
            ylabel = r"$t/t_{\rm dyn}$"
        else:
            ylabel = r"$tc/r_g$"

        quantities = clean_reduced_quantities_list(quantities, self.header)
        q_to_retrieve = get_quantities_to_retrieve(quantities)
        q_to_retrieve.append("vrr_bl")

        cycle_quantities = quantities.copy()
        if normalization == "infall":
            cycle_quantities.remove("tinfall")

        kwargs["shell_quantities"] = q_to_retrieve
        tstart_rgc, tend_rgc, shell_avgs = self.retrieve_shell_averages(**kwargs)
        figdir_base = self.path_to_figures + "shell_averages/"

        dump_nums, dump_rgcs = self.load_dump_times("shells")

        # Indices of tstart and tend
        tstart_ind = (np.abs(tstart_rgc - dump_rgcs)).argmin()
        tend_ind = (np.abs(tend_rgc - dump_rgcs)).argmin()
        # Code values of tstart and tend
        tstart_dump_rgc = dump_rgcs[tstart_ind]
        tend_dump_rgc = dump_rgcs[tend_ind]

        r_vals = np.array(self.r_grid[:, 0, 0])
        EH_ind = (np.abs(self.rEH - r_vals)).argmin()

        if to_time_average:
            tit_str = "Time-averaged from $t={:.2f}$ --- ${:.2f}~GM/c^3$".format(tstart_dump_rgc, tend_dump_rgc)
            figdir = figdir_base + "tAvg{:.0f}-{:.0f}rgc/".format(tstart_dump_rgc, tend_dump_rgc - 1)
            if not os.path.exists(figdir): os.makedirs(figdir)
            figname = species + "_timescales_norm-" + normalization
        else:
            tit_str = "Snapshot at t={:.2f} $GM/c^3$".format(tstart_dump_rgc)
            figdir = figdir_base + "snapshot/timescales/"
            if not os.path.exists(figdir): os.makedirs(figdir)
            figname = species + "_t{:.0f}rgc_norm-".format(tstart_dump_rgc) + normalization

        plt.figure()
        for quantity in cycle_quantities:
            tinfall = calculate_reduced_quantity("tinfall", shell_avgs, r_vals, sim_units=self.header, EH_ind=EH_ind)
            if to_time_average and tinfall.ndim > 1:
                tinfall = np.mean(tinfall, axis=0)
            if quantity in get_list_of_reduced_calculated_quantities():
                shell_data = calculate_reduced_quantity(quantity, shell_avgs, r_vals, EH_ind=EH_ind)
                if quantity == "tCool_e":
                    tcool_data = np.mean(shell_avgs["Tcool"], axis=0)
            elif quantity in shell_avgs:
                shell_array = shell_avgs[quantity]
                if to_time_average and shell_array.ndim > 1:
                    shell_data = np.mean(shell_array, axis=0)
                else:
                    shell_data = shell_array
            else:
                continue
            # Skip shell data is None. Error message output in calculate_reduced_quantity
            if shell_data is None:
                continue
            if shell_data.shape != tinfall.shape:
                shell_data = shell_data[EH_ind:]
                if quantity == "tCool_e":
                    tcool_data = tcool_data[EH_ind:]
            if r_vals.shape != tinfall.shape:
                temp_rvals = r_vals[EH_ind:]
            else:
                temp_rvals = r_vals

            if normalization == "infall":
                shell_data = shell_data/(tinfall + 1.e-20)
                if quantity == "tCool_e":
                    tcool_data = tcool_data/(tinfall + 1.e-20)
            elif normalization == "dynamical":
                # tcool = 1.0/(Omega*tcoolOmega0)
                # So Omega = 1.0/(tcool*tcoolOmega0) and
                # tdyn = 1.0/Omega = tcool*tcoolOmega0
                tcool_factor = self.header["tcoolOmega0"]
                tdyn = tcool_factor*shell_avgs["Tcool"]
                if to_time_average and tdyn.ndim > 1:
                    tdyn = np.mean(tdyn, axis=0)
                shell_data = shell_data/(tdyn[EH_ind:])
                if quantity == "tCool_e":
                    tcool_data = tcool_data/(tdyn[EH_ind:])

            plt.plot(temp_rvals, shell_data, label=self.labels[quantity])
            if quantity == "tCool_e":
                plt.plot(temp_rvals, tcool_data, color='k', ls=':')

        if normalization in ["infall", "dynamical"]:
            plt.gca().axhline([1.0], color='k', ls='--')
        tit_str = "Density-weighted shell averages\n " + tit_str
        plt.title(tit_str)
        if "vrr_bl" in shell_avgs:
            # Plot equilibrium radius, req = t|v^r| using earliest time
            vrr_data = np.abs(np.mean(shell_avgs["vrr_bl"], axis=0))
            req_ind_start = (np.abs(vrr_data*tstart_rgc - r_vals)).argmin()
            r_eq = r_vals[req_ind_start]
            # plt.gca().axvline([r_eq], color='red', ls=':', label=r"$r_{\rm eq}$")
            plt.gca().axvline([r_eq], color='red', ls=':')
            plt.xlim([self.rEH*1.1, np.max([50, r_eq*1.1])])
        else:
            plt.xlim([self.rEH*1.1, 50])
        plt.gca().axvline([self.rISCO], color='black', ls=':')
        # # Plot geodesic (free-fall) time. Integrate Mummery & Balbus 2022 eq. 13 for u^r
        # dense_r = np.linspace(self.rEH, self.rISCO, 1000, endpoint = False)
        # uR = np.sqrt(2./(3.*self.rISCO))*(self.rISCO/dense_r - 1.)**1.5
        # freefall_time = np.hstack([0, integrate.cumtrapz(1.0/uR, dense_r)])
        # plt.plot(dense_r, freefall_time, label=r"$t_{\rm freefall}c/r_g$")
        plt.yscale('log')
        plt.xlabel(r"$r/r_g$")
        ylabel = "Proper frame timescales " + ylabel
        plt.ylabel(ylabel)
        plt.xscale('log')
        plt.legend(ncol=2)
        if "timescales_" + normalization in fix_shell_clims:
            plt.ylim(fix_shell_clims["timescales_" + normalization])
            figname = "ylim_" + figname
        figname += ".png"
        plt.tight_layout()
        print("Saving figure " + figdir + figname)
        plt.savefig(figdir + figname, bbox_inches='tight')
        plt.close()
        # plt.show()

    def plot_shell_averages(self, **kwargs):
        """
        tstart and tend: put in rg/c, NOT dump number.
        """
        print("----------------------------------------------------------")
        print("Plotting shell averages")
        to_time_average = kwargs.get("time_average_shells", True)
        fix_shell_clims = kwargs.get("fix_shell_clims", {})
        tstart_rgc = kwargs.get("tstart_shells", 0.0) # in rg/c
        with_Qcool = kwargs.get("with_Qcool", False)
        if to_time_average:
            tend_rgc = kwargs.get("tend_shells", -1)
        else:
            tend_rgc = tstart_rgc
            kwargs["tend_shells"] = tend_rgc

        quantities = kwargs.get("shell_quantities_to_plot", get_list_of_all_quantities())
        # print("shell to get")
        # print(quantities)
        # quantities = clean_reduced_quantities_list(quantities, self.header)
        # q_to_retrieve = get_quantities_to_retrieve(quantities)
        # if "vrr_bl" not in q_to_retrieve:
            # q_to_retrieve.append("vrr_bl")
        # kwargs["shell_quantities"] = q_to_retrieve
        # tstart_rgc, tend_rgc, shell_avgs = self.retrieve_shell_averages(**kwargs)
        tstart_rgc, tend_rgc, shell_avgs = self.check_for_time_averaged_shells(quantities, **kwargs)
        figdir_base = self.path_to_figures + "shell_averages/"

        dump_nums, dump_rgcs = self.load_dump_times("shells")

        # Indices of tstart and tend
        tstart_ind = (np.abs(tstart_rgc - dump_rgcs)).argmin()
        tend_ind = (np.abs(tend_rgc - dump_rgcs)).argmin()
        # Code values of tstart and tend
        tstart_dump_rgc = dump_rgcs[tstart_ind]
        tend_dump_rgc = dump_rgcs[tend_ind]

        r_vals = np.array(self.r_grid[:, 0, 0])

        for quantity in quantities:
            if to_time_average:
                tit_str = "Time-averaged from $t={:.2f}$ --- ${:.2f}~GM/c^3$".format(tstart_dump_rgc, tend_dump_rgc)
                figdir = figdir_base + "tAvg{:.0f}-{:.0f}rgc/".format(tstart_dump_rgc, tend_dump_rgc - 1)
                if not os.path.exists(figdir): os.makedirs(figdir)
                figname = quantity
            else:
                tit_str = "Snapshot at t={:.2f} $GM/c^3$".format(tstart_dump_rgc)
                figdir = figdir_base + "snapshot/" + quantity + "/"
                if not os.path.exists(figdir): os.makedirs(figdir)
                figname = quantity + "_t{:.0f}rgc".format(tstart_dump_rgc)

            if quantity in get_list_of_reduced_calculated_quantities():
                shell_data = calculate_reduced_quantity(quantity, shell_avgs, r_vals, sim_units=self.header)
            elif quantity in shell_avgs:
                shell_array = shell_avgs[quantity]
                if to_time_average and shell_array.ndim > 1:
                    shell_data = np.mean(shell_array, axis=0)
                else:
                    shell_data = shell_array
            else:
                continue
            # Skip shell data is None. Error message output in calculate_reduced_quantity
            if shell_data is None:
                continue
            if quantity in ["vrr_bl", "vrr", "ucon_bl1"]:
                shell_data = np.abs(shell_data)

            tit_str = "Density-weighted shell averages\n " + tit_str
            plt.figure()
            plt.title(tit_str)
            print(quantity)
            print(shell_data.shape)
            print(r_vals.shape)
            plt.plot(r_vals, shell_data)
            if "vrr_bl" in shell_avgs:
                # Plot equilibrium radius, req = t|v^r| using earliest time
                vrr_data = np.abs(np.mean(shell_avgs["vrr_bl"], axis=0))
                req_ind_start = (np.abs(vrr_data*tstart_rgc - r_vals)).argmin()
                req_ind_end = (np.abs(vrr_data*tend_rgc - r_vals)).argmin()
                plt.gca().axvline(r_vals[req_ind_start], color='red', ls=':')
                plt.gca().axvline(r_vals[req_ind_end], color='red', ls=':')

            if quantity == "H_over_R" or quantity == "HT_over_R":
                target_temp = self.header['Tel_target']*r_vals**(-self.header["Tel_rslope"])
                thin_disk = get_Hr_from_targetT(target_temp, r_vals, self.header['gam'])
                # thin_disk = np.sqrt(units['KBOL']/(units['MP']*units['CL']**2))
                # thin_disk = thin_disk*np.sqrt(self.header['gam']*self.header["Tel_target"]*r_vals)
                plt.plot(r_vals, thin_disk, label=r'Thin disk', ls='--', color='black')
            if quantity == "vphir_bl":
                keplerian_vphi = 1.0/(self.header["a"] + r_vals**1.5)
                plt.plot(r_vals, keplerian_vphi, color='red', ls='--', label=r"Keplerian $v^\phi$")
                plt.legend()
            if quantity in ["Thetae", "Thetap", "Te", "Tp"] and "Tel_target" in self.header:
                T_target = self.header["Tel_target"]*r_vals**(-self.header["Tel_rslope"])
                if quantity == "Thetae":
                    target = get_thetae(T_target)
                    label_str = r"Target $\theta_e=$" + format_exp(get_thetae(self.header["Tel_target"]))
                elif quantity == "Thetap":
                    target = get_thetap(T_target)
                    label_str = r"Target $\theta_p=$" + format_exp(get_thetap(self.header["Tel_target"]))
                else:
                    target = T_target
                    label_str = r"Target $T_e=$" + format_exp(self.header["Tel_target"])
                label_str += r"$(r/r_g)^{{-{:.1f}}}$ K".format(self.header["Tel_rslope"])
                plt.plot(r_vals, target, color='k', ls='--', label=label_str)
                # if quantity == "Te":
                    # Delta = r_vals**2 - 2.0*r_vals + self.header['a']**2
                    # rho2_mid = r_vals**2
                    # sqrtg00_mid = np.sqrt(Delta/rho2_mid)
                    # plt.plot(r_vals, target/sqrtg00_mid, color='red', ls='--', label=r"$T_e/\sqrt{g_{00}}$ at $\theta=\pi/2$")
                    # rho2_pi4 = r_vals**2 + self.header['a']**2*(np.cos(np.pi/6))**2
                    # sqrtg00_pi4 = np.sqrt(Delta/rho2_pi4)
                    # plt.plot(r_vals, target/sqrtg00_pi4, color='blue', ls='--', label=r"$T_e/\sqrt{g_{00}}$ at $\theta=\pi/6$")
                plt.legend()
            if quantity == "ue" and "Tel_target" in self.header:
                T_target = self.header["Tel_target"]
                if shell_avgs["RHO"].ndim > 1:
                    rho_data = np.mean(shell_avgs["RHO"], axis=0)
                else:
                    rho_data = shell_avgs["RHO"]
                gammae = self.header["game"]
                target = rho_data*units['KBOL']*T_target/(units['MP']*(gammae - 1.0)*units['CL']**2)
                label_str = r"Target $T_e=$" + format_exp(self.header["Tel_target"])
                plt.plot(r_vals, target, color='k', ls='--', label=None)
            if quantity == "ue" and with_Qcool:
                if shell_avgs["Qcool"].ndim > 1:
                    qcool_data = np.mean(shell_avgs["Qcool"], axis=0)
                else:
                    qcool_data = shell_avgs["Qcool"]
                plt.plot(r_vals, qcool_data, ls='-', label=self.labels["Qcool"])
                figname = "withQcool_" + figname
                plt.legend()
            if quantity == "coulombQuality":
                plt.gca().axhline(1.0, color='k', ls='--')
            if quantity == "TpTe":
                plt.gca().axhline(self.header["tptemin"], color='k', ls='--')
                plt.gca().axhline(self.header["tptemax"], color='k', ls='--')
                plt.gca().axhline(1.0, color='k', ls='--')
            if quantity == "KEL":
                # From electrons.c fixup_electrons_1zone function
                ktot_data = np.mean(shell_avgs["KTOT"], axis=0)
                rho_data = np.mean(shell_avgs["RHO"], axis=0)
                gam = self.header["gam"]
                game = self.header["game"]
                gamp = self.header["gamp"]

                T_target = self.header["Tel_target"]*r_vals**(-self.header["Tel_rslope"])
                thetae_target = get_thetae(T_target)
                # kel_target = thetae_target*units['ME']/units['MP']*rho_data**(1.0 - game)
                # kel_max = ktot_data*rho_data**(gam - game)/(self.header['tptemin']*(gam - 1.0)/(gamp-1.0)+(gam-1.0)/(game-1.0))
                # kel_min = ktot_data*rho_data**(gam - game)/(self.header['tptemax']*(gam - 1.0)/(gamp-1.0)+(gam-1.0)/(game-1.0))
                # plt.plot(r_vals, kel_max, ls='--', color='k', label="Max")
                # plt.plot(r_vals, kel_min, ls='--', color='k', label="Min")
                # plt.plot(r_vals, kel_target, ls='--', color='red', label="Target")
            if "Ratio" in quantity:
                plt.gca().axhline([1.0], ls='--', color='black')
            if quantity in get_log_scales() and get_log_scales()[quantity]:
                plt.yscale('log')
            elif quantity in fix_shell_clims and fix_shell_clims[quantity][0] > 0:
                plt.yscale('log')
            else:
                plt.ylim([0.0, None])
            plt.xlabel(r"$r/r_g$")
            plt.xscale('log')
            # plt.xlim([None, 10])
            plt.xlim([self.rEH, 50])
            plt.xlim([self.rEH, r_vals[-1]])
            # plt.xlim([self.rEH, None])
            # plt.gca().axvline([self.rEH], color='black', ls=':')
            # plt.gca().axvline([r_eq], color='red', ls=':')
            plt.gca().axvline([self.rISCO], color='black', ls=':')
            # plt.gca().relim()
            # plt.gca().autoscale_view(); plt.draw()
            if quantity in self.labels:
                label_str = self.labels[quantity]
            else:
                label_str = quantity
            plt.ylabel(label_str)
            if quantity in fix_shell_clims:
                plt.ylim(fix_shell_clims[quantity])
                figname = "ylim_" + figname
            figname += ".png"
            if plt.gca().get_ylim()[0] < 0:
                plt.gca().axhline([0.0], color='black', ls='--')
            plt.tight_layout()
            print("Saving figure " + figdir + figname)
            plt.savefig(figdir + figname, bbox_inches='tight')
            plt.close()
        # plt.show()

    def plot_shell_averages_over_time(self, **kwargs):
        """
        rgcs_to_plot:
            - if None, then just plot from earliest time to
              latest time in even spaces.
            - if [tstart, tend], then plot from tstart to tend
              in even spaces
            - if [t1, t2, t3, ...tn] just plot those times
        tstart and tend: put in rg/c, NOT dump number.
        """
        print("----------------------------------------------------------")
        print("Plotting shell averages over time, i.e. across radius")
        print("color-coded by time.")
        tit_str = "Density-weighted shell averages"
        to_time_average = False
        fix_shell_clims = kwargs.get("fix_shell_clims", {})
        rgcs_to_plot = kwargs.get("rgcs_to_plot_shells", None)
        n_to_plot = kwargs.get("n_to_plot", 12)

        quantities = kwargs.get("shell_quantities_to_plot", get_list_of_all_quantities())
        q_to_retrieve = get_quantities_to_retrieve(quantities)

        # First, see how many time entries there are between tstart and tend
        dump_nums, dump_rgcs = self.load_dump_times("shells")
        if rgcs_to_plot is None:
            dumps_to_plot = dump_nums[::10]
            n_dumps = len(dump_nums)
            dumps_to_plot = dump_nums[::int(n_dumps/n_to_plot)]
            type_str = "_allEven"
        elif len(rgcs_to_plot) == 2:
            tstart_ind = (np.abs(rgcs_to_plot[0] - dump_rgcs)).argmin()
            tend_ind = (np.abs(rgcs_to_plot[1] - dump_rgcs)).argmin()
            t1 = dump_rgcs[tstart_ind]
            t2 = dump_rgcs[tend_ind]
            n_dumps = len(dump_nums[tstart_ind:tend_ind+1])
            dumps_to_plot = dump_nums[tstart_ind:tend_ind][::int(n_dumps/n_to_plot)]
            type_str = "_t{:.0f}-{:.0f}rgcEven".format(t1, t2)
        else:
            dumps_to_plot = []
            type_str = "_t"
            for rgc in rgcs_to_plot:
                t_ind = (np.abs(rgc - dump_rgcs)).argmin()
                dumps_to_plot.append(t_ind)
                type_str += "{:.0f}-".format(dump_rgcs[t_ind])
            type_str += "rgc"
        times_to_plot = []
        for dump in dumps_to_plot:
            t_ind = (np.abs(dump - dump_nums)).argmin()
            times_to_plot.append(dump_rgcs[t_ind])


        # Make colormap have the number of entries in dumps_to_plot
        # normalize = matplotlib.colors.Normalize(vmin=times_to_plot[0], vmax=times_to_plot[-1])
        # normalize = matplotlib.colors.BoundaryNorm(times_to_plot, len(times_to_plot))
        # normalize = matplotlib.colors.Normalize(vmin=0, vmax=len(dumps_to_plot))
        # colormap = matplotlib.cm.rainbow_r
        cmap = matplotlib.cm.get_cmap('rainbow_r')
        rgba = cmap(np.linspace(0, 1, len(dumps_to_plot)))
        # colors = pl.cm.rainbow(np.linspace(0, 1, len(dumps_to_plot)))[::-1]

        figdir = self.path_to_figures + "shell_averages/time_evolution" + type_str + "/"
        if not os.path.exists(figdir): os.makedirs(figdir)
        r_vals = np.array(self.r_grid[:, 0, 0])
        shell_avgs = self.create_shell_array(dumps_to_plot[0], dumps_to_plot[-1], q_to_retrieve, "shells", dumps_to_plot)

        for quantity in quantities:
            figname = quantity
            plt.figure()
            plt.title(tit_str)
            if quantity in get_list_of_reduced_calculated_quantities():
                shell_array = calculate_reduced_quantity(quantity, shell_avgs, r_vals, sim_units=self.header, tavg=False)
            for j, dump in enumerate(dumps_to_plot):
                dump = int(dump)
                if quantity in get_list_of_reduced_calculated_quantities():
                    shell_data = shell_array[j]
                elif quantity in shell_avgs:
                    shell_data = shell_avgs[quantity][j]
                else:
                    continue
                # Skip shell data is None. Error message output in calculate_reduced_quantity
                if shell_data is None:
                    continue
                if quantity in ["vrr_bl", "vrr", "ucon_bl1"]:
                    shell_data = np.abs(shell_data)

                t_ind = (np.abs(dump - dump_nums)).argmin()
                # times_to_plot.append(dump_rgcs[t_ind])
                # plt.plot(r_vals, shell_data, color=colormap(normalize(times_to_plot[j])), alpha=0.5, label=r"$t={:.0f}r_g/c$".format(dump_rgcs[t_ind]))
                plt.plot(r_vals, shell_data, color=rgba[j], alpha=0.5, label=r"$t={:.0f}r_g/c$".format(dump_rgcs[t_ind]))

            if quantity == "H_over_R" or quantity == "HT_over_R":
                target_temp = self.header['Tel_target']*r_vals**(-self.header["Tel_rslope"])
                thin_disk = get_Hr_from_targetT(target_temp, r_vals, self.header['gam'])
                # thin_disk = np.sqrt(units['KBOL']/(units['MP']*units['CL']**2))
                # thin_disk = thin_disk*np.sqrt(self.header['gam']*self.header["Tel_target"]*r_vals)
                plt.plot(r_vals, thin_disk, label=r'Thin disk', ls='--', color='black')
            if quantity == "vphir_bl":
                keplerian_vphi = 1.0/(self.header["a"] + r_vals**1.5)
                plt.plot(r_vals, keplerian_vphi, color='red', ls='--', label=r"Keplerian $v^\phi$")
            if quantity in ["Thetae", "Thetap", "Te", "Tp"] and "Tel_target" in self.header:
                T_target = self.header["Tel_target"]*r_vals**(-self.header["Tel_rslope"])
                if quantity == "Thetae":
                    target = get_thetae(T_target)
                    label_str = r"Target $\theta_e=$" + format_exp(get_thetae(self.header["Tel_target"]))
                elif quantity == "Thetap":
                    target = get_thetap(T_target)
                    label_str = r"Target $\theta_p=$" + format_exp(get_thetap(self.header["Tel_target"]))
                else:
                    target = T_target
                    label_str = r"Target $T_e=$" + format_exp(self.header["Tel_target"])
                label_str += r"$(r/r_g)^{{-{:.1f}}}$ K".format(self.header["Tel_rslope"])
                plt.plot(r_vals, target, color='k', ls='--', label=label_str)
                # if quantity == "Te":
                    # Delta = r_vals**2 - 2.0*r_vals + self.header['a']**2
                    # rho2_mid = r_vals**2
                    # sqrtg00_mid = np.sqrt(Delta/rho2_mid)
                    # plt.plot(r_vals, target/sqrtg00_mid, color='red', ls='--', label=r"$T_e/\sqrt{g_{00}}$ at $\theta=\pi/2$")
                    # rho2_pi4 = r_vals**2 + self.header['a']**2*(np.cos(np.pi/6))**2
                    # sqrtg00_pi4 = np.sqrt(Delta/rho2_pi4)
                    # plt.plot(r_vals, target/sqrtg00_pi4, color='blue', ls='--', label=r"$T_e/\sqrt{g_{00}}$ at $\theta=\pi/6$")
            if quantity == "ue" and "Tel_target" in self.header:
                T_target = self.header["Tel_target"]
                if shell_avgs["RHO"].ndim > 1:
                    rho_data = np.mean(shell_avgs["RHO"], axis=0)
                else:
                    rho_data = shell_avgs["RHO"]
                gammae = self.header["game"]
                target = rho_data*units['KBOL']*T_target/(units['MP']*(gammae - 1.0)*units['CL']**2)
                label_str = r"Target $T_e=$" + format_exp(self.header["Tel_target"])
                plt.plot(r_vals, target, color='k', ls='--', label=None)
            if quantity == "coulombQuality":
                plt.gca().axhline(1.0, color='k', ls='--')
            if quantity == "TpTe":
                plt.gca().axhline(self.header["tptemin"], color='k', ls='--')
                plt.gca().axhline(self.header["tptemax"], color='k', ls='--')
                plt.gca().axhline(1.0, color='k', ls='--')
            if quantity == "KEL":
                # From electrons.c fixup_electrons_1zone function
                ktot_data = np.mean(shell_avgs["KTOT"], axis=0)
                rho_data = np.mean(shell_avgs["RHO"], axis=0)
                gam = self.header["gam"]
                game = self.header["game"]
                gamp = self.header["gamp"]

                T_target = self.header["Tel_target"]*r_vals**(-self.header["Tel_rslope"])
                thetae_target = get_thetae(T_target)
                kel_target = thetae_target*units['ME']/units['MP']*rho_data**(1.0 - game)
                kel_max = ktot_data*rho_data**(gam - game)/(self.header['tptemin']*(gam - 1.0)/(gamp-1.0)+(gam-1.0)/(game-1.0))
                kel_min = ktot_data*rho_data**(gam - game)/(self.header['tptemax']*(gam - 1.0)/(gamp-1.0)+(gam-1.0)/(game-1.0))
                print(kel_max.shape)
                print(kel_target.shape)
                plt.plot(r_vals, kel_max, ls='--', color='k', label="Max")
                plt.plot(r_vals, kel_min, ls='--', color='k', label="Min")
                plt.plot(r_vals, kel_target, ls='--', color='red', label="Target")
            if "Ratio" in quantity:
                plt.gca().axhline([1.0], ls='--', color='black')
            if quantity in get_log_scales() and get_log_scales()[quantity]:
                plt.yscale('log')
            elif quantity in fix_shell_clims and fix_shell_clims[quantity][0] > 0:
                plt.yscale('log')
            else:
                plt.ylim([0.0, None])
            plt.xlabel(r"$r/r_g$")
            plt.xscale('log')
            # plt.xlim([None, 10])
            plt.xlim([self.rEH, 50])
            # plt.xlim([self.rEH, None])
            # plt.gca().axvline([self.rEH], color='black', ls=':')
            # plt.gca().axvline([r_eq], color='red', ls=':')
            plt.gca().axvline([self.rISCO], color='black', ls=':')
            # plt.gca().relim()
            # plt.gca().autoscale_view(); plt.draw()
            if quantity in self.labels:
                label_str = self.labels[quantity]
            else:
                label_str = quantity
            plt.ylabel(label_str)
            if quantity in fix_shell_clims:
                plt.ylim(fix_shell_clims[quantity])
                figname = "ylim_" + figname
            figname += ".png"
            if plt.gca().get_ylim()[0] < 0:
                plt.gca().axhline([0.0], color='black', ls='--')
            # scalarmappable = matplotlib.cm.ScalarMappable(norm=normalize, cmap=colormap)
            # scalarmappable.set_array(times_to_plot)
            # cb = plt.colorbar(scalarmappable, ticks=times_to_plot[::int(len(times_to_plot)/4)])
            cb = plt.colorbar(matplotlib.cm.ScalarMappable(cmap=matplotlib.colors.ListedColormap(rgba)), orientation='vertical', ticks=np.linspace(0, 1, len(rgba)*2+1)[1::2])
            cb.set_ticklabels(['{:.0f}'.format(t) for t in times_to_plot])
            cb.set_label(r"$tc/r_g$")
            plt.tight_layout()
            print("Saving figure " + figdir + figname)
            plt.savefig(figdir + figname, bbox_inches='tight')
            plt.close()
        # plt.show()

    def plot_shell_over_time(self, radius, **kwargs):
        """
        radius in rg.
        """
        print("----------------------------------------------------------")
        print("Plotting shell quantities over time at r={:.2f} rg.".format(radius))
        kwargs["over_time"] = True
        fix_shell_clims = kwargs.get("fix_shell_clims", {})
        smooth_ratios = kwargs.get("smooth_ratios", False)
        neighbor_smooth = kwargs.get("neighbor_smooth", 10)

        quantities = kwargs.get("shell_quantities_to_plot", get_list_of_all_quantities())
        quantities = clean_reduced_quantities_list(quantities, self.header)
        q_to_retrieve = get_quantities_to_retrieve(quantities)

        # kwargs["tstart_shells"] = 7000.0
        kwargs["tstart_shells"] = 0.0
        kwargs["tend_shells"] = -1
        kwargs["shell_quantities"] = q_to_retrieve
        tstart_rgc, tend_rgc, shell_avgs = self.retrieve_shell_averages(**kwargs)
        figdir = self.path_to_figures + "shell_averages_over_time/r{:.2f}/".format(radius)
        if not os.path.exists(figdir): os.makedirs(figdir)

        dump_nums, dump_rgcs = self.load_dump_times("shells")
        tend_rgc = dump_rgcs[-1]
        tstart_rgc = dump_rgcs[0]
        # Indices of tstart and tend
        tstart_ind = (np.abs(tstart_rgc - dump_rgcs)).argmin()
        tend_ind = (np.abs(tend_rgc - dump_rgcs)).argmin()

        # Radial values
        r_vals = np.array(self.r_grid[:, 0, 0])
        r_index = (np.abs(r_vals - radius)).argmin()
        r_val = r_vals[r_index]
        eh_zones = (np.abs(r_vals - self.rEH)).argmin()
        isco_zones = (np.abs(r_vals - self.rISCO)).argmin()
        print("There are {:d} zones within the event horizon and {:d} within the ISCO".format(eh_zones, isco_zones))

        for quantity in quantities:
            figname = quantity
            times = dump_rgcs[tstart_ind:tend_ind + 1]
            if quantity in get_list_of_reduced_calculated_quantities():
                shell_data = calculate_reduced_quantity(quantity, shell_avgs, r_vals, r_index, smooth_ratios, neighbor_smooth)
            elif quantity in shell_avgs:
                shell_data = shell_avgs[quantity][:, r_index]
            else:
                print("Unknown quantity " + quantity)
                continue

            if shell_data.shape != times.shape:
                print("Quantity " + quantity + " has not been calculated for all times.")
                print("    Recommend running postprocessing again. Sim: " + self.sim_name)
                continue

            tit_str = "Density-weighted shell averages at $r={:.2f}r_g$\n ".format(r_val)
            if smooth_ratios and "Post" in quantity:
                # box = np.ones(neighbor_smooth)/neighbor_smooth
                # shell_data = np.convolve(shell_data, box, mode='same')
                tit_str += "Smoothing over {:d} neighbors".format(neighbor_smooth)

            if quantity in ["vrr_bl", "vrr", "ucon_bl1"]:
                shell_data = np.abs(shell_data)
            plt.figure()
            plt.title(tit_str)
            if quantity == "Qvisc_e":
                num_neg = np.sum(shell_data < 0.0)
                last_neg_ind = np.where(shell_data < 0.)[0][-1]
                last_neg = times[last_neg_ind]
                print("There are {:d} times with shell-averaged Qvisc_e < 0,".format(num_neg))
                print("the latest being at t={:.2f} rg/c".format(last_neg))
            plt.plot(times, shell_data)
            if quantity == "KEL" and "Tel_target" in self.header:
                # From electrons.c fixup_electrons_1zone function
                ktot_data = shell_avgs["KTOT"][:, r_index]
                rho_data = shell_avgs["RHO"][:, r_index]
                gam = self.header["gam"]
                game = self.header["game"]
                gamp = self.header["gamp"]

                T_target = self.header["Tel_target"]*np.ones(len(times))*r_val**(-self.header["Tel_rslope"])
                thetae_target = get_thetae(T_target)
                kel_target = thetae_target*units['ME']/units['MP']*rho_data**(1.0 - game)
                kel_max = ktot_data*rho_data**(gam - game)/(self.header['tptemin']*(gam - 1.0)/(gamp-1.0)+(gam-1.0)/(game-1.0))
                kel_min = ktot_data*rho_data**(gam - game)/(self.header['tptemax']*(gam - 1.0)/(gamp-1.0)+(gam-1.0)/(game-1.0))
                plt.plot(times, kel_max, ls='--', color='k', label="Max")
                plt.plot(times, kel_min, ls='--', color='k', label="Min")
                plt.plot(times, kel_target, ls='--', color='red', label="Target")
            if quantity == "ue" and "Tel_target" in self.header:
                T_target = self.header["Tel_target"]*np.ones(len(times))*r_val**(-self.header["Tel_rslope"])
                rho_data = shell_avgs["RHO"][:, r_index]
                gammae = self.header["game"]
                target = rho_data*units['KBOL']*T_target/(units['MP']*(gammae - 1.0)*units['CL']**2)
                plt.plot(times, target, ls='--', color='k')
            if quantity in ["Thetae", "Thetap", "Te", "Tp"] and "Tel_target" in self.header:
                T_target = self.header["Tel_target"]*np.ones(len(times))*r_val**(-self.header["Tel_rslope"])
                if quantity == "Thetae":
                    target = get_thetae(T_target)
                    label_str = r"Target $\theta_e=$" + format_exp(get_thetae(self.header["Tel_target"]))
                elif quantity == "Thetap":
                    target = get_thetap(T_target)
                    label_str = r"Target $\theta_p=$" + format_exp(get_thetap(self.header["Tel_target"]))
                else:
                    target = T_target
                    label_str = r"Target $T_e=$" + format_exp(self.header["Tel_target"])
                label_str += r"$(r/r_g)^{{-{:.1f}}}$ K".format(self.header["Tel_rslope"])
                plt.plot(times, target, color='k', ls='--', label=label_str)
                if quantity == "Tp":
                    print("Tp0 at r={:.2f} is {:.2e} K".format(radius, shell_data[0]))
            if "Ratio" in quantity or quantity == "coulombQuality":
                plt.gca().axhline([1.0], ls='--', color='black')
            if quantity in fix_shell_clims and fix_shell_clims[quantity][0] > 0:
                plt.yscale('log')
            elif quantity in fix_shell_clims and fix_shell_clims[quantity][0] < 0:
                plt.yscale('symlog')
                plt.gca().axhline([0.0], ls='--', color='black')
            elif quantity in get_log_scales() and get_log_scales()[quantity]:
                plt.yscale('log')
            else:
                plt.ylim([0.0, None])
                if "Ratio" in quantity:
                    plt.gca().axhline([1.0], ls='--', color='black')
            plt.xlabel(r"$tc/r_g$")
            # plt.gca().axvline([1600], color='k', ls=':')
            # plt.xlim([0.0, 1000])
            # plt.xscale('log')
            if quantity in self.labels:
                label_str = self.labels[quantity]
            else:
                label_str = quantity
            plt.ylabel(label_str)
            if quantity in fix_shell_clims:
                plt.ylim(fix_shell_clims[quantity])
                figname = "ylim_" + figname
            if smooth_ratios and "Post" in quantity:
                figname = "smooth{:d}_".format(neighbor_smooth) + figname
            figname += ".png"
            plt.legend()
            plt.tight_layout()
            print("Saving figure " + figdir + figname)
            plt.savefig(figdir + figname, bbox_inches='tight')
            plt.close()
        # plt.show()

    def plot_mass_conservation_quantities(self, **kwargs):
        """
        tstart and tend: put in rg/c, NOT dump number.
        """
        print("----------------------------------------------------------")
        print("Plotting mass conservation quantities")
        to_time_average = kwargs.get("time_average_shells", True)
        fix_mass_conservation_clims = kwargs.get("fix_mass_conservation_clims", None)
        tstart_rgc = kwargs.get("tstart_shells", 0.0) # in rg/c
        if to_time_average:
            tend_rgc = kwargs.get("tend_shells", -1)
        else:
            tend_rgc = tstart_rgc
            kwargs["tend_shells"] = tend_rgc

        shell_quantities = ["vrr_bl", "H2", "RHO"]
        integral_quantities = ["mass_flux"]
        q_to_retrieve = get_quantities_to_retrieve(shell_quantities)

        kwargs["shell_quantities"] = q_to_retrieve
        tstart_rgc, tend_rgc, shell_avgs = self.retrieve_shell_averages(**kwargs)
        q_to_retrieve = get_quantities_to_retrieve(integral_quantities)
        kwargs["integral_quantities"] = q_to_retrieve
        tstart_rgc, tend_rgc, integrals = self.retrieve_radial_integrals(**kwargs)

        figdir_base = self.path_to_figures + "mass_conservation_quantities/"

        dump_nums, dump_rgcs = self.load_dump_times("shells")
        dump_nums_int, dump_rgcs_int = self.load_dump_times("integrals")
        if (dump_nums != dump_nums_int).any():
            print("Problem: dump numbers don't match up.")
            print(dump_nums)
            print(dump_nums_int)
        if (dump_rgcs != dump_rgcs_int).any():
            print("Problem: dump rgcs don't match up.")
            print(dump_rgcs)
            print(dump_rgcs_int)
            print("Problem: dump rgcs don't match up. {:d} for shells and {:d} for integrals".formta(dump_nums, dump_nums_int))

        # Indices of tstart and tend
        tstart_ind = (np.abs(tstart_rgc - dump_rgcs)).argmin()
        tend_ind = (np.abs(tend_rgc - dump_rgcs)).argmin()
        # Code values of tstart and tend
        tstart_dump_rgc = dump_rgcs[tstart_ind]
        tend_dump_rgc = dump_rgcs[tend_ind]

        r_vals = np.array(self.r_grid[:, 0, 0])
        EH_ind = (np.abs(self.rEH - r_vals)).argmin()

        if to_time_average:
            tit_str = "Time-averaged from $t={:.2f}$ --- ${:.2f}~GM/c^3$".format(tstart_dump_rgc, tend_dump_rgc)
            figdir = figdir_base + "tAvg{:.0f}-{:.0f}rgc/".format(tstart_dump_rgc, tend_dump_rgc - 1)
            if not os.path.exists(figdir): os.makedirs(figdir)
            figname = "mass_conservation_properties"
        else:
            tit_str = "Snapshot at t={:.2f} $GM/c^3$".format(tstart_dump_rgc)
            figdir = figdir_base + "snapshot/mass_conservation_properties/"
            if not os.path.exists(figdir): os.makedirs(figdir)
            figname = "t{:.0f}rgc".format(tstart_dump_rgc)

        scale_height = np.sqrt(shell_avgs["H2"])
        vr = shell_avgs["vrr_bl"]
        rho = shell_avgs["RHO"]
        mdot = integrals["mass_flux"]

        if to_time_average and scale_height.ndim > 1:
            scale_height = np.mean(scale_height, axis=0)
            vr = np.mean(vr, axis=0)
            rho = np.mean(rho, axis=0)
            mdot = np.mean(mdot, axis=0)
        # Normalize to value at event horizon
        scale_height = scale_height/scale_height[EH_ind]
        vr = vr/vr[EH_ind]
        rho = rho/rho[EH_ind]
        mdot = mdot/mdot[EH_ind]

        calculated_H = mdot/(4.0*np.pi*rho*vr)
        calculated_H = 1./(rho*vr)
        calculated_H = calculated_H/calculated_H[EH_ind]

        # Plot equilibrium radius, req = t|v^r| using earliest time
        vrr_data = np.abs(np.mean(shell_avgs["vrr_bl"], axis=0))
        req_ind_start = (np.abs(vrr_data*tstart_rgc - r_vals)).argmin()
        r_eq = r_vals[req_ind_start]

        plt.figure()
        ax = plt.gca()
        tit_str = "Mass conservation values\n " + tit_str
        if fix_mass_conservation_clims:
            plt.ylim(fix_mass_conservation_clims)
            figname = "ylim_" + figname
        plt.suptitle(tit_str)
        plt.yscale('log')
        plt.xlabel(r"$r/r_g$")
        # plt.xscale('log')
        plt.xlim([self.rEH, np.max([50, r_eq*1.1])])
        plt.xlim([self.rEH, 10.])

        plt.xlabel(r"$r/r_g$")
        plt.ylabel(r"$f(r)/f(r_{\rm EH})$")
        plt.plot(r_vals, calculated_H, color='black', ls='--', label=r"$H=1/(\langle v^r\rangle\langle\rho\rangle)$")
        plt.plot(r_vals, scale_height, color='black', label=r"$H$")
        plt.plot(r_vals, 1.0/vr, label=r"$1/v^r$")
        plt.plot(r_vals, 1.0/rho, label=r"$1/\rho$")
        # plt.plot(r_vals, rho, label=r"$\rho$")
        plt.plot(r_vals, mdot, label=r"$\dot M$")

        ax.axhline([1.0], color='black', ls=':')
        ax.axvline([r_eq], color='red', ls=':')
        ax.axvline([self.rISCO], color='black', ls=':')
        plt.legend(ncol=3)

        plt.tight_layout()

        figname += ".png"
        print("Saving figure " + figdir + figname)
        plt.savefig(figdir + figname, bbox_inches='tight')
        plt.close()

    def plot_vertical_integrals_midplane(self, **kwargs):
        """
        tstart and tend: put in rg/c, NOT dump number.
        Plot optical depth and compton y parameters.
        Difference from shell averages is you want the value at the midplane.
        This is literally just a line-out of the vertical slices.
        """
        print("----------------------------------------------------------")
        print("Plotting vertical integrals' value at the midplane")
        phi_ind = kwargs.get("phi_ind", "Avg")
        azimuthal_average = False
        if phi_ind == "Avg":
            azimuthal_average = True
        fix_vert_integral_clims = kwargs.get("fix_vert_integral_clims", {})
        tstart_rgc = kwargs.get("tstart_vert_integrals_midplane", 0.0) # in rg/c
        tend_rgc = kwargs.get("tend_vert_integrals_midplane", tstart_rgc)
        to_time_average = False
        if tend_rgc != tstart_rgc:
            to_time_average = True
        # Load dump_times file
        dump_nums, dump_rgcs = self.load_dump_times("slices")
        # Take last value in dump_times file
        if tend_rgc == -1:
            tend_rgc = dump_rgcs[-1]
        if tend_rgc > dump_rgcs[-1]:
            print("ERROR: requested end time {:.2f} rg/c is greater than the last known time.".format(tend_rgc))
            print("You might want to re-reduce data.")
            print("Setting tend_rgc = last known time ({:.2f} rg/c) for now...".format(dump_rgcs[-1]))
            tend_rgc = dump_rgcs[-1]
        if tstart_rgc > tend_rgc:
            print("tstart was greater than tend. Resetting to tstart = tend - 50 rg/c.")
            tstart_rgc = tend_rgc - 50.
        # Indices of tstart and tend
        tstart_ind = (np.abs(tstart_rgc - dump_rgcs)).argmin()
        tend_ind = (np.abs(tend_rgc - dump_rgcs)).argmin()
        # Code values of tstart and tend
        tstart_dump_rgc = dump_rgcs[tstart_ind]
        tend_dump_rgc = dump_rgcs[tend_ind]
        kwargs["tstart_slices"] = tstart_dump_rgc
        kwargs["tend_slices"] = tend_dump_rgc
        print(tstart_dump_rgc)
        print(tend_dump_rgc)
        figdir_base = self.path_to_figures + "vertical_integrals_midplane/"
        if azimuthal_average:
            figdir_base += "azimuthally_averaged/"
        else:
            figdir_base += "slice_phi{:d}/".format(phi_ind)
        if to_time_average:
            figdir = figdir_base + "tAvg{:.0f}-{:.0f}/".format(tstart, tend)
        else:
            figdir_base += "snapshot/"

        optical_depth_data = self.check_for_time_averaged_slice("optical_depth", **kwargs)
        Thetae_data = self.check_for_time_averaged_slice("Thetae", **kwargs)

        quantities = kwargs.get("vert_integral_quantities_to_plot", get_list_of_vert_integral_quantities())
        r_vals = np.array(self.r_grid[:, 0, 0])
        midplane_ind = int(optical_depth_data.shape[1]/2)
        tau_data = optical_depth_data[:, midplane_ind]
        max_tau = np.where(tau_data > 1., tau_data**2, tau_data)
        thetae_vals = Thetae_data[:, midplane_ind]

        for quantity in quantities:
            if to_time_average:
                tit_str = "Time-averaged from $t={:.2f}$ --- ${:.2f}~GM/c^3$".format(tstart_dump_rgc, tend_dump_rgc)
                figdir = figdir_base + "tAvg{:.0f}-{:.0f}rgc/".format(tstart_dump_rgc, tend_dump_rgc - 1)
                if not os.path.exists(figdir): os.makedirs(figdir)
                figname = quantity
            else:
                tit_str = "Snapshot at t={:.2f} $GM/c^3$".format(tstart_dump_rgc)
                figdir = figdir_base + quantity + "/"
                if not os.path.exists(figdir): os.makedirs(figdir)
                figname = quantity + "_t{:.0f}rgc".format(tstart_dump_rgc)

            if quantity == "optical_depth":
                # Take midplane values
                integral_data = tau_data
            elif "compton" in quantity:
                if quantity == "compton_yR":
                    integral_data = 16.0*thetae_vals**2.*max_tau
                elif quantity == "compton_yNR":
                    integral_data = 4.0*thetae_vals*max_tau

            if integral_data is None:
                continue

            plt.figure()
            plt.title(tit_str)
            plt.plot(r_vals, integral_data)
            plt.axhline([1.0], color='k', ls='--')

            if quantity in get_log_scales() and get_log_scales()[quantity]:
                plt.yscale('log')
            elif quantity in fix_vert_integral_clims and fix_vert_integral_clims[quantity][0] > 0:
                plt.yscale('log')
            else:
                plt.ylim([0.0, None])
            plt.xlabel(r"$r/r_g$")
            plt.xscale('log')
            # plt.xlim([None, 10])
            # plt.xlim([self.rEH, 50])
            plt.xlim([self.rEH, None])
            plt.gca().axvline([self.rISCO], color='black', ls=':')
            if quantity in self.labels:
                label_str = self.labels[quantity]
            else:
                label_str = quantity
            plt.ylabel(label_str)
            if quantity in fix_vert_integral_clims:
                plt.ylim(fix_vert_integral_clims[quantity])
                figname = "ylim_" + figname
            figname += ".png"
            if plt.gca().get_ylim()[0] < 0:
                plt.gca().axhline([0.0], color='black', ls='--')
            plt.tight_layout()
            print("Saving figure " + figdir + figname)
            plt.savefig(figdir + figname, bbox_inches='tight')
            plt.close()

    def plot_integrals(self, **kwargs):
        """
        tstart and tend: put in rg/c, NOT dump number.
        """
        print("----------------------------------------------------------")
        print("Plotting radial integrals")
        to_time_average = kwargs.get("time_average_integrals", True)
        fix_integral_clims = kwargs.get("fix_integral_clims", {})
        tstart_rgc = kwargs.get("tstart_integrals", 0.0) # in rg/c
        if to_time_average:
            tend_rgc = kwargs.get("tend_integrals", -1)
        else:
            tend_rgc = tstart_rgc
            kwargs["tend_integrals"] = tend_rgc

        quantities = kwargs.get("integral_quantities_to_plot", get_list_of_integral_quantities())
        q_to_retrieve = get_quantities_to_retrieve(quantities)
        kwargs["integral_quantities"] = q_to_retrieve
        tstart_rgc, tend_rgc, integrals = self.retrieve_radial_integrals(**kwargs)
        figdir_base = self.path_to_figures + "radial_integrals/"

        dump_nums, dump_rgcs = self.load_dump_times("integrals")

        # Indices of tstart and tend
        tstart_ind = (np.abs(tstart_rgc - dump_rgcs)).argmin()
        tend_ind = (np.abs(tend_rgc - dump_rgcs)).argmin()
        # Code values of tstart and tend
        tstart_dump_rgc = dump_rgcs[tstart_ind]
        tend_dump_rgc = dump_rgcs[tend_ind]

        r_vals = np.array(self.r_grid[:, 0, 0])

        for quantity in quantities:
            if to_time_average:
                tit_str = "Time-averaged from $t={:.2f}$ --- ${:.2f}~GM/c^3$".format(tstart_dump_rgc, tend_dump_rgc)
                figdir = figdir_base + "tAvg{:.0f}-{:.0f}rgc/".format(tstart_dump_rgc, tend_dump_rgc - 1)
                if not os.path.exists(figdir): os.makedirs(figdir)
                figname = quantity
            else:
                tit_str = "Snapshot at t={:.2f} $GM/c^3$".format(tstart_dump_rgc)
                figdir = figdir_base + "snapshot/" + quantity + "/"
                if not os.path.exists(figdir): os.makedirs(figdir)
                figname = quantity + "_t{:.0f}rgc".format(tstart_dump_rgc)

            if quantity in integrals:
                if quantity == "magnetic_flux":
                    integral_array = integrals[quantity]/np.sqrt(integrals["mass_flux"])
                else:
                    integral_array = integrals[quantity]
                if to_time_average and integral_array.ndim > 1:
                    integral_data = np.mean(integral_array, axis=0)
                else:
                    integral_data = integral_array

            else:
                continue
            # Skip integral data is None. Error message output in calculate_reduced_quantity
            if integral_data is None:
                continue

            plt.figure()
            plt.title(tit_str)
            plt.plot(r_vals, integral_data)

            if quantity in get_log_scales() and get_log_scales()[quantity]:
                plt.yscale('log')
            elif quantity in fix_integral_clims and fix_integral_clims[quantity][0] > 0:
                plt.yscale('log')
            else:
                plt.ylim([0.0, None])
            plt.xlabel(r"$r/r_g$")
            plt.xscale('log')
            # plt.xlim([None, 10])
            plt.xlim([self.rEH, 50])
            # plt.xlim([self.rEH, None])
            plt.gca().axvline([self.rISCO], color='black', ls=':')
            if quantity in self.labels:
                label_str = self.labels[quantity]
            else:
                label_str = quantity
            plt.ylabel(label_str)
            if quantity in fix_integral_clims:
                plt.ylim(fix_integral_clims[quantity])
                figname = "ylim_" + figname
            figname += ".png"
            if plt.gca().get_ylim()[0] < 0:
                plt.gca().axhline([0.0], color='black', ls='--')
            plt.tight_layout()
            print("Saving figure " + figdir + figname)
            plt.savefig(figdir + figname, bbox_inches='tight')
            plt.close()

    def plot_diag_quantities_over_time(self, **kwargs):
        print("----------------------------------------------------------")
        print("Plotting diagonal quantities over time.")
        diag = io.load_diag(self.path_to_reduced_data, self.header)
        quantities = kwargs.get("diag_quantities_to_plot", ["mdot_eh"])
        fix_diag_clims = kwargs.get("fix_diag_clims", {})
        for q in quantities:
            figname = q
            if q not in diag:
                print("Quantity " + q + " not in diag; removing...")
                quantities.remove(q)
            else:
                diag_data = self.retrieve_diag_quantity(diag, q)
                if q not in diag:
                    if q == "visc_efficiency" and "Qvisc" in diag:
                        visc_data = self.retrieve_diag_quantity(diag, "Qvisc")
                        mdot_data = self.retrieve_diag_quantity(diag, "mdot_eh", False)
                        diag_data = visc_data/(mdot_data)
                    elif q == "cool_efficiency" and "Qcool" in diag:
                        cool_data = self.retrieve_diag_quantity(diag, "Qcool")
                        mdot_data = self.retrieve_diag_quantity(diag, "mdot_eh", False)
                        diag_data = cool_data/(mdot_data)
                    else:
                        print("Quantity " + q + " not in diag.")
                        continue
                plt.figure()
                plt.plot(diag['t'], diag_data)
                if q in fix_diag_clims:
                    figname = "ylim_" + figname
                    plt.ylim(fix_diag_clims[q])
                plt.xlabel(r"$tc/r_g$")
                plt.ylabel(self.labels[q])
                if plt.gca().get_ylim()[0] > 0:
                    plt.yscale('log')
                # plt.xlim([7000, None])
                plt.tight_layout()
                figdir = self.path_to_figures + "1d_over_time/"
                if not os.path.exists(figdir): os.makedirs(figdir)
                figname += ".png"
                print("Saving figure " + figdir + figname)
                plt.savefig(figdir + figname)
        return

    def plot_vertical_slice(self, **kwargs):
        """
        The one plotting function in the postprocessor, since slices
        are too big to transfer over to laptop.

        TODO: rewrite to mesh with azimuthally-averaged slices.
        """
        phi_ind = kwargs.get("phi_ind", "Avg")
        azimuthal_average = False
        if phi_ind == "Avg":
            azimuthal_average = True
        kwargs["phi_ind"] = phi_ind
        tstart_rgc = kwargs.get("tstart_slices", 0)
        tend_rgc = kwargs.get("tend_slices", tstart_rgc)
        cbar = kwargs.get("cbar_slices", True)
        label = kwargs.get("cbar_label_slices", None)
        ticks = kwargs.get("ticks_slices", None)
        fix_slice_clims = kwargs.get("fix_slice_clims", {})
        cmaps = kwargs.get('cmaps', {})

        to_time_average = False
        if tend_rgc != tstart_rgc:
            to_time_average = True

        # Load dump_times file
        dump_nums, dump_rgcs = self.load_dump_times("slices")
        # Take last value in dump_times file
        if tend_rgc == -1:
            tend_rgc = dump_rgcs[-1]
        if tend_rgc > dump_rgcs[-1]:
            print("ERROR: requested end time {:.2f} rg/c is greater than the last known time.".format(tend_rgc))
            print("You might want to re-reduce data.")
            print("Setting tend_rgc = last known time ({:.2f} rg/c) for now...".format(dump_rgcs[-1]))
            tend_rgc = dump_rgcs[-1]
        if tstart_rgc > tend_rgc:
            print("tstart was greater than tend. Resetting to tstart = tend - 50 rg/c.")
            tstart_rgc = tend_rgc - 50.

        # Indices of tstart and tend
        tstart_ind = (np.abs(tstart_rgc - dump_rgcs)).argmin()
        tend_ind = (np.abs(tend_rgc - dump_rgcs)).argmin()
        # Code values of tstart and tend
        tstart_dump_rgc = dump_rgcs[tstart_ind]
        tend_dump_rgc = dump_rgcs[tend_ind]
        print("tstart/tend: ({:.0f}, {:.0f}) rg/c, dump numbers: ({:d}, {:d})".format(tstart_dump_rgc, tend_dump_rgc, int(dump_nums[tstart_ind]), int(dump_nums[tend_ind])))

        quantities = kwargs.get("slice_quantities_to_plot", self.list_of_standard_quantities)
        quantities = clean_reduced_quantities_list(quantities, self.header)
        q_to_retrieve = get_quantities_to_retrieve(quantities)
        if "sigma" not in q_to_retrieve:
            q_to_retrieve.append("sigma")
        if "RHO" not in q_to_retrieve:
            q_to_retrieve.append("RHO")
        if "beta" not in q_to_retrieve:
            q_to_retrieve.append("beta")
        if "surface_density" not in q_to_retrieve:
            q_to_retrieve.append("surface_density")
        if "Thetae" not in q_to_retrieve:
            q_to_retrieve.append("Thetae")
        kwargs["slice_quantities"] = q_to_retrieve

        if self.xyz_geom is None:
            self.load_xyz_geom()

        sigma_data = self.check_for_time_averaged_slice("sigma", **kwargs)
        rho_data = self.check_for_time_averaged_slice("RHO", **kwargs)
        beta_data = self.check_for_time_averaged_slice("beta", **kwargs)
        # surface_density_data = self.check_for_time_averaged_slice("surface_density", **kwargs)
        if self.header["COOLING"]:
            optical_depth_data = self.check_for_time_averaged_slice("optical_depth", **kwargs)
        Thetae_data = self.check_for_time_averaged_slice("Thetae", **kwargs)
        if sigma_data.ndim > 2:
            sigma_data = flatten_xz(sigma_data, self.header, azi_avg=azimuthal_average)
            rho_data = flatten_xz(rho_data, self.header, azi_avg=azimuthal_average)
            beta_data = flatten_xz(beta_data, self.header, azi_avg=azimuthal_average)
        elif sigma_data.ndim == 3:
            sigma_data = sigma_data[:, :, 0]
            rho_data = rho_data[:, :, 0]
            beta_data = beta_data[:, :, 0]

        for quantity in quantities:
            if quantity in cmaps:
                cmap = cmaps[quantity]
            else:
                cmap = "viridis"
            if quantity in fix_slice_clims:
                vmin, vmax = fix_slice_clims[quantity]
            else:
                vmin = None; vmax = None

            if quantity in get_list_of_vert_integral_quantities():
                if quantity == "surface_density":
                    slice_data = surface_density_data
                elif quantity == "optical_depth" and self.header["COOLING"]:
                    # Convert to cgs
                    # slice_data = surface_density_data*(self.header["M_unit"]/self.header["L_unit"]**2)*0.4
                    slice_data = optical_depth_data
                elif quantity == "compton_yNR" and self.header["COOLING"]:
                    # tau_data = surface_density_data*(self.header["M_unit"]/self.header["L_unit"]**2)*0.4
                    max_tau = np.where(optical_depth_data > 1., optical_depth_data**2, optical_depth_data)
                    slice_data = 4.0*Thetae_data*max_tau
                elif quantity == "compton_yR" and self.header["COOLING"]:
                    # tau_data = surface_density_data*(self.header["M_unit"]/self.header["L_unit"]**2)*0.4
                    max_tau = np.where(optical_depth_data > 1., optical_depth_data**2, optical_depth_data)
                    slice_data = 16.0*Thetae_data**2*max_tau
            else:
                # Time averaging is included in retrieve function
                print("Checking for " + quantity)
                slice_data = self.check_for_time_averaged_slice(quantity, **kwargs)
            # slice_data = self.retrieve_quantity_slice(quantity, **kwargs)

            print("Plotting " + quantity)
            plt.figure()
            ax = plt.gca()

            max_val = np.nanmax(slice_data)
            min_val = np.nanmin(slice_data)
            if (np.isnan(slice_data).all()):
                print("Encountered all slice nan for ts = {:.0f}, te = {:.0f}".format(tstart_dump_rgc, tend_dump_rgc) + " quantity: " + quantity)
                plt.close()
                continue
            else:
                print("Min/max value for " + quantity + " (not azi-averaged): {:.3e}; {:.3e}".format(min_val, max_val))
                print("Min magnitude value for " + quantity + " (not azi-averaged): {:.3e}".format(np.nanmin(np.abs(slice_data))))
                print(slice_data.shape)
                min_inds = np.unravel_index(np.argmin(slice_data), slice_data.shape)
                max_inds = np.unravel_index(np.argmax(slice_data), slice_data.shape)
                print(min_inds)
                print(max_inds)

            # Mask Qcool. Array in invalid where mask is true.
            if quantity == "Qcool":
                slice_data = np.ma.masked_array(slice_data, mask=(slice_data<2.e-30))
            # elif quantity == "Te":
                # slice_data = np.ma.masked_array(slice_data, mask=(slice_data>1.e9))
            # elif quantity == "Tp":
                # slice_data = np.ma.masked_array(slice_data, mask=(slice_data>1.e9))
            # elif quantity == "TpTe":
                # slice_data = np.ma.masked_array(slice_data, mask=(slice_data<1.))
            elif quantity == "Qcoul":
                slice_data = np.ma.masked_array(slice_data, mask=(slice_data<0.))
            elif quantity == "Qvisc_e":
                slice_data = np.ma.masked_array(slice_data, mask=(slice_data<0.))
            # elif quantity == "coulombQuality":
                # slice_data = np.abs(slice_data)

            rcyl, z, mesh, sigma_colors = plot_xz(ax, self.xyz_geom, slice_data, self.header, vmin=vmin, vmax=vmax, xlim=None, ylim=None, name=quantity, cmap=cmap, azi_avg=azimuthal_average)
            # plt.contour(rcyl, z, sigma_data, levels=np.array([1.0]), colors=sigma_colors, linewidths=0.5)
            # plt.contour(rcyl, z, beta_data, levels=np.array([1.0]), colors=sigma_colors, linewidths=0.5, linestyles='dotted')
            # plt.contour(rcyl, z, beta_data, levels=np.array([1.0]), colors='k', linewidths=0.5, linestyles='dotted')
            # plt.contour(rcyl, z, rho_data, levels=np.array([1.0, 10.]), colors=sigma_colors, linewidths=0.5, linestyles='dashed')

            # Convert to cgs
            # tau_data = surface_density_data*(self.header["M_unit"]/self.header["L_unit"]**2)*0.4
            # plt.contour(rcyl, z, tau_data, levels=np.array([1.0]), colors='k', linewidths=0.5, linestyles='solid')
            if self.header["COOLING"]:
                plt.contour(rcyl, z, optical_depth_data, levels=np.array([1.0]), colors='k', linewidths=0.5, linestyles='solid')
            if cbar:
                from mpl_toolkits.axes_grid1 import make_axes_locatable
                divider = make_axes_locatable(ax)
                cax = divider.append_axes("right", size="5%", pad=0.05)
                if label:
                    plt.colorbar(mesh, cax=cax, label=label, ticks=ticks)
                else:
                    plt.colorbar(mesh, cax=cax, ticks=ticks)
            plt.ylabel(self.labels[quantity])
            # Run through the slice views and save
            slice_views = get_slice_view_lims()
            for slice_view in slice_views:
                xlim, ylim = slice_views[slice_view]
                ax.set_xlim([0, xlim])
                if ylim is not None:
                    ax.set_ylim([-ylim, ylim])
                figdir_base = self.path_to_figures + "vertical_slices/" + slice_view + "_view/"
                if azimuthal_average:
                    figdir_base += "azimuthally_averaged/"
                else:
                    figdir_base += "slice_phi{:d}/".format(phi_ind)
                if to_time_average:
                    figdir = figdir_base + "tAvg{:.0f}-{:.0f}/".format(tstart, tend)
                else:
                    figdir_base += "snapshot/"

                if not to_time_average:
                    figdir = figdir_base + quantity + "/"
                if not os.path.exists(figdir): os.makedirs(figdir)
                if to_time_average:
                    tit_str = "Time-averaged from t={:.2f} - {:.2f} $GM/c^3$\n".format(tstart_rgc, tend_rgc)
                    if azimuthal_average:
                        tit_str += "Azimuthally-averaged"
                    else:
                        tit_str += "Azimuthal slice at $\phi=0$"
                    figname = quantity + ".png"
                # Snapshot
                else:
                    tit_str = "Snapshot at t={:.2f} $GM/c^3$\n".format(tstart_rgc)
                    figname = quantity + "_t{:.0f}rgc.png".format(tstart_dump_rgc)
                if quantity in fix_slice_clims:
                    figname = "clim_" + figname
                # plt.title(tit_str)
                # ax.set_xlabel(r"$r/r_g$")
                plt.tight_layout()
                print("Saving figure " + figdir + figname)
                plt.savefig(figdir + figname, bbox_inches='tight', dpi=300)
            plt.close()
        # plt.show()


if __name__ == '__main__':
    # setup = "lia_hp"
    setup = "seagate_tests"
    setup = "lia_dell"
    sim_name = "torus_ecool_3DT1e9Mu25_hr_restarted"
    # sim_name = "test_torus_2DT1e9Mu25_Qv1em6_QcLimiter"
    sim_name = "torus_ecool_3DT1e10Mu25_ieCoulomb_Pf-rMu21"
    sim_name = "torus_ecool_3DT1e9Mu25_ieCoulomb_Pf-rMu21-2"
    sim_name = "torus_ecool_3DT1e9Mu25_ieCoulomb_Pf-rMu21"
    sim_name = "torus_ecool_3DT1e9Mu21"
    sim_name = "torus_ecool_3DT1e9Mu25_ieCoulomb_Pf-rMu21-2"
    sim_name = "torus_github_3DMu21_explicit_prasun"
    sim_name = "torus_zgithub_3DMu21_explicit"
    # sim_name = "torus_ecool_3DT1e9Mu27_ieCoulomb_Pf-rMu21"
    # sim_name = "torus_ecool_3DT1e9Mu21_bak"
    # sim_name = "torus_ecool_3DT5e9Mu25_ieCoulomb-rMu21"
    # sim_name = "torus_ecool_3DT1e9Mu25_ieCoulomb-rMu21_newCompiler"
    # sim_name = "torus_ecool_3DT1e9Mu25_ieCoulomb_Pf-rMu21_hr"
    # sim_name = "torus_ecool_3DT1e9Mu21_ieCoulomb_crashTest"

    kwargs = {}
    kwargs["quiet"] = False
    if setup == "lia_lou":
        sim_names = ["test_torus_ecool_3DT1e9Mu21_lowres"]
        for sim_name in sim_names:
            sim_plotter = simulation_plotter(sim_name, setup, **kwargs)
            print("Plotting slices for " + sim_name)
            plot_cluster_slices(sim_plotter)
        exit()
    else:
        plt.style.use('lia.mplstyle')
        sim_plotter = simulation_plotter(sim_name, setup, **kwargs)

    # sim_plotter.clean_times_and_quantities_file("shells")
    # sim_plotter.clean_times_and_quantities_file("slices")
    # -------------------------------------
    # Deal with converting reduced data from previous formats
    # if need be
    # -------------------------------------
    if os.path.exists(sim_plotter.path_to_reduced_data + "/shell_averages/times_and_quantities.p"):
        if not os.path.exists(sim_plotter.path_to_reduced_data + "/shell_averages/times_and_quantities.txt"):
            sim_plotter.convert_times_quantities(**kwargs)
    if os.path.exists(sim_plotter.path_to_reduced_data + "shell_averages.p"):
        if not os.path.exists(sim_plotter.path_to_reduced_data + "/shell_averages/"):
            sim_plotter.convert_shell_avgs_storage()
    # ----------------------------------------------
    # Plot Mdot
    # ----------------------------------------------
    diag_quantities_to_plot = ["mdot_eh", "num_super", "lum_super", "phi"]
    diag_quantities_to_plot = ["mdot_eh", "num_super", "lum_super", "phi", "Qcoul", "Qvisc", "Qcool"]
    # diag_quantities_to_plot = ["phi"]
    kwargs["diag_quantities_to_plot"] = diag_quantities_to_plot
    kwargs["fix_diag_clims"] = {
        "num_super":[1, 1000],
        # "mdot_eh":[1e-8, 1]
        "mdot_eh":[1e-6, 1e2],
        "Qcoul":[1e-6, 1e2],
        "Qcool":[1e-6, 1e2],
    }
    # sim_plotter.plot_diag_quantities_over_time(**kwargs)

    # ----------------------------------------------
    # Plot shell averages
    # ----------------------------------------------
    overwrite_shell_avgs = False
    # overwrite_shell_avgs = True
    time_average_shells = False
    tstart = 9955.0; tend = tstart
    time_average_shells = True
    # tstart = 7000.0 # rg/c
    # tstart = 10000.0 # rg/c
    # tend = 16500.0 # rg/c
    tend = 10000.0 # rg/c
    tstart = 19000.0 # rg/c
    tend = 20000.0 # rg/c
    # tstart = 23000.0 # rg/c
    tend = 25000.0 # rg/c
    shell_quantities_to_plot = [
        # "Tcool",
        # "Thetae", "Thetap",
        # "sigma", "beta",
        # "Te", "Tp", "TpTe",
        # "H_over_R",
        # "Hmean",
        # "H_over_R0",
        # "HT_over_R",
        # "coulCoronaPost",
        # "coolCoronaPost",
        # "coolCoulCoronaRatioPost",
        # "Qcool_corona",
        # "Qcoul_corona",
        # "RHO",
        # "ue", "up", "UU",
        # "optical_depth",
        # "Qcool_opt_thin",
        # "Qcoul_opt_thin",
        # "Qvisc_e_opt_thin",
        # "Qvisc_p_opt_thin",
        # "coolOptThinPost",
        # "coulOptThinPost",
        # "viscEOptThinPost",
        # "viscPOptThinPost",
        # "Qvisc_e", "Qcool", "Qcoul",
        # "coulViscRatioPost",
        # "coolViscRatioPost",
        # "coolTotalRatioPost",
        # "coolCoulRatioPost",
        # "coolViscRatio", "coolTotalRatio",
        # "KEL", "KTOT",
        # "coulombQuality",
        # "tinfall",
        # "tCoulomb_e",
        # "tCoulomb_p",
        # "theat",
        # "theat_e",
        # "theat_p",
        # "tCool_e",
        # "vphir_bl"
        # "vrr", "vrr_bl",
        # "ucov0", "ucov1",
        # "ucon_bl0",
        # "ucon_bl1",
        # "Qmri_theta",
        # "Qmri_phi",
    ]
    # No touchy
    kwargs["overwrite_shell_avgs"] = overwrite_shell_avgs
    kwargs["shell_quantities_to_plot"] = shell_quantities_to_plot
    kwargs["tstart_shells"] = tstart
    kwargs["tend_shells"] = tend
    kwargs["time_average_shells"] = time_average_shells
    kwargs["quiet"] = False

    kwargs["fix_shell_clims"] = {
        # "Thetae":[1e-2, 10.0], "Thetap": [8e-5, 1],
        # # "Qvisc_e":[1e-7, 1e-1],
        # # "Qvisc_e":[-1e-5, 1e-5],
        "Qcool":[1e-5, 1e0],
        "Qcool_corona":[1e-5, 2e-2],
        "Qcoul_corona":[-2e-3, 2e-3],
        "Qcoul_opt_thin":[1e-5, 1e0],
        "Qcool_opt_thin":[1e-5, 1e0],
        # "Qcoul":[1e-9, 1e-6],
        "coolViscRatio":[-1, 1],
        "coolTotalRatio":[-1, 1],
        "coolCoulRatioPost":[1e-4, 1e4],
        "coolOptThinPost":[1e-5, 1.01],
        "coulOptThinPost":[1e-5, 1.01],
        "Te":[5e8, 1e11], "Tp": [5e8, 1e12],
        "H_over_R":[1e-2, 1.0],
        "HT_over_R":[1e-2, 1.0],
        "H_over_R0":[1e-2, 1.0],
        "Hmean":[-1, 1.0],
        "coolCoronaPost":[1.e-3, 1.],
        # "coulCoronaPost":[1.e-3, 1.],
        "coulCoronaPost":[-1.e-1, 1.e-1],
        "coolCoulCoronaRatioPost":[1.e-2, 1.e2],
        "ue":[1e-4, 1e0],
        "up":[1e-6, 1e0],
        "UU":[1e-4, 1e0],
        # "KEL":[1e-6, 1e-3],
        # "KTOT":[1e-5, 1e-1],
        "vrr_bl":[1.e-4, 0.1],
        "vrr":[1.e-4, 0.1],
        "vphir_bl":[1.e-3, 1.0],
        "tinfall":[0.01, 100.0],
        "tCoulomb":[1.0, 1.e8],
        "theat":[1.0, 1.e4],
        "timescales":[1.e-2, 1.e8],
        "timescales_infall":[1.e-3, 1.e3],
        "timescales_dynamical":[1.e-1, 1.e4],
        "timescales_none":[1.e-1, 1.e7],
        # "ucon0":[1, 8], "ucon1":[-0.0055, 0.0055],
        # "ucon2":[-0.005, 0.005], "ucon3":[0, 3],
        # "ucov0":[-1, 0], "ucov1":[0., 5],
        # "ucov2":[-0.02, 0.02], "ucov3":[0, 5],
        "Qmri_theta":[1e-1, 1e3],
        "Qmri_phi":[1e-1, 1e3],
        "Tcool":[1e-6, 1e0],
        # "RHO":[1e-1,1e1],
        "mass_conservation":[1.e-3, 1.e3],
        "beta":[1.e-1, 1.e3]
    }
    # kwargs["with_Qcool"] = True
    # print("Plotting time-averaged shell averages")
    # sim_plotter.plot_shell_differences(**kwargs)
    # sim_plotter.plot_shell_averages(**kwargs)
    kwargs["rgcs_to_plot_shells"] = [tstart, tend]
    # sim_plotter.plot_shell_averages_over_time(**kwargs)
    norms = ["infall", "dynamical", "none"]
    species = ["electron", "proton", "gas"]
    print("----------------------------------------------------------")
    print("Plotting shell timescales")
    for norm in norms:
        for specie in species:
            kwargs["normalization"] = norm
            kwargs["species"] = specie
            # sim_plotter.plot_shell_timescales(**kwargs)

    # ----------------------------------------------
    # Plot shells over time
    # ----------------------------------------------
    kwargs["shell_quantities_to_plot"] = [
        "Qcoul",
        # "Qvisc_e",
        "Qcool",
        # "ue", "up", "UU",
        "RHO",
        "Te", "Tp",
        # "KEL", "KTOT",
        "TpTe",
        "H_over_R",
        # "Tcool",
        # "coolViscRatioPost", "coulViscRatio", "coolTotalRatioPost",
        # "coolCoulRatio",
        "coulombQuality",
        "beta",
    ]
    kwargs["fix_shell_clims"] = {
        "Thetae":[1e-2, 10.0], "Thetap": [8e-5, 1],
        # "Qvisc_e":[1e-4, 1e-2],
        "Qvisc_e":[-1e-2, 1e-2],
        # "Qcool":[1e-4, 1e-2],
        # "RHO":[1, 1e2],
        "Qcool":[1e-10, 1e-1],
        "Qcoul":[1e-10, 1e-1],
        "coolViscRatio":[1e-2, 10],
        "coolCoulRatio":[1e-4, 1e2],
        "coolTotalRatio":[1e-2, 10],
        "Te":[5e8, 1e11],
        "Tp": [1e9, 1e12],
        "TpTe":[0.9, 1e3],
        "Tcool":[1e-6, 10.],
        "KEL":[1e-5,1e-3],
        "H_over_R":[1e-2, 1.0],
        "ue":[1e-5,1e-3]
    }
    # kwargs["tstart_shells"] = 0
    # kwargs["tend_shells"] = -1
    rISCO = sim_plotter.rISCO
    rEH = sim_plotter.rEH + 0.1
    radii_to_plot = [10.0, 5.0, rISCO, rEH]
    # for r in radii_to_plot:
        # sim_plotter.plot_shell_over_time(r, **kwargs)

    # ------------------------------------------------
    # Plot vertical integrals: optical depth/compton y
    # ------------------------------------------------
    kwargs["tstart_vert_integrals_midplane"] = 24000
    kwargs["tend_vert_integrals_midplane"] = 25000
    kwargs["quiet"] = False

    kwargs["fix_vert_integral_clims"] = {
        "compton_yNR":[1.e-5, 1.e5],
        "compton_yR":[1.e-5, 1.e5],
        "optical_depth":[1.e-5, 1.e5],
    }
    # sim_plotter.plot_vertical_integrals_midplane(**kwargs)
    # ------------------------------------------------
    # tstart = 50
    # tend = 100
    kwargs["tstart_integrals"] = tstart
    kwargs["tend_integrals"] = tend
    kwargs["time_average_integrals"] = True
    kwargs["quiet"] = False

    kwargs["fix_integral_clims"] = {
    }
    # sim_plotter.plot_integrals(**kwargs)
    kwargs["fix_mass_conservation_clims"] = [1.e-1, 1.e2]
    # sim_plotter.plot_mass_conservation_quantities(**kwargs)

    # ------------------------------------------------
    # Plot slices
    # ------------------------------------------------
    # tstart = 9950
    # tend = 9990
    # tstart = 14000
    # tend = 15000
    kwargs["slice_quantities_to_plot"] = [
        "RHO",
        "bsq",
        "divb",
        "sigma",
        # "B1",
        # "B2",
        # "coulombQuality",
        # "Te",
        # "Tp",
        # "TpTe",
        # "coulViscRatioPost",
        # "coolViscRatioPost",
        # "coolCoulRatioPost",
        # "Qcool",
        # "Qcoul",
        # "Qcool_opt_thin",
        # "Qcoul_opt_thin",
        # "Qcool_corona",
        # "Qcoul_corona",
        # "Qvisc_e",
        # "beta",
        # "surface_density",
        # "optical_depth",
        # "compton_yNR",
        # "compton_yR",
        # "coolCoulRatio",
        # "coulViscRatio",
        # "coolViscRatio",
    ]
    kwargs["cmaps"] = {
        "TpTe":"RdBu_r",
        # "TpTe":"Reds",
        "Te":"RdBu_r",
        "beta":"PuOr_r",
        "coolCoulRatio":"RdBu_r",
        "coulViscRatio":"RdBu_r",
        "coolViscRatio":"RdBu_r",
        "coulombQuality":"RdBu_r",
        "optical_depth":"RdGy_r",
        "compton_yNR":"RdGy_r",
        "compton_yR":"RdGy_r",
    }
    fix_slice_clims = {
        # "RHO":[1.e-2, 1000],
        # "RHO":[1.e-2, 10],
        "RHO":[1.e-2, 1.e4],
        "sigma":[1.e-30, 1.e2],
        "divb":[1.e-20, 1.e0],
        "beta":[1.e-2, 1.e2],
        "Te":[1.e8, 1.e10],
        "Tp":[1.e9, 1.e12],
        "TpTe":[sim_plotter.header["tptemin"], sim_plotter.header["tptemax"]],
        # "TpTe":[1.0, 1.e3],
        "Qcool":[1e-7, 1.e-4],
        # "Qcool":[1e-2, 1.e2],
        # "Qcool":[1e-9, 1.e-7],
        "Qcool_opt_thin":[1e-7, 1.e-4],
        # "Qcoul":[1e-7, 1.e-4],
        "Qcoul":[1e-9, 1.e-7],
        # "Qcoul_opt_thin":[1e-7, 1.e-4],
        "Qcoul_opt_thin":[1e-9, 1.e-7],
        "Qcool_corona":[1e-7, 1.e-4],
        "Qcoul_corona":[1e-7, 1.e-4],
        "Qvisc_e":[1e-7, 1.e-4],
        "coolCoulRatio":[1e-3, 1.e3],
        "coulViscRatio":[1e-3, 1.e3],
        "coolViscRatio":[1e-3, 1.e3],
        "coulombQuality":[-1.e5, 1e5],
        "surface_density":[1e0, 1e5],
        "optical_depth":[1e-4, 1e4],
        "compton_yNR":[1e-4, 1e4],
        "compton_yR":[1e-4, 1e4],
    }
    tstart = 13000
    tend = 15000
    tstart = 23000
    tend = 25000
    # tstart = 9900
    # tend = 9995
    kwargs["tstart_slices"] = tstart
    kwargs["tend_slices"] = tend
    kwargs["phi_ind"] = 0
    # kwargs["phi_ind"] = "Avg"
    kwargs["fix_slice_clims"] = fix_slice_clims
    # print("Plotting vertical slice time-averaged")
    # sim_plotter.plot_vertical_slice(**kwargs)
    dump_nums, dump_rgcs = sim_plotter.load_dump_times("slices")
    dump_rgcs = np.sort(dump_rgcs)
    print(dump_rgcs.shape)
    times_rgc = dump_rgcs
    # times_rgc = dump_rgcs[::-1]
    # times_rgc = (dump_rgcs[-100:-40])[::-1]
    # dump_ind = np.where(dump_nums == 1990)[0][0]
    # times_rgc = [dump_rgcs[dump_ind]]
    # print(times_rgc)
    print("Plotting vertical slice snapshots")
    # ---- Snapshots ------------
    for tstart_rgc in times_rgc:
        kwargs["tstart_slices"] = tstart_rgc
        kwargs["tend_slices"] = tstart_rgc
        kwargs["fix_slice_clims"] = fix_slice_clims
        sim_plotter.plot_vertical_slice(**kwargs)
        # kwargs["fix_slice_clims"] = {}
        # sim_plotter.plot_vertical_slice(**kwargs)
