import numpy as np
import os
import h5py
import glob
from scipy.interpolate import interp1d
import pickle
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import sys;
sys.path.append('modules/')
from plotting_utils import *
from ebhlight_tools import *
import hdf5_to_dict as io
import units
units = units.get_cgs()
from analytic_solution import *

class cartesian_analyzer:
    def __init__(self, sim_name, setup="seagate_tests", **kwargs):
        overwrite_self = kwargs.get("overwrite_self", False)

        # ------------------------------
        #      Default file paths here
        # ------------------------------
        if setup == "seagate_tests":
            path_to_raw_data = "/mnt/d/ebhlight_ecool_tests/raw_data/" + sim_name + "/dumps/"
            path_to_reduced_data = "/mnt/d/ebhlight_ecool_tests/reduced_data/" + sim_name + "/"
            path_to_figures = "/mnt/d/ebhlight_ecool_tests/figures/" + sim_name + "/"
            path_to_pickled_self = path_to_reduced_data + "postprocessed.p"
        elif setup == "lia_hp":
            path_to_raw_data = "/mnt/c/Users/liaha/research/projects/implementing_ebhlight_ecool/raw_data/" + sim_name + "/dumps/"
            path_to_reduced_data = "/mnt/c/Users/liaha/research/projects/implementing_ebhlight_ecool/reduced_data/" + sim_name + "/"
            path_to_figures = "/mnt/c/Users/liaha/research/projects/implementing_ebhlight_ecool/figures/" + sim_name + "/"
            path_to_pickled_self = path_to_reduced_data + "postprocessed.p"
        # ------------------------------
        # If paths are non-standard, set them here using kwargs
        # Otherwise, use the defaults from above
        # ------------------------------
        self.path_to_raw_data = kwargs.get("path_to_raw_data", path_to_raw_data)
        self.path_to_reduced_data = kwargs.get("path_to_reduced_data", path_to_reduced_data)
        self.path_to_figures = kwargs.get("path_to_figures", path_to_figures)
        self.path_to_pickled_self = kwargs.get("path_to_pickled_self", path_to_pickled_self)

        print(self.path_to_reduced_data)
        # ------------------------------
        # Find whether we need to reload reduced data
        # ------------------------------
        need_to_initialize = True
        if os.path.exists(self.path_to_pickled_self) and (not overwrite_self):
            print("Loading simulation analyzer from pickle file.")
            success = self._load_from_pickle()
            if not success:
                print("Something went wrong with loading pickle file.")
            else:
                print("Done.")
            need_to_initialize = not success

        # ------------------------------
        # Initialization process
        # ------------------------------
        if need_to_initialize:
            self._initialize(**kwargs)

        # ------------------------------
        # Get updated list of quantities
        # ------------------------------
        self.get_list_of_quantities()
        self.get_labels()

        return

    def _initialize(self, **kwargs):
        print("Initializing...")
        self.get_list_of_quantities()
        self.get_labels()
        # Check for path existence
        if not os.path.exists(self.path_to_raw_data):
            print("Warning: the path to raw data does not exist. " + self.path_to_raw_data)
        if not os.path.exists(self.path_to_reduced_data):
            os.makedirs(self.path_to_reduced_data)
        if not os.path.exists(self.path_to_figures):
            os.makedirs(self.path_to_figures)

        # TODO: get list of dumps requires having raw data around
        # Load various information that doesn't change.
        file0 = kwargs.get("file0", self.get_list_of_dumps(**kwargs)[0])
        self.header = io.load_hdr(file0)
        self.geom = io.load_geom(self.header)
        self.gdet = self.geom['gdet']
        self.has_radiation = self.header['RADIATION']
        self.has_electrons = self.header['ELECTRONS']
        self.has_cooling = self.header['COOLING']
        # -----------------------------------
        # Dump to file
        # -----------------------------------
        with open(self.path_to_pickled_self, 'wb') as fh:
            pickle.dump(self, fh)

    def _load_from_pickle(self):
        try:
            with open(self.path_to_pickled_self, 'rb') as fh:
                self.__dict__.update(pickle.load(fh).__dict__)
                file0 = kwargs.get("file0", self.get_list_of_dumps(**kwargs)[0])
                self.header = io.load_hdr(file0)
                self.get_list_of_quantities()
                return True
        except:
            return False

    def get_list_of_quantities(self):
        self.list_of_standard_quantities = get_list_of_standard_quantities()
        self.list_of_raw_calculated_quantities = get_list_of_raw_calculated_quantities()
        self.list_of_reduced_calculated_quantities = get_list_of_reduced_calculated_quantities()
        self.list_of_error_quantities = [
            "Te", "Tp", "Qcoul", "Thetae", "Thetap"
        ]
        return

    def get_labels(self):
        self.labels = get_list_of_labels()
        return

    def get_list_of_dumps(self, **kwargs):
        raw_data_path = kwargs.get("path_to_raw_data", self.path_to_raw_data)
        files = sorted(glob.glob(raw_data_path + "dump*.h5"))
        return files

    def retrieve_vol_averages(self, **kwargs):
        """
        Get density-weighted volume averages.
        See e.g. Dexter+ 2021 Eq. 1.

        Do NOT pass quantities calculated after the vol average is already done
        (current example, coulViscRatioPost)
        """
        overwrite_vol_avgs = kwargs.get("overwrite_vol_avgs", False)
        quantities = kwargs.get("vol_quantities", self.list_of_raw_calculated_quantities)
        vol_avg_path = self.path_to_reduced_data + "vol_averages.p"
        vol_avg_path = kwargs.get("vol_avg_path", vol_avg_path)
        tstart = kwargs.get("tstart_vols", 0)
        tend = kwargs.get("tend_vols", -1)
        if tend == -1:
            trange = [tstart, tend]
        else:
            trange = [*range(tstart, tend)]

        quantities_to_calculate = []
        times_to_calculate = {}
        if not os.path.exists(vol_avg_path) or overwrite_vol_avgs:
            for quantity in quantities:
                if quantity in self.list_of_standard_quantities or quantity in self.list_of_raw_calculated_quantities:
                    quantities_to_calculate.append(quantity)
                    times_to_calculate[quantity] = trange
                elif quantity not in self.list_of_reduced_calculated_quantities:
                    print("Unknown quantity " + quantity)
        else:
            # Load existing data.
            data = pickle.load(open(vol_avg_path, 'rb'))
            # Do all the checking of which times and which quantities are needed.
            for quantity in quantities:
                if quantity not in data.keys() or len(data[quantity].keys()) == 0:
                    if quantity in self.list_of_standard_quantities or quantity in self.list_of_raw_calculated_quantities:
                        quantities_to_calculate.append(quantity)
                        if tend == -1:
                            times_to_calculate[quantity] = [tstart, tend]
                        else:
                            times_to_calculate[quantity] = [*range(tstart, tend)]
                    elif quantity not in self.list_of_reduced_calculated_quantities:
                        print("Unknown quantity " + quantity)
                # quantity is in data.keys, so need to check which times to calculate
                else:
                    if quantity in self.list_of_standard_quantities or quantity in self.list_of_raw_calculated_quantities:
                        times_already_calculated = sorted(data[quantity].keys())
                        latest_time = times_already_calculated[-1]
                        if tend == -1:
                            desired_times = [*range(tstart, latest_time + 1)]
                            to_append = True
                        else:
                            desired_times = [*range(tstart, tend + 1)]
                            to_append = False
                        qtimes_to_calculate = list(set(desired_times).difference(times_already_calculated))
                        if to_append:
                            qtimes_to_calculate.append(-1)

                        if qtimes_to_calculate:
                            times_to_calculate[quantity] = qtimes_to_calculate
                            quantities_to_calculate.append(quantity)
                    else:
                        print("Unknown quantity " + quantity)

        if quantities_to_calculate:
            self.calculate_vol_averages(quantities_to_calculate, times_to_calculate, **kwargs)

        return pickle.load(open(vol_avg_path, 'rb'))

    def calculate_vol_averages(self, quantities_to_calculate, times_to_calculate, **kwargs):
        """
        times_to_calculate is a dictionary with quantites to calculate as keys. Each element
        contains an array of times to be calculated for that quantity. If there is a -1
        appended to that array, will do all the times from the last listed to the end.
        """
        # NOTE: must have access to raw data to call this function.
        vol_avg_path = self.path_to_reduced_data + "vol_averages.p"
        vol_avg_path = kwargs.get("vol_avg_path", vol_avg_path)
        print(vol_avg_path)
        overwrite_vol_avgs = kwargs.get("overwrite_vol_avgs", False)
        tstart = kwargs.get("tstart_vols", 0)
        files = self.get_list_of_dumps(**kwargs)

        # Initialize arrays if need be, open existing file
        if (not os.path.exists(vol_avg_path)) or overwrite_vol_avgs:
            vol_avgs = {}
            vol_avgs["times"] = {}
        else:
            vol_avgs = pickle.load(open(vol_avg_path, 'rb'))

        # Need to do all this fanciness in this function because the retrieve
        # function will not necessarily have access to raw data
        tend = len(files)
        all_times_needed = []
        for quantity in times_to_calculate:
            # Check for -1 in times_to_calculate array, add on full times
            # if necessary
            if times_to_calculate[quantity][-1] == -1:
                if quantity in vol_avgs:
                    times_already_calculated = sorted(vol_avgs[quantity].keys())
                    latest_time = times_already_calculated[-1]
                    # qtimes: to calculate, not involving -1
                    qtimes = times_to_calculate[quantity][:-1]
                    if latest_time != tend - 1:
                        t_to_add = [*range(times_already_calculated[-1], tend)]
                    else:
                        t_to_add = []
                    times_to_calculate[quantity] = qtimes + t_to_add
                else:
                    times_to_calculate[quantity] = [*range(tstart, tend)]

            times_to_add = list(set(times_to_calculate[quantity]).difference(all_times_needed))
            all_times_needed = all_times_needed + times_to_add

        for quantity in quantities_to_calculate:
            if quantity not in vol_avgs:
                if quantity in self.list_of_standard_quantities or quantity in self.list_of_raw_calculated_quantities:
                    vol_avgs[quantity] = {}
                else:
                    print("Unknown quantity " + quantity)

        # Loop through and compute.
        files_to_calculate = np.array(files)[all_times_needed]
        for t, f in zip(all_times_needed, files_to_calculate):
            print("Computing vol averages for dump {:d}".format(t))
            # print(all_times_needed)
            dfile = io.load_dump(f, geom=self.geom)
            rho = dfile['RHO']
            bsq = dfile['bsq']
            # Take only locations where magnetization is less than 30.
            # sigma_cut = bsq/rho < 30.0
            # Take only locations where magnetization is less than 1.
            sigma_cut = bsq/rho < 1.0
            # Density-weighted.
            weight = rho*sigma_cut*self.gdet
            # Denominator of all vol averages: density in each vol.
            normalization = np.sum(np.sum(np.sum(weight, axis=-1), axis=-1), axis=-1)

            if t not in vol_avgs["times"]:
                vol_avgs["times"][t] = dfile['t']
            # Calculate vol-averages
            for quantity in quantities_to_calculate:
                if t not in times_to_calculate[quantity]:
                    continue
                if quantity in self.list_of_standard_quantities:
                    data = dfile[quantity]
                # case by case calculation.
                else:
                    if quantity == "coulViscRatio":
                        data = dfile["Qcoul"]/dfile["Qvisc_e"]
                    elif quantity == "coolViscRatio":
                        data = dfile["Qcool"]/dfile["Qvisc_e"]
                    elif quantity == "coolCoulRatio":
                        data = dfile["Qcool"]/dfile["Qcoul"]
                    elif quantity == "coolTotalRatio":
                        data = dfile["Qcool"]/(dfile["Qvisc_e"] + dfile["Qcoul"])
                    else:
                        print(quantity + " calculation not implemented yet.")
                        data = None
                if data is not None:
                    vol_avg_at_t = np.sum(np.sum(np.sum(weight*data, axis=-1), axis=-1), axis=-1)/normalization
                    vol_avgs[quantity][t] = vol_avg_at_t
            if t%50 == 0:
                pickle.dump(vol_avgs, open(vol_avg_path, 'wb'))
        pickle.dump(vol_avgs, open(vol_avg_path, 'wb'))
        return

    def plot_vol_error(self, **kwargs):
        tstart = kwargs.get("tstart_vols", 0)
        tend = kwargs.get("tend_vols", -1)
        analytic_solution = kwargs.get("analytic_solution", None)
        if analytic_solution is None:
            print("Whoops need to add something to get analytic solution.")
            print("Exiting...")
            return

        quantities = kwargs.get("error_quantities_to_plot", self.list_of_standard_quantities)
        q_to_retrieve = quantities.copy()
        for q in quantities:
            if q not in self.list_of_error_quantities:
                q_to_retrieve.remove(q)

        kwargs["vol_quantities"] = q_to_retrieve
        vol_avgs = self.retrieve_vol_averages(**kwargs)
        vol_data, vol_times = vol_dict_to_array(vol_avgs, q_to_retrieve[0], tstart, tend)
        figdir_base = self.path_to_figures + "vol_averages/"

        analytic_solution.set_tf_code(vol_times[-1])
        (with_cooling, without_cooling) = analytic_solution.get_analytic_solution()
        Te_analytic = with_cooling.y[0]
        Tp_analytic = with_cooling.y[1]
        times = with_cooling.t
        thetae_analytic = Te_analytic*units['KBOL']/(units['CL']**2*units['ME'])
        thetap_analytic = Tp_analytic*units['KBOL']/(units['CL']**2*units['MP'])
        times_in_cgs = times*analytic_solver.L_unit/units['CL']

        for quantity in quantities:
            if quantity in vol_avgs:
                if tend == -1:
                    tend = sorted(vol_avgs[quantity].keys())[-1]
                figdir = figdir_base + "t{:d}-{:d}/".format(tstart, tend)
                if not os.path.exists(figdir): os.makedirs(figdir)
                vol_data, vol_times = vol_dict_to_array(vol_avgs, quantity, tstart, tend)
                figname = "error_" + quantity + ".png"
            else:
                print("Can't plot quantity " + quantity)

            # Get the error
            if quantity == "Te":
                interp_f = interp1d(times, Te_analytic)
                Te_values = interp_f(vol_times)
                error = np.abs(vol_data - Te_values)/Te_values
            elif quantity == "Tp":
                interp_f = interp1d(times, Tp_analytic)
                Tp_values = interp_f(vol_times)
                error = np.abs(vol_data - Tp_values)/Tp_values
            elif quantity == "Thetae":
                interp_f = interp1d(times, thetae_analytic)
                thetae_values = interp_f(vol_times)
                error = np.abs(vol_data - thetae_values)/thetae_values
            elif quantity == "Thetap":
                interp_f = interp1d(times, thetap_analytic)
                thetap_values = interp_f(vol_times)
                error = np.abs(vol_data - thetap_values)/thetap_values
            elif quantity == "Qcoul":
                qC = []
                for Te, Tp, t in zip(Te_analytic, Tp_analytic, times):
                    qC.append(analytic_solution.coulombHeating(Te, Tp, t))
                coulomb = np.array(qC)
                # Convert to code units
                coulomb = coulomb*analytic_solution.T_unit/analytic_solution.U_unit
                interp_f = interp1d(times, coulomb)
                coulomb_values = interp_f(vol_times)
                error = np.abs(vol_data - coulomb_values)/coulomb_values

            plt.figure()
            # plt.plot(vol_times, vol_data, marker='o')
            plt.plot(vol_times, error)

            plt.yscale('log')
            plt.ylim([1e-6, 1e-1])
            # if (error > 0.0).all():
                # plt.yscale('log')
            plt.xlabel(r"$tc/L$")
            plt.xscale('log')
            plt.xlim([vol_times[1], vol_times[-1]])
            # plt.xlim([None, 10])
            # plt.xlim([None, 50])
            # plt.gca().relim()
            # plt.gca().autoscale_view(); plt.draw()
            label_str = "Error in "
            if quantity in self.labels:
                label_str += self.labels[quantity]
            else:
                label_str += quantity
            plt.ylabel(label_str)
            plt.title(r"$t_{{\rm cool}}=$" + format_exp(analytic_solution.tcool_code) + " $L/c$")
            # plt.tight_layout()
            plt.savefig(figdir + figname, bbox_inches='tight')
            plt.close()
        # plt.show()

    def plot_Te_Tp(self, **kwargs):
        tstart = kwargs.get("tstart_vols", 0)
        tend = kwargs.get("tend_vols", -1)
        analytic_solution = kwargs.get("analytic_solution", None)
        marker = 'o'

        kwargs2 = kwargs.copy()
        kwargs2["vol_quantities"] = ["Thetae", "Thetap"]

        vol_avgs = self.retrieve_vol_averages(**kwargs2)
        figdir_base = self.path_to_figures + "vol_averages/"
        figname = "compare_Te_Tp"
        if analytic_solution is None:
            print("Specify analytic solution! Exiting...")
            # return
        if tend == -1:
            tend = sorted(vol_avgs["Thetae"].keys())[-1]

        # Set up analytic solver
        time_keys = sorted(vol_avgs["times"].keys())
        new_time = vol_avgs["times"][time_keys[-1]]
        if analytic_solution is not None:
            analytic_solution.set_tf_code(new_time)
            (with_cooling, without_cooling) = analytic_solution.get_analytic_solution()
            Te_analytic = with_cooling.y[0]
            Tp_analytic = with_cooling.y[1]
            times = with_cooling.t

        figdir = figdir_base + "t{:d}-{:d}/".format(tstart, tend)
        if not os.path.exists(figdir): os.makedirs(figdir)
        Thetae_data, vol_times = vol_dict_to_array(vol_avgs, "Thetae", tstart, tend)
        Thetap_data, vol_times = vol_dict_to_array(vol_avgs, "Thetap", tstart, tend)
        Te_data = get_Te(Thetae_data)
        Tp_data = get_Tp(Thetap_data)

        plt.figure()
        markevery = 0.05
        markevery = None
        ls = 'none'

        line1, = plt.plot(vol_times, Te_data, ls=ls, marker=marker, color="C0", label=r"Electrons", fillstyle='none', markevery=markevery)
        line2, = plt.plot(vol_times, Tp_data, ls=ls, marker=marker, color="C1", label=r"Protons", fillstyle='none', markevery=markevery)

        line1 = mlines.Line2D([], [], color="C0", ls='-', label=r"Electrons")
        line2 = mlines.Line2D([], [], color="C1", ls='-', label=r"Protons")
        first_legend = plt.legend(handles=[line1, line2])
        plt.gca().add_artist(first_legend)
        if analytic_solution is not None:
            plt.plot(times, Te_analytic, color="C0", ls='-')
            plt.plot(times, Tp_analytic, color="C1", ls='-')

        dashed_line = mlines.Line2D([], [], color="k", ls='-', label=r"Analytic")
        solid_line = mlines.Line2D([], [], color="k", ls='none', marker=marker, label=r"$\texttt{ebhlight}$", fillstyle='none')
        if self.has_cooling:
            T_target = self.header["Tel_target"]*np.array(vol_times)**0.0
            label_str = r"Target $T_e=$" + format_exp(self.header["Tel_target"]) + " K"
            line3, = plt.plot(vol_times, T_target, color='k', ls=':', label=label_str)
            dotted_line = mlines.Line2D([], [], color="k", ls=':', label=label_str)
            handles = [solid_line, dashed_line, dotted_line]
        else:
            handles = [solid_line, dashed_line]

        # plt.title(r"$t_{{\rm cool}}=$" + format_exp(analytic_solution.tcool_code) + " $L/c$")
        plt.ylabel("Temperature [K]")
        plt.yscale('log')
        plt.xlabel(r"$tc/L$")
        # plt.xscale('log')
        # plt.ylim([analytic_solution.Te0, analytic_solution.Tp0*1.1])
        # plt.xlim([vol_times[1], vol_times[-1]])
        plt.xlim([vol_times[0], vol_times[-1]])
        # plt.xlim([0, 50])
        plt.gca().legend(handles=handles, ncol=3,
                         bbox_to_anchor=(-0.05, 1.02, 1., 1.02), loc="lower center")
        # plt.tight_layout()
        plt.savefig(figdir + figname + ".png", bbox_inches='tight')
        plt.close()
        # plt.show()
        return

    def plot_vol_averages(self, **kwargs):
        tstart = kwargs.get("tstart_vols", 0)
        tend = kwargs.get("tend_vols", -1)
        analytic_solution = kwargs.get("analytic_solution", None)
        fix_vol_clims = kwargs.get("fix_vol_clims", {})
        tight = kwargs.get("tight", False)
        marker = None
        marker = "o"
        if tight:
            marker = "o"
        markevery = 1
        # markevery = 5

        quantities = kwargs.get("vol_quantities_to_plot", self.list_of_standard_quantities)
        q_to_retrieve = get_quantities_to_retrieve(quantities)

        kwargs["vol_quantities"] = q_to_retrieve
        vol_avgs = self.retrieve_vol_averages(**kwargs)
        figdir_base = self.path_to_figures + "vol_averages/"
        if analytic_solution and ("Te" in quantities or "Tp" in quantities or "Qcoul" in quantities):
            time_keys = sorted(vol_avgs["times"].keys())
            new_time = vol_avgs["times"][time_keys[-1]]
            analytic_solver.set_tf_code(new_time)
            (with_cooling, without_cooling) = analytic_solver.get_analytic_solution()
            Te_analytic = with_cooling.y[0]
            Tp_analytic = with_cooling.y[1]
            times = with_cooling.t

        for quantity in quantities:
            if quantity in vol_avgs or quantity in ["Te", "Tp"]:
                if quantity == "Te": q = "Thetae"
                elif quantity == "Tp": q = "Thetap"
                else: q = quantity
                if tend == -1:
                    tend = sorted(vol_avgs[q].keys())[-1]
                figdir = figdir_base + "t{:d}-{:d}/".format(tstart, tend)
                if not os.path.exists(figdir): os.makedirs(figdir)
                if quantity in vol_avgs:
                    vol_data, vol_times = vol_dict_to_array(vol_avgs, quantity, tstart, tend)
                elif quantity in ["Tp", "Te"]:
                    if quantity == "Te":
                        vol_data, vol_times = vol_dict_to_array(vol_avgs, "Thetae", tstart, tend)
                        vol_data = get_Te(np.array(vol_data))
                    else:
                        vol_data, vol_times = vol_dict_to_array(vol_avgs, "Thetap", tstart, tend)
                        vol_data = get_Tp(np.array(vol_data))

                figname = quantity
            else:
                print("Can't plot quantity " + quantity)

            # if quantity == "Qcoul": vol_data = -np.array(vol_data)
            tit_str = "Density-weighted vol averages\n "
            plt.figure()
            ax = plt.gca()
            plt.title(tit_str)
            vol_times = np.array(vol_times)
            tStart_ind = (np.abs(vol_times - 15000.0)).argmin()
            tEnd_ind = (np.abs(vol_times - 16000.0)).argmin()
            # tEnd_ind = (np.abs(vol_times - 10700.0)).argmin()
            tEnd_ind = -2
            tStart_ind = -200
            vol_data = np.array(vol_data)
            # if quantity == "Qcool":
                # vol_data = vol_data/2.
            print(quantity)
            # print(vol_times[-1])
            print(vol_data[:20])
            print(vol_data[-20:])
            # print("{:.3e}".format(np.max(vol_data[tStart_ind:tEnd_ind+1])))
            # print("{:.3e}".format(np.min(vol_data[tStart_ind:tEnd_ind+1])))
            plt.plot(vol_times, vol_data, marker=marker, label=r"$\texttt{ebhlight}$", markevery=markevery)
            if quantity == "Thetae" and "Tel_target" in self.header:
                # NOTE: only works for a slope of 0!!
                T_target = self.header["Tel_target"]*np.array(vol_times)**0.0
                theta_target = units['KBOL']*T_target/(units['CL']**2*units['ME'])
                label_str = r"Target $\theta_e={:.2f}$".format(theta_target[0])
                plt.plot(vol_times, theta_target, color='k', ls='--', label=label_str)
                plt.legend()
            if (quantity == "Te" or quantity == "Tp") and "Tel_target" in self.header:
                # NOTE: only works for a slope of 0!!
                T_target = self.header["Tel_target"]*np.array(vol_times)**0.0
                label_str = r"Target $T_e=$" + format_exp(self.header["Tel_target"]) + " K"
                plt.plot(vol_times, T_target, color='k', ls='--', label=label_str)
                plt.legend()
            if quantity == "ue" and "Tel_target" in self.header:
                T_target = self.header["Tel_target"]*np.array(vol_times)**0.0
                rho_data, rho_times = vol_dict_to_array(vol_avgs, "RHO", tstart, tend)
                rho_data = np.array(rho_data)
                gammae = self.header["game"]
                target = rho_data*units['KBOL']*T_target/(units['MP']*(gammae - 1.0)*units['CL']**2)
                label_str = r"Target $u_e$"
                plt.plot(vol_times, target, color='k', ls='--', label=label_str)
                plt.legend()
            if "Ratio" in quantity or quantity == "TpTe":
                plt.gca().axhline([1.0], ls='--', color='black')
            if quantity in get_log_scales() and get_log_scales()[quantity]:
                if quantity not in fix_vol_clims or fix_vol_clims[quantity][0] > 0:
                    plt.yscale('log')
                else:
                    plt.yscale('symlog')
                    plt.gca().axhline([0.0], ls='--', color='black')
                # plt.ylim([0.15, 0.19])
                # plt.ylim([0.9e9, 1.1e9])
                # plt.ylim([1e-1, 10])
                # plt.ylim([1e-2, 10])
            else:
                plt.ylim([0.0, None])
            # plt.xscale('log')
            # plt.xlim([7500, None])
            # plt.xlim([5000, None])
            # plt.xlim([9000, 10000])
            # plt.xlim([16000, 15000])
            # plt.xlim([1e2, None])
            # plt.xlim([50, 100])
            plt.xlabel(r"$tc/L$")
            if quantity in self.labels:
                label_str = self.labels[quantity]
            else:
                label_str = quantity
            plt.ylabel(label_str)

            # Add analytic solution on top
            if analytic_solution:
                if quantity == "Te":
                    print("Plotting Te analytic")
                    plt.plot(times, Te_analytic, ls=':', color='black', label="Analytic", marker=marker)
                elif quantity == "Tp":
                    print("Plotting Tp analytic")
                    plt.plot(times, Tp_analytic, ls=':', color='black', label="Analytic", marker=marker)
                elif quantity == "Qcoul":
                    print("Plotting Qcoul analytic")
                    qC = []
                    for Te, Tp, t in zip(Te_analytic, Tp_analytic, times):
                        qC.append(analytic_solver.coulombHeating(Te, Tp, t))
                    coulomb = np.array(qC)
                    # Convert to code units
                    coulomb = coulomb*analytic_solver.T_unit/analytic_solver.U_unit
                    # plt.plot(times, coulomb, ls='-', color='black', label="Analytic", marker='o')
                    plt.plot(times, coulomb, ls=':', color='black', label="Analytic", marker=marker)
                    plt.ylim([1e-13, None])
                # def LC_to_s(t):
                    # return t*analytic_solver.L_unit/units['CL']
                # def s_to_LC(t):
                    # return t*units['CL']/analytic_solver.L_unit
                # plt.gca().tick_params(axis="x", top=False)
                # secax = plt.gca().secondary_xaxis('top', functions=(LC_to_s, s_to_LC))
                # secax.set_xlabel(r"$t$ [s]")

                if tight:
                    ax.set_xlim([vol_times[1], 15.5])
                    figname += "_tight"
                # else:
                    # plt.xscale('log')
                    # ax.set_xlim([vol_times[1], vol_times[-1]])

            # plt.xlim([0, 100])
            if quantity in fix_vol_clims:
                plt.ylim(fix_vol_clims[quantity])
                figname = "ylim_" + figname
            plt.legend()
            # plt.gca().set_xlim(196.0, 208.0)
            # plt.tight_layout()
            print("Saving figure " + figdir + figname + ".png")
            plt.savefig(figdir + figname + ".png", bbox_inches='tight')
            plt.close()
        # plt.show()

    def plot_vertical_slice(self, **kwargs):
        to_time_average = kwargs.get("time_average_vslice", True)
        tstart = kwargs.get("tstart_slices", 0)
        fix_slice_clims = kwargs.get("fix_slice_clims", {})
        if to_time_average:
            tend = kwargs.get("tend_slices", -1)

        figdir_base = self.path_to_figures + "vertical_slices/"
        if to_time_average:
            figdir = figdir_base + "tAvg{:d}-{:d}/".format(tstart, tend)
        else:
            figdir_base += "snapshot/"
        quantities = kwargs.get("slice_quantities_to_plot", self.list_of_standard_quantities)

        files = self.get_list_of_dumps(**kwargs)
        times = []
        for quantity in quantities:
            if not to_time_average:
                figdir = figdir_base + quantity + "/"
            if not os.path.exists(figdir): os.makedirs(figdir)
            if to_time_average:
                slice_data = None
                for i, f in enumerate(files[tstart:tend]):
                    # print((dfile['Thetae'] == dfile["Tel_test"]).all())
                    dfile = io.load_dump(f, geom=self.geom)
                    times.append(dfile['t'])
                    if quantity in self.list_of_standard_quantities:
                        if slice_data is None:
                            slice_data = dfile[quantity]
                        else:
                            slice_data += dfile[quantity]
                    else:
                        print("Not implemented yet.")
                slice_data = slice_data/(i + 1)
                tit_str = "Time-averaged from t={:.2f} - {:.2f} $GM/c^3$".format(times[0], times[-1])
                figname = quantity + ".png"
            else:
                dfile = io.load_dump(files[tstart], geom=self.geom)
                if quantity in self.list_of_standard_quantities:
                    slice_data = dfile[quantity]
                    if quantity == "Thetae":
                        slice_data = np.ma.masked_where(dfile["sigma"]>1.0, slice_data)
                else:
                    print("Not implemented yet.")
                tit_str = "Snapshot at t={:.2f} $GM/c^3$".format(dfile['t'])
                figname = quantity + "_t{:d}.png".format(tstart)

            plt.figure()
            ax = plt.gca()
            plt.title(tit_str)
            if quantity in fix_slice_clims:
                vmin, vmax = fix_slice_clims[quantity]
                figname = "clim_" + figname
            else:
                vmin = None; vmax = None

            print("t = {:.2f} ".format(dfile['t']))
            print("Min/max value for " + quantity + ": {:.3e}; {:.3e}".format(slice_data.min(), slice_data.max()))
            plot_X1X2(ax, self.geom, slice_data, 0, vmin=vmin, vmax=vmax)
            # plt.xscale('log')
            # plt.xlim([None, 10])
            # if self.log_scales[quantity]:
                # plt.yscale('log')
            plt.ylabel(self.labels[quantity])
            ax.set_xlabel(r"$r/r_g$")
            plt.tight_layout()
            plt.savefig(figdir + figname)
            plt.close()
        # plt.show()

    def plot_radial_profile(self, **kwargs):
        # NOT density-weighted.
        to_time_average = kwargs.get("time_average_rprofile", False)
        to_azi_average = kwargs.get("azi_average_rprofile", False)
        if not to_azi_average:
            azi_ind = kwargs.get("azi_index", 0)
        tstart = kwargs.get("tstart_slices", 0)
        fix_slice_clims = kwargs.get("fix_slice_clims", {})
        if to_time_average:
            tend = kwargs.get("tend_slices", -1)
        index = kwargs.get("rprofile_index", "midplane")

        figdir_base = self.path_to_figures + "radial_profile/" + str(index) + "/"
        if to_time_average:
            figdir = figdir_base + "tAvg{:d}-{:d}/".format(tstart, tend)
        else:
            figdir_base += "snapshot/"
        if to_azi_average:
            figdir_base += "azimuthal_average/"
        quantities = kwargs.get("rprofile_quantities_to_plot", self.list_of_standard_quantities)

        files = self.get_list_of_dumps(**kwargs)
        times = []
        for quantity in quantities:
            if not to_time_average:
                figdir = figdir_base + quantity + "/"
            if not os.path.exists(figdir): os.makedirs(figdir)
            if to_time_average:
                profile_data = None
                for i, f in enumerate(files[tstart:tend]):
                    # print((dfile['Thetae'] == dfile["Tel_test"]).all())
                    dfile = io.load_dump(f, geom=self.geom)
                    times.append(dfile['t'])
                    if quantity in self.list_of_standard_quantities:
                        if index == "midplane":
                            ind = int(np.shape(dfile[quantity])[1]/2)
                        else:
                            ind = int(index)
                        data = dfile[quantity][:, ind, :]
                        if to_azi_average:
                            data = np.mean(data, axis=2)
                        else:
                            data = data[:, :, azi_ind]
                        # Do the average
                        if profile_data is None:
                            profile_data = data
                        else:
                            profile_data += data
                    else:
                        print("Not implemented yet.")
                profile_data = profile_data/(i + 1)
            else:
                dfile = io.load_dump(files[tstart], geom=self.geom)
                if index == "midplane":
                    ind = int(np.shape(dfile[quantity])[1]/2)
                else:
                    ind = int(index)
                profile_data = dfile[quantity][:, ind, :]
                if len(profile_data.shape) > 2:
                    if to_azi_average:
                        profile_data = np.mean(profile_data, axis=2)
                    else:
                        profile_data = profile_data[:, :, azi_ind]
                tit_str = "Snapshot at t={:.2f} $GM/c^3$\n".format(dfile['t'])
            tit_str += "NOT weighted radial profile, "
            if not to_azi_average:
                tit_str += "NOT "
            tit_str += "azimuthally-averaged"
            figname = quantity + "_t{:d}.png".format(tstart)

            plt.figure()
            ax = plt.gca()
            plt.title(tit_str)
            if quantity in fix_slice_clims:
                vmin, vmax = fix_slice_clims[quantity]
                figname = "clim_" + figname
            else:
                vmin = None; vmax = None

            print("t = {:.2f} ".format(dfile['t']))
            # plt.xscale('log')
            # plt.xlim([None, 10])
            # if self.log_scales[quantity]:
                # plt.yscale('log')
            plt.plot(self.geom['X1'][:, 0, 0], profile_data)
            if quantity == "Thetae" and "Tel_target" in self.header:
                T_target = self.header["Tel_target"]*self.geom['X1'][:, 0, 0]**(-self.header["Tel_rslope"])
                theta_target = units['KBOL']* T_target/(units['CL']**2*units['ME'])

                label_str = r"Target $T_e=$" + format_exp(self.header["Tel_target"])
                label_str += r"$r^{{-{:.1f}}}$".format(self.header["Tel_rslope"])
                # plt.plot(self.geom['X1'][:, 0, 0], theta_target, color='k', ls='--', label=label_str)
                # plt.legend()
            plt.ylabel(self.labels[quantity])
            ax.set_xlabel(r"$r/r_g$")
            plt.tight_layout()
            plt.savefig(figdir + figname)
            plt.close()
        # plt.show()

    def plot_diag_quantities_over_time(self, **kwargs):
        print("----------------------------------------------------------")
        print("Plotting diagonal quantities over time.")
        diag = io.load_diag(self.path_to_raw_data, self.header)
        quantities = kwargs.get("diag_quantities_to_plot", ["mdot_eh"])
        fix_diag_clims = kwargs.get("fix_diag_clims", {})
        # self.save_properties()
        # self.load_properties()
        for q in quantities:
            print(q)
            figname = q
            if q not in diag:
                print("Quantity " + q + " not in diag; removing...")
            else:
                diag_data = self.retrieve_diag_quantity(diag, q)
                ind = diag_data.argmin()
                t200_ind = (np.abs(diag['t'] - 200.0)).argmin()
                # print(diag_data[t200_ind - 1])
                # print(diag_data[t200_ind])
                # print(diag_data[t200_ind + 1])
                tStart_ind = (np.abs(diag['t'] - 2500.0)).argmin()
                tEnd_ind = (np.abs(diag['t'] - 2600.0)).argmin()
                print(diag_data[tStart_ind:tEnd_ind+1].max())
                print(diag_data[tStart_ind:tEnd_ind+1].min())
                plt.figure()
                plt.plot(diag['t'], diag_data, marker='o')
                if q in fix_diag_clims:
                    figname = "ylim_" + figname
                    plt.ylim(fix_diag_clims[q])
                plt.xlabel(r"$tc/r_g$")
                plt.ylabel(self.labels[q])
                # plt.xlim([197, 206])
                # plt.xlim([4500, 5000])
                if plt.gca().get_ylim()[0] > 0:
                    plt.yscale('log')
                plt.tight_layout()
                figdir = self.path_to_figures + "1d_over_time/"
                if not os.path.exists(figdir): os.makedirs(figdir)
                figname += ".png"
                print("Saving figure " + figdir + figname)
                plt.savefig(figdir + figname)
        return

    def retrieve_diag_quantity(self, diag, q):
        """
        Convert diagonal quantities.
        """
        if "mdot" in q:
            # Mbh is already in cgs. mbh is mass in solar units.
            edd_factor = 0.1
            try:
                Mass_g = self.header["Mbh"]
            except:
                self.set_mbh(1.e8)
                Mass_g = self.header["Mbh"]
            Ledd = 4.*np.pi*units["GNEWT"]*units["MP"]*units["CL"]*Mass_g/units["THOMSON"]
            MdotEdd = edd_factor*Ledd/(units["CL"]**2)
            Mdot_cgs = -diag[q]*self.header["M_unit"]/self.header["T_unit"]
            return Mdot_cgs/MdotEdd
        elif 'mdot' in q:
            return -diag[q]
        # else:
            # print("Diagonal quantity " + q + " not implemented yet")
        return diag[q]


def vol_dict_to_array(vol_avgs, quantity, tstart, tend):
    vol_array = [vol_avgs[quantity][tstart]]
    vol_times = [vol_avgs["times"][tstart]]
    for t in [*range(tstart + 1, tend)]:
        new_row = vol_avgs[quantity][t]
        new_time = vol_avgs["times"][t]
        vol_array.append(new_row)
        vol_times.append(new_time)
    return (vol_array, vol_times)

if __name__ == '__main__':
    # setup = "lia_hp"
    setup = "seagate_tests"
    sim_name = "gas_box_template"
    sim_name = "test_gas_box_Te1e8Tp1e10tc1e2_n1e2"
    # sim_name = "test_gas_box_Lceiling2_Qvisc1e0"
    # sim_name = "test_gas_box_Lceiling2_rhoHigh_Qvisc1e0"
    sim_name = "test_gas_box_rhoBreak"
    sim_name = "test_gas_box_isothermalE"
    # sim_name = "test_gas_box_QcoulFloor"
    sim_name = "test_gas_box_noViscLowDensity_implicit"
    sim_name = "test_gas_box_noViscCoolCoul_singleT_ieUelL"

    kwargs = {}
    kwargs["path_to_raw_data"] = "/mnt/c/Users/liaha/OneDrive/research/projects/ebhlight_code/ebhlight_ecool/prob/" + sim_name + "/dumps/"
    plt.style.use('lia.mplstyle')
    sim_analysis = cartesian_analyzer(sim_name, setup, **kwargs)
    sim_analysis.get_labels()
    sim_analysis.get_list_of_quantities()
    # vol_avgs = sim_analysis.retrieve_vol_averages(**kwargs)

    # ----------------------------------------------
    # Plot vol averages
    # ----------------------------------------------
    overwrite_vol_avgs = False
    # overwrite_vol_avgs = True
    tstart = 0
    tend = -1
    # tend = 950
    # tend = 7250
    # tend = 250
    # tend = 12000
    # tend = 32500
    vol_quantities_to_plot = [
        "Te", "Tp", "TpTe",
        "Qcoul",
        "Qcool",
        # "Qvisc_e",
        # "coolCoulRatio", "coolViscRatio",
        # "coulViscRatio", "coolTotalRatio",
        # "Thetae", "Thetap",
        # "up", "ue", "UU",
        # "KEL",
        # "KTOT",
        # "RHO"
    ]
    kwargs["fix_vol_clims"] = {
        # "Qcool":[1.e-2, 1e1],
        # "Qcool":[1.e-5, 1e3],
        # "Qcoul":[1.e-30, 1.e9],
        # "Qcoul":[1.e-5, 1.e8],
        "Qcoul":[1.e-30, 1.e-2],
        "Qcool":[1.e-30, 1.e8],
        # "Qcoul":[-1e-2, 1e-2],
        # "Qcoul":[-1e9, 1e1],
        "Qvisc_e":[1.e-0, 1.e4],
        # "Te":[9.999e8, 1.0001e9],
        # "Tp":[9.994e8, 1.000e9],
        # "ue":[2754400, 2756000],
        # "coolCoulRatio":[1.e-3, 1.e3],
        "coolCoulRatio":[1.e-1, 1.e1],
        # "coolViscRatio":[0.1, 10],
        # "coolTotalRatio":[0.1, 2.1],
        "coulViscRatio":[-1e2, 1.e5],
    }
    # No touchy
    kwargs["overwrite_vol_avgs"] = overwrite_vol_avgs
    kwargs["vol_quantities_to_plot"] = vol_quantities_to_plot
    kwargs["tstart_vols"] = tstart
    kwargs["tend_vols"] = tend

    # print("Creating analytic solution")
    # analytic_solver = analytic_solution(sim_analysis)
    # analytic_solver.set_tf_code(1e5)
    # kwargs["analytic_solution"] = analytic_solver
    # kwargs["tight"] = True

    sim_analysis.plot_vol_averages(**kwargs)
    # kwargs["error_quantities_to_plot"] = ["Tp", "Te", "Qcoul"]
    # sim_analysis.plot_vol_error(**kwargs)
    sim_analysis.plot_Te_Tp(**kwargs)
    kwargs["diag_quantities_to_plot"] = ["Qcool", "Qvisc", "Qcoul", "num_super", "lum_super"]
    kwargs["fix_diag_clims"] ={
        "Qcool":[1e-5, 2e8],
        # "Qcool":[1e0, 1e3],
        # "Qcool":[1.e5, 1.3e5],
        "Qcoul":[1e-5, 2e8],
        # "Qcoul":[1.e5, 1.3e5],
        # "Qvisc_e":[1e-31, 1e-8]
        # "Qvisc_e":[1e-10, 5e-5]
    }
    # sim_analysis.plot_diag_quantities_over_time(**kwargs)

    exit()
    # ----------------------------------------------
    # Plot slices at basically every time step
    # ----------------------------------------------
    kwargs["slice_quantities_to_plot"] = ["Thetae", "Thetap", "KEL", "RHO", "UU"]
    kwargs["slice_quantities_to_plot"] = ["Qcoul", "Qvisc_e", "Qcool"]
    kwargs["slice_quantities_to_plot"] = ["Thetae", "Thetap"]
    kwargs["slice_quantities_to_plot"] = ["Thetae", "Thetap", "Qcoul", "Qcool", "Qvisc_e"]
    kwargs["tstart_slices"] = 0
    # kwargs["tend_slices"] = -50
    kwargs["time_average_vslice"] = False
    # kwargs["fix_slice_clims"] = {"sigma":[1e-3, 10], "RHO":[1e-9, 1], "beta":[1e-4, 1e2], "Thetae":[1e-5, 10], "Tel_AH":Tel_AH_lims, "Tel_JD":Tel_JD_lims}
    files = sim_analysis.get_list_of_dumps(**kwargs)
    for i in np.arange(len(files))[:]:
        kwargs["tstart_slices"] = i
        sim_analysis.plot_vertical_slice(**kwargs)
