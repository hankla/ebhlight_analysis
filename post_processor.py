import numpy as np
import os
import shutil
import pickle
import h5py as h5
from scipy.integrate import cumtrapz
import sys;
sys.path.append('modules/')
from plotting_utils import *
from ebhlight_tools import *
import hdf5_to_dict as io
from base_simulation_analyzer import base_simulation_analyzer


# The post processor has to have access to the raw data.
# Its job is to reduce all data so that the simulation plotter
# does not need access to the raw data.
# i.e. run post processor on supercomputer and plotter on laptop.
class post_processor(base_simulation_analyzer):
    def __init__(self, sim_name, setup="lia_lou", **kwargs):
        # Inherit all of base simulation analyzer methods
        super().__init__(sim_name, setup, **kwargs)

        # ------------------------------
        # Initialization process
        # ------------------------------
        self._initialize_postprocessor(**kwargs)
        # Copy over some essential files that aren't dumps to reduced data
        # (already checked that path to raw data exists in _initialize)
        # Always copy over diag.out since it updates while simulation runs.
        if os.path.exists(self.path_to_raw_data + "diag.out"):
            shutil.copyfile(self.path_to_raw_data + "diag.out", self.path_to_reduced_data + "diag.out")
        # Don't copy grid.h5 over to reduced data path since it's so huge.
        # if not os.path.exists(self.path_to_reduced_data + "grid.h5"):
            # shutil.copy(self.path_to_raw_data + "grid.h5", self.path_to_reduced_data + "grid.h5")
        return

    def _initialize_postprocessor(self, **kwargs):
        """
        Initialize the postprocessor:
            - get list of quantities
            - get labels
            - make necessary filesystems
            - make properties file if necessary
        """
        if not self.quiet:
            print("----------------------------------------")
            print("Initializing simulation " + self.sim_name)
        self.get_list_of_quantities()
        self.get_labels()
        # Check for path existence, make dirs if necessary
        if not os.path.exists(self.path_to_raw_data):
            print("---------------------------------------")
            print("            ERROR")
            print("The path to raw data does not exist! For the postprocessor,")
            print("this means death. Please find the path: ")
            print(self.path_to_raw_data)
            print("Exiting.....")
            print("---------------------------------------")
            exit()
        if not os.path.exists(self.path_to_reduced_data):
            print("Making reduced data folder: " + self.path_to_reduced_data)
            os.makedirs(self.path_to_reduced_data)
        if not os.path.exists(self.path_to_reduced_data + "shell_averages/"):
            os.makedirs(self.path_to_reduced_data + "shell_averages/")
        if not os.path.exists(self.path_to_reduced_data + "radial_integrals/"):
            os.makedirs(self.path_to_reduced_data + "radial_integrals/")
        if not os.path.exists(self.path_to_reduced_data + "vertical_integrals_midplane/"):
            os.makedirs(self.path_to_reduced_data + "vertical_integrals_midplane/")
        if not os.path.exists(self.path_to_figures):
            print("Making figures folder: " + self.path_to_figures)
            os.makedirs(self.path_to_figures)

        # Load various information that doesn't change. Either load from
        # properties file or create properties file. Always overwrite.
        # Important for runs done on supercomputer; i.e. no access to raw data
        # on laptop.
        if self.setup != "seagate_tests" or not os.path.exists(self.path_to_properties):
            self.save_geom(**kwargs)
            self.save_properties(**kwargs)
        # self.save_geom(**kwargs)
        # self.save_properties(**kwargs)
        self.load_properties()

    def save_geom(self, **kwargs):
        """
        Save the geom file to a separate file, which enables the plotter to
        access relevant information without needing the ginormous grid.h5 file
        or any dump file. Also allows properties file to be smaller.

        Since creating the geom file does require access to raw data,
        it belongs only with the post processor, not the plotter.
        """
        if not os.path.exists(self.path_to_raw_data):
            print("ERROR: geom file has not been made and raw data doesn't exist!")
            print("This means death. Sim name: " + self.sim_name)
            print("Raw data path: " + self.path_to_raw_data)
            print("Exiting...")
            exit()
        else:
            print(self.path_to_raw_data)
            file0 = kwargs.get("file0", io.get_dumps_reduced(self.path_to_raw_data)[0])
            header = io.load_hdr(file0)
            geom = io.load_geom(header, full=True, half=False, grid_path=self.path_to_raw_data + "grid.h5")
            with open(self.path_to_geom, 'wb') as fh:
                pickle.dump(geom, fh)
        return

    def save_properties(self, **kwargs):
        """
        Save the properties file, which enables the plotter to access
        relevant information without needing the ginormous grid.h5 file
        or any dump file.

        Since creating the properties file does require access to raw data,
        it belongs only with the post processor, not the plotter.
        """
        if not os.path.exists(self.path_to_raw_data):
            print("ERROR: properties file has not been made and raw data doesn't exist!")
            print("This means death. Sim name: " + self.sim_name)
            print("Raw data path: " + self.path_to_raw_data)
            print("Exiting...")
            exit()
        else:
            if os.path.exists(self.path_to_properties):
                os.remove(self.path_to_properties)
            print("Saving properties file: " + self.path_to_properties)
            file0 = kwargs.get("file0", io.get_dumps_reduced(self.path_to_raw_data)[0])
            header = io.load_hdr(file0)
            if self.geom is None:
                self.load_geom()
            properties = {}
            properties["header"] = header
            # self.diag = io.load_diag(self.path_to_reduced_data)
            properties["gdet"] = self.geom['gdet']
            # Boyer-Lindquist coordinates (hdf5_to_dict)
            properties["r_grid"] = self.geom['r'] # goes from event horizon to outer domain. r_grid[i, :] gives same values
            properties["theta_grid"] = self.geom['th'] # goes from 0 to 1
            properties["has_radiation"] = header['RADIATION']
            properties["has_electrons"] = header['ELECTRONS']
            properties["has_cooling"] = header['COOLING']
            properties["rEH"] = header["Reh"]
            properties["rISCO"] = header["Risco"]
            with open(self.path_to_properties, 'wb') as fh:
                pickle.dump(properties, fh)
        return

    def calculate_dump_integrals(self, quantities_to_calculate, dump_path, **kwargs):
        """
        quantities_to_calculate is the quantities that are needed.
             Will only calculate the needed quantities, overwrite if kwargs["overwrite"]
        dump_path is the full path to the dump file

        Equivalent to get_ravg function in shipped ebhlight analysis script avg.py
        except for sigmacut.

        NOTE: must have access to raw data to call this function.
        """
        dump_num = get_dump_num_from_path(dump_path)
        if not os.path.exists(dump_path):
            print("Cannot calculate shell averages for dump {:d}".format(dump_num))
            print(quantities_to_calculate)
            print("Path to dump does not exist! " + dump_path)
            print("Exiting...")
            exit()
        else:
            print("Calculating integrals for dump {:d}".format(dump_num))
        integral_path = self.path_to_reduced_data + "radial_integrals/t{:08d}.p".format(dump_num)
        integral_path = kwargs.get("integral_path", integral_path)
        overwrite = kwargs.get("overwrite_radial_integrals", False)

        # Create or load shell averages file for this time step
        if not os.path.exists(integral_path) or overwrite:
            integrals = {}
        else:
            integrals = pickle.load(open(integral_path, 'rb'))

        if self.geom is None:
            self.load_geom()
        # -----------------------------------------
        # Loading and calculating
        # -----------------------------------------
        kwargs["quantities_to_load"] = get_quantities_to_retrieve(quantities_to_calculate)
        dfile = io.load_dump(dump_path, geom=self.geom, **kwargs)

        calculated = False
        # Calculate integrals
        for quantity in quantities_to_calculate:
            if not (overwrite or quantity not in integrals.keys()):
                continue
            if quantity in self.list_of_standard_quantities:
                if quantity in dfile.keys():
                    data = dfile[quantity]
                else:
                    continue
            # case by case calculation.
            elif quantity in self.list_of_raw_calculated_quantities:
                to_calculate = True
                for q in self.list_of_raw_calculated_quantities[quantity]:
                    if q not in dfile.keys():
                        to_calculate = False
                if to_calculate:
                    data = calculate_raw_quantity(quantity, self.header, self.geom, dfile)
                else:
                    continue
            else:
                print(quantity + " not found")
            # -------------------------------------------------
            # NOTE: these are integrals over theta and phi,
            # not weighted averages
            # -------------------------------------
            dx2 = self.geom['X2'][0, 1, 0] - self.geom['X2'][0, 0, 0]
            dx3 = self.geom['X3'][0, 1, 0] - self.geom['X3'][0, 0, 0]
            # Decide whether to average 4d or not
            if data.ndim == 4:
                for k in np.arange(0, 4):
                    qdim = quantity + "{:d}".format(k)
                    quantity_over_r = data[:, :, :, k].sum(-1).sum(-1)*dx2*dx3
                    if qdim not in integrals:
                        integrals[qdim] = {}
                    integrals[qdim] = quantity_over_r
            else:
                calculated = True
                quantity_over_r = data.sum(-1).sum(-1)*dx2*dx3
                integrals[quantity] = quantity_over_r
        if calculated:
            print("Calculated integrals for " + self.sim_name + " dump {:d}".format(dump_num))
        pickle.dump(integrals, open(integral_path, 'wb'))

        # Update meta files
        self.update_dump_times_file(dump_num, dfile['t'], "integrals")
        self.update_times_quantities_file(dump_num, quantities_to_calculate, "integrals")
        return

    def calculate_all_shell_averages(self, quantities_to_calculate, **kwargs):
        """
        Using access to raw data, calculate shell averages for all dumps present.
        """
        list_of_dumps = io.get_dumps_reduced(self.path_to_raw_data)
        for dump_path in list_of_dumps:
            self.calculate_dump_shell_averages(quantities_to_calculate, dump_path, **kwargs)
        return

    def calculate_dump_shell_averages(self, quantities_to_calculate, dump_path, **kwargs):
        """
        quantities_to_calculate is the quantities that are needed.
             Will only calculate the needed quantities, overwrite if kwargs["overwrite"]
        dump_path is the full path to the dump file

        Equivalent to get_ravg function in shipped ebhlight analysis script avg.py
        except for sigmacut.

        NOTE: must have access to raw data to call this function.
        """
        dump_num = get_dump_num_from_path(dump_path)
        if not os.path.exists(dump_path):
            print("Cannot calculate shell averages for dump {:d}".format(dump_num))
            print(quantities_to_calculate)
            print("Path to raw data does not exist! " + self.path_to_raw_data)
            print("Exiting...")
            exit()
        else:
            print("Calculating shell averages for dump {:d}".format(dump_num))
            print(quantities_to_calculate)
        shell_avg_path = self.path_to_reduced_data + "shell_averages/t{:08d}.p".format(dump_num)
        shell_avg_path = kwargs.get("shell_avg_path", shell_avg_path)
        overwrite = kwargs.get("overwrite_shell_avgs", False)

        # Create or load shell averages file for this time step
        if not os.path.exists(shell_avg_path) or overwrite:
            shell_avgs = {}
        else:
            shell_avgs = pickle.load(open(shell_avg_path, 'rb'))


        # -----------------------------------------
        # Loading and calculating
        # -----------------------------------------
        qs_to_load = get_quantities_to_retrieve_raw(quantities_to_calculate)
        qs_to_load = clean_raw_quantities_list(qs_to_load, self.header)
        print("qs to load:")
        print(qs_to_load)
        kwargs["quantities_to_load"] = qs_to_load
        if self.geom is None:
            self.load_geom()
        dfile = io.load_dump(dump_path, geom=self.geom, **kwargs)
        rho = dfile['RHO']
        bsq = dfile['bsq']
        # Take only locations where magnetization is less than 30.
        # sigma_cut = bsq/rho < 30.0
        # Take only locations where magnetization is less than 1.
        sigma_cut = bsq/rho < 1.0
        # Density-weighted.
        weight = rho*sigma_cut*self.gdet
        # Denominator of all shell averages: density in each shell.
        normalization = np.sum(np.sum(weight, axis=-1), axis=-1)
        if (normalization == 0.0).any():
            print("NOTE: there are no cells with sigma < 1.0!")
            print(normalization[:10])

        calculated = False
        qs_in_shell_dump = list(shell_avgs.keys())
        # Calculate shell-averages
        for quantity in quantities_to_calculate:
            # Don't recalculate if already in shell avgs (and overwrite is false)
            if not (overwrite or quantity not in shell_avgs.keys()):
                qs_in_shell_dump.append(quantity)
                continue
            if quantity in self.list_of_standard_quantities:
                try:
                    data = dfile[quantity]
                except:
                    print(quantity + " not in dfile.")
                    continue
            # case by case calculation.
            elif quantity in self.list_of_raw_calculated_quantities:
                to_calculate = True
                for q in self.list_of_raw_calculated_quantities[quantity]:
                    if q not in dfile.keys():
                        to_calculate = False
                        print(q + " not in dfile!")
                if to_calculate:
                    data = calculate_raw_quantity(quantity, self.header, self.geom, dfile)
                else:
                    continue
            else:
                print(quantity + " not found")
            # -------------------------------------
            # Decide whether to average 4d or not
            # -------------------------------------
            if data.ndim == 4:
                for k in np.arange(0, 4):
                    qdim = quantity + "{:d}".format(k)
                    quantity_over_r = np.sum(np.sum(weight*data[:, :, :, k], axis=-1), axis=-1)/normalization
                    if qdim not in shell_avgs:
                        shell_avgs[qdim] = {}
                    shell_avgs[qdim] = quantity_over_r
                    qs_in_shell_dump.append(qdim)
            else:
                calculated = True
                quantity_over_r = np.sum(np.sum(weight*data, axis=-1), axis=-1)/normalization
                shell_avgs[quantity] = quantity_over_r
                qs_in_shell_dump.append(quantity)
        if calculated:
            print("Calculated shell averages for " + self.sim_name + " dump {:d}".format(dump_num))
        pickle.dump(shell_avgs, open(shell_avg_path, 'wb'))

        qs_in_shell_dump = list(sorted(set(qs_in_shell_dump)))
        if sorted(set(qs_in_shell_dump)) != sorted(set(shell_avgs.keys())):
            print("Something went wrong with matching the shell keys to what ")
            print("will be written in times_and_quantities.txt.")
            print(sorted(qs_in_shell_dump))
            print(sorted(set(shell_avgs.keys())))

        # Update meta files
        self.update_dump_times_file(dump_num, dfile['t'], "shells")
        self.update_times_quantities_file(dump_num, qs_in_shell_dump, "shells")
        return

    def calculate_all_slice_data(self, quantities_to_calculate, **kwargs):
        """
        Using access to raw data, calculate azimuthally averaged
        and phi index slices for all dumps present.
        """
        list_of_dumps = io.get_dumps_reduced(self.path_to_raw_data)
        for dump_path in list_of_dumps:
            self.calculate_dump_slice_data(quantities_to_calculate, dump_path, **kwargs)
        return

    def calculate_dump_slice_data(self, quantities, dump_path, **kwargs):
        """
        Calculate the reduced slice data for a given dump
        for specified variables. Save to file.

        Save both the azimuthally-averaged vertical slice and phi
        slices (default index: 0).

        Store in hdf5 groups labelled by quantity, then datasets
        labelled by phi index (or "Avg")
        """
        dump_num = get_dump_num_from_path(dump_path)
        if not os.path.exists(dump_path):
            print("Cannot calculate slice data for dump {:d}".format(dump_num))
            print(quantities_to_calculate)
            print("Path to raw data does not exist! " + self.path_to_raw_data)
            print("Exiting...")
            exit()
        else:
            print("Calculating slice data for dump {:d}".format(dump_num))

        # print("Accessing azimuthally-averaged vertical slice at dump {:d}".format(dump_num))
        overwrite = kwargs.get("overwrite_slice_avgs", False)
        quantities = clean_reduced_quantities_list(quantities, self.header)
        phi_ind = kwargs.get("phi_ind", 0)
        two_dim = False

        slice_path = self.path_to_reduced_data + "reduced_slices/"
        if not os.path.exists(slice_path):
            os.makedirs(slice_path)
        slice_path += "slice_t{:08d}.h5".format(dump_num)

        if self.geom is None:
            self.load_geom()
        # -----------------------------------------
        # Loading and calculating
        # -----------------------------------------
        if self.header["N3"] == 1:
            phi_ind = 0
            two_dim = True
        dfile = None
        with h5.File(slice_path, 'a') as reduced_file:
            for quantity in quantities:
                calculated = False
                # Make sure group for quantity is created
                if quantity not in reduced_file:
                    qgroup = reduced_file.create_group(quantity)
                else:
                    qgroup = reduced_file[quantity]
                # Skipping condition: both "Avg" and phi_ind are in the group
                # and overwrite is False
                phi_str = "{:d}".format(phi_ind)
                if two_dim:
                    dont_skip = phi_str not in qgroup.keys()
                else:
                    dont_skip = "Avg" not in qgroup.keys() or phi_str not in qgroup.keys()
                do_skip = not (overwrite or dont_skip)
                if do_skip:
                    continue
                # Only load dump if necessary (i.e. if don't skip all)
                elif dfile is None:
                    dfile = io.load_dump(dump_path, geom=self.geom)
                    io.write_scalar(reduced_file, 't', dfile['t'])

                # Grab the full 3D data either from dump or calculated from dump
                if quantity in self.list_of_standard_quantities:
                    if quantity in dfile.keys():
                        data = dfile[quantity]
                    else:
                        continue
                # case by case calculation.
                elif quantity in self.list_of_raw_calculated_quantities:
                    to_calculate = True
                    for q in self.list_of_raw_calculated_quantities[quantity]:
                        if q not in dfile.keys():
                            to_calculate = False
                    if to_calculate:
                        data = calculate_raw_quantity(quantity, self.header, self.geom, dfile)
                    else:
                        continue

                # Now we have the full 3D calculated data.
                calculated = True
                if data.ndim == 4:
                    avg_slice_data = np.mean(data, axis=-2)
                else:
                    avg_slice_data = np.mean(data, axis=-1)

                phi_slice_data = data[:, :, phi_ind]

                # By this point, don't need to check overwrite
                # Check if quantity group has the datasets for phi ind and Avg
                if "Avg" in qgroup:
                    qgroup["Avg"][...] = avg_slice_data
                elif not two_dim:
                    qgroup.create_dataset("Avg", data=avg_slice_data)
                if phi_str in qgroup:
                    qgroup[phi_str][...] = phi_slice_data
                else:
                    qgroup.create_dataset(phi_str, data=phi_slice_data)

            if calculated:
                print("Calculating slices for " + self.sim_name + " dump {:d}".format(int(dump_num)))

        # Update meta files
        if dfile is not None:
            self.update_dump_times_file(dump_num, dfile['t'], "slices")
            self.update_times_quantities_file(dump_num, quantities, "slices")
        return

    def reduce_data(self, **kwargs):
        # ----------------------------------------------
        # Shell averages
        # ----------------------------------------------
        quantities = get_list_of_all_quantities()
        quantities = clean_reduced_quantities_list(quantities, self.header)
        q_to_retrieve = get_quantities_to_retrieve(quantities)
        list_of_dumps = io.get_dumps_reduced(self.path_to_raw_data)
        tstart_dump = get_dump_num_from_path(list_of_dumps[0])
        tend_dump = get_dump_num_from_path(list_of_dumps[-1])
        tstart_dump = kwargs.get("tstart_dump", tstart_dump)
        tend_dump = kwargs.get("tend_dump", tend_dump)
        if "tstart_dump" in kwargs:
            del kwargs["tstart_dump"]
        if "tend_dump" in kwargs:
            del kwargs["tend_dump"]

        # print("Checking for missing shell averages between dump {:d} and {:d}.".format(tstart_dump, tend_dump))
        # self.remove_time_from_qtimes(1990, "shells")
        q_to_retrieve = get_quantities_to_retrieve_raw(q_to_retrieve)
        q_to_retrieve = clean_raw_quantities_list(q_to_retrieve, self.header)
        print("Quantities to retrieve.")
        print(q_to_retrieve)
        self.check_for_missing_quantities(tstart_dump, tend_dump, q_to_retrieve, "shells", **kwargs)
        # self.remove_quantity_from_qtimes("vrr_bl", "shells")
        # self.check_for_missing_quantities(tstart_dump, tend_dump, ["vrr_bl"], "shells", **kwargs)
        # ----------------------------------------------
        # Radial integrals: mass flux, etc.
        # ----------------------------------------------
        # print("Checking for missing integrals between dump {:d} and {:d}.".format(tstart_dump, tend_dump))
        # integral_qs = get_list_of_integral_quantities()
        # self.check_for_missing_quantities(tstart_dump, tend_dump, integral_qs, "integrals", **kwargs)
        # ----------------------------------------------
        # Cumulative/vertical integrals: optical depth/compton y
        # ----------------------------------------------
        # vert_integral_qs = get_list_of_vert_integral_quantities()
        # self.check_for_missing_quantities(tstart_dump, tend_dump, vert_integral_qs, "vert_integrals", **kwargs)

        # if self.setup == "seagate_tests":
            # return
        # ----------------------------------------------
        # Azimuthally-averaged and phi slices
        # ----------------------------------------------
        if "2D" in self.sim_name:
            return
        if os.path.exists(sim_postprocess.path_to_reduced_data + "/reduced_slices/times_and_quantities.txt"):
            self.clean_times_and_quantities_file("slices")
        quantities = get_list_of_all_quantities()
        quantities = clean_raw_quantities_list(quantities, self.header)
        # Default: do last 2500 rg/c
        if len(list_of_dumps) < 100:
            avg_start = kwargs.get("avg_start", 0)
        else:
            avg_start = kwargs.get("avg_start", -100)
        avg_end = kwargs.get("avg_end", len(list_of_dumps))
        # First, do last 100 dumps all in a row (for time-averaging final state)
        dumps_to_avg = (list_of_dumps[avg_start:avg_end + 1])[::-1]
        for dump_path in dumps_to_avg:
           self.calculate_dump_slice_data(quantities, dump_path, **kwargs)
        # Next, do every 5 dumps (for snapshots, movie)
        # dumps_to_avg = (list_of_dumps)[::5]
        for dump_path in dumps_to_avg:
           self.calculate_dump_slice_data(quantities, dump_path, **kwargs)
        return


if __name__ == '__main__':
    # NOTE: for some reason this mplstyle doesn't work on the nasa computer
    # because it can't find latex??
    # plt.style.use('lia.mplstyle')

    # Arguments passed (cluster usage)
    setup = "lia_nasa"

    import argparse
    parser = argparse.ArgumentParser(
        description='Postprocess ebhlight data.')
    parser.add_argument('--sim',
                        type=str,
                        help='Name of simulation to post process.',
                        default=None)
    parser.add_argument('--setup',
                        type=str,
                        help='Setup for default filepaths.',
                        default=setup)
    parser.add_argument('--reverse',
                        action='store_false',
                        help='Reverse most common ordering.',
                        default=setup)
    args = parser.parse_args()

    if args.sim is not None:
        kwargs = {}
        kwargs["quiet"] = False
        # kwargs["tend_dump"] = 2600
        kwargs["reverse"] = args.reverse
        sim_postprocess = post_processor(args.sim, args.setup, **kwargs)
        # sim_postprocess.save_properties(**kwargs)
        # sim_postprocess.convert_shell_avgs_storage(**kwargs)
        print("Reducing data for " + args.sim)
        if os.path.exists(sim_postprocess.path_to_reduced_data + "/shell_averages/times_and_quantities.p"):
            if not os.path.exists(sim_postprocess.path_to_reduced_data + "/shell_averages/times_and_quantities.txt"):
                sim_postprocess.convert_times_quantities(**kwargs)
        kwargs["avg_start"] = 0
        sim_postprocess.reduce_data(**kwargs)
        if os.path.exists(sim_postprocess.path_to_reduced_data + "/shell_averages/times_and_quantities.txt"):
            sim_postprocess.clean_times_and_quantities_file("shells")
        exit()

    # No argument parsing (laptop usage)
    setup = "seagate_tests"
    setup = "lia_dell"
    sim_names = ["torus_ecool_3DT1e9Mu21_bak"]
    sim_names = ["torus_github_3DMu21_explicit_prasun"]
    sim_names = ["torus_zgithub_3DMu21_explicit"]
    # sim_names = ["test_torus_laptop_implicitCoulomb"]

    kwargs = {}
    # kwargs["path_to_raw_data"] = "/mnt/e/ebhlight_ecool_tests/raw_data/test_torus_3DT1e9Mu21_lowres/dumps/"
    kwargs["quiet"] = False
    for sim_name in sim_names:
        # kwargs["path_to_raw_data"] = "/mnt/c/Users/liaha/Downloads/" + sim_name + "/dumps/"
        # kwargs["path_to_reduced_data"] = "/mnt/c/Users/liaha/Downloads/" + sim_name + "/reduced_data/"
        # kwargs["path_to_figures"] = "/mnt/c/Users/liaha/Downloads/" + sim_name + "/figures/"
        # kwargs["path_to_raw_data"] = "/mnt/c/Users/liaha/research/projects/ebhlight_code/ebhlight_ecool/prob/" + sim_name + "/dumps/"
        # kwargs["tstart_dump"] = 1990
        # kwargs["tend_dump"] = 1992
        sim_postprocess = post_processor(sim_name, setup, **kwargs)
        if os.path.exists(sim_postprocess.path_to_reduced_data + "/shell_averages/times_and_quantities.txt"):
            sim_postprocess.clean_times_and_quantities_file("shells")
        # sim_postprocess.save_properties(**kwargs)
        # sim_postprocess.convert_shell_avgs_storage(**kwargs)
        print("Reducing data for " + sim_name)
        # if not os.path.exists(sim_postprocess.path_to_reduced_data + "/shell_averages/times_and_quantities.txt"):
            # sim_postprocess.convert_times_quantities(**kwargs)
        sim_postprocess.reduce_data(**kwargs)
    exit()
