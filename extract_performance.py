import os
import numpy as np
import matplotlib.pyplot as plt

def extract_performance(output_filename, reduced_file):
    """
    Find performance and output numbers to a new file.
    """
    with open(output_filename, 'r') as f:
        lines = f.readlines()

    performance_times = []

    for line in lines:
        if "ALL:" in line:
            time = line.strip(" ").replace("ALL:","").replace("s","")
            if float(time) < 4.: #relic from the accident with mpiprocs
                performance_times.append(float(time))

    performance_times = np.array(performance_times)
    np.savetxt(reduced_file, performance_times)
    return


def plot_performance(reduced_files, legend_labels):
    for reduced_file in reduced_files:
        performance_times = np.loadtxt(reduced_file + ".txt")
        plt.plot(performance_times, label=legend_labels[reduced_file])
    plt.legend()
    plt.ylabel("Time step wall time [s]")
    plt.xlabel("Log number")
    plt.show()

if __name__ == '__main__':
    output_filename = 'output_ele_m7_ie'
    reduced_file = 'performance_m7_ie.txt'
    # extract_performance(output_filename, reduced_file)

    legend_labels = {
        'performance_m3_explicit':"M3, fully explicit",
        'performance_m3_implicit':"M3, fully implicit",
        'performance_m3_ie':'M3, switch implicit/explicit',
        'performance_m7_explicit':"M7, fully explicit",
        'performance_m7_implicit':"M7, fully implicit",
        'performance_m7_ie':'M7, switch implicit/explicit',
    }
    reduced_files = [
        # 'performance_m7_explicit',
        'performance_m7_implicit',
        'performance_m7_ie',
        # 'performance_m3_explicit',
        # 'performance_m3_implicit',
        # 'performance_m3_ie',
    ]

    # if not os.path.exists(reduced_file):
        # extract_performance()

    plot_performance(reduced_files, legend_labels)
