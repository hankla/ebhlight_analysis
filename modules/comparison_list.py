################################################################################
#                                                                              #
# List of categories for comparisons and their properties                      #
#                                                                              #
################################################################################
import sys;
sys.path.append('modules/')
from ebhlight_tools import colorblind_colors


def set_categories(category, comparison):
    """
    Note that last_common_time will be set by loading in dumps from shells.
    default sim_colors will be set at the end.
    Target temperature is read by plotting functions.
    """
    # ----------------------------------------------
    # Most recent up top please.
    # ----------------------------------------------
    comparison.line_styles = None
    comparison.sim_colors = None
    if category == "compare_restart_Mu25":
        # Compare Mu25 when restarted from Mu21's 7000 rg/c vs.
        # started from scratch. Should address how different starting
        # from t=0 or restarting affects implicit evolution
        # 2023-06-10
        comparison.title = ""
        comparison.sim_names = [
            "torus_ecool_3DT1e9Mu21",
            "torus_ecool_3DT1e9Mu25_restarted",
            # "torus_ecool_3DT1e9Mu25_implicitCoulomb_Pf", #made it to 9000rg/c
            # "torus_ecool_3DT1e9Mu25_ieCoulomb_Pf", # made it to 5200 rg/c
            "torus_ecool_3DT1e9Mu25_ieCoulomb_Pf-rMu21",
        ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e9Mu21":r"$\dot m_0\sim 10^{-7}$",
            "torus_ecool_3DT1e9Mu25_restarted":r"Explicit $10^4\dot m_0$, started from Mu21's $t=7000~r_g/c$",
            "torus_ecool_3DT1e9Mu25_implicitCoulomb_Pf":r"Implicit $10^4\dot m_0$",
            "torus_ecool_3DT1e9Mu25_ieCoulomb_Pf":r"Implicit $10^4\dot m_0$, started from $t=0$",
            "torus_ecool_3DT1e9Mu25_ieCoulomb_Pf-rMu21":r"Implicit $10^4\dot m_0$, started from Mu21's $t=7000~r_g/c$",
        }
    elif category == "compare_3D_high-res":
        # Compare high-resolution simulations
        # 2023-08-24
        comparison.title = ""
        comparison.sim_names = [
            "torus_ecool_3DT1e9Mu21_hr",
            "torus_ecool_3Dh005Mu25_ieCoulomb_Pf-rMu21_hr",
        ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e9Mu21_hr":r"M7",
            "torus_ecool_3Dh005Mu25_ieCoulomb_Pf-rMu21_hr":"M3",
        }
    elif category == "compare_3D_implicitCoulomb_floors":
        # Compare Mdot for non-operator split
        # implicit Coulomb with different floors/restarts, in 3D.
        # rMu21 = restart from Mu21's 7000 rg/c.
        # 2023-06-15
        comparison.title = ""
        comparison.sim_names = [
            "torus_ecool_3DT1e9Mu25_implicitCoulomb_Pf",
            "torus_ecool_3DT1e9Mu25_ieCoulomb_Pf-rMu21-2",
            # "torus_ecool_3DT1e9Mu25_ieCoulomb_Pf-rMu21",
        ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e9Mu25_implicitCoulomb_Pf":r"Not restarted; Coulomb quality floor", # ran to 15k rg/c
            "torus_ecool_3DT1e9Mu25_ieCoulomb_Pf-rMu21":r"Restarted; no abs", # ran to 10k rg/c
            "torus_ecool_3DT1e9Mu25_ieCoulomb_Pf-rMu21-2":r"Restarted; with abs", # ran to 15k rg/c
        }
    elif category == "compare_3D_ieCoulomb_Mdot":
        # Compare Mdot for non-operator split
        # implicit Coulomb with the right floors, in 3D.
        # Restart from Mu21's 7000 rg/c.
        # 2023-06-15
        comparison.title = ""
        comparison.sim_names = [
            "torus_ecool_3DT1e9Mu21",
            "torus_ecool_3DT1e9Mu25_ieCoulomb_Pf-rMu21-2",
            "torus_ecool_3DT1e9Mu27_ieCoulomb_Pf-rMu21",
        ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e9Mu21":r"M7", # ran to 15k
            "torus_ecool_3DT1e9Mu25_ieCoulomb_Pf-rMu21-2":r"M3", # ran to 25k rg/c
            "torus_ecool_3DT1e9Mu27_ieCoulomb_Pf-rMu21":r"M1", # ran to 15k rg/c
        }
    elif category == "compare_3D_constHr":
        # Try fixing H/r = 0.1 (roughly).
        # Mu25 restarted from Mu21 (I think with const T=1e9)'s 7000 rg/c
        # 2023-06-15
        comparison.title = ""
        comparison.sim_names = [
            "torus_ecool_3DT1e9Mu25_ieCoulomb_Pf-rMu21-2", #ran to 15k rg/c
            "torus_ecool_3Dh01Mu25_ieCoulomb_Pf-rMu21", #ran to 15k rg/c
            "torus_ecool_3Dh01Mu27_ieCoulomb_Pf-rMu21", #ran to 15k rg/c
        ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e9Mu25_ieCoulomb_Pf-rMu21-2":r"M3, Constant $T_{\rm target}=10^9$ K",
            "torus_ecool_3Dh01Mu25_ieCoulomb_Pf-rMu21":r"M3, Constant $H/r\sim0.1$",
            "torus_ecool_3Dh01Mu27_ieCoulomb_Pf-rMu21":r"M1, Constant $H/r\sim0.1$",
        }
    elif category == "compare_2D_constHr":
        # Try fixing H/r = 0.1 (roughly).
        # 2023-06-14
        comparison.title = ""
        comparison.sim_names = [
            "test_torus_2Dh01Mu25_ieCoulomb_Pf",
            "test_torus_2Dh01Mu27_ieCoulomb_Pf",
        ]
        comparison.sim_labels = {
            "test_torus_2Dh01Mu25_ieCoulomb_Pf":r"M3",
            "test_torus_2Dh01Mu27_ieCoulomb_Pf":r"M1",
        }
    elif category == "compare_2D_ieCoulomb_targetTemp":
        # After calculating H/r at the ISCO, found needed to increase
        # the target temperature.
        # Why doesn't Mu25 collapse for target 1e10 K?
        # 2023-06-11
        comparison.title = ""
        comparison.sim_names = [
            "test_torus_2DT1e9Mu25_ieCoulomb_Pf",
            "test_torus_2DT1e10Mu25_ieCoulomb_Pf",
        ]
        comparison.sim_labels = {
            "test_torus_2DT1e10Mu25_ieCoulomb_Pf":r"$T_{e, {\rm target}}=10^{10}$ K",
            "test_torus_2DT1e9Mu25_ieCoulomb_Pf":r"$T_{e, {\rm target}}=10^{9}$ K",
        }
    elif category == "compare_3D_T1e10_Mdot":
        # After calculating H/r at the ISCO, found needed to increase
        # the target temperature.
        # Compare how much of a difference switching to the explicit
        # solver makes when due/ue < 1e-5.
        # 2023-06-13
        comparison.title = ""
        comparison.sim_names = [
            "torus_ecool_3DT1e10Mu21_ieCoulomb_Pf", # ran to 10k
            "torus_ecool_3DT1e10Mu25_ieCoulomb_Pf-rMu21", # ran to 13,795 rg/c
        ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e10Mu21_ieCoulomb_Pf":r"$\dot m_0\sim 10^{-7}$",
            "torus_ecool_3DT1e10Mu25_ieCoulomb_Pf-rMu21":r"$\dot m\sim10^4\dot m_0$",
        }
    elif category == "compare_3D_ieCoulomb_targetTemp_Mu25":
        # After calculating H/r at the ISCO, found needed to increase
        # the target temperature.
        # Restarted from Mu21's 7000 rg/c
        # 2023-06-15
        comparison.title = ""
        comparison.sim_names = [
            "torus_ecool_3DT1e9Mu25_ieCoulomb_Pf-rMu21-2", # ran to 15k rg/c
            "torus_ecool_3DT5e9Mu25_ieCoulomb-rMu21", # ran to 15k rg/c
            "torus_ecool_3DT1e10Mu25_ieCoulomb_Pf-rMu21", # ran to 13.795k rg/c
        ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e9Mu25_ieCoulomb_Pf-rMu21-2":r"$T_{e, {\rm target}}=10^{9}$ K",
            "torus_ecool_3DT1e10Mu25_ieCoulomb_Pf-rMu21":r"$T_{e, {\rm target}}=10^{10}$ K",
            "torus_ecool_3DT5e9Mu25_ieCoulomb-rMu21":r"$T_{e, {\rm target}}=5\times10^{9}$ K",
        }
    elif category == "compare_3D_ieCoulomb_targetTemp":
        # After calculating H/r at the ISCO, found needed to increase
        # the target temperature.
        # Compare how much of a difference switching to the explicit
        # solver makes when due/ue < 1e-5.
        # 2023-06-12
        comparison.title = ""
        comparison.sim_names = [
            # "test_torus_2DT1e9Mu21_ieCoulomb_Pf",
            # "test_torus_2DT1e9Mu25_ieCoulomb_Pf",
            "torus_ecool_3DT1e9Mu21_ieCoulomb_Pf",
            "torus_ecool_3DT1e10Mu21_ieCoulomb_Pf", # ran to 10k rg/c
        ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e10Mu21_ieCoulomb_Pf":r"$T_{e, {\rm target}}=10^{10}$ K",
            "torus_ecool_3DT1e9Mu21_ieCoulomb_Pf":r"$T_{e, {\rm target}}=10^{9}$ K",
        }
    elif category == "compare_2D_ieCoulomb_targetTemp1e10":
        # After calculating H/r at the ISCO, found needed to increase
        # the target temperature.
        # Compare how much of a difference switching to the explicit
        # solver makes when due/ue < 1e-5.
        # 2023-06-10
        comparison.title = ""
        comparison.sim_names = [
            # "test_torus_2DT1e9Mu21_ieCoulomb_Pf",
            # "test_torus_2DT1e9Mu25_ieCoulomb_Pf",
            "test_torus_2DT1e10Mu21_ieCoulomb_Pf",
            "test_torus_2DT1e10Mu25_ieCoulomb_Pf",
            "test_torus_2DT1e10Mu27_ieCoulomb_Pf",
        ]
        comparison.sim_labels = {
            "test_torus_2DT1e10Mu21_ieCoulomb_Pf":r"$\dot m_0\sim 10^{-7}$",
            "test_torus_2DT1e10Mu25_ieCoulomb_Pf":r"$\dot m\sim 10^4\dot m_0$",
            "test_torus_2DT1e10Mu27_ieCoulomb_Pf":r"$\dot m\sim 10^6\dot m_0$",
        }
    elif category == "compare_2D_Mu21_ieCoulomb":
        # Compare how much of a difference switching to the explicit
        # solver makes when due/ue < 1e-5.
        # 2023-06-10
        comparison.title = ""
        comparison.sim_names = [
            "test_torus_2DT1e9Mu21-2",
            "test_torus_2DT1e9Mu21_implicitCoulomb2_uelFrac",
            "test_torus_2DT1e9Mu21_ieCoulomb_Pf",
        ]
        comparison.sim_labels = {
            "test_torus_2DT1e9Mu21_implicitCoulomb2_uelFrac":r"Only implicit",
            "test_torus_2DT1e9Mu21_ieCoulomb_Pf":r"Switch I/E",
            "test_torus_2DT1e9Mu21-2":r"Only explicit",
        }
    elif category == "compare_2D_Mu25_ieCoulomb":
        # Compare how much of a difference switching to the explicit
        # solver makes when due/ue < 1e-5.
        # 2023-06-10
        comparison.title = ""
        comparison.sim_names = [
            "test_torus_2DT1e9Mu25",
            "test_torus_2DT1e9Mu25_implicitCoulomb2_uelFrac",
            "test_torus_2DT1e9Mu25_ieCoulomb_Pf",
        ]
        comparison.sim_labels = {
            "test_torus_2DT1e9Mu25":r"Only explicit",
            "test_torus_2DT1e9Mu25_implicitCoulomb2_uelFrac":r"Only implicit",
            "test_torus_2DT1e9Mu25_ieCoulomb_Pf":r"Switch I/E",
        }
    elif category == "compare_3D_Mu25_implicitCoulomb":
        # Compare operator-split and non-operator-split implementations
        # for implicit Coulomb at high Mdot in 3D.
        # 2023-06-09
        comparison.title = ""
        comparison.sim_names = [
            # "torus_ecool_3DT1e9Mu21_implicitCoulomb_Pf",
            "torus_ecool_3DT1e9Mu25_implicitCoulomb_Pf", # run to 9500 rg/c
            "torus_ecool_3DT1e9Mu25_implicitCoulomb_Ps",
        ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e9Mu21_implicitCoulomb_Pf":r"M7; Not operator split",
            "torus_ecool_3DT1e9Mu25_implicitCoulomb_Pf":r"M3; Not operator split",
            "torus_ecool_3DT1e9Mu25_implicitCoulomb_Ps":r"M3; Operator split",
        }
    elif category == "compare_3D_Mu21_implicitExplicit":
        # Compare operator-split and non-operator-split implementations
        # for implicit Coulomb at low Mdot in 3D.
        # 2023-06-09, 2023-06-12
        comparison.title = ""
        comparison.sim_names = [
            "torus_ecool_3DT1e9Mu21",
            "torus_ecool_3DT1e9Mu21_implicitCoulomb_Pf", # ran to 4100 rg/c
            "torus_ecool_3DT1e9Mu21_ieCoulomb_Pf", # ran to 10k rg/c
            "torus_ecool_3DT1e9Mu21_ieCoulomb_Pf_uelFrac", # restarted, purely for spike
        ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e9Mu21":r"Explicit",
            "torus_ecool_3DT1e9Mu21_implicitCoulomb_Pf":r"Implicit; Not operator split",
            "torus_ecool_3DT1e9Mu21_ieCoulomb_Pf":"Switches implicit/explicit",
            "torus_ecool_3DT1e9Mu21_ieCoulomb_Pf_uelFrac":r"Has $|\delta u_e/u_e|$ floor",
        }
    elif category == "compare_3D_implicitCoulomb_Mdot":
        # Compare Mdot for non-operator split
        # implicit Coulomb in 3D.
        # 2023-06-12
        comparison.title = ""
        comparison.sim_names = [
            "torus_ecool_3DT1e9Mu21",
            "torus_ecool_3DT1e9Mu25_implicitCoulomb_Pf",
        ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e9Mu21":r"M7",
            "torus_ecool_3DT1e9Mu25_implicitCoulomb_Pf":r"M3", # ran to 10k rg/c
        }
    if category == "compare_3D_Mu25_implicitExplicit":
        # Compare operator-split and non-operator-split implementations
        # for implicit Coulomb at high Mdot in 3D.
        # 2023-06-09, 2023-06-12
        comparison.title = ""
        comparison.sim_names = [
            "torus_ecool_3DT1e9Mu25",
            "torus_ecool_3DT1e9Mu25_implicitCoulomb_Pf",
            # "torus_ecool_3DT1e9Mu25_implicitCoulomb_Ps", # ran to 9400 rg/c
            # "torus_ecool_3DT1e9Mu25_ieCoulomb_Pf", # ran to 5200 rg/c
        ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e9Mu25":r"Explicit",
            "torus_ecool_3DT1e9Mu25_implicitCoulomb_Pf":r"Implicit; Not operator split", # ran to 10k rg/c
            "torus_ecool_3DT1e9Mu25_implicitCoulomb_Ps":r"Implicit; Operator split", # ran to 3900 rg/c
            "torus_ecool_3DT1e9Mu25_ieCoulomb_Pf":r"Switch I/E", # ran to 5200 rg/c
        }
    elif category == "compare_2D_Mdot_implicitCoulomb":
        # Compare accretion rates for implicit Coulomb, all with
        # the operator-split method.
        # 2023-06-09
        comparison.title = ""
        comparison.sim_names = [
            "test_torus_2DT1e9Mu21_implicitCoulomb2_uelFrac",
            "test_torus_2DT1e9Mu25_implicitCoulomb2_uelFrac",
            "test_torus_2DT1e9Mu27_implicitCoulomb_Pf",
        ]
        comparison.sim_labels = {
            "test_torus_2DT1e9Mu21_implicitCoulomb2_uelFrac":r"$\dot m_0\sim 10^{-7}$",
            "test_torus_2DT1e9Mu25_implicitCoulomb2_uelFrac":r"$\dot m\sim 10^4\dot m_0$",
            "test_torus_2DT1e9Mu27_implicitCoulomb_Pf":r"$\dot m\sim 10^6\dot m_0$",
        }
    elif category == "compare_2D_Mu27_implicitCoulomb":
        # Compare operator-split and non-operator-split implementations
        # for implicit Coulomb at high Mdot. Does operator-split method
        # break like it did in gas box?
        # 2023-06-08
        comparison.title = ""
        comparison.sim_names = [
            "test_torus_2DT1e9Mu27_implicitCoulomb_Pf",
            "test_torus_2DT1e9Mu27_implicitCoulomb_Ps",
        ]
        comparison.sim_labels = {
            "test_torus_2DT1e9Mu27_implicitCoulomb_Pf":r"Non-operator split",
            "test_torus_2DT1e9Mu27_implicitCoulomb_Ps":r"Operator split",
        }
    elif category == "compare_2D_Mu25_implicitCoulomb_singleTtriggerNOS":
        # For all implicit Coulomb implementations with operator-splitting,
        # compare different triggers for the single-temperature regime.
        # 2023-06-06
        comparison.title = ""
        comparison.sim_names = [
            "test_torus_2DT1e9Mu25_implicitCoulomb2_uelFrac",
            "test_torus_2DT1e9Mu25_implicitCoulomb5",
        ]
        comparison.sim_labels = {
            "test_torus_2DT1e9Mu25_implicitCoulomb2_uelFrac":r"$\delta u_e/u_e > 1/10$ and dTe, dTp $<\epsilon$",
            "test_torus_2DT1e9Mu25_implicitCoulomb5":r"$(u_e/Q_{\rm coul})/dt > 10$",
        }
    elif category == "compare_2D_Mu25_implicitCoulomb_singleTtriggerOS":
        # For all implicit Coulomb implementations with operator-splitting,
        # compare different triggers for the single-temperature regime.
        # 2023-06-06
        comparison.title = ""
        comparison.sim_names = [
            "test_torus_2DT1e9Mu25_implicitCoulomb3",
            "test_torus_2DT1e9Mu25_implicitCoulomb4",
        ]
        comparison.sim_labels = {
            "test_torus_2DT1e9Mu25_implicitCoulomb3":r"$\delta u_e/u_e > 1/10$ and dTe, dTp $<\epsilon$",
            "test_torus_2DT1e9Mu25_implicitCoulomb4":r"$(u_e/Q_{\rm coul})/dt > 10$",
        }
    elif category == "compare_2D_Mu25_implicitCoulomb":
        # Compare implicit Coulomb scheme to normal
        # 2023-06-01
        comparison.title = ""
        comparison.sim_names = [
            "test_torus_2DT1e9Mu25",
            # "test_torus_2DT1e9Mu25_implicitCoulomb", # this one used Ps but messed up ue
            # "test_torus_2DT1e9Mu25_implicitCoulomb2", # this one didn't have uelfrac
            "test_torus_2DT1e9Mu25_implicitCoulomb2_uelFrac",
            "test_torus_2DT1e9Mu25_implicitCoulomb3",
        ]
        comparison.sim_labels = {
            "test_torus_2DT1e9Mu25":r"Explicit evolution",
            # "test_torus_2DT1e9Mu25_implicitCoulomb":r"Implicit evolution using Ps, updating Pf",
            # "test_torus_2DT1e9Mu25_implicitCoulomb2":r"Implicit evolution using Pf, updating Pf",
            "test_torus_2DT1e9Mu25_implicitCoulomb2_uelFrac":r"Implicit, not operator split", # i.e. Pf values. also has uel frac floor
            "test_torus_2DT1e9Mu25_implicitCoulomb3":r"Implicit, operator split" , #ie Ps values
        }
    elif category == "compare_resolution":
        # Compare high-resolution and lower resolution
        # 2023-01-09, updated 2023-02-28, 2023-03-03
        comparison.title = ""
        comparison.sim_names = [
            "torus_ecool_3DT1e9Mu21",
            "torus_ecool_3DT1e9Mu21_hr",
        ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e9Mu21":r"160x128x80 zones",
            "torus_ecool_3DT1e9Mu21_hr":r"320x256x160 zones",
        }
    # ----------------------------------------------
    # 3D runs
    # ----------------------------------------------
    elif category == "compare_3D_Mdot":
        # Changed 2023-04-07 plot label (fix eddington error)
        comparison.title = r"Cooling time: $t_{\rm cool}=1/\Omega$"
        comparison.sim_names = [
            "torus_ecool_3DT1e9Mu21",
            "torus_ecool_3DT1e9Mu22",
            "torus_ecool_3DT1e9Mu23",
        ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e9Mu21":r"$\dot m_0\sim10^{-7}$",
            "torus_ecool_3DT1e9Mu22":r"$10\dot m_0$",
            "torus_ecool_3DT1e9Mu23":r"$10^2\dot m_0$",
        }
    elif category == "test_3D_Mdot_tc5":
        # Run on 2023-01-16.
        # Changed 2023-04-07 plot label (fix eddington error)
        comparison.title = ""
        comparison.sim_names = [
            "torus_ecool_3DT1e9Mu21",
            "torus_ecool_3DT1e9Mu21tc5",
            "torus_ecool_3DT1e9Mu23",
            "torus_ecool_3DT1e9Mu23tc5",
        ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e9Mu21":r"$\dot m_0\sim10^{-7}$, $t_{\rm cool}=1/\Omega$",
            "torus_ecool_3DT1e9Mu23":r"$10^2\dot m_0$, $t_{\rm cool}=1/\Omega$",
            "torus_ecool_3DT1e9Mu21tc5":r"$\dot m_0\sim10^{-7}$, $t_{\rm cool}=1/5\Omega$",
            "torus_ecool_3DT1e9Mu23tc5":r"$10^2\dot m_0$, $t_{\rm cool}=1/5\Omega$",
        }
    elif category == "test_3D_Mdot":
        # Run on 2022-11-16.
        # Ran for much longer in Jan. 2023
        # comparison.title = r"Cooling time: $t_{\rm cool}=1/\Omega$"
        # Changed 2023-04-07 plot label (fix eddington error)
        comparison.title = r""
        comparison.sim_names = [
            "torus_ecool_3DT1e9Mu21",
            "torus_ecool_3DT1e9Mu23",
            # "torus_ecool_3DT1e9Mu25",
            "torus_ecool_3DT1e9Mu23tc5",
        ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e9Mu21":r"$\dot m_0\sim10^{-7}$",
            "torus_ecool_3DT1e9Mu23":r"$10^2\dot m_0$",
            "torus_ecool_3DT1e9Mu25":r"$10^4\dot m_0$",
            "torus_ecool_3DT1e9Mu23tc5":r"$10^2\dot m_0$, $t_{\rm cool}=1/5\Omega$",
        }
    elif category == "test_3D_cooling":
        comparison.sim_names = [
            "torus_ecool_3DT1e9Mu21",
            "torus_ecool_3DT1e9Mu21_noQc",
            # "torus_ecool_3DT1e9Mu21_noQcnoQcool",
        ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e9Mu21":r"MHD heating",
            "torus_ecool_3DT1e9Mu21_noQc":r"MHD heating, no Coulomb collisions",
            "torus_ecool_3DT1e9Mu21_noQcnoQcool":r"No heating, no Coulomb collisions",
        }
    elif category == "compare_magnetization_Mu21":
        # Compare Mu21 magnetization evolution.
        # 2023-03-07
        comparison.title = ""
        comparison.sim_names = [
            "torus_ecool_3DT1e9Mu21",
            "torus_ecool_3DT1e9Mu21_mad6",
        ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e9Mu21":r"Low magnetization",
            "torus_ecool_3DT1e9Mu21_mad6":r"High magnetization",
        }

    # ------------------------------------------------
    if comparison.line_styles is None:
        comparison.line_styles = {}
        for sim in comparison.sim_names:
            comparison.line_styles[sim] = '-'
    if comparison.sim_colors is None:
        comparison.sim_colors = {}
        for i, sim in enumerate(comparison.sim_names):
            comparison.sim_colors[sim] = colorblind_colors[i]
    return True
