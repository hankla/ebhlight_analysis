import numpy as np
import os
import matplotlib.lines as mlines
import matplotlib.pyplot as plt
import sys
sys.path.insert(0, '../')
sys.path.append('modules/')
from ebhlight_tools import *
import hdf5_to_dict as io
from default_filepaths import get_default_filepaths
from base_simulation_analyzer import base_simulation_analyzer


def plot_timescales_panel(com, **kwargs):
    """
    Plot panel figure comparing Mdot simulations
    Each column is a simulation with certain Mdot
    Row 1: timescales. Row 2: Heating ratios.
    Normalization options:
        - "infall": normalize to infall time
        - "dynamical": normalize to 1/Omega_K
        - "none": no normalization, units of rg/c
    """
    tstart_rgc = kwargs.get("tstart_rgc", 9000.0)
    tend_rgc = kwargs.get("tend_rgc", 10000.0)
    normalization = kwargs.get("normalization", "none")
    figdir = "/mnt/e/ebhlight_ecool_tests/figures/paper_figures/"
    figdir = kwargs.get("figdir", figdir)
    figname = "compare_electron-proton_timescales_t{:.0f}-{:.0f}rgc_norm-".format(tstart_rgc, tend_rgc)
    figname += normalization + ".png"
    figname = kwargs.get("figname", figname)
    ylimits = [[1.e-1, 1.e5], [1.e-3, 1.e3]]
    ylimits = [[1.e-3, 1.e5], [1.e-3, 1.e5]]
    ylimits = kwargs.get("ylimits", ylimits)
    save_pdf = kwargs.get("save_pdf", True)
    dpi = kwargs.get("dpi", 300)

    # setup = kwargs.get("setup", "seagate_tests")
    # category = kwargs.get("category", "test_3D_Mdot")
    # com = compare.comparison(category, setup)
    ncol = len(com.sim_names)
    nrow = 2
    xlims = [com.rEH*1.1, 50.0]
    xlims = kwargs.get("xlimits", xlims)

    ptime_q_set = ["theat_p", "tCoulomb_p", "Tcool"]
    etime_q_set = ["theat_e", "tCoulomb_e", "tCool_e"]
    ptime_q_labels = [r"$t_{\rm heat}^i$", r"$t_{\rm Coul}^i$", r"$t_{\rm cool}$"]
    etime_q_labels = [r"$t_{\rm heat}^e$", r"$t_{\rm Coul}^e$", r"$t_{\rm cool}^e$"]
    if normalization == "infall":
        ylabels = [r"Proton $t^i/t_{\rm infall}$", r"Electron $t^e/t_{\rm infall}$"]
    else:
        ptime_q_set.append("tinfall")
        etime_q_set.append("tinfall")
        ptime_q_labels = [r"$t_{\rm heat}^i$", r"$t_{\rm Coul}^i$", r"$t_{\rm cool}$", r"$t_{\rm infall}$"]
        etime_q_labels = [r"$t_{\rm heat}^e$", r"$t_{\rm Coul}^e$", r"$t_{\rm cool}^e$", r"$t_{\rm infall}$"]
        if normalization == "dynamical":
            ylabels = [r"Proton $t^i/t_{\rm dyn}$", r"Electron $t^e/t_{\rm dyn}$"]
        else:
            ylabels = [r"Proton $t^ic/r_g$", r"Electron $t^ec/r_g$"]

    quantity_sets = [ptime_q_set, etime_q_set]
    labels = [
        ptime_q_labels,
        etime_q_labels,
    ]
    linestyles = ['-', '--', '-.', ':']

    kwargs = {}
    kwargs["quiet"] = True
    kwargs["tstart_shells"] = tstart_rgc
    kwargs["tend_shells"] = tend_rgc

    width_to_height = 8.0/12.0
    fig_width = ncol*3.0 + 3.0
    fig_height = fig_width*width_to_height

    fig, axs = plt.subplots(nrow, ncol, sharex=True, figsize=(fig_width, fig_height), sharey='row')
    tit_str = com.title + "\n Time-averaged from $t={:.0f}$ --- ${:.0f}~r_g/c$".format(tstart_rgc, tend_rgc)
    fig.suptitle(tit_str)
    # Set column titles
    for ax, sim_name in zip(axs[0], com.sim_names):
        ax.set_title(com.sim_labels[sim_name])
    # Plot data
    for row_num, quantities in enumerate(quantity_sets):
        cycle_quantities = quantities.copy()
        if normalization == "infall":
            quantities.append("tinfall")
        q_to_retrieve = get_quantities_to_retrieve(quantities)
        # q_to_retrieve.append("vrr_bl")
        q_to_retrieve.append("ucon_bl1")
        q_to_retrieve.append("Tcool")
        kwargs["shell_quantities"] = q_to_retrieve
        # Move across each simulation
        for col_num, sim_name in enumerate(com.sim_names):
            handles = []
            sim = com.simulations[sim_name]
            r_vals = sim.r_grid[:, 0, 0]
            EH_ind = (np.abs(sim.rEH - r_vals)).argmin()
            tstart_rgc_sim, tend_rgc_sim, shell_avgs = sim.retrieve_shell_averages(**kwargs)
            for j, q in enumerate(cycle_quantities):
                if q in get_list_of_reduced_calculated_quantities():
                    if q == "theat":
                        shell_data = calculate_reduced_quantity(q, shell_avgs, r_vals, smooth=True, neighbors=20, sim_units=sim.header, EH_ind=EH_ind)
                    else:
                        shell_data = calculate_reduced_quantity(q, shell_avgs, r_vals, sim_units=sim.header, EH_ind=EH_ind)
                elif q in shell_avgs:
                    shell_array = shell_avgs[q]
                    if tstart_rgc_sim != tend_rgc_sim:
                        shell_data = np.nanmean(shell_array, axis=0)
                else:
                    print("Shell data is none!")
                    shell_data = None
                tinfall = calculate_reduced_quantity("tinfall", shell_avgs, r_vals, sim_units=sim.header, EH_ind=EH_ind)
                if shell_data is not None:
                    if shell_data.shape != tinfall.shape:
                        shell_data = shell_data[EH_ind:]
                    if r_vals.shape != tinfall.shape:
                        temp_rvals = r_vals[EH_ind:]
                    else:
                        temp_rvals = r_vals
                    if normalization == "infall":
                        shell_data = shell_data/(tinfall + 1.e-20)
                    elif normalization == "dynamical":
                        # tcool = 1.0/(Omega*tcoolOmega0)
                        # So Omega = 1.0/(tcool*tcoolOmega0) and
                        # tdyn = 1.0/Omega = tcool*tcoolOmega0
                        tcool_factor = sim.header["tcoolOmega0"]
                        tdyn = tcool_factor*shell_avgs["Tcool"]
                        if tdyn.ndim > 1:
                            tdyn = np.mean(tdyn, axis=0)
                        if tdyn.shape != shell_data.shape:
                            tdyn = tdyn[EH_ind:]
                        shell_data = shell_data/tdyn
                if normalization in ["infall", "dynamical"]:
                    axs[row_num, col_num].axhline([1.0],
                                                    color='black', ls='--')
                if shell_data is not None:
                    axs[row_num][col_num].plot(temp_rvals, shell_data,
                                            # label=labels[row_num][j],
                                            color=com.sim_colors[sim_name],
                                            ls=linestyles[j])
                handles.append(mlines.Line2D([], [],
                                             color='k',
                                             ls=linestyles[j],
                                             label=labels[row_num][j]))
            # Plot equilibrium radius
            if shell_data is not None:
                if "vrr_bl" in shell_avgs:
                    vrr_data = np.abs(np.mean(shell_avgs["vrr_bl"], axis=0))
                    req_ind_start = (np.abs(vrr_data*tstart_rgc_sim - r_vals)).argmin()
                    axs[row_num, col_num].axvline(r_vals[req_ind_start],
                                                color=com.sim_colors[sim_name],
                                                ls=':')
            axs[row_num, col_num].axvline(com.rISCO, color='black', ls=':')
            # Legend only in right-most column
            if col_num == ncol - 1:
                # axs[row_num, col_num].legend(bbox_to_anchor=(1, 0.75), loc='upper left', frameon=False)
                axs[row_num, col_num].legend(handles=handles,
                                             bbox_to_anchor=(1, 0.75),
                                             loc='upper left', frameon=False)
            # Set xlabel/xlim for each
            if row_num == nrow - 1:
                axs[row_num, col_num].set_xlabel(r"$r/r_g$")
                axs[row_num, col_num].set_xlim(xlims)
                # axs[row_num, col_num].axhline([1.0], color='black', ls=':')
            # Set all to have log y scales
            axs[row_num, col_num].set_yscale('log')
            axs[row_num, col_num].set_xscale('log')
        # Set y-limits for each row
        axs[row_num][0].set_ylim(ylimits[row_num])
        axs[row_num][0].set_ylabel(ylabels[row_num])

    plt.tight_layout()
    print("Saving figure " + figdir + figname)
    if not os.path.exists(figdir): os.makedirs(figdir)
    plt.savefig(figdir + figname, bbox_inches='tight', dpi=dpi)
    if save_pdf:
        if not os.path.exists(figdir + "pdfs/"): os.makedirs(figdir + "pdfs/")
        plt.savefig(figdir + "pdfs/" + figname.replace(".png", ".pdf"), bbox_inches='tight', dpi=dpi)
    return


def plot_timescales_and_heating_panel(com, **kwargs):
    """
    Plot panel figure comparing Mdot simulations
    Each column is a simulation with certain Mdot
    Row 1: timescales. Row 2: Heating ratios.
    Normalization options:
        - "infall": normalize to infall time
        - "dynamical": normalize to 1/Omega_K
        - "none": no normalization, units of rg/c
    """
    tstart_rgc = kwargs.get("tstart_rgc", 9000.0)
    tend_rgc = kwargs.get("tend_rgc", 10000.0)
    normalization = kwargs.get("normalization", "none")
    figdir = "/mnt/e/ebhlight_ecool_tests/figures/paper_figures/"
    figdir = kwargs.get("figdir", figdir)
    figname = "compare_times_and_heating_t{:.0f}-{:.0f}rgc_norm-".format(tstart_rgc, tend_rgc)
    figname += normalization + ".png"
    figname = kwargs.get("figname", figname)
    ylimits = [[1.e-3, 1.e5], [1.e-5, 1.e3]]
    ylimits = kwargs.get("ylimits", ylimits)
    save_pdf = kwargs.get("save_pdf", True)
    dpi = kwargs.get("dpi", 300)

    # setup = kwargs.get("setup", "seagate_tests")
    # category = kwargs.get("category", "test_3D_Mdot")
    # com = compare.comparison(category, setup)
    ncol = len(com.sim_names)
    nrow = 2
    xlims = [com.rEH*1.1, 50.0]
    xlims = kwargs.get("xlimits", xlims)

    if normalization == "infall":
        time_q_set = ["theat", "Tcool", "tCoulomb_p"]
        time_q_labels = [r"$t_{\rm heat}^i$", r"$t_{\rm cool}$", r"$t_{\rm Coul}^i$"]
        ylabels = [r"$t/t_{\rm infall}$", "Heating ratios"]
    else:
        time_q_set = ["theat", "Tcool", "tCoulomb_p", "tinfall"]
        time_q_labels = [r"$t_{\rm heat}^i$", r"$t_{\rm cool}$", r"$t_{\rm Coul}^i$", r"$t_{\rm infall}$"]
        if normalization == "dynamical":
            ylabels = [r"$t/t_{\rm dyn}$", "Heating ratios"]
        else:
            ylabels = [r"$tc/r_g$", "Heating ratios"]

    quantity_sets = [time_q_set, ["coolTotalRatioPost", "coulViscRatioPost", "coolCoulRatioPost"]]
    labels = [
        time_q_labels,
        [r"$\langle Q_{\rm cool}\rangle/\langle Q_{\rm heat}\rangle$",
         r"$\langle Q_{\rm coul}\rangle/\langle Q_{\rm visc}^e\rangle$",
         r"$\langle Q_{\rm cool}\rangle/\langle Q_{\rm coul}\rangle$"]
    ]
    linestyles = ['-', '--', '-.', ':']


    kwargs = {}
    kwargs["quiet"] = True
    kwargs["tstart_shells"] = tstart_rgc
    kwargs["tend_shells"] = tend_rgc

    width_to_height = 8.0/12.0
    fig_width = ncol*3.0 + 3.0
    fig_height = fig_width*width_to_height

    fig, axs = plt.subplots(nrow, ncol, sharex=True, figsize=(fig_width, fig_height), sharey='row')
    tit_str = com.title + "\n Time-averaged from $t={:.0f}$ --- ${:.0f}~r_g/c$".format(tstart_rgc, tend_rgc)
    fig.suptitle(tit_str)
    # Set column titles
    for ax, sim_name in zip(axs[0], com.sim_names):
        ax.set_title(com.sim_labels[sim_name])
    # Plot data
    for row_num, quantities in enumerate(quantity_sets):
        cycle_quantities = quantities.copy()
        if normalization == "infall" and row_num == 0:
            quantities.append("tinfall")
        q_to_retrieve = get_quantities_to_retrieve(quantities)
        q_to_retrieve.append("vrr_bl")
        kwargs["shell_quantities"] = q_to_retrieve
        # Move across each simulation
        for col_num, sim_name in enumerate(com.sim_names):
            handles = []
            sim = com.simulations[sim_name]
            r_vals = sim.r_grid[:, 0, 0]
            EH_ind = (np.abs(sim.rEH - r_vals)).argmin()
            tstart_rgc_sim, tend_rgc_sim, shell_avgs = sim.retrieve_shell_averages(**kwargs)
            for j, q in enumerate(cycle_quantities):
                if q in get_list_of_reduced_calculated_quantities():
                    if q == "theat":
                        shell_data = calculate_reduced_quantity(q, shell_avgs, r_vals, smooth=True, neighbors=20, sim_units=sim.header, EH_ind=EH_ind)
                    else:
                        shell_data = calculate_reduced_quantity(q, shell_avgs, r_vals, sim_units=sim.header, EH_ind=EH_ind)
                elif q in shell_avgs:
                    shell_array = shell_avgs[q]
                    if tstart_rgc_sim != tend_rgc_sim:
                        shell_data = np.nanmean(shell_array, axis=0)
                else:
                    print("Shell data is none!")
                    shell_data = None
                if row_num == 0:
                    tinfall = calculate_reduced_quantity("tinfall", shell_avgs, r_vals, sim_units=sim.header, EH_ind=EH_ind)
                    if shell_data is not None:
                        if shell_data.shape != tinfall.shape:
                            shell_data = shell_data[EH_ind:]
                        if r_vals.shape != tinfall.shape:
                            temp_rvals = r_vals[EH_ind:]
                        else:
                            temp_rvals = r_vals
                        if normalization == "infall":
                            shell_data = shell_data/(tinfall + 1.e-20)
                        elif normalization == "dynamical":
                            # tcool = 1.0/(Omega*tcoolOmega0)
                            # So Omega = 1.0/(tcool*tcoolOmega0) and
                            # tdyn = 1.0/Omega = tcool*tcoolOmega0
                            tcool_factor = sim.header["tcoolOmega0"]
                            tdyn = tcool_factor*shell_avgs["Tcool"]
                            if tdyn.ndim > 1:
                                tdyn = np.mean(tdyn, axis=0)
                            if tdyn.shape != shell_data.shape:
                                tdyn = tdyn[EH_ind:]
                            shell_data = shell_data/tdyn
                    if normalization in ["infall", "dynamical"]:
                        axs[row_num, col_num].axhline([1.0],
                                                      color='black', ls='--')
                else:
                    temp_rvals = r_vals
                if shell_data is not None:
                    axs[row_num][col_num].plot(temp_rvals, shell_data,
                                            # label=labels[row_num][j],
                                            color=com.sim_colors[sim_name],
                                            ls=linestyles[j])
                handles.append(mlines.Line2D([], [],
                                             color='k',
                                             ls=linestyles[j],
                                             label=labels[row_num][j]))
            # Plot equilibrium radius
            if shell_data is not None:
                if "vrr_bl" in shell_avgs:
                    vrr_data = np.abs(np.mean(shell_avgs["vrr_bl"], axis=0))
                    req_ind_start = (np.abs(vrr_data*tstart_rgc_sim - r_vals)).argmin()
                    axs[row_num, col_num].axvline(r_vals[req_ind_start],
                                                color=com.sim_colors[sim_name],
                                                ls=':')
            axs[row_num, col_num].axvline(com.rISCO, color='black', ls=':')
            # Legend only in right-most column
            if col_num == ncol - 1:
                # axs[row_num, col_num].legend(bbox_to_anchor=(1, 0.75), loc='upper left', frameon=False)
                axs[row_num, col_num].legend(handles=handles,
                                             bbox_to_anchor=(1, 0.75),
                                             loc='upper left', frameon=False)
            # Set xlabel/xlim for each
            if row_num == nrow - 1:
                axs[row_num, col_num].set_xlabel(r"$r/r_g$")
                axs[row_num, col_num].set_xlim(xlims)
                axs[row_num, col_num].axhline([1.0], color='black', ls=':')
            # Set all to have log y scales
            axs[row_num, col_num].set_yscale('log')
            axs[row_num, col_num].set_xscale('log')
        # Set y-limits for each row
        axs[row_num][0].set_ylim(ylimits[row_num])
        axs[row_num][0].set_ylabel(ylabels[row_num])

    plt.tight_layout()
    print("Saving figure " + figdir + figname)
    if not os.path.exists(figdir): os.makedirs(figdir)
    plt.savefig(figdir + figname, bbox_inches='tight', dpi=dpi)
    if save_pdf:
        if not os.path.exists(figdir + "pdfs/"): os.makedirs(figdir + "pdfs/")
        plt.savefig(figdir + "pdfs/" + figname.replace(".png", ".pdf"), bbox_inches='tight', dpi=dpi)
    return


def plot_gas_timescales_panel(com, **kwargs):
    """
    Plot panel figure comparing Mdot simulations
    Each column is a simulation with certain Mdot
    Normalization options:
        - "infall": normalize to infall time
        - "dynamical": normalize to 1/Omega_K
        - "none": no normalization, units of rg/c
    """
    tstart_rgc = kwargs.get("tstart_rgc", 9000.0)
    tend_rgc = kwargs.get("tend_rgc", 10000.0)
    normalization = kwargs.get("normalization", "none")
    figdir = "/mnt/e/ebhlight_ecool_tests/figures/paper_figures/"
    figdir = kwargs.get("figdir", figdir)
    figname = "compare_gas_timescales_t{:.0f}-{:.0f}rgc_norm-".format(tstart_rgc, tend_rgc)
    figname += normalization + ".png"
    figname = kwargs.get("figname", figname)
    ylimits = [1.e-3, 1.e4]
    ylimits = kwargs.get("ylimits", ylimits)
    save_pdf = kwargs.get("save_pdf", True)
    dpi = kwargs.get("dpi", 300)

    # setup = kwargs.get("setup", "seagate_tests")
    # category = kwargs.get("category", "test_3D_Mdot")
    # com = compare.comparison(category, setup)
    ncol = len(com.sim_names)
    nrow = 1
    xlims = [com.rEH*1.1, 50.0]
    xlims = kwargs.get("xlimits", xlims)

    if normalization == "infall":
        time_q_set = ["tCoulomb_p", "tCool_e"]
        time_q_labels = [r"$t_{\rm Coul}^i$", r"$t_{\rm cool}^e$"]
        ylabels = r"$t/t_{\rm infall}$"
    else:
        time_q_set = ["tCoulomb_p", "tCool_e", "tinfall"]
        time_q_labels = [r"$t_{\rm Coul}^i$", r"$t_{\rm cool}^e$", r"$t_{\rm infall}$"]
        if normalization == "dynamical":
            ylabels = r"$t/t_{\rm dyn}$"
        else:
            ylabels = r"$tc/r_g$"

    quantity_sets = [time_q_set]
    labels = time_q_labels
    linestyles = ['-', '--', '-.', ':']

    kwargs = {}
    kwargs["quiet"] = True
    kwargs["tstart_shells"] = tstart_rgc
    kwargs["tend_shells"] = tend_rgc

    height_to_width = 8.0/8.0
    fig_width = ncol*3.0 + 3.0
    fig_height = fig_width*height_to_width
    fig_width = 8
    fig_height = 4

    fig, axs = plt.subplots(nrow, ncol, sharex=True, figsize=(fig_width, fig_height), sharey='row')
    tit_str = com.title + "\n Time-averaged from $t={:.0f}$ --- ${:.0f}~r_g/c$".format(tstart_rgc, tend_rgc)
    fig.suptitle(tit_str)
    # Set column titles
    for ax, sim_name in zip(axs, com.sim_names):
        ax.set_title(com.sim_labels[sim_name])
    # Plot data
    for row_num, quantities in enumerate(quantity_sets):
        cycle_quantities = quantities.copy()
        if normalization == "infall" and row_num == 0:
            quantities.append("tinfall")
        q_to_retrieve = get_quantities_to_retrieve(quantities)
        q_to_retrieve.append("vrr_bl")
        kwargs["shell_quantities"] = q_to_retrieve
        # Move across each simulation
        for col_num, sim_name in enumerate(com.sim_names):
            handles = []
            sim = com.simulations[sim_name]
            r_vals = sim.r_grid[:, 0, 0]
            EH_ind = (np.abs(sim.rEH - r_vals)).argmin()
            tstart_rgc_sim, tend_rgc_sim, shell_avgs = sim.retrieve_shell_averages(**kwargs)
            for j, q in enumerate(cycle_quantities):
                if q in get_list_of_reduced_calculated_quantities():
                    if q == "theat":
                        shell_data = calculate_reduced_quantity(q, shell_avgs, r_vals, smooth=True, neighbors=20, sim_units=sim.header, EH_ind=EH_ind)
                    else:
                        shell_data = calculate_reduced_quantity(q, shell_avgs, r_vals, sim_units=sim.header, EH_ind=EH_ind)
                elif q in shell_avgs:
                    shell_array = shell_avgs[q]
                    if tstart_rgc_sim != tend_rgc_sim:
                        shell_data = np.nanmean(shell_array, axis=0)
                else:
                    print("Shell data is none!")
                    shell_data = None
                tinfall = calculate_reduced_quantity("tinfall", shell_avgs, r_vals, sim_units=sim.header, EH_ind=EH_ind)
                if shell_data is not None:
                    if shell_data.shape != tinfall.shape:
                        shell_data = shell_data[EH_ind:]
                    if r_vals.shape != tinfall.shape:
                        temp_rvals = r_vals[EH_ind:]
                    else:
                        temp_rvals = r_vals
                    if normalization == "infall":
                        shell_data = shell_data/(tinfall + 1.e-20)
                    elif normalization == "dynamical":
                        # tcool = 1.0/(Omega*tcoolOmega0)
                        # So Omega = 1.0/(tcool*tcoolOmega0) and
                        # tdyn = 1.0/Omega = tcool*tcoolOmega0
                        tcool_factor = sim.header["tcoolOmega0"]
                        tdyn = tcool_factor*shell_avgs["Tcool"]
                        if tdyn.ndim > 1:
                            tdyn = np.mean(tdyn, axis=0)
                        if tdyn.shape != shell_data.shape:
                            tdyn = tdyn[EH_ind:]
                        shell_data = shell_data/tdyn
                if normalization in ["infall", "dynamical"]:
                    axs[col_num].axhline([1.0],
                                         color='black', ls='--', lw=1)
                if shell_data is not None:
                    axs[col_num].plot(temp_rvals, shell_data,
                                            # label=labels[row_num][j],
                                            color=com.sim_colors[sim_name],
                                            ls=linestyles[j])
                handles.append(mlines.Line2D([], [],
                                             color='gray',
                                             ls=linestyles[j],
                                             label=labels[j]))
            # Plot equilibrium radius
            if shell_data is not None:
                if "vrr_bl" in shell_avgs:
                    vrr_data = np.abs(np.mean(shell_avgs["vrr_bl"], axis=0))
                    # req_ind_start = (np.abs(vrr_data*tstart_rgc_sim - r_vals)).argmin()
                    req_ind_start = np.argsort(np.abs(vrr_data*tstart_rgc_sim - r_vals))[1]
                    axs[col_num].axvline(r_vals[req_ind_start],
                                                color=com.sim_colors[sim_name],
                                                ls=':')
            axs[col_num].axvline(com.rISCO, color='black', ls=':')
            # Legend only in right-most column
            if col_num == ncol - 1:
                # axs[row_num, col_num].legend(bbox_to_anchor=(1, 0.75), loc='upper left', frameon=False)
                axs[col_num].legend(handles=handles,
                                             bbox_to_anchor=(1, 0.75),
                                             loc='upper left', frameon=False)
            # Set xlabel/xlim for each
            if row_num == nrow - 1:
                axs[col_num].set_xlabel(r"$r/r_g$")
                axs[col_num].set_xlim(xlims)
                axs[col_num].axhline([1.0], color='black', ls='-', lw=1)
            # Set all to have log y scales
            axs[col_num].set_yscale('log')
            axs[col_num].set_xscale('log')
        # Set y-limits for each row
        axs[0].set_ylim(ylimits)
        axs[0].set_ylabel(ylabels)

    plt.tight_layout()
    print("Saving figure " + figdir + figname)
    if not os.path.exists(figdir): os.makedirs(figdir)
    plt.savefig(figdir + figname, bbox_inches='tight', dpi=dpi)
    if save_pdf:
        if not os.path.exists(figdir + "pdfs/"): os.makedirs(figdir + "pdfs/")
        plt.savefig(figdir + "pdfs/" + figname.replace(".png", ".pdf"), bbox_inches='tight', dpi=dpi)
    return

if __name__ == '__main__':
    plt.style.use('lia.mplstyle')

    # ----------------------------------------------
    # Plot panel figure comparing Mdot simulations
    # Each column is a simulation with certain Mdot
    # Row 1: timescales. Row 2: Heating ratios.
    # ----------------------------------------------
    plot_mdot_panel()
