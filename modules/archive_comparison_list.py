################################################################################
#                                                                              #
# List of categories for comparisons and their properties                      #
# Once a set of simulations is moved to an archive folder,                     #
# any relevant comparisons should be moved here from comparison_list.py.       #
#                                                                              #
################################################################################


def old_category_values(category, comparison):
    # ----------------------------------------------
    # Keep all the old categories in this separate
    # function so I don't have to look at them.
    # ----------------------------------------------

    # ----------------------------------------------
    # Tests of the implicit Coulomb solver and various
    # (typically wrong) implementations. Done April - June 2023.
    # ----------------------------------------------
    if category == "compare_2D_Mu21_implicitCoulomb":
        # Compare implicit Coulomb scheme to normal.
        # NOTE these are mostly all wrong conceptually and were desperate attempts
        # to see what worked. Only 2_uelFrac and 3 actually make sense...see
        # note from 2023-06-05 Daily
        # 2023-06-01, 2023-06-05
        comparison.title = ""
        comparison.sim_names = [
            "test_torus_2DT1e9Mu21-2",
            # "test_torus_2DT1e9Mu21_implicitCoulomb",
            # "test_torus_2DT1e9Mu21_implicitCoulomb2",
            "test_torus_2DT1e9Mu21_implicitCoulomb2_uelFrac",
            "test_torus_2DT1e9Mu21_implicitCoulomb3",
        ]
        comparison.sim_labels = {
            "test_torus_2DT1e9Mu21-2":r"Explicit evolution",
            "test_torus_2DT1e9Mu21_implicitCoulomb":r"Implicit evolution using Ps, updating Pf",
            "test_torus_2DT1e9Mu21_implicitCoulomb2":r"Implicit evolution using Pf, updating Pf",
            "test_torus_2DT1e9Mu21_implicitCoulomb2_uelFrac":r"Implicit, not operator split", # also has ue frac, uses Pf values
            "test_torus_2DT1e9Mu21_implicitCoulomb3":r"Implicit, operator split",
        }
    # ----------------------------------------------
    # Tests that don't address the need for an implicit
    # Coulomb solver.
    # Done early 2023 (before ~April).
    elif category == "compare_3D_Mdot_hr":
        # Changed 2023-04-07 plot label (fix eddington error)
        comparison.title = r"Cooling time: $t_{\rm cool}=1/\Omega$"
        comparison.sim_names = [
            "torus_ecool_3DT1e9Mu21_hr",
            # "torus_ecool_3DT1e9Mu24_hr_restarted",
            "torus_ecool_3DT1e9Mu25_hr_restarted",
        ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e9Mu21_hr":r"M7",
            "torus_ecool_3DT1e9Mu24_hr_restarted":r"M4",
            "torus_ecool_3DT1e9Mu25_hr_restarted":r"M3",
        }
        comparison.sim_colors = {
            "torus_ecool_3DT1e9Mu21_hr":colorblind_colors[0],
            "torus_ecool_3DT1e9Mu24_hr_restarted":colorblind_colors[1],
            "torus_ecool_3DT1e9Mu25_hr_restarted":colorblind_colors[2],
        }
        comparison.last_common_time = 15000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_restart_Mu23":
        # Compare Mu25 when restarted from Mu21's 7000 rg/c vs.
        # started from scratch. Should address whether the supercooling
        # is due to no heating at large radii
        # 2023-02-25
        # Changed 2023-04-07 plot label (fix eddington error)
        comparison.title = ""
        comparison.sim_names = [
            "torus_ecool_3DT1e9Mu21",
            "torus_ecool_3DT1e9Mu23",
            "torus_ecool_3DT1e9Mu23_restarted",
        ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e9Mu21":r"$\dot m_0\sim 10^{-7}$",
            "torus_ecool_3DT1e9Mu23":r"$10^2\dot m_0$, started from $t=0$",
            "torus_ecool_3DT1e9Mu23_restarted":r"$10^2\dot m_0$, started from Mu21's $t=7000~r_g/c$",
        }
        comparison.sim_colors = {
            "torus_ecool_3DT1e9Mu21": colorblind_colors[0],
            "torus_ecool_3DT1e9Mu23":colorblind_colors[1],
            "torus_ecool_3DT1e9Mu23_restarted":colorblind_colors[2],
        }
        comparison.last_common_time = 10000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_restart_Mu25":
        # Compare Mu25 when restarted from Mu21's 7000 rg/c vs.
        # started from scratch. Should address whether the supercooling
        # is due to no heating at large radii
        # 2023-02-23
        # Changed 2023-04-07 plot label (fix eddington error)
        comparison.title = ""
        comparison.sim_names = [
            "torus_ecool_3DT1e9Mu21",
            "torus_ecool_3DT1e9Mu25",
            "torus_ecool_3DT1e9Mu25_restarted",
        ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e9Mu21":r"$\dot m_0\sim 10^{-7}$",
            "torus_ecool_3DT1e9Mu25":r"$10^4\dot m_0$, started from $t=0$",
            "torus_ecool_3DT1e9Mu25_restarted":r"$10^4\dot m_0$, started from Mu21's $t=7000~r_g/c$",
        }
        comparison.sim_colors = {
            "torus_ecool_3DT1e9Mu21": colorblind_colors[0],
            "torus_ecool_3DT1e9Mu25":colorblind_colors[1],
            "torus_ecool_3DT1e9Mu25_restarted":colorblind_colors[2],
        }
        comparison.last_common_time = 10000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_tcool_Mu25":
        # Compare cooling times to see if the supercooling gets better
        # 2023-02-24
        comparison.title = ""
        comparison.sim_names = [
            "torus_ecool_3DT1e9Mu25",
            "torus_ecool_3DT1e9Mu25tc01",
        ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e9Mu25":r"$t_{\rm cool}=1/\Omega$",
            "torus_ecool_3DT1e9Mu25tc01":r"$t_{\rm cool}=10/\Omega$",
        }
        comparison.sim_colors = {
            "torus_ecool_3DT1e9Mu25":colorblind_colors[0],
            "torus_ecool_3DT1e9Mu25tc01":colorblind_colors[1],
        }
        comparison.last_common_time = 10000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_2D_QcLimiter_Mu25":
        # Comparing how limiting Qcoul affects evolution
        # 2023-04-19
        comparison.title = ""
        comparison.sim_names = [
            "test_torus_2DT1e9Mu25",
            # "test_torus_2DT1e9Mu25_QcLimiter",
            "test_torus_2DT1e9Mu25_QcLimiter-2",
            # "test_torus_2DT1e9Mu25_Qv1em6_QcLimiter",
            ]
        comparison.sim_labels = {
            "test_torus_2DT1e9Mu25":"No Limiter",
            "test_torus_2DT1e9Mu25_QcLimiter":"With Limiter + horrendous supercooling",
            "test_torus_2DT1e9Mu25_QcLimiter-2":"With Limiter",
            "test_torus_2DT1e9Mu25_Qv1em6_QcLimiter":"With Limiter, injected Qvisc",
        }
        comparison.last_common_time = 5000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_2D_Mu25-restart":
        # 2023-05-01
        comparison.title = ""
        comparison.sim_names = [
            "test_torus_2DT1e9Mu25",
            "test_torus_2DT1e9Mu25-r",
            ]
        comparison.sim_labels = {
            "test_torus_2DT1e9Mu25":r"Run from $t=0$",
            "test_torus_2DT1e9Mu25-r":r"Restarted from Mu21 $t=5000~r_g/c$",
        }
        comparison.last_common_time = 10000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_2D_Ymaxtest":
        # 2023-05-01
        comparison.title = ""
        comparison.sim_names = [
            "test_torus_2DT1e9Mu25",
            "test_torus_2DT1e9Mu25_Ymax500",
            "test_torus_2DT1e9Mu25_Ymax500-r",
            ]
        comparison.sim_labels = {
            "test_torus_2DT1e9Mu25":r"$Y<1000$",
            "test_torus_2DT1e9Mu25_Ymax500":r"$Y<500$",
            "test_torus_2DT1e9Mu25_Ymax500-r":r"$Y<500$ restarted",
        }
        comparison.last_common_time = 2500.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_2D_gamma53":
        # 2023-05-04
        comparison.title = ""
        comparison.sim_names = [
            "test_torus_2DT1e9Mu21-2",
            "test_torus_2DT1e9Mu21_gamma53",
            # "test_torus_2DT1e9Mu25",
            # "test_torus_2DT1e9Mu25_gamma53",
            ]
        comparison.sim_labels = {
            "test_torus_2DT1e9Mu25":r"$10^4\dot m_0$, $\gamma_e=4/3$",
            "test_torus_2DT1e9Mu25_gamma53":r"$10^4\dot m_0$, $\gamma_e=5/3$",
            # "test_torus_2DT1e9Mu21-2":r"$\dot m_0\sim10^{-7}$, $\gamma_e=4/3$",
            # "test_torus_2DT1e9Mu21_gamma53":r"$\dot m_0\sim10^{-7}$, $\gamma_e=5/3$",
            "test_torus_2DT1e9Mu21-2":r"$\gamma_e=4/3$",
            "test_torus_2DT1e9Mu21_gamma53":r"$\gamma_e=5/3$",
        }
        # comparison.last_common_time = 2500.0 # rg/c
        comparison.last_common_time = 5000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_3D_Mu25_TpTe":
        # 2023-05-08
        comparison.title = ""
        comparison.sim_names = [
            "torus_ecool_3DT1e9Mu25",
            "torus_ecool_3DT1e9Mu25_tpte3",
            ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e9Mu25":r"$T_p/T_e\in(10^{-4}, 10^4)$",
            "torus_ecool_3DT1e9Mu25_tpte3":r"$T_p/T_e\in(10^{-3}, 10^3)$",
        }
        comparison.last_common_time = 8000.0 # rg/c
        # comparison.last_common_time = 10000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_3D_Mdot_LimiterCorr":
        # 2023-05-08
        comparison.title = ""
        comparison.sim_names = [
            "torus_ecool_3DT1e9Mu21_QcLimiterCorr",
            "torus_ecool_3DT1e9Mu24_QcLimiterCorr",
            "torus_ecool_3DT1e9Mu25_QcLimiterCorr",
            "torus_ecool_3DT1e9Mu26_QcLimiterCorr",
            ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e9Mu21_QcLimiterCorr":r"$\dot m_0\sim10^{-7}$",
            "torus_ecool_3DT1e9Mu24_QcLimiterCorr":r"$10^3\dot m_0$",
            "torus_ecool_3DT1e9Mu25_QcLimiterCorr":r"$10^4\dot m_0$",
            "torus_ecool_3DT1e9Mu26_QcLimiterCorr":r"$10^5\dot m_0$",
        }
        comparison.last_common_time = 10000.0 # rg/c
        # comparison.last_common_time = 8400.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_3D_LimiterCorr":
        # 2023-05-07
        comparison.title = ""
        comparison.sim_names = [
            "torus_ecool_3DT1e9Mu21",
            "torus_ecool_3DT1e9Mu21_QcLimiterCorr",
            "torus_ecool_3DT1e9Mu25",
            "torus_ecool_3DT1e9Mu25_QcLimiterCorr",
            ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e9Mu21":r"$\dot m_0\sim10^{-7}$ half-step values; no limiter",
            "torus_ecool_3DT1e9Mu21_QcLimiterCorr":r"$\dot m_0$ corrector values and limiter",
            "torus_ecool_3DT1e9Mu25_tpte3":r"$10^4\dot m_0$ half-step values; no limiter",
            "torus_ecool_3DT1e9Mu25_QcLimiterCorr":r"$10^4\dot m_0$ corrector values and limiter",
        }
        comparison.last_common_time = 8000.0 # rg/c
        # comparison.last_common_time = 10000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_3D_Mdot21_LimiterCorr":
        # 2023-05-07
        comparison.title = ""
        comparison.sim_names = [
            "torus_ecool_3DT1e9Mu21",
            "torus_ecool_3DT1e9Mu21_QcLimiterCorr",
            ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e9Mu21":r"Half-step values; no limiter",
            "torus_ecool_3DT1e9Mu21_QcLimiterCorr":r"Corrector values; with Coulomb limiter",
        }
        comparison.last_common_time = 8000.0 # rg/c
        # comparison.last_common_time = 10000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_3D_Mdot25_LimiterCorr":
        # 2023-05-07
        comparison.title = ""
        comparison.sim_names = [
            "torus_ecool_3DT1e9Mu25_tpte3",
            "torus_ecool_3DT1e9Mu25_QcLimiter",
            "torus_ecool_3DT1e9Mu25_QcLimiterCorr",
            ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e9Mu25_tpte3":r"Half-step values; no limiter",
            "torus_ecool_3DT1e9Mu25_QcLimiter":r"Half-step values; with Coulomb limiter",
            "torus_ecool_3DT1e9Mu25_QcLimiterCorr":r"Corrector values; with Coulomb limiter",
        }
        comparison.last_common_time = 9000.0 # rg/c
        # comparison.last_common_time = 10000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_2D_Mdot25_LimiterCorr":
        # 2023-05-04
        comparison.title = ""
        comparison.sim_names = [
            "test_torus_2DT1e9Mu25",
            "test_torus_2DT1e9Mu25_QcLimiter-2",
            "test_torus_2DT1e9Mu25_QcLimiterCorr",
            ]
        comparison.sim_labels = {
            "test_torus_2DT1e9Mu25":r"Half-step values; no limiter",
            "test_torus_2DT1e9Mu25_QcLimiter-2":r"Half-step values; with Coulomb limiter",
            "test_torus_2DT1e9Mu25_QcLimiterCorr":r"Corrector values; with Coulomb limiter",
        }
        comparison.last_common_time = 5000.0 # rg/c
        # comparison.last_common_time = 10000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_2D_Mdot24-r23":
        # 2023-05-01
        comparison.title = ""
        comparison.sim_names = [
            "test_torus_2DT1e9Mu24-r23",
            "test_torus_2DT1e9Mu24-r23_QcLimiter",
            "test_torus_2DT1e9Mu24-r23_dt10",
            ]
        comparison.sim_labels = {
            "test_torus_2DT1e9Mu24-r23":r"$10^3\dot m_0$ restarted from $10^2\dot m_0$",
            "test_torus_2DT1e9Mu24-r23_QcLimiter":r"With Coulomb limiter",
            "test_torus_2DT1e9Mu24-r23_dt10":r"With $dt/10$",
        }
        # comparison.last_common_time = 2500.0 # rg/c
        comparison.last_common_time = 10000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_2D_MdotGradual":
        # 2023-05-01
        comparison.title = ""
        comparison.sim_names = [
            "test_torus_2DT1e9Mu21-2",
            "test_torus_2DT1e9Mu23",
            "test_torus_2DT1e9Mu24",
            # "test_torus_2DT1e9Mu24-r23",
            # "test_torus_2DT1e9Mu25",
            # "test_torus_2DT1e9Mu25_tc05-r", #restarted
            # "test_torus_2DT1e9Mu25_tc01-r", #restarted
            ]
        comparison.sim_labels = {
            "test_torus_2DT1e9Mu21-2":r"$\dot m_0\sim10^{-7}$",
            "test_torus_2DT1e9Mu23":r"$10^2\dot m_0$",
            "test_torus_2DT1e9Mu24-r23":r"$10^3\dot m_0$ restarted from $10^2\dot m_0$",
            "test_torus_2DT1e9Mu24":r"$10^3\dot m_0$",
        }
        # comparison.last_common_time = 2500.0 # rg/c
        comparison.last_common_time = 9000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_2D_varyTargetTe":
        # 2023-05-16
        comparison.title = ""
        comparison.sim_names = [
            "test_torus_2DT1e9Mu25",
            "test_torus_2DT1e9Mu25_tc01-r",
            "test_torus_2DT5e8Mu25_tc01",
            ]
        comparison.sim_labels = {
            "test_torus_2DT1e9Mu25":r"$T_{e,{\rm target}}=10^9~{\rm K}$, $t_{\rm cool}=1/\Omega$",
            "test_torus_2DT1e9Mu25_tc01-r":r"$T_{e,{\rm target}}=10^9~{\rm K}$, $t_{\rm cool}=10/\Omega$",
            "test_torus_2DT5e8Mu25_tc01":r"$T_{e,{\rm target}}=5\times10^8~{\rm K}$, $t_{\rm cool}=10/\Omega$",
        }
        # comparison.last_common_time = 2500.0 # rg/c
        comparison.last_common_time = 7300.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_2D_tcShort":
        # 2023-05-01, 2023-05-04, 2023-05-16
        comparison.title = ""
        comparison.sim_names = [
            # "test_torus_2DT1e9Mu21-2",
            # "test_torus_2DT1e9Mu21_tc01",
            "test_torus_2DT1e9Mu25",
            # "test_torus_2DT1e9Mu25_tc05-r", #restarted
            "test_torus_2DT1e9Mu25_tc01-r", #restarted
            "test_torus_2DT1e9Mu25_tc1-r", #restarted from 10/Omega
            ]
        comparison.sim_labels = {
            "test_torus_2DT1e9Mu21-2":r"$\dot m_0\sim10^{-7}$, $t_{\rm cool}=1/\Omega$",
            "test_torus_2DT1e9Mu25":r"$10^4\dot m_0$, $t_{\rm cool}=1/\Omega$",
            "test_torus_2DT1e9Mu21_tc01":r"$\dot m_0\sim10^{-7}$, $t_{\rm cool}=10/\Omega$",
            "test_torus_2DT1e9Mu25_tc01-r":r"$10^4\dot m_0$, $t_{\rm cool}=10/\Omega$",
            "test_torus_2DT1e9Mu25_tc1-r":r"$10^4\dot m_0$, $t_{\rm cool}=1/\Omega$ from $10/\Omega$",
            "test_torus_2DT1e9Mu25_tc05-r":r"$t_{\rm cool}=2/\Omega$",
        }
        # comparison.last_common_time = 2500.0 # rg/c
        comparison.last_common_time = 8500.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_2D_qtestAll":
        # 2023-05-03
        # Note these are not restarted.
        comparison.title = ""
        comparison.sim_names = [
            "test_torus_2DT1e9Mu25_q0125",
            "test_torus_2DT1e9Mu25",
            "test_torus_2DT1e9Mu25_q1",
            "test_torus_2DT1e9Mu25_q2",
            ]
        comparison.sim_labels = {
            "test_torus_2DT1e9Mu25":r"$q=0.5$",
            "test_torus_2DT1e9Mu25_q1":r"$q=1.0$",
            "test_torus_2DT1e9Mu25_q0125":r"$q=0.125$",
            "test_torus_2DT1e9Mu25_q2":r"$q=2.0$",
        }
        comparison.last_common_time = 5000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_2D_qtestHigh":
        # 2023-05-03
        # Note these are not restarted.
        comparison.title = ""
        comparison.sim_names = [
            "test_torus_2DT1e9Mu25",
            "test_torus_2DT1e9Mu25_q1",
            "test_torus_2DT1e9Mu25_q2",
            ]
        comparison.sim_labels = {
            "test_torus_2DT1e9Mu25":r"$q=0.5$",
            "test_torus_2DT1e9Mu25_q1":r"$q=1.0$",
            "test_torus_2DT1e9Mu25_q2":r"$q=2.0$",
        }
        comparison.last_common_time = 5000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_2D_qtest":
        # 2023-04-28
        comparison.title = ""
        comparison.sim_names = [
            "test_torus_2DT1e9Mu25",
            "test_torus_2DT1e9Mu25_q025-r",
            "test_torus_2DT1e9Mu25_q0125",
            "test_torus_2DT1e9Mu25_q0125-r",
            ]
        comparison.sim_labels = {
            "test_torus_2DT1e9Mu25":r"$q=0.5$",
            "test_torus_2DT1e9Mu25_q0125":r"$q=0.125$",
            "test_torus_2DT1e9Mu25_q0125-r":r"$q=0.125$ restarted",
            "test_torus_2DT1e9Mu25_q025-r":r"$q=0.25$ restarted",
        }
        comparison.last_common_time = 5000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_2D_supercooling":
        # Isothermal electron implementation from April 2023
        # 2023-04-27
        comparison.title = ""
        comparison.sim_names = [
            # "test_torus_2DT1e9Mu25",
            "test_torus_2DT1e9Mu25-3",
            # "test_torus_2DT1e9Mu25-4",
            "test_torus_2DT1e9Mu25-2",
            ]
        comparison.sim_labels = {
            "test_torus_2DT1e9Mu25":r"No max $L$, supercool when $\Delta u_e=L~Dt$",
            "test_torus_2DT1e9Mu25-3":"With max $L$, supercool when $\Delta u_e=L~Dt$",
            "test_torus_2DT1e9Mu25-4":"No max $L$, supercool when $\Delta u_e=L~Dt/u^0$",
            "test_torus_2DT1e9Mu25-2":"With max $L$, supercool when $\Delta u_e=L~Dt/u^0$",
        }
        comparison.sim_colors = {
            "test_torus_2DT1e9Mu25":colorblind_colors[0],
            "test_torus_2DT1e9Mu25-2":colorblind_colors[1],
            "test_torus_2DT1e9Mu25-3":colorblind_colors[0],
            "test_torus_2DT1e9Mu25-4":colorblind_colors[1],
        }
        comparison.last_common_time = 5000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_2D_QcoulFloor_Mu25":
        # Compare with/without Qcoul floor
        # 2023-02-21
        comparison.title = ""
        comparison.sim_names = [
            "test_torus_ecool_2DT1e9Mu25",
            "test_torus_ecool_2DT1e9Mu25Qcoulfloor",
            "test_torus_ecool_2DT1e9Mu25_noQcool",
        ]
        comparison.sim_labels = {
            "test_torus_ecool_2DT1e9Mu25":r"No $Q_{\rm coul}$ floor",
            "test_torus_ecool_2DT1e9Mu25Qcoulfloor":r"$Q_{\rm coul}>0$",
            "test_torus_ecool_2DT1e9Mu25_noQcool":r"No cooling (also no Coulomb)",
        }
        comparison.sim_colors = {
            "test_torus_ecool_2DT1e9Mu25":colorblind_colors[0],
            "test_torus_ecool_2DT1e9Mu25Qcoulfloor":colorblind_colors[1],
            "test_torus_ecool_2DT1e9Mu25_noQcool":colorblind_colors[2],
        }
        comparison.last_common_time = 10000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_resolution_Mu25":
        # Compare high-resolution and lower resolution
        # 2023-02-21, updated 2023-04-28 with hr
        comparison.title = ""
        comparison.sim_names = [
            "torus_ecool_3DT1e9Mu25_hr_restarted",
            "torus_ecool_3DT1e9Mu25_restarted",
            # "test_torus_ecool_2DT1e9Mu25",
        ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e9Mu25_hr_restarted":r"320x256x160 zones",
            "torus_ecool_3DT1e9Mu25_restarted":r"160x128x80 zones",
            "test_torus_ecool_2DT1e9Mu25":r"2D",
        }
        comparison.sim_colors = {
            "torus_ecool_3DT1e9Mu25":colorblind_colors[0],
            "test_torus_ecool_2DT1e9Mu25":colorblind_colors[1],
        }
        comparison.last_common_time = 15000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_3D_Mdot-Transition":
        comparison.title = r"Cooling time: $t_{\rm cool}=1/\Omega$"
        comparison.sim_names = [
            "torus_ecool_3DT1e9Mu23_restarted",
            # "torus_ecool_3DT1e9Mu231_restarted",
            "torus_ecool_3DT1e9Mu235_restarted",
            "torus_ecool_3DT1e9Mu24_restarted",
        ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e9Mu23_restarted":r"$\dot m_0\sim10^{-5}$",
            "torus_ecool_3DT1e9Mu231_restarted":r"$1.6\dot m_0$",
            "torus_ecool_3DT1e9Mu235_restarted":r"$5\dot m_0$",
            "torus_ecool_3DT1e9Mu24_restarted":r"$10\dot m_0$",
        }
        comparison.sim_colors = {
            "torus_ecool_3DT1e9Mu23_restarted":colorblind_colors[0],
            "torus_ecool_3DT1e9Mu231_restarted":colorblind_colors[1],
            "torus_ecool_3DT1e9Mu235_restarted":colorblind_colors[2],
            "torus_ecool_3DT1e9Mu24_restarted":colorblind_colors[3],
        }
        comparison.last_common_time = 15000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_3D_MdotHigh":
        # Changed 2023-04-07 plot label (fix eddington error)
        comparison.title = r"Cooling time: $t_{\rm cool}=1/\Omega$"
        comparison.sim_names = [
            "torus_ecool_3DT1e9Mu21",
            "torus_ecool_3DT1e9Mu23_restarted",
            "torus_ecool_3DT1e9Mu24_restarted",
            "torus_ecool_3DT1e9Mu25_restarted",
        ]
        comparison.sim_labels = {
            # "torus_ecool_3DT1e9Mu21":r"$\dot m_0\sim10^{-7}$",
            # "torus_ecool_3DT1e9Mu23_restarted":r"$10^2\dot m_0$",
            # "torus_ecool_3DT1e9Mu24_restarted":r"$10^3\dot m_0$",
            # "torus_ecool_3DT1e9Mu25_restarted":r"$10^4\dot m_0$",
            "torus_ecool_3DT1e9Mu21":r"M7",
            "torus_ecool_3DT1e9Mu23_restarted":r"M5",
            "torus_ecool_3DT1e9Mu24_restarted":r"M4",
            "torus_ecool_3DT1e9Mu25_restarted":r"M3",
        }
        comparison.sim_colors = {
            "torus_ecool_3DT1e9Mu21":colorblind_colors[0],
            "torus_ecool_3DT1e9Mu23_restarted":colorblind_colors[1],
            "torus_ecool_3DT1e9Mu24_restarted":colorblind_colors[2],
            "torus_ecool_3DT1e9Mu25_restarted":colorblind_colors[3],
        }
        comparison.last_common_time = 15000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_3D_MdotThree":
        # Changed 2023-04-07 plot label (fix eddington error)
        comparison.title = r"Cooling time: $t_{\rm cool}=1/\Omega$"
        comparison.sim_names = [
            "torus_ecool_3DT1e9Mu21",
            "torus_ecool_3DT1e9Mu24_restarted",
            "torus_ecool_3DT1e9Mu25_restarted",
        ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e9Mu21":r"$\dot m_0\sim10^{-7}$",
            "torus_ecool_3DT1e9Mu24_restarted":r"$10^3\dot m_0$",
            "torus_ecool_3DT1e9Mu25_restarted":r"$10^4\dot m_0$",
        }
        comparison.sim_colors = {
            "torus_ecool_3DT1e9Mu21":colorblind_colors[0],
            "torus_ecool_3DT1e9Mu24_restarted":colorblind_colors[1],
            "torus_ecool_3DT1e9Mu25_restarted":colorblind_colors[2],
        }
        comparison.last_common_time = 15000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_3D_Mdot_tc5":
        # Changed 2023-04-07 plot label (fix eddington error)
        # Run on 2023-01-16.
        comparison.title = r"Cooling time: $t_{\rm cool}=1/5\Omega$"
        comparison.sim_names = [
            "torus_ecool_3DT1e9Mu21tc5",
            "torus_ecool_3DT1e9Mu23tc5",
            "torus_ecool_3DT1e9Mu25tc5",
        ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e9Mu21tc5":r"$\dot m_0\sim10^{-7}$",
            "torus_ecool_3DT1e9Mu23tc5":r"$10^2\dot m_0$",
            "torus_ecool_3DT1e9Mu25tc5":r"$10^2\dot m_0$",
        }
        comparison.sim_colors = {
            "torus_ecool_3DT1e9Mu21tc5":colorblind_colors[0],
            "torus_ecool_3DT1e9Mu23tc5":colorblind_colors[1],
            "torus_ecool_3DT1e9Mu25tc5":colorblind_colors[2],
        }
        comparison.last_common_time = 10000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_2D_dt_Mu25":
        # Comparing how hardcoding the timestep changes the evolution
        # 2023-04-25
        comparison.title = ""
        comparison.sim_names = [
            "test_torus_2DT1e9Mu25",
            "test_torus_2DT1e9Mu25_dt10",
            ]
        comparison.sim_labels = {
            "test_torus_2DT1e9Mu25":"Normal dt",
            "test_torus_2DT1e9Mu25_dt10":"dt decreased by 10",
        }
        comparison.last_common_time = 5000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_2D_Lcutoff_Mu25":
        # Compare L=0 triggering when uel < dt*L or uel < dt*L/100
        # 2023-02-23
        comparison.title = ""
        comparison.sim_names = [
            "test_torus_ecool_2DT1e9Mu25",
            "test_torus_ecool_2DT1e9Mu25_L001",
        ]
        comparison.sim_labels = {
            "test_torus_ecool_2DT1e9Mu25":r"$L=0$ when $u_e<Ldt$",
            "test_torus_ecool_2DT1e9Mu25_L001":r"$L=0$ when $u_e<Ldt/100$",
        }
        comparison.sim_colors = {
            "test_torus_ecool_2DT1e9Mu25":colorblind_colors[0],
            "test_torus_ecool_2DT1e9Mu25_L001":colorblind_colors[1],
        }
        comparison.last_common_time = 10000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    # Test problems with fixed Te and fake Coulomb heating
    # and other isothermal electron implementation, but all
    # do NOT include implicit Coulomb heating and thus are
    # ultimately not good in the high density regime.
    elif category == "compare_2D_fakeQcoul":
        # Isothermal electron implementation from April 2023
        # 2023-04-26
        comparison.title = ""
        comparison.sim_names = [
            "test_torus_2DT1e9Mu21-2",
            "test_torus_2DT1e9Mu21_fixedTeNotIsothermal",
            "test_torus_2DT1e9Mu25",
            "test_torus_2DT1e9Mu25_fixedTeNotIsothermal",
            ]
        comparison.sim_labels = {
            "test_torus_2DT1e9Mu21-2":r"$\dot m_0$",
            "test_torus_2DT1e9Mu21_fixedTeNotIsothermal":"$\dot m_0$, Coulomb uses $T_e=10^9$ K",
            "test_torus_2DT1e9Mu25":"$10^4\dot m_0$",
            "test_torus_2DT1e9Mu25_fixedTeNotIsothermal":"$10^4\dot m_0$, Coulomb uses $T_e=10^9$ K",
        }
        comparison.sim_colors = {
            "test_torus_2DT1e9Mu21-2":colorblind_colors[0],
            "test_torus_2DT1e9Mu21_fixedTeNotIsothermal":colorblind_colors[1],
            "test_torus_2DT1e9Mu25":colorblind_colors[0],
            "test_torus_2DT1e9Mu25_fixedTeNotIsothermal":colorblind_colors[1],
        }
        comparison.last_common_time = 5000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_2D_fakeQcoul_Mu25":
        # Isothermal electron implementation from April 2023
        # 2023-04-25
        comparison.title = ""
        comparison.sim_names = [
            "test_torus_2DT1e9Mu25",
            "test_torus_2DT1e9Mu25_fixedTeNotIsothermal",
            ]
        comparison.sim_labels = {
            "test_torus_2DT1e9Mu25":"Regular electrons",
            "test_torus_2DT1e9Mu25_fixedTeNotIsothermal":"Coulomb set using $T_e=10^9$ K",
        }
        comparison.last_common_time = 5000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_2D_Mu26":
        # Isothermal electron implementation from April 2023
        # 2023-04-28
        comparison.title = ""
        comparison.sim_names = [
            "test_torus_2DT1e9Mu25",
            "test_torus_2DT1e9Mu26",
            ]
        comparison.sim_labels = {
            "test_torus_2DT1e9Mu25":r"$10^4\dot m_0$",
            "test_torus_2DT1e9Mu26":r"$10^5\dot m_0$",
        }
        comparison.last_common_time = 5000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_2D_fakeQcoul_Mu21":
        # Isothermal electron implementation from April 2023
        # 2023-04-25
        comparison.title = ""
        comparison.sim_names = [
            "test_torus_2DT1e9Mu21-2",
            "test_torus_2DT1e9Mu21_fixedTeNotIsothermal",
            ]
        comparison.sim_labels = {
            "test_torus_2DT1e9Mu21-2":"Regular electrons",
            "test_torus_2DT1e9Mu21_fixedTeNotIsothermal":"Coulomb set using $T_e=10^9$ K",
        }
        comparison.last_common_time = 5000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_2D_isothermalE":
        # Isothermal electron implementation from April 2023
        # 2023-04-26
        comparison.title = ""
        comparison.sim_names = [
            "test_torus_2DT1e9Mu21-2",
            "test_torus_2DT1e9Mu21_fixedTe",
            "test_torus_2DT1e9Mu25",
            "test_torus_2DT1e9Mu25_fixedTe",
            "test_torus_2DT1e9Mu25_isothermalE-2",
            ]
        comparison.sim_labels = {
            "test_torus_2DT1e9Mu21-2":r"$\dot m_0$",
            "test_torus_2DT1e9Mu21_fixedTe":"$\dot m_0$, isothermal electrons",
            "test_torus_2DT1e9Mu25":"$10^4\dot m_0$",
            "test_torus_2DT1e9Mu25_fixedTe":"$10^4\dot m_0$, isothermal electrons",
            "test_torus_2DT1e9Mu25_isothermalE-2":"$10^4\dot m_0$, isothermal electrons",
        }
        comparison.sim_colors = {
            "test_torus_2DT1e9Mu21-2":colorblind_colors[0],
            "test_torus_2DT1e9Mu21_fixedTe":colorblind_colors[1],
            "test_torus_2DT1e9Mu25":colorblind_colors[0],
            "test_torus_2DT1e9Mu25_fixedTe":colorblind_colors[1],
        }
        comparison.last_common_time = 5000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_2D_isothermalE_Mu21":
        # Isothermal electron implementation from April 2023
        # 2023-04-21
        comparison.title = ""
        comparison.sim_names = [
            "test_torus_2DT1e9Mu21-2",
            # "test_torus_2DT1e9Mu21_fixedTe",
            "test_torus_2DT1e9Mu21_isothermalE-2",
            ]
        comparison.sim_labels = {
            "test_torus_2DT1e9Mu21-2":"Regular electrons",
            "test_torus_2DT1e9Mu21_fixedTe":"Isothermal electrons",
            "test_torus_2DT1e9Mu21_isothermalE-2":"Isothermal electrons",
        }
        comparison.last_common_time = 5000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_2D_isothermalE_Mu25":
        # Isothermal electron implementation from April 2023
        # 2023-04-21, 2023-04-28
        comparison.title = ""
        comparison.sim_names = [
            "test_torus_2DT1e9Mu25",
            "test_torus_2DT1e9Mu25_fixedTe",
            "test_torus_2DT1e9Mu25_isothermalE-2",
            ]
        comparison.sim_labels = {
            "test_torus_2DT1e9Mu25":"Regular electrons",
            "test_torus_2DT1e9Mu25_fixedTe":"Isothermal electrons (old implementation)",
            "test_torus_2DT1e9Mu25_isothermalE-2":"Isothermal electrons",
        }
        comparison.last_common_time = 5000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "compare_2D_isothermal_Mu25":
        # Isothermal electron implementation.
        # Since I also changed how cooling was implemented,
        # wanted to compare without isothermal
        # 2023-02-23
        comparison.title = ""
        comparison.sim_names = [
            # "test_torus_ecool_2DT1e9Mu25_isothermal",  # mistake
            # "test_torus_ecool_2DT1e9Mu25_notIsothermal", # mistake
            "test_torus_ecool_2DT1e9Mu25_isothermal2",
            "test_torus_ecool_2DT1e9Mu25_notIsothermal2",
            "test_torus_ecool_2DT1e9Mu25_halfStep",
        ]
        comparison.sim_labels = {
            "test_torus_ecool_2DT1e9Mu25_isothermal":r"Isothermal, no sigmacut",
            "test_torus_ecool_2DT1e9Mu25_notIsothermal":r"radG = 0 always",
            "test_torus_ecool_2DT1e9Mu25_isothermal2":r"Isothermal",
            "test_torus_ecool_2DT1e9Mu25_notIsothermal2":r"New cooling implementation, not isothermal",
            "test_torus_ecool_2DT1e9Mu25_halfStep":r"Old cooling implementation, not isothermal",
        }
        comparison.sim_colors = {
            # "test_torus_ecool_2DT1e9Mu25_isothermal":colorblind_colors[0],
            # "test_torus_ecool_2DT1e9Mu25_notIsothermal":colorblind_colors[1],
            "test_torus_ecool_2DT1e9Mu25_isothermal2":colorblind_colors[0],
            "test_torus_ecool_2DT1e9Mu25_notIsothermal2":colorblind_colors[1],
            "test_torus_ecool_2DT1e9Mu25_halfStep":colorblind_colors[2],
        }
        comparison.last_common_time = 10000.0 # rg/c
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    # Test problems fixing the electrons to a target temperature.
    elif category == "test_isothermal":
        # Compare viscous heating and isothermal electrons
        # 2022-12-15
        comparison.sim_names = [
            "torus_ecool_3DT1e9Mu21",
            "torus_ecool_3DT1e9Mu21_isothermal",
        ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e9Mu21":r"Viscous heating",
            "torus_ecool_3DT1e9Mu21_isothermal":r"Isothermal electrons",
        }
        comparison.last_common_time = 417
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044

    # Test problems injecting a small amount of viscous heating
    # so as not to trigger the KEL ceiling. These tests show
    # that the problems with Te exceeding the target temperature
    # within the ISCO and being too small by ~20% outside the ISCO
    # are a result of the MHD heating/truncation error. Those problems
    # are not present in these test problems.
    # These do NOT include implicit Coulomb heating.
    elif category == "test_3D_compare_Qv":
        # Run on 2022-11-16. Probably didn't run for long enough to be
        # super useful, but still ok for general idea.
        comparison.sim_names = [
            "torus_ecool_3DT1e9Mu21_noQcnoQcoolQv1em6",
            "torus_ecool_3DT1e9Mu21_noQcnoQcool",
            "torus_ecool_3DT1e9Mu21_noQc",
            "torus_ecool_3DT1e9Mu21_noQcQv1em6",
        ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e9Mu21_noQcnoQcoolQv1em6":r"No cooling, Injected",
            "torus_ecool_3DT1e9Mu21_noQcnoQcool":r"No cooling, MHD",
            "torus_ecool_3DT1e9Mu21_noQcQv1em6":r"$t_{\rm cool}=1/\Omega$, Injected",
            "torus_ecool_3DT1e9Mu21_noQc":r"$t_{\rm cool}=1/\Omega$, MHD",
        }
        comparison.last_common_time = 388
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_3D_cooling_Qv1em6":
        # Run on 2022-11-16. Probably didn't run for long enough to be
        # super useful, but still ok for general idea.
        comparison.sim_names = [
            "torus_ecool_3DT1e9Mu21_Qv1em6",
            "torus_ecool_3DT1e9Mu21_noQcQv1em6",
            "torus_ecool_3DT1e9Mu21_noQcnoQcoolQv1em6",
        ]
        comparison.sim_labels = {
            "torus_ecool_3DT1e9Mu21_Qv1em6":r"$t_{\rm cool}=1/\Omega$",
            "torus_ecool_3DT1e9Mu21_noQcQv1em6":r"$t_{\rm cool}=1/\Omega$, no Coulomb collisions",
            "torus_ecool_3DT1e9Mu21_noQcnoQcoolQv1em6":r"No cooling, no Coulomb collisions",
        }
        comparison.last_common_time = 339
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_Qv1em6Qc":
        # Compare Coulomb collisions presence/absence
        # 2022-09-30
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21_noQcQv1em6noQcool",
            "test_torus_ecool_T1e9Mu21_noQcQv1em6",
            "test_torus_ecool_T1e9Mu21_QcQv1em6",
        ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21_noQcQv1em6noQcool":r"No cooling",
            "test_torus_ecool_T1e9Mu21_noQcQv1em6":r"No Coulomb collisions",
            "test_torus_ecool_T1e9Mu21_QcQv1em6":r"With Coulomb collisions",
        }
        comparison.last_common_time = 800
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_compare_Qv1em6":
        # Compare no cooling and cooling times
        # 2022-09-29
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21_noQcQv1em6noQcool",
            "test_torus_ecool_T1e9Mu21_noQcQvMHDnoQcool",
            "test_torus_ecool_T1e9Mu21_noQcQvMHD",
            "test_torus_ecool_T1e9Mu21_noQcQv1em6",
        ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21_noQcQv1em6noQcool":r"No cooling, Injected",
            "test_torus_ecool_T1e9Mu21_noQcQv1em6":r"$t_{\rm cool}=1/\Omega$, Injected",
            "test_torus_ecool_T1e9Mu21_noQcQvMHD":r"$t_{\rm cool}=1/\Omega$, MHD",
            "test_torus_ecool_T1e9Mu21_noQcQvMHDnoQcool":r"No cooling, MHD",
        }
        comparison.last_common_time = 800
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_Qv1em6":
        # Compare no cooling and cooling times
        # 2022-09-29
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21_noQcQv1em6noQcool",
            "test_torus_ecool_T1e9Mu21_noQcQv1em6",
            "test_torus_ecool_T1e9Mu21_noQcQv1em6_tc5",
        ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21_noQcQv1em6noQcool":r"No cooling",
            "test_torus_ecool_T1e9Mu21_noQcQv1em6":r"$t_{\rm cool}=1/\Omega$",
            "test_torus_ecool_T1e9Mu21_noQcQv1em6_tc5":r"$t_{\rm cool}=1/5\Omega$",
        }
        comparison.last_common_time = 800
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_MHDheating_tcool":
        # Compare MHD heating simulations with NO Coulomb collisions.
        # Test no cooling and different cooling times.
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21_noQcQvMHDnoQcool",
            "test_torus_ecool_T1e9Mu21_noQcQvMHD",
            "test_torus_ecool_T1e9Mu21_noQcQvMHD_tc5",
        ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21_noQcQvMHD":r"$t_{\rm cool}=1/\Omega$",
            "test_torus_ecool_T1e9Mu21_noQcQvMHDnoQcool":r"No cooling",
            "test_torus_ecool_T1e9Mu21_noQcQvMHD_tc5":r"$t_{\rm cool}=1/5\Omega$",
        }
        comparison.last_common_time = 800
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044

    # ----------------------------------------------
    # Coulomb collisions ALWAYS ON
    # ----------------------------------------------
    elif category == "test_sigmaCut_3D":
        comparison.sim_names = [
            "test_torus_ecool_3DT1e9Mu21_lowres",
            "test_torus_ecool_3DT1e9Mu21_sigmacut10",
        ]
        comparison.sim_labels = {
            "test_torus_ecool_3DT1e9Mu21_lowres":r"$\sigma_c=1$",
            "test_torus_ecool_3DT1e9Mu21_sigmacut10":r"$\sigma_c=10$",
        }
        comparison.last_common_time = 2000
        # comparison.last_common_time = 418 # if including 1em1
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_tcoolOmega_noNegQcool":
        # Now that Qcool>0 always, how much impact does changing the
        # cooling time have? noNegQcool sims done 2022-07-15.
        # NOTE Coulomb collisions are ON.
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21_noNegTcool2",
            # "test_torus_ecool_T1e9Mu21TO2_noNegQcool",
            "test_torus_ecool_T1e9Mu21TO3_noNegQcool"
        ]
        comparison.sim_labels = {
            # "test_torus_ecool_T1e20Mu21":r"No cooling",
            "test_torus_ecool_T1e9Mu21_noNegTcool2":r"$t_{{\rm cool}}=1/\Omega$",
            "test_torus_ecool_T1e9Mu21TO2_noNegQcool":r"$t_{{\rm cool}}=1/2\Omega$",
            "test_torus_ecool_T1e9Mu21TO3_noNegQcool":r"$t_{{\rm cool}}=1/3\Omega$"
        }
        comparison.last_common_time = 501
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_sigmaCut_noNegQcool":
        # All of these have M_unit = 8e21, so Coulomb collisions negligible
        # BUT NOT TURNED OFF!
        # All have tcool=1/Omega.
        # They were run after the explicit "don't cool if L < 0" edit
        # done on 2022-07-12ish. sigmaCut10 and noSigmaCut were run
        # on 2022-07-14.
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21_noSigmaCut_noNegQcool",
            "test_torus_ecool_T1e9Mu21_sigmaCut10_noNegQcool",
            "test_torus_ecool_T1e9Mu21_noNegTcool2"
            ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21_noSigmaCut_noNegQcool":r"No $\sigma_{{\rm cut}}$",
            "test_torus_ecool_T1e9Mu21_noNegTcool2":r"$\sigma_{{\rm cut}}=1$",
            "test_torus_ecool_T1e9Mu21_sigmaCut10_noNegQcool":r"$\sigma_{{\rm cut}}=10$"
        }
        comparison.last_common_time = 501
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    else:
        print(category + " is unknown.")
        return False
    if comparison.line_styles is None:
        comparison.line_styles = {}
        for sim in comparison.sim_names:
            comparison.line_styles[sim] = '-'
    # ----------------------------------------------
    # Tests that prescribe a certain amount of viscous
    # heating; in this case, WAY TOO MUCH viscous heating.
    # Any simulation with "Qv1" in the name probably bumps
    # up against the KEL ceiling resulting from Tp/Te ceiling.
    # See (archive; detailed) Prescribing Electron heating
    # note. Details include setting different ktots, time step,
    # supercooling cut-off, setting radG at full step not half.
    # Done around August/September 2022.
    elif category == "test_QvInjected":
        # Compare different amounts of injected heating to see
        # which amount doesn't hit the KEL ceiling.
        # 2022-09-29 ish
        comparison.sim_names = [
            # "test_torus_ecool_T1e9Mu21_noQcQv1",
            "test_torus_ecool_T1e9Mu21_noQcQv01",
            # "test_torus_ecool_T1e9Mu21_noQcQv0001",
            # "test_torus_ecool_T1e9Mu21_noQcQv00001",
            "test_torus_ecool_T1e9Mu21_noQcQv1em6",
        ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21_noQcQv1":r"$Q_{\rm visc}={\rm Huge}$",
            "test_torus_ecool_T1e9Mu21_noQcQv01":r"$Q_{\rm visc}={\rm Not so huge}$",
            "test_torus_ecool_T1e9Mu21_noQcQv0001":r"$Q_{\rm visc}={\rm small}$",
            "test_torus_ecool_T1e9Mu21_noQcQv00001":r"$Q_{\rm visc}={\rm verysmall}$",
            "test_torus_ecool_T1e9Mu21_noQcQv1em6":r"$Q_{\rm visc}={\rm supersmall}$",
        }
        comparison.last_common_time = 800
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_Qv1_compareCooling":
        # Both set Kharm
        # Both have viscous heating set to 1 (HUGE)
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21_noQcQv1noQcool",
            "test_torus_ecool_T1e9Mu21_noQcQv1_tc0001",
            "test_torus_ecool_T1e9Mu21_noQcQv1",
        ]
        comparison.line_styles = {
            "test_torus_ecool_T1e9Mu21_noQcQv1":"-",
            "test_torus_ecool_T1e9Mu21_noQcQv1noQcool":'-',
            "test_torus_ecool_T1e9Mu21_noQcQv1_tc0001":':'
        }
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21_noQcQv1":r"$t_{\rm cool}=1/\Omega$",
            "test_torus_ecool_T1e9Mu21_noQcQv1noQcool":r"Without cooling",
            "test_torus_ecool_T1e9Mu21_noQcQv1_tc0001":r"$t_{\rm cool}=10^3/\Omega$",
        }
        comparison.last_common_time = 334
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_MHDheating":
        # Artificial heating sets Kharm
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21_noQcQv1noQcool",
            "test_torus_ecool_T1e9Mu21_noQcQv1",
            # "test_torus_ecool_T1e9Mu21_noQcQv1_tc5",
            "test_torus_ecool_T1e9Mu21_noQvnoQcnoQcool",
            "test_torus_ecool_T1e9Mu21_noQcQvMHD",
            "test_torus_ecool_T1e9Mu21_noQcQvMHDnoQcool",
            # "test_torus_ecool_T1e9Mu21_noQcQvMHD_tc5",
        ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21_noQcQv1_tc5":r"Artificial heating, $t_{\rm cool}=1/5\Omega$",
            "test_torus_ecool_T1e9Mu21_noQcQv1":r"Artificial heating, $t_{\rm cool}=1/\Omega$",
            "test_torus_ecool_T1e9Mu21_noQcQv1noQcool":r"Artificial heating, no cooling",
            "test_torus_ecool_T1e9Mu21_noQvnoQcnoQcool":r"No heating, no cooling",
            "test_torus_ecool_T1e9Mu21_noQcQvMHD":r"MHD heating, $t_{\rm cool}=1/\Omega$",
            "test_torus_ecool_T1e9Mu21_noQcQvMHDnoQcool":r"MHD heating, no cooling",
            "test_torus_ecool_T1e9Mu21_noQcQvMHD_tc5":r"MHD heating, $t_{\rm cool}=1/5\Omega$",
        }
        comparison.last_common_time = 674
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_rin20_injectedQv":
        # Both set Kharm
        # Both have viscous heating set to 1 (HUGE)
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21_noQvnoQc_rin20",
            "test_torus_ecool_T1e9Mu21_noQcQvT_rin20",
            "test_torus_ecool_T1e9Mu21_noQcQv1_rin20",
        ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21_noQvnoQc_rin20":r"$Q_{\rm visc}=0$",
            "test_torus_ecool_T1e9Mu21_noQcQvT_rin20":r"$Q_{\rm visc}=$ truncation scheme",
            "test_torus_ecool_T1e9Mu21_noQcQv1_rin20":r"$Q_{\rm visc}=1.0$ (large)",
        }
        comparison.last_common_time = 700
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_Qv1_postCorrector":
        # Both set Kharm
        # Both have viscous heating set to 1 (HUGE)
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21_noQcQv1",
            "test_torus_ecool_T1e9Mu21_noQcQv1_postCorrector",
        ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21_noQcQv1":r"Half-step",
            "test_torus_ecool_T1e9Mu21_noQcQv1_postCorrector":r"Post corrector step",
        }
        comparison.last_common_time = 800
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_Qv1_compareCoolingTime":
        # Both set Kharm
        # Both have viscous heating set to 1 (HUGE)
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21_noQcQv1noQcool",
            "test_torus_ecool_T1e9Mu21_noQcQv1",
            "test_torus_ecool_T1e9Mu21_noQcQv1_tc5",
        ]
        comparison.line_styles = {
            "test_torus_ecool_T1e9Mu21_noQcQv1":"-",
            "test_torus_ecool_T1e9Mu21_noQcQv1noQcool":'-',
            "test_torus_ecool_T1e9Mu21_noQcQv1_tc5":'-',
        }
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21_noQcQv1":r"$t_{\rm cool}=1/\Omega$",
            "test_torus_ecool_T1e9Mu21_noQcQv1noQcool":r"Without cooling",
            "test_torus_ecool_T1e9Mu21_noQcQv1_tc5":r"$t_{\rm cool}=1/5\Omega$",
        }
        comparison.last_common_time = 517
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_Qv1_compareSupercoolingCutoff":
        # Both set Kharm
        # Both have viscous heating set to 1 (HUGE)
        # the original flags supercooling for uel < L dt,
        # whereas sc10 flags it for uel < L dt/10
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21_noQcQv1",
            # "test_torus_ecool_T1e9Mu21_noQcQv1_v2",
            "test_torus_ecool_T1e9Mu21_noQcQv1_sc10",
        ]
        comparison.line_styles = {
            "test_torus_ecool_T1e9Mu21_noQcQv1":"-",
            "test_torus_ecool_T1e9Mu21_noQcQv1_v2":"-",
            "test_torus_ecool_T1e9Mu21_noQcQv1_sc10":'-',
        }
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21_noQcQv1":r"$uel<L~dt$",
            "test_torus_ecool_T1e9Mu21_noQcQv1_v2":r"$uel<L~dt$ v2",
            "test_torus_ecool_T1e9Mu21_noQcQv1_sc10":r"$uel<L~dt/10$",
        }
        comparison.last_common_time = 534
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_QvT_rin20":
        # Both set Kharm
        # NOTE this test thought had Qvisc=1, but actually wasn't setting
        # ktotharm so they have the QvTruncated.
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21_noQcQv1noQcool_rin20",
            "test_torus_ecool_T1e9Mu21_noQcQvT_rin20",
            "test_torus_ecool_T1e9Mu21_noQcQvT_rin20_tc5",
            "test_torus_ecool_T1e9Mu21_noQcQvT_rin20_tc10",
        ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21_noQcQv1noQcool_rin20":r"Without cooling",
            "test_torus_ecool_T1e9Mu21_noQcQvT_rin20":r"$t_{\rm cool}=1/\Omega$",
            "test_torus_ecool_T1e9Mu21_noQcQvT_rin20_tc5":r"$t_{\rm cool}=1/5\Omega$",
            "test_torus_ecool_T1e9Mu21_noQcQvT_rin20_tc10":r"$t_{\rm cool}=1/10\Omega$",
        }
        comparison.last_common_time = 710
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_Qv1_compareCoolingVariation":
        # Both set Kharm
        # Both have viscous heating set to 1 (HUGE)
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21_noQcQv1noQcool",
            "test_torus_ecool_T1e9Mu21_noQcQv1",
            "test_torus_ecool_T1e9Mu21_noQcQv1_v2",
            "test_torus_ecool_T1e9Mu21_noQcQv1_v3",
        ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21_noQcQv1":r"With cooling",
            "test_torus_ecool_T1e9Mu21_noQcQv1noQcool":r"Without cooling",
            "test_torus_ecool_T1e9Mu21_noQcQv1_v2":r"With cooling (take 2)",
            "test_torus_ecool_T1e9Mu21_noQcQv1_v3":r"With cooling (take 3)",
        }
        comparison.last_common_time = 334
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    # ----------------------------------------------
    # Tests that prescribe a certain amount of viscous
    # heating; in this case, NO viscous heating.
    # See (archive; detailed) Prescribing Electron heating
    # note. Details include setting different values,
    # 2D vs. 3D explorations (including learning that
    # in 2D adiabatic heating is much less efficient).
    # Also, 2D with r=20rg is a BAD idea...the dynamics
    # get very strange because of channel modes.
    # ----------------------------------------------
    elif category == "test_noQv_rin20_postCorrector":
        # Both set Kharm
        # Both have viscous heating set to 1 (HUGE)
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21_noQvnoQc_rin20",
            "test_torus_ecool_T1e9Mu21_noQvnoQc_rin20_postCorrector",
        ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21_noQvnoQc_rin20":r"Half-step",
            "test_torus_ecool_T1e9Mu21_noQvnoQc_rin20_postCorrector":r"Post corrector step",
        }
        comparison.last_common_time = 800
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_noQv_rin20_radG":
        # 2022-09-12. Pointless!
        # both set ktotharm
        # both set Qvisc = 0
        # both catch supercooling for uel < L dt
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21_noQvnoQc_rin20",
            "test_torus_ecool_T1e9Mu21_noQvnoQc_rin20_noRadG",
            "test_torus_ecool_T1e9Mu21_noQvnoQc_rin20_noRadGFluid",
        ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21_noQvnoQc_rin20":r"With radG",
            "test_torus_ecool_T1e9Mu21_noQvnoQc_rin20_noRadG":r"Set radG = 0 (no cooling)",
            "test_torus_ecool_T1e9Mu21_noQvnoQc_rin20_noRadGFluid":r"no radG on total fluid",
        }
        comparison.last_common_time = 800
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_noQv_rin20_tc10_radG":
        # 2022-09-12
        # both set ktotharm
        # both set Qvisc = 0
        # both catch supercooling for uel < L dt
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21_noQvnoQc_rin20_tc10",
            "test_torus_ecool_T1e9Mu21_noQvnoQc_rin20_tc10_noRadG",
        ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21_noQvnoQc_rin20_tc10":r"With radG",
            "test_torus_ecool_T1e9Mu21_noQvnoQc_rin20_tc10_noRadG":r"Without radG",
        }
        comparison.last_common_time = 800
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_noQv_rin20_tcool":
        # 2022-09-12
        # both set ktotharm
        # both set Qvisc = 0
        # both catch supercooling for uel < L dt
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21_noQvnoQc_rin20",
            "test_torus_ecool_T1e9Mu21_noQvnoQc_rin20_tc5",
            "test_torus_ecool_T1e9Mu21_noQvnoQc_rin20_tc10",
        ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21_noQvnoQc_rin20":r"$t_{\rm cool}=1/\Omega$",
            "test_torus_ecool_T1e9Mu21_noQvnoQc_rin20_tc5":r"$t_{\rm cool}=1/5\Omega$",
            "test_torus_ecool_T1e9Mu21_noQvnoQc_rin20_tc10":r"$t_{\rm cool}=1/10\Omega$",
        }
        comparison.last_common_time = 800
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_noQv_rin":
        # 2022-09-12
        # both set ktotharm
        # both set Qvisc = 0
        # both have tcool=1/Omega
        # both catch supercooling for uel < L dt
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21_noQvnoQcdt_setKHarm",
            "test_torus_ecool_T1e9Mu21_noQvnoQc_rin20",
            "test_torus_ecool_T1e9Mu21_noQvnoQcnoQcool_rin20",
        ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21_noQvnoQcdt_setKHarm":r"$r_{\rm in}=6~r_g$",
            "test_torus_ecool_T1e9Mu21_noQvnoQc_rin20":r"$r_{\rm in}=20~r_g$",
            "test_torus_ecool_T1e9Mu21_noQvnoQcnoQcool_rin20":r"$r_{\rm in}=20~r_g$, no cooling",
        }
        comparison.last_common_time = 800
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_3D_noQv_compareCoulomb":
        # Both set Kharm?
        # Both have viscous heating set to zero
        comparison.sim_names = [
            "test_torus_ecool_3DT1e9Mu21_noQvdt_setKHarm",
            "test_torus_ecool_3DT1e9Mu21_noQvnoQc",
            "test_torus_ecool_3DT1e9Mu21_noQvnoQcnoQcool",
        ]
        comparison.sim_labels = {
            "test_torus_ecool_3DT1e9Mu21_noQvdt_setKHarm":r"With Coulomb collisions",
            "test_torus_ecool_3DT1e9Mu21_noQvnoQc":r"No Coulomb collisions",
            "test_torus_ecool_3DT1e9Mu21_noQvnoQcnoQcool":r"No cooling or Coulomb collisions",
        }
        comparison.last_common_time = 514
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_2D_noQc":
        # Both set ktotharm
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21_noQvdt_setKHarm",
            "test_torus_ecool_T1e9Mu21_noQvnoQcdt_setKHarm",
            "test_torus_ecool_T1e9Mu21_noQvnoQcnoQcool",
        ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21_noQvdt_setKHarm":r"Coulomb collisions",
            "test_torus_ecool_T1e9Mu21_noQvnoQcdt_setKHarm":r"No Coulomb collisions",
            "test_torus_ecool_T1e9Mu21_noQvnoQcnoQcool":r"No Coulomb or cooling",
        }
        comparison.last_common_time = 800
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_2D_noQv_dt":
        # Both set ktotadv
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21_noQv",
            "test_torus_ecool_T1e9Mu21_noQvdt",
            "test_torus_ecool_T1e9Mu21_noQvdt_setKHarm",
        ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21_noQv":r"Limited by $\tau_0$, sets ktotadv",
            "test_torus_ecool_T1e9Mu21_noQvdt":r"Limited by dt, sets ktotadv",
            "test_torus_ecool_T1e9Mu21_noQvdt_setKHarm":r"Limited by dt, sets ktotharm"
        }
        comparison.last_common_time = 534
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_3D_noQv_compareK":
        comparison.sim_names = [
            "test_torus_ecool_3DT1e9Mu21_noQvdt_setKHarm",
            "test_torus_ecool_3DT1e9Mu21_noQvdt",
        ]
        comparison.sim_labels = {
            "test_torus_ecool_3DT1e9Mu21_noQvdt":r"Sets ktotadv",
            "test_torus_ecool_3DT1e9Mu21_noQvdt_setKHarm":r"Sets ktotharm",
        }
        comparison.last_common_time = 434
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_3D_noQv_compareDt":
        # Both set ktotharm (NOT ktotadv)
        comparison.sim_names = [
            "test_torus_ecool_3DT1e9Mu21_noQv",
            "test_torus_ecool_3DT1e9Mu21_noQvdt_setKHarm",
        ]
        comparison.sim_labels = {
            "test_torus_ecool_3DT1e9Mu21_noQv":r"Limited by d$\tau_0$",
            "test_torus_ecool_3DT1e9Mu21_noQvdt_setKHarm":r"Limited by dt",
        }
        comparison.last_common_time = 434
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_2D_noQv_compareK":
        # Both have dt, not dtau
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21_noQvdt",
            "test_torus_ecool_T1e9Mu21_noQvdt_setKHarm",
        ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21_noQvdt":r"Sets ktotadv",
            "test_torus_ecool_T1e9Mu21_noQvdt_setKHarm":r"Sets ktotharm",
        }
        comparison.last_common_time = 534
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_2D_noQv_compareDt":
        # Both set ktotadv
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21_noQv",
            "test_torus_ecool_T1e9Mu21_noQvdt",
        ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21_noQv":r"Limited by $\tau_0$",
            "test_torus_ecool_T1e9Mu21_noQvdt":r"Limited by dt",
        }
        comparison.last_common_time = 229
        # comparison.last_common_time = 418 # if including 1em1
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    # ----------------------------------------------
    # Quick test taking after Avara+ 2016 method
    # of tcool to avoid supercooling.
    # 2022-08-20
    # ----------------------------------------------
    elif category == "test_AvaraSupercooling_3D":
        comparison.sim_names = [
            "test_torus_ecool_3DT1e9Mu21_lowres",
            "test_torus_ecool_3DT1e9Mu21_AvaraEq3",
        ]
        comparison.sim_labels = {
            "test_torus_ecool_3DT1e9Mu21_lowres":r"$L=0$ when supercooled",
            "test_torus_ecool_3DT1e9Mu21_AvaraEq3":r"$L=u_e/(2\Delta t)$ when supercooled",
        }
        comparison.last_common_time = 2000
        # comparison.last_common_time = 418 # if including 1em1
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    # ----------------------------------------------
    # NOTE that a constant cooling time really doesn't make much sense.
    # It's better than 1/Omega at some radii and worse at others.
    # Now that Qcool>0 always, how much impact does changing the
    # cooling time have? 2022-07-20. Checked there is no supercooling in
    # 1/Omega or Tcc1e0. The 5em1 simulation has 10 instances of supercooling.
    # The 1em1 simulations has TONS of supercooling...9.1 million.
    elif category == "test_tcoolConstant_noNegQcool":
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21_noNegTcool2",
            "test_torus_ecool_T1e9Mu21Tcc1e0",
            "test_torus_ecool_T1e9Mu21Tcc5em1",
            # "test_torus_ecool_T1e9Mu21Tcc1em1"
        ]
        comparison.sim_labels = {
            # "test_torus_ecool_T1e20Mu21":r"No cooling",
            "test_torus_ecool_T1e9Mu21_noNegTcool2":r"$t_{{\rm cool}}=1/\Omega$",
            "test_torus_ecool_T1e9Mu21Tcc1e0":r"$t_{{\rm cool}}=1~{{\rm GM/c^3}}$",
            "test_torus_ecool_T1e9Mu21Tcc5em1":r"$t_{{\rm cool}}=0.5~{{\rm GM/c^3}}$",
            "test_torus_ecool_T1e9Mu21Tcc1em1":r"$t_{{\rm cool}}=0.1~{{\rm GM/c^3}}$"
        }
        comparison.last_common_time = 501
        # comparison.last_common_time = 418 # if including 1em1
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    # ----------------------------------------------
    # Now that Qcool>0 always, how much impact does changing the
    # pre-factor s? sim done 2022-07-21.
    # Not very physically motivated.
    # ----------------------------------------------
    elif category == "test_changeS_noNegQcool":
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21_noNegTcool2",
            "test_torus_ecool_T1e9Mu21s5"
        ]
        comparison.sim_labels = {
            # "test_torus_ecool_T1e20Mu21":r"No cooling",
            "test_torus_ecool_T1e9Mu21_noNegTcool2":r"$s=1$",
            "test_torus_ecool_T1e9Mu21s5":r"$s=5$"
        }
        comparison.last_common_time = 501
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    # ----------------------------------------------
    # Comparison that tests the difference made by
    # the restriction that
    # L (Qcool) > 0. That change was made 2022-07-15.
    # ----------------------------------------------
    elif category == "test_negTcool":
        # The first simulation allows for tcool < 0, i.e. heating.
        # Other do not (2022-07-12 and 07-13).
        # Negative tcool should only happen within the event horizon, so
        # in ghost cells...curious how much this will change.
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21_withTcool",
            "test_torus_ecool_T1e9Mu21_noNegTcool",
            "test_torus_ecool_T1e9Mu21_noNegTcool2",
            "test_torus_ecool_T1e9Mu21_noNegTcool3",
            ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21_withTcool":r"Allow $t_{\rm cool}<0$",
            "test_torus_ecool_T1e9Mu21_noNegTcool":r"No $t_{\rm cool}<0$",
            "test_torus_ecool_T1e9Mu21_noNegTcool2":r"No $L<0$",
            "test_torus_ecool_T1e9Mu21_noNegTcool3":r"No $L<0$ duplicate"
        }
        comparison.last_common_time = 501
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    # ----------------------------------------------
    # Comparisons that tests whether L should be in
    # coord or fluid rest frame. Checking expectation
    # from walkthrough of Noble+ 2009 that it should
    # be in fluid frame.
    # Not a very physically motivated test.
    # Has sigma_cut = 1, tcool = 1/Omega.
    # Done 2022-07-15 - 2022-07-18.
    # ----------------------------------------------
    elif category == "test_Lframes":
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21_noNegTcool2",
            "test_torus_ecool_T1e9Mu21_testQcool",
            "test_torus_ecool_T1e9Mu21_testQcool2"
        ]
        comparison.sim_labels = {
            # "test_torus_ecool_T1e20Mu21":r"No cooling",
            "test_torus_ecool_T1e9Mu21_noNegTcool2":r"Fluid frame $L$",
            "test_torus_ecool_T1e9Mu21_testQcool":r"$L\alpha u^0$",
            "test_torus_ecool_T1e9Mu21_testQcool2":r"$L u^0$"
        }
        comparison.last_common_time = 501
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    # ----------------------------------------------
    # Comparisons that implement cooling via
    # (uel - ueltarget)/tcool rather than Noble+ 2009
    # prescription. Determined that implementation
    # was causing unphysical overcooling at large radii,
    # therefore not doing its job even in test problem
    # regime. Done until 2022-08-18.
    # Plots from these categories can be found in
    # figures/archive_uel_difference_implementation.
    # ----------------------------------------------
    elif category == "test_ue_propertime_implementation":
        # Testing how different implementations of the
        # instantaneous cooling works. dt test has
        # L = delta uel/dt, where dt is in the coordinate frame.
        # tcool0 has L = delta uel/dtau0, where dtau0 is dt in
        # the comoving frame.
        # tcool0 calculates radG after the predictor-corrector step;
        # dt calculates radG during it. 2022-08-17
        comparison.sim_names = [
            "test_torus_dt",
            "test_torus_tcool0"
            ]
        comparison.sim_labels = {
            "test_torus_dt":r"$t_{{\rm cool}}={\rm dt}$",
            "test_torus_tcool0":r"$t_{{\rm cool}}={\rm d\tau_0}$"
        }
        comparison.last_common_time = 50
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_ue_tcool_sigmaCut1":
        # Comparison of different cooling times for the
        # new implementation (target ue) done on 2022-08-14
        # but including the sigma cut (i.e. no cooling for sigma > 1)
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21ueTcc0_sigmaCut1",
            "test_torus_ecool_T1e9Mu21ueTcc1em2_sigmaCut1",
            "test_torus_ecool_T1e9Mu21ueTcc1em1_sigmaCut1",
            # "test_torus_ecool_T1e9Mu21ueTcc1e0_sigmaCut1"
            # "test_torus_ecool_T1e9Mu21ueTO1_sigmaCut1"
            ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21ueTcc0_sigmaCut1":r"$t_{{\rm cool}} = {\rm dt}$",
            "test_torus_ecool_T1e9Mu21ueTO1_sigmaCut1":r"$t_{{\rm cool}}=1/\Omega$",
            "test_torus_ecool_T1e9Mu21ueTcc1e0_sigmaCut1":r"$t_{{\rm cool}}=1~r_g/c$",
            "test_torus_ecool_T1e9Mu21ueTcc1em1_sigmaCut1":r"$t_{{\rm cool}}=0.1~r_g/c$",
            "test_torus_ecool_T1e9Mu21ueTcc1em2_sigmaCut1":r"$t_{{\rm cool}}=0.01~r_g/c$"
        }
        comparison.last_common_time = 500
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_ue_compare_sigmaCut":
        # Comparison of different sigma cut values for the
        # new implementation (target ue) done on 2022-08-14
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21ueTcc0",
            "test_torus_ecool_T1e9Mu21ueTcc0_sigmaCut1",
            "test_torus_ecool_T1e9Mu21ueTcc0_sigmaCut5"
            ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21ueTcc0":r"No $\sigma_c$",
            "test_torus_ecool_T1e9Mu21ueTcc0_sigmaCut1":r"$\sigma_c=1$",
            "test_torus_ecool_T1e9Mu21ueTcc0_sigmaCut5":r"$\sigma_c=5$"
        }
        comparison.last_common_time = 500
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_ue_tcool":
        # Comparison of different cooling times for the
        # new implementtion (target ue) done on 2022-08-14
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21ueTcc0",
            "test_torus_ecool_T1e9Mu21ueTcc1e0",
            # "test_torus_ecool_T1e9Mu21ueTO1"
            ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21ueTcc0":r"Instantaneous cooling",
            "test_torus_ecool_T1e9Mu21ueTO1":r"$t_{{\rm cool}}=1/\Omega$",
            "test_torus_ecool_T1e9Mu21ueTcc1e0":r"$t_{{\rm cool}}=1~{{\rm r_g/c}}$"
        }
        comparison.last_common_time = 500
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_ueImplementation_tO":
        # Test the new implementation of ue difference with
        # simulations of comparable resolution. 2022-08-14
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21_noNegTcool2",
            "test_torus_ecool_T1e9Mu21ueTO1",
            "test_torus_laptop3"
            ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21_noNegTcool2":"Noble+ 2009",
            "test_torus_T1e9Mu21ueTO1":r"$\Delta u_e$ Implementation (high res)",
            "test_torus_laptop3":r"$\Delta u_e$ Implementation (low res)"
        }
        comparison.last_common_time = 500
        comparison.last_common_time = 100
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_ueImplementation_tcc1e0":
        # Test the new implementation of ue difference with
        # simulations of comparable resolution. 2022-08-14
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21Tcc1e0",
            "test_torus_ecool_T1e9Mu21ueTcc1e0",
            # "test_torus_laptop2"
            ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21Tcc1e0":"Noble+ 2009",
            "test_torus_ecool_T1e9Mu21ueTcc1e0":r"$\Delta u_e$ Implementation (high res)",
            "test_torus_laptop2":r"$\Delta u_e$ Implementation (low res)"
        }
        comparison.last_common_time = 500
        # comparison.last_common_time = 100
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_ue_tcool_laptop":
        # Laptop tests because cluster down. 64x64 res. 2022-08-11
        comparison.sim_names = [
            "test_torus_laptop",
            "test_torus_laptop2",
            "test_torus_laptop3"
            ]
        comparison.sim_labels = {
            "test_torus_laptop":r"Instantaneous cooling",
            "test_torus_laptop2":r"$t_{{\rm cool}}=1/\Omega$",
            "test_torus_laptop3":r"$t_{{\rm cool}}=1~{{\rm r_g/c}}$"
        }
        comparison.last_common_time = 100
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_ueImplementation_tO_laptop":
        # Laptop tests because cluster down. 64x64 res. 2022-08-11
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21_noNegTcool2",
            "test_torus_laptop3"
            ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21_noNegTcool2":"Noble+ 2009",
            "test_torus_laptop3":r"$\Delta u_e$ Implementation"
        }
        comparison.last_common_time = 100
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_ueImplementation_tcc0_laptop":
        # Laptop tests because cluster down. 64x64 res. 2022-08-11
        # Instantaneous cooling
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21Tcc1e0",
            "test_torus_laptop"
            ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21Tcc1e0":"Noble+ 2009",
            "test_torus_laptop":r"$\Delta u_e$ Implementation"
        }
        comparison.last_common_time = 100
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_ueImplementation_tcc1e0_laptop":
        # Laptop tests because cluster down. 64x64 res. 2022-08-11
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21Tcc1e0",
            "test_torus_laptop2"
            ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21Tcc1e0":"Noble+ 2009",
            "test_torus_laptop2":r"$\Delta u_e$ Implementation"
        }
        comparison.last_common_time = 100
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044

    # ----------------------------------------------
    # Comparisons that attempt to cool only the
    # electron fluid (violating conservation of
    # energy). Done 2022-07-19.
    # Plots from these categories can be found in
    # figures/archive_cooling_eF_only.
    # ----------------------------------------------
    elif category == "test_coolFluid":
        # Has sigma_cut = 1, tcool = 1/Omega.
        # 2022-07-19 test of whether we should cool entire fluid or not.
        # First two tests have the L conversion of Qcool2, third doesn't.
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21_testQcool2",
            "test_torus_ecool_T1e9Mu21_testQcool3",
            "test_torus_ecool_T1e9Mu21_testQcool4"
        ]
        comparison.sim_labels = {
            # "test_torus_ecool_T1e20Mu21":r"No cooling",
            "test_torus_ecool_T1e9Mu21_testQcool2":r"Cooling total fluid ($Lu^0$)",
            "test_torus_ecool_T1e9Mu21_testQcool3":r"Cooling electron fluid only ($Lu^0$)",
            "test_torus_ecool_T1e9Mu21_testQcool4":r"Cooling electron fluid only ($L$)"
        }
        comparison.last_common_time = 501
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_tcoolOmega_eFonly":
        # Has sigma_cut = 1
        # 2022-07-19 test of whether we should cool entire fluid or not.
        # Deleted the L transformation thing. Checked and none have supercooling.
        # Decided that cooling only electron fluid violates cons. of energy
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21_noNegTcool2",
            "test_torus_ecool_T1e9Mu21_testQcool4",
            # "test_torus_ecool_T1e9Mu21TO2_eFonly",
            "test_torus_ecool_T1e9Mu21TO5_eFonly"
        ]
        comparison.sim_labels = {
            # "test_torus_ecool_T1e20Mu21":r"No cooling",
            "test_torus_ecool_T1e9Mu21_noNegTcool2":r"$t_{\rm cool}=1/\Omega$, total fluid",
            "test_torus_ecool_T1e9Mu21_testQcool4":r"$t_{\rm cool}=1/\Omega$, e- fluid",
            "test_torus_ecool_T1e9Mu21TO2_eFonly":r"$t_{\rm cool}=1/2\Omega$",
            "test_torus_ecool_T1e9Mu21TO5_eFonly":r"$t_{\rm cool}=1/5\Omega$"
        }
        comparison.last_common_time = 501
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044

    # ----------------------------------------------
    # Comparisons that pre-date the restriction that
    # L (Qcool) > 0. That change was made 2022-07-15.
    # Plots from these categories can be found in
    # figures/archive_allows_negQcool.
    # ----------------------------------------------
    elif category == "test_sigmaCut":
        # All of these have M_unit = 8e21, so Coulomb collisions negligible
        # All have a constant cooling time and no supercooling.
        comparison.sim_names = [
            "test_torus_ecool_T1e20Mu21",
            "test_torus_ecool_T1e9Mu21_noSigmaCut",
            "test_torus_ecool_T1e9Mu21",
            "test_torus_ecool_T1e9Mu21_sigmaCut10"
            ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e20Mu21":r"No cooling",
            "test_torus_ecool_T1e9Mu21":r"$\sigma_{{\rm cut}}=1$",
            "test_torus_ecool_T1e9Mu21_noSigmaCut":r"No $\sigma_{{\rm cut}}$",
            "test_torus_ecool_T1e9Mu21_sigmaCut10":r"$\sigma_{{\rm cut}}=10$"
        }
        comparison.last_common_time = 501
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_qconstantMu21":
        # Note that both have the tcool ISCO modification
        # and both have the no cooling for tcool < 0 addition (2022-07-12)
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21_noNegTcool",
            "test_torus_ecool_T1e9Mu21q10"
            ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21_noNegTcool":r"$q=0.5$",
            "test_torus_ecool_T1e9Mu21q10":r"$q=1.0$"
        }
        comparison.last_common_time = 501
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_target_Te":
        comparison.sim_names = ["test_torus_ecool_T1e20", "test_torus_ecool_T1e10", "test_torus_ecool_T1e9"]
        comparison.sim_labels = {
            "test_torus_ecool_T1e20":r"$T_{e,{\rm target}}=10^{{20}}$",
            "test_torus_ecool_T1e10":r"$T_{e, {\rm target}}=10^{{10}}$",
            "test_torus_ecool_T1e9":r"$T_{e, {\rm target}}=10^9$"}
        comparison.last_common_time = 61
        comparison.targetTemp = None
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_qconstant":
        # Note that both have the tcool ISCO modification
        comparison.sim_names = ["test_torus_ecool_T1e9ISCO", "test_torus_ecool_T1e9q10"]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9ISCO":r"$q=0.5$",
            "test_torus_ecool_T1e9q10":r"$q=1.0$"
        }
        comparison.last_common_time = 91
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_targetTe_mpi":
        # All these simulations have the 2022-06-07 implementation of tcool within the ISCO
        comparison.sim_names = ["test_torus_ecool_T1e20mpi", "test_torus_ecool_T1e10mpi", "test_torus_ecool_T1e9ISCOmpi"]
        comparison.sim_labels = {
            "test_torus_ecool_T1e20mpi":r"$T_{e,{\rm target}}=10^{{20}}$ K",
            "test_torus_ecool_T1e10mpi":r"$T_{e, {\rm target}}=10^{{10}}$ K",
            "test_torus_ecool_T1e9ISCOmpi":r"$T_{e, {\rm target}}=10^9$ K"}
        comparison.last_common_time = 501
        comparison.targetTemp = None
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_tcoolOutput":
        # Just make sure the simulations are the same.
        comparison.sim_names = [
            "test_torus_ecool_T1e9Mu21",
            "test_torus_ecool_T1e9Mu21_withTcool"
        ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9Mu21":r"No $t_{\rm cool}$ output",
            "test_torus_ecool_T1e9Mu21_withTcool":r"With $t_{\rm cool}$ output"
        }
        comparison.last_common_time = 501
        comparison.targetTemp = None
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_Munit":
        # Test how differing mass accretion rates affect esp. Coulomb coupling
        # All these have the 2022-06-08 implementation of the ISCO cooling time.
        comparison.sim_names = ["test_torus_ecool_T1e9Mu21",
                          "test_torus_ecool_T1e9ISCOmpi",
                          "test_torus_ecool_T1e9Mu25"]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9ISCOmpi":r"$\dot m_0\sim10^{-3}$", # Munit=8e23
            "test_torus_ecool_T1e9Mu21":r"$10^{-2}\dot m_0$", # Munit=8e21
            "test_torus_ecool_T1e9Mu25":r"$10^2\dot m_0$" # Munit=8e25
        }
        comparison.last_common_time = 501
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_tcoolOmega_mpi":
        # T1e9ISCOmpi changes tcool within the ISCO with the 2022-06-07 mpi implementation
        # T1e9Tc2mpi has the same implementation as above, but has tcool=1/(2Omega)
        # comparison.sim_names = ["test_torus_ecool_T1e9ISCOmpi", "test_torus_ecool_T1e9ISCOTc2mpi", "test_torus_ecool_T1e9ISCOTc5mpi"]
        comparison.sim_names = ["test_torus_ecool_T1e9ISCOmpi", "test_torus_ecool_T1e9ISCOTc2mpi"]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9ISCOmpi":r"$t_{{\rm cool}}=1/\Omega$",
            "test_torus_ecool_T1e9ISCOTc2mpi":r"$t_{{\rm cool}}=1/2\Omega$",
            "test_torus_ecool_T1e9Tc5mpi":r"$t_{{\rm cool}}=1/5\Omega$"
        }
        comparison.last_common_time = 501
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_tcoolOmega":
        # Allows L<0
        comparison.sim_names = ["test_torus_ecool_T1e9", "test_torus_ecool_T1e9Tc2", "test_torus_ecool_T1e9Tc5"]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9":r"$t_{{\rm cool}}=1/\Omega$",
            "test_torus_ecool_T1e9Tc2":r"$t_{{\rm cool}}=1/2\Omega$",
            "test_torus_ecool_T1e9Tc5":r"$t_{{\rm cool}}=1/5\Omega$"
        }
        comparison.last_common_time = 61
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_tcoolOmega_lowMdot":
        # these permit L<0?
        comparison.sim_names = [
            "test_torus_ecool_T1e20Mu21",
            "test_torus_ecool_T1e9Mu21",
            "test_torus_ecool_T1e9Mu21TO2", "test_torus_ecool_T1e9Mu21TO5"
        ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e20Mu21":r"No cooling",
            "test_torus_ecool_T1e9Mu21":r"$t_{{\rm cool}}=1/\Omega$",
            "test_torus_ecool_T1e9Mu21TO2":r"$t_{{\rm cool}}=1/2\Omega$",
            "test_torus_ecool_T1e9Mu21TO5":r"$t_{{\rm cool}}=1/5\Omega$"
        }
        comparison.last_common_time = 501
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_tcoolOmega_lowMdot_noSC":
        # TO5 had lots of supercooling (SC), so make new comparison
        # with simulations that have NO supercooling (checked output files)
        # these permit L<0?
        comparison.sim_names = [
            "test_torus_ecool_T1e20Mu21",
            "test_torus_ecool_T1e9Mu21",
            "test_torus_ecool_T1e9Mu21TO2",
            "test_torus_ecool_T1e9Mu21TO3"
        ]
        comparison.sim_labels = {
            "test_torus_ecool_T1e20Mu21":r"No cooling",
            "test_torus_ecool_T1e9Mu21":r"$t_{{\rm cool}}=1/\Omega$",
            "test_torus_ecool_T1e9Mu21TO2":r"$t_{{\rm cool}}=1/2\Omega$",
            "test_torus_ecool_T1e9Mu21TO3":r"$t_{{\rm cool}}=1/3\Omega$"
        }
        comparison.last_common_time = 501
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    elif category == "test_tcoolConstant":
        # All of these have M_unit = 8e21, so Coulomb collisions negligible
        # All have a constant cooling time and no supercooling.
        comparison.sim_names = [
            "test_torus_ecool_T1e20Mu21",
            "test_torus_ecool_T1e9Mu21Tcc5em1",
            "test_torus_ecool_T1e9Mu21Tcc1e0",
            "test_torus_ecool_T1e9Mu21Tcc1e1",
            "test_torus_ecool_T1e9Mu21Tcc1e2"]
        comparison.sim_labels = {
            "test_torus_ecool_T1e20Mu21":r"No cooling",
            "test_torus_ecool_T1e9Mu21Tcc5em1":r"$t_{{\rm cool}}=0.5~{{\rm GM/c^2}}$",
            "test_torus_ecool_T1e9Mu21Tcc1e0":r"$t_{{\rm cool}}=1.0~{{\rm GM/c^2}}$",
            "test_torus_ecool_T1e9Mu21Tcc1e1":r"$t_{{\rm cool}}=10.0~{{\rm GM/c^2}}$",
            "test_torus_ecool_T1e9Mu21Tcc1e2":r"$t_{{\rm cool}}=100.0~{{\rm GM/c^2}}$",
        }
        comparison.last_common_time = 501
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
    # ----------------------------------------------
    # Comparisons that pre-date the addition of a
    # tcool changing within the ISCO according to
    # Noble+ 2009 (instead of just Omega all the way
    # down to the event horizon). 2022-06-07.
    # Plots from these categories can be found in
    # figures/archive_no_ISCO.
    # ----------------------------------------------
    elif category == "test_ISCO_implementation":
        # T1e9 does NOT change the cooling time within the ISCO, i.e.
        # it keeps (r^3/2 + a) the whole time.
        # T1e9ISCO has the cooling change within the ISCO implemented in a serial way
        # T1e9ISCOmpi has the cooling change within the ISCO implemented for mpi
        # Hopefully, the latter two are the same!
        # comparison.sim_names = ["test_torus_ecool_T1e9", "test_torus_ecool_T1e9ISCO", "test_torus_ecool_T1e9ISCOmpi"]
        comparison.sim_names = ["test_torus_ecool_T1e9ISCO", "test_torus_ecool_T1e9ISCOmpi"]
        comparison.sim_labels = {
            "test_torus_ecool_T1e9":r"No ISCO cooling change",
            "test_torus_ecool_T1e9ISCO":r"ISCO cooling (serial)",
            "test_torus_ecool_T1e9ISCOmpi":r"ISCO cooling (mpi)"
        }
        comparison.last_common_time = 501
        comparison.targetTemp = 1.e9
        comparison.rEH = 1.348
        comparison.rISCO = 2.044
