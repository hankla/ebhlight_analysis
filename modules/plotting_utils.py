import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.colors import SymLogNorm, LogNorm, ListedColormap


# GET XZ SLICE OF GRID DATA
# AMH: 2022-08-19 generalized to include azimuthal average
def flatten_xz(array, hdr, flip=False, azi_avg=False, phi_ind=0):
    sign = 1.
    flat = np.zeros([2*hdr['N1'],hdr['N2']])
    if azi_avg:
        array = np.mean(array, axis=-1)
    for j in range(hdr['N2']):
        if azi_avg:
            for i in range(hdr['N1']):
                flat[i,j] = sign*array[hdr['N1'] - 1 - i,j]
            for i in range(hdr['N1']):
                flat[i+hdr['N1'],j] = array[i,j]
        else:
            for i in range(hdr['N1']):
                phi_ind2 = (phi_ind + hdr['N3'])//2
                flat[i,j] = sign*array[hdr['N1'] - 1 - i,j,phi_ind2]
            for i in range(hdr['N1']):
                flat[i+hdr['N1'],j] = array[i,j,phi_ind]
    if flip:
        flat[:,0] = 0
        flat[:,-1] = 0
    return flat


# GET XY SLICE OF GRID DATA
def flatten_xy(array, hdr):
  return np.vstack((array.transpose(),array.transpose()[0])).transpose()


def plot_X1X2(ax, geom, var, dump, cmap='viridis', vmin=None, vmax=None, cbar=True,
              label=None, ticks=None, shading='gouraud'):
    # Note these are HARM coordinates...
    X1 = geom['X1'][:,:,0]
    X2 = 1.-geom['X2'][:,:,0]
    lognorm = True
    if (var < 0).any():
        lognorm = False
        cmap = 'RdBu'
    if lognorm:
        mesh = ax.pcolormesh(X1, X2, var[:,:,0], cmap=cmap, norm=LogNorm(vmin=vmin, vmax=vmax))
    else:
        mesh = ax.pcolormesh(X1, X2, var[:,:,0], cmap=cmap, vmin=vmin, vmax=vmax)
    if cbar:
        from mpl_toolkits.axes_grid1 import make_axes_locatable
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.05)
        if label:
            plt.colorbar(mesh, cax=cax, label=label, ticks=ticks)
        else:
            plt.colorbar(mesh, cax=cax, ticks=ticks)
    # ax.set_xticklabels([]); ax.set_yticklabels([])


def plot_X1X3(ax, geom, var, dump, cmap='jet', vmin=None, vmax=None, cbar=True,
              label=None, ticks=None, shading='gouraud'):
    j = dump['hdr']['N2']//2
    X1 = geom['X1'][:, j, :]
    X3 = geom['X3'][:, j, :]
    mesh = ax.pcolormesh(X1, X3, var[:, j, :], cmap=cmap, vmin=vmin, vmax=vmax)
    if cbar:
        from mpl_toolkits.axes_grid1 import make_axes_locatable
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.05)
        if label:
            plt.colorbar(mesh, cax=cax, label=label, ticks=ticks)
        else:
            plt.colorbar(mesh, cax=cax, ticks=ticks)
    ax.set_xticklabels([]); ax.set_yticklabels([])


def plot_xz(ax, geom, var, header, cmap=None, vmin=None, vmax=None, cbar=True,
            label=None, ticks=None, shading='nearest', xlim=None, ylim=None, name=None, azi_avg=True, phi_ind=0, first_axis=True):
    x = geom['x'].copy()
    y = geom['y'].copy()
    z = geom['z'].copy()
    if var.ndim > 2:
        x = flatten_xz(x, header, flip=True, phi_ind=phi_ind)
        y = flatten_xz(y, header, flip=True, phi_ind=phi_ind)
        z = flatten_xz(z, header, phi_ind=phi_ind)
        var = flatten_xz(var, header, azi_avg=azi_avg, phi_ind=phi_ind)
        rcyl = np.sqrt(x**2 + y**2)
        rcyl[np.where(x<0)] *= -1
    else: # 2D case
        phi_ind = 0
        x = x[:,:,phi_ind]
        x[:, 0] = 0; x[:, -1] = 0
        y = y[:,:,phi_ind]
        z = z[:,:,phi_ind]
        if var.ndim == 3:
            var = var[:,:,phi_ind]
        rcyl = np.sqrt(x**2 + y**2)
    lognorm = True
    symlognorm = False
    sigma_colors = 'white'
    # edgecolors = np.full(var.shape, 'k', dtype=str)
    edgecolors = 'k'
    # edgecolors.ravel()[::10] = 'r'
    plt.rcParams["axes.axisbelow"] = False  # Force the grid to be on top

    if vmin is None and vmax is None:
        # Center around 1
        if name is not None and "Ratio" in name:
            vmin = 1e-2
            vmax = 1e2
            sigma_colors = 'black'
            if cmap is None: cmap = "RdBu"
        # Center around 0
        elif (var < 0).any():
            lognorm = False
            vmax = np.std(var)
            vmin = -vmax
            sigma_colors = 'black'
            if cmap is None: cmap = "RdBu"
    elif vmin < 0:
        lognorm = False
        symlognorm = True
        if cmap is None: cmap = "RdBu"
    elif cmap is None: cmap = "viridis"
    if cmap is not None and ("RdBu" in cmap or "PRGn" in cmap or "PuOr" in cmap):
        sigma_colors = 'black'
    if lognorm:
        mesh = ax.pcolormesh(rcyl, z, var, cmap=cmap, shading=shading, norm=LogNorm(vmin=vmin, vmax=vmax))
        ax.pcolormesh(rcyl, z, var, shading=shading, linewidth=0.1, edgecolors='k', facecolors='none')
        # mesh = ax.pcolormesh(rcyl, z, var, cmap=cmap, shading=shading, norm=MidPointLogNorm(vmin=vmin, vmax=vmax, midpoint=3.e2))
        # mesh = ax.pcolormesh(rcyl, z, var, cmap=cmap, shading=shading, norm=MidPointLogNorm(vmin=vmin, vmax=vmax, midpoint=1.e9))
    elif symlognorm:
        linthresh = (vmax - vmin)/100.
        mesh = ax.pcolormesh(rcyl, z, var, cmap=cmap, shading=shading, norm=SymLogNorm(linthresh=linthresh, vmin=vmin, vmax=vmax))
    else:
        mesh = ax.pcolormesh(rcyl, z, var, cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
    ax.set_xlim([0, xlim])
    if ylim is not None:
        ax.set_ylim([-ylim, ylim])
    circle1 = plt.Circle((0,0), header['Reh'],color='k', ls='-', fill=True);
    ax.add_artist(circle1)
    circle2 = plt.Circle((0,0), header['Risco'],color='red', ls='--', fill=False);
    ax.add_artist(circle2)
    rmax = np.max(rcyl[-15, :])
    circle3 = plt.Circle((0,0), rmax,color='r', ls='-', fill=False);
    ax.add_artist(circle3)
    # if cbar:
        # from mpl_toolkits.axes_grid1 import make_axes_locatable
        # divider = make_axes_locatable(ax)
        # NOTE: this append axes needs to after the contour for some reason, so
        # move to plot_vertical_slice
        # cax = divider.append_axes("right", size="5%", pad=0.05)
        # if label:
            # plt.colorbar(mesh, cax=cax, label=label, ticks=ticks)
        # else:
            # plt.colorbar(mesh, cax=cax, ticks=ticks)
    ax.set_aspect('equal')
    ax.set_xlabel('$x/r_g$');
    if first_axis:
        ax.set_ylabel('$z/r_g$')
    return rcyl, z, mesh, sigma_colors

def overlay_field(ax, geom, dump, NLEV=20, linestyle='-', linewidth=1,
  linecolor='k'):
  from scipy.integrate import trapz
  hdr = dump['hdr']
  N1 = hdr['N1']; N2 = hdr['N2']
  x = flatten_xz(geom['x'], hdr).transpose()
  z = flatten_xz(geom['z'], hdr).transpose()
  A_phi = np.zeros([N2, 2*N1])
  gdet = geom['gdet'][:,:,0].transpose()
  B1 = dump['B1'].mean(axis=-1).transpose()
  B2 = dump['B2'].mean(axis=-1).transpose()
  print((gdet.shape))
  for j in range(N2):
    for i in range(N1):
      A_phi[j,N1-1-i] = (trapz(gdet[j,:i]*B2[j,:i], dx=hdr['dx'][1]) -
                         trapz(gdet[:j, i]*B1[:j, i], dx=hdr['dx'][2]))
      A_phi[j,i+N1] = (trapz(gdet[j,:i]*B2[j,:i], dx=hdr['dx'][1]) -
                         trapz(gdet[:j, i]*B1[:j, i], dx=hdr['dx'][2]))
  A_phi -= (A_phi[N2//2-1,-1] + A_phi[N2//2,-1])/2.
  Apm = np.fabs(A_phi).max()
  if np.fabs(A_phi.min()) > A_phi.max():
    A_phi *= -1.
  #NLEV = 20
  levels = np.concatenate((np.linspace(-Apm,0,NLEV)[:-1],
                           np.linspace(0,Apm,NLEV)[1:]))
  ax.contour(x, z, A_phi, levels=levels, colors=linecolor, linestyles=linestyle,
    linewidths=linewidth)

def plot_xy(ax, geom, var, dump, cmap='jet', vmin=None, vmax=None, cbar=True,
  label=None, ticks=None, shading='gouraud'):
  hdr = dump['hdr']
  x = geom['x']
  y = geom['y']
  #x = geom['x'][:,hdr['N2']/2,:]
  #y = geom['y'][:,hdr['N2']/2,:]
  #var = var[:,dump['hdr']['N2']/2,:]
  x = flatten_xy(x[:,dump['hdr']['N2']//2,:], dump['hdr'])
  y = flatten_xy(y[:,dump['hdr']['N2']//2,:], dump['hdr'])
  var = flatten_xy(var[:,dump['hdr']['N2']//2,:], dump['hdr'])
  mesh = ax.pcolormesh(x, y, var, cmap=cmap, vmin=vmin, vmax=vmax,
      shading=shading)
  circle1=plt.Circle((0,0),dump['hdr']['Reh'],color='k');
  ax.add_artist(circle1)
  if cbar:
    from mpl_toolkits.axes_grid1 import make_axes_locatable
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    if label:
      plt.colorbar(mesh, cax=cax, label=label, ticks=ticks)
    else:
      plt.colorbar(mesh, cax=cax, ticks=ticks)
  ax.set_aspect('equal')
  ax.set_xlabel('x/M'); ax.set_ylabel('y/M')
  #ax.grid(True, linestyle=':', color='k', alpha=0.5, linewidth=0.5)

def plot_cluster_slices(sim_analysis, **kwargs):
    cvp_lims = [1e-3, 1.0]
    kwargs["fix_slice_clims"] = {
        "sigma":[1e-1, 10], "RHO":[1e-7, 1],
        "beta":[1e-4, 1e2], "Thetae":[1e-4, 10],
        "Thetap": [1e-4, 1],
        "Te":[1e8, 1e10],
        "Qcool":[-8e-4, 8e-4],
        "Qvisc_e":[-8e-4, 8e-4],
        # "coulViscRatioPost":cvp_lims,
        # "coolViscRatioPost":cvp_lims,
        # "coolTotalRatio": cvp_lims, "coolTotalRatioPost": cvp_lims
    }
    tend = 500

    for view in ["ISCO", "inner", "medium"]:
        kwargs["slice_view"] = view
        # ----------------------------------------------
        # Snapshots, not azimuthally-averaged
        # ----------------------------------------------
        kwargs["time_average_vslice"] = False
        kwargs["azimuthal_average"] = False
        # kwargs["slice_quantities_to_plot"] = ["Thetae", "Thetap", "RHO", "coolTotalRatio", "coulViscRatio"]
        # kwargs["slice_quantities_to_plot"] = ["Qvisc_e"]
        # for j in np.arange(475, 500)[::5]:
        for j in np.arange(0, 800)[::50]:
            kwargs["tstart_slices"] = int(j)
            print("Slice {:d}".format(j))

            kwargs["cmap"] = "RdBu_r"
            kwargs["slice_quantities_to_plot"] = ["Qcool", "Te", "sigma"]
            sim_analysis.plot_vertical_slice(**kwargs)

            kwargs["cmap"] = "viridis"
            kwargs["slice_quantities_to_plot"] = ["RHO"]
            sim_analysis.plot_vertical_slice(**kwargs)
        for j in np.arange(750, 800)[::5]:
            kwargs["tstart_slices"] = int(j)
            print("Slice {:d}".format(j))

            kwargs["slice_quantities_to_plot"] = ["Qcool", "Te", "sigma"]
            kwargs["cmap"] = "RdBu_r"
            sim_analysis.plot_vertical_slice(**kwargs)

            kwargs["slice_quantities_to_plot"] = ["RHO"]
            kwargs["cmap"] = "viridis"
            sim_analysis.plot_vertical_slice(**kwargs)
        # ----------------------------------------------
        # Time-average azimuthally-averaged slices
        # ----------------------------------------------
        kwargs["tend_slices"] = tend
        kwargs["time_average_vslice"] = True
        kwargs["azimuthal_average"] = True
        kwargs["slice_quantities_to_plot"] = ["Te", "Qcool", "Qvisc_e", "sigma"]
        kwargs["cmap"] = "RdBu_r"
        kwargs["tstart_slices"] = 600
        sim_analysis.plot_vertical_slice(**kwargs)
        kwargs["slice_quantities_to_plot"] = ["Thetae", "Thetap", "RHO", "coolTotalRatioPost", "coulViscRatioPost", "coolViscRatioPost", "coolViscRatio", "coolTotalRatio"]
        kwargs["cmap"] = "viridis"
        sim_analysis.plot_vertical_slice(**kwargs)
        # kwargs["tstart_slices"] = 1900
        # sim_analysis.plot_vertical_slice(**kwargs)
        # kwargs["slice_quantities_to_plot"] = ["Te", "Qcool", "Qvisc_e"]
        # kwargs["cmap"] = "RdBu_r"
        # sim_analysis.plot_vertical_slice(**kwargs)
    return


# Normalization class that centers a log norm around midpoint value.
# Copied from:
# https://stackoverflow.com/questions/48625475/python-shifted-logarithmic-colorbar-white-color-offset-to-center
class MidPointLogNorm(LogNorm):
    def __init__(self, vmin=None, vmax=None, midpoint=None, clip=False):
        LogNorm.__init__(self,vmin=vmin, vmax=vmax, clip=clip)
        self.midpoint=midpoint

    def __call__(self, value, clip=None):
        result, is_scalar = self.process_value(value)
        x, y = [np.log(self.vmin), np.log(self.midpoint), np.log(self.vmax)], [0, 0.5, 1]
        return np.ma.array(np.interp(np.log(value), x, y), mask=result.mask, copy=False)
