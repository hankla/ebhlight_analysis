################################################################################
#                                                                              #
# FUNDAMENTAL AND ASTROPHYSICAL CONSTANTS                                      #
#                                                                              #
################################################################################
class Consts:
  pass

Consts.G_cgs = 6.67*10**(-8) # cm^3/g/s^2
Consts.solar_mass_cgs = 1.99*10**33 # g
Consts.c_cgs = 2.99792458e10 # cm/s
Consts.h_cgs = 6.626*10**(-27) # erg s
Consts.me_g = 9.1093826e-28 # g
Consts.mp_g = 1.67262171e-24 # g
Consts.thomson_cross_section_cgs = 6.652*10**(-25)
Consts.kB_cgs = 1.3806505e-16 # erg/K
Consts.e_cgs = 3.3*10**(-10) # statC
Consts.restmass_cgs = Consts.me_g * Consts.c_cgs**2.0
Consts.restmass_proton_cgs = Consts.mp_g * Consts.c_cgs**2.0
Consts.coulomb_log = 20.0
Consts.SB_cgs = 5.6e-5
Consts.gaunt_factor = 1.2

cgs = {}
cgs['CL']      = 2.99792458e10
cgs['QE']      = 4.80320680e-10
cgs['ME']      = 9.1093826e-28
cgs['MP']      = 1.67262171e-24
cgs['MN']      = 1.67492728e-24
cgs['HPL']     = 6.6260693e-27
cgs['HBAR']    = 1.0545717e-27
cgs['KBOL']    = 1.3806505e-16
cgs['GNEWT']   = 6.6742e-8
cgs['SIG']     = 5.670400e-5
cgs['AR']      = 7.5657e-15
cgs['THOMSON'] = 0.665245873e-24
cgs['JY']      = 1.e-23
cgs['PC']      = 3.085678e18
cgs['AU']      = 1.49597870691e13
cgs['MSOLAR']  = 1.989e33
cgs['RSOLAR']  = 6.96e10
cgs['LSOLAR']  = 3.827e33
cgs['COULOMBLOG'] = 20.0

def get_cgs():
  return cgs
