import numpy as np
import scipy.special as special
import glob
import sys;
import scipy.integrate as integrate
sys.path.insert(0, '../')
import hdf5_to_dict as io
import units
units = units.get_cgs()

# colorblind_colors = ["#2691db", "#2e692c", "C1", "#723b7a", "#e41a1c"]
# Matches lia.mplstyle
colorblind_colors = ["#2691db", "#ff7f0e", "#2e692c", "#723b7a", "#e41a1c"]


def get_list_of_labels():
    labels = {
        "RHO":r"$\rho$",
        "Thetae":r"$\theta_e$", "Thetap":r"$\theta_p$",
        "Te":r"$T_e$", "Tp":r"$T_p$", "TpTe":r"$T_p/T_e$",
        "Tp_over_Te":r"$T_p/T_e$",
        "ue":r"$u_e$", "up":r"$u_p$",
        "ueRatio":r"$u_e/U$",
        "ucon0":r"$u^t$", "ucon1":r"$u^r$", "ucon2":r"$u^\theta$", "ucon3":r"$u^\phi$",
        "ucov0":r"$u_t$", "ucov1":r"$u_r$", "ucov2":r"$u_\theta$", "ucov3":r"$u_\phi$",
        "ucon_bl0":r"$u^t$", "ucon_bl1":r"$|u^r|$", "ucon_bl2":r"$u^\theta$", "ucon_bl3":r"$u^\phi$",
        "vrr":r"KS $|v^r|$", "vrr_bl":r"BL $|v^r|$",
        "vphir":r"KS $|v^\phi|$", "vphir_bl":r"BL $|v^\phi|$",
        "tinfall":r"$t_{\rm infall}$",
        "tCoulomb_p":r"$t_{\rm Coul}^{i}$",#=(u_p/Q_{\rm coul})c/r_g$",
        "tCoulomb_e":r"$t_{\rm Coul}^{e}$",
        "theat":r"$t_{\rm heat}^{g}$",
        "theat_p":r"$t_{\rm heat}^{i}$",
        "theat_e":r"$t_{\rm heat}^{e}$",
        "tCool_e":r"$t_{\rm cool}^{e}$",
        "coulombQuality":r"$(u_e/Q_{\rm coul})/\Delta t$",
        "KEL":r"$\kappa_e$", "KTOT":r"$\kappa_{\rm tot}$",
        "beta":r"$\beta$", "sigma":r"$\sigma$",
        "Qcoul":r"Coulomb heating rate $Q_{coul}$",
        "Qvisc_e":r"$Q_{visc}^e$",
        "Qvisc_p":r"$Q_{visc}^p$",
        "Qvisc":r"$Q_{visc}$",
        # "Qvisc_e":r"Viscous electron heating rate $Q_{visc}^e$",
        # "Qvisc_p":r"Viscous proton heating rate $Q_{visc}^p$",
        "coulViscRatio":r"Coulomb/viscous heating $Q_{coul}/Q_{visc}^e$",
        "coolViscRatio":r"Cooling/viscous heating $Q_{cool}/Q_{visc}^e$",
        "coolCoulRatio":r"Cooling/Coulomb heating $Q_{cool}/Q_{coul}$",
        # "coolTotalRatio":r"Cooling/total heating $Q_{cool}/\left(Q_{visc}^e + Q_{coul}\right)$",
        "coolTotalRatio":r"$Q_{cool}/\left(Q_{visc}^e + Q_{coul}\right)$",
        # "coulViscRatioPost":r"Coulomb/viscous heating $\langle Q_{coul}\rangle/\langle Q_{visc}^e\rangle$",
        # "coolViscRatioPost":r"Cooling/viscous heating $\langle Q_{cool}\rangle/\langle Q_{visc}^e\rangle$",
        # "coolCoulRatioPost":r"Cooling/Coulomb heating $\langle Q_{cool}\rangle/\langle Q_{coul}\rangle$",
        # "coolTotalRatioPost":r"Cooling/total heating $\langle Q_{cool}\rangle/\langle Q_{visc}^e + Q_{coul}\rangle$",
        "coulViscRatioPost":r"$\langle Q_{coul}\rangle/\langle Q_{visc}^e\rangle$",
        "coolViscRatioPost":r"$\langle Q_{cool}\rangle/\langle Q_{visc}^e\rangle$",
        "coolCoulRatioPost":r"$\langle Q_{cool}\rangle/\langle Q_{coul}\rangle$",
        "coolTotalRatioPost":r"$\langle Q_{cool}\rangle/\langle Q_{visc}^e + Q_{coul}\rangle$",
        "Qcool":r"Removed energy $Q_{\rm cool}$",
        # "Qcool":r"Removed energy $Q_{\rm cool}/Q_{{\rm cool},0}$",
        "Qcool_corona":r"Removed energy in corona $Q_{\rm cool}$",
        "Qcoul_corona":r"Removed energy in corona $Q_{\rm coul}$",
        "Qcool_opt_thin":r"Removed energy $Q_{\rm cool}$ where $\tau<1$",
        "Qcoul_opt_thin":r"Removed energy $Q_{\rm coul}$ where $\tau<1$",
        "Qvisc_e_opt_thin":r"Added energy $Q_{\rm visc}^e$ where $\tau<1$",
        "Qvisc_p_opt_thin":r"Added energy $Q_{\rm visc}^p$ where $\tau<1$",
        "coolCoronaPost":r"$\langle Q_{\rm cool}\rangle_{\beta < 1}/\langle Q_{\rm cool}\rangle$",
        "coulCoronaPost":r"$\langle Q_{\rm coul}\rangle_{\beta < 1}/\langle Q_{\rm coul}\rangle$",
        "coolOptThinPost":r"$\langle Q_{\rm cool}\rangle_{\tau < 1}/\langle Q_{\rm cool}\rangle$",
        "coulOptThinPost":r"$\langle Q_{\rm coul}\rangle_{\tau < 1}/\langle Q_{\rm coul}\rangle$",
        "viscEOptThinPost":r"$\langle Q_{\rm visc}^e\rangle_{\tau < 1}/\langle Q_{\rm visc}^e\rangle$",
        "viscPOptThinPost":r"$\langle Q_{\rm visc}^p\rangle_{\tau < 1}/\langle Q_{\rm visc}^p\rangle$",
        "coolCoulCoronaRatioPost":r"$\langle Q_{\rm cool}\rangle_{\beta < 1}/\langle Q_{\rm coul}\rangle_{\beta<1}$",
        "frac_above_target_e":r"Gas fraction with $T_e>T_{\rm target}$",
        "frac_above_target_p":r"Gas fraction with $T_p>T_{\rm target}$",
        "Qcool_hotPhase_e":r"$Q_{\rm cool}$ where $T_e>T_{\rm target}$",
        "Qcool_hotPhase_p":r"$Q_{\rm cool}$ where $T_p>T_{\rm target}$",
        "Qcoul_hotPhase_e":r"$Q_{\rm coul}$ where $T_e>T_{\rm target}$",
        "Qcoul_hotPhase_p":r"$Q_{\rm coul}$ where $T_p>T_{\rm target}$",
        "hotPhaseFracPost_e":r"Gas fraction with $\langle T_e>T_{\rm target}\rangle$",
        "hotPhaseFracPost_p":r"Gas fraction with $\langle T_p>T_{\rm target}\rangle$",
        "QcoulHotPhaseFracPost_e":r"$langle Q_{\rm coul}\rangle_{T_e>T_{\rm target}}/\langle Q_{\rm coul}\rangle$",
        "QcoulHotPhaseFracPost_p":r"$langle Q_{\rm coul}\rangle_{T_p>T_{\rm target}}/\langle Q_{\rm coul}\rangle$",
        "QcoolHotPhaseFracPost_e":r"$langle Q_{\rm cool}\rangle_{T_e>T_{\rm target}}/\langle Q_{\rm cool}\rangle$",
        "QcoolHotPhaseFracPost_p":r"$langle Q_{\rm cool}\rangle_{T_p>T_{\rm target}}/\langle Q_{\rm cool}\rangle$",
        "H_over_R":r"$\langle H\rangle/r$",
        "H_over_R0":r"$\langle H\rangle/r$",
        "HT_over_R":r"$\langle H_T\rangle/r$",
        "Hmean":r"$\langle H \rangle$",
        "H2":r"$\langle H^2\rangle$",
        "Qmri_theta":r"$Q^{\theta}_{\rm MRI}$",
        "Qmri_phi":r"$Q^{\phi}_{\rm MRI}$",
        "mdot_eh":r"$\dot{{M}}/\dot{{M}}_{{\rm Edd}}$",
        "Tcool":r"$t_{{\rm cool}}$",
        "num_super":r"$N_{{\rm super}}$",
        "lum_super":r"$L_{{\rm super}}$",
        "mass_flux":r"$\dot M(r)$",
        "surface_density":r"$\Sigma$",
        "magnetic_flux":r"$\Phi/\sqrt{\dot M}$",
        "Phi":r"$\Phi$",
        "phi":r"$\Phi/\dot M^{1/2}$",
        "visc_efficiency":r"$Q_{\rm visc}/\dot Mc^2$",
        "cool_efficiency":r"$Q_{\rm cool}/\dot Mc^2$",
        "optical_depth":r"$\tau_{es}$",
        "compton_yNR":r"$y_{\rm NR}$",
        "compton_yR":r"$y_{\rm R}$",
    }
    for quantity in get_list_of_all_quantities():
        if quantity not in labels:
            labels[quantity] = quantity.replace("_", " ")
    return labels


def get_slice_view_lims():
    slice_views = {
        "all":(None, None),
        "medium":(40, 40),
        "inner":(10, 10),
        "ISCO":(2, 2),
        "large":(100, 100),
    }
    return slice_views


def get_thetae(Te):
    return units['KBOL']*np.array(Te)/(units['ME']*units['CL']**2)


def get_Te(thetae):
    return np.array(thetae)*units['ME']*units['CL']**2/units['KBOL']


def get_thetap(Tp):
    return units['KBOL']*np.array(Tp)/(units['MP']*units['CL']**2)


def get_Tp(thetap):
    return np.array(thetap)*units['MP']*units['CL']**2/units['KBOL']


def get_thetam(thetae, thetai):
    return thetae*thetai/(thetae + thetai)


def safeKn(n, x):
    # n is the order. x is the argument
    if x > 100.:
        return np.exp(-x)*np.sqrt(np.pi/(2.0*x))
    else:
        return special.kn(n, x)


def format_exp(number):
    a, b = '{:.2e}'.format(number).split('e')
    b = int(b)
    if np.allclose(float(a), 1.0):
        return r'$10^{{{}}}$'.format(b)
    return r'${}\times10^{{{}}}$'.format(a, b)


def get_list_of_standard_vector_quantities():
    return ["ucon", "ucov", "bcon", "bcov", "jcon", "jcov", "ucon_bl"]


def get_list_of_integral_quantities():
    return ["mass_flux", "magnetic_flux"]


def get_list_of_vert_integral_quantities():
    return ["optical_depth", "compton_yNR", "compton_yR"]


def get_list_of_target_quantities():
    return ["Thetae", "Thetap", "Te", "Tp", "H_over_R", "HT_over_R", "H_over_R0"]


def get_list_of_standard_quantities():
    """
    Quantities that are dumped in ebhlight output
    files, so don't need to be post-processed.
    """
    list_of_standard_quantities = [
        "RHO", "UU",
        "U1", "U2", "U3",
        "ucon", "ucov", "ucon_bl",
        "B1", "B2", "B3",
        "divb", "bcon", "bcov",
        "KEL", "KTOT",
        "Thetae", "Thetap", "ue", "up",
        "TpTe", "PRESS",
        "bsq", "beta", "sigma",
        "jcon", "jcov", "j2", "Tcool",
        "Qcoul", "Qvisc_e", "Qvisc_p", "Qcool",
        # Single-fluid quantities
        # "Theta", "ENT",
    ]
    return list_of_standard_quantities


def get_list_of_qs_full_geom():
    """
    Raw calculated quantities that need the full geom file.
    """
    list_of_qs_full_geom = [
        "Hmean",
        "H2",
        "Qmri_theta",
        "Qmri_phi"
    ]
    return list_of_qs_full_geom


def get_list_of_coulomb_quantities():
    """
    Quantities that require header["COULOMB"] is True.
    """
    list_of_coulomb_quantities = [
        "Qcoul",
        "coulViscRatio", "coulViscRatioPost",
        "coolCoulRatio", "coolCoulRatioPost",
        "coolTotalRatio", "coolTotalRatioPost",
        "Qcoul_hotPhase_e", "Qcoul_hotPhase_p",
        "Qcoul_corona", "Qcoul_opt_thin",
        "coulCoronaPost", "coulOptThinPost",
        "QcoulHotPhaseFracPost_e", "QcoulHotPhaseFracPost_p",
        "coolCoulCoronaRatioPost",
        "tCoulomb_e", "tCoulomb_p",
        "coulombQuality",
        "Qcoul_opt_thin",
    ]
    return list_of_coulomb_quantities


def get_list_of_cooling_quantities():
    """
    Quantities that require header["COOLING"] is True.
    """
    list_of_cooling_quantities = [
        "Tcool", "Qcool",
        "coolViscRatio", "coolViscRatioPost",
        "coolCoulRatio", "coolCoulRatioPost",
        "coolTotalRatio", "coolTotalRatioPost",
        "frac_above_target_p", "frac_above_target_e",
        "Qcoul_hotPhase_e", "Qcoul_hotPhase_p",
        "Qcool_hotPhase_e", "Qcool_hotPhase_p",
        "Qcool_corona", "Qcool_opt_thin",
        "coolCoronaPost", "coolOptThinPost",
        "hotPhaseFracPost_e", "hotPhaseFracPost_p",
        "QcoolHotPhaseFracPost_e", "QcoolHotPhaseFracPost_p",
        "QcoulHotPhaseFracPost_e", "QcoulHotPhaseFracPost_p",
        "coolCoulCoronaRatioPost",
        "tCool_e",
        # Optical depth requires cgs units, which
        # needs M_unit and L_unit (not in non-cooling sims)
        "optical_depth",
        "Qcoul_opt_thin", "Qcool_opt_thin",
        "Qvisc_e_opt_thin", "Qvisc_p_opt_thin",
    ]
    return list_of_cooling_quantities


def get_list_of_raw_calculated_quantities():
    """
    Quantities that need to be calculated from
    standard quantities before time/space averaging
    occurs. Key is the quantity name. Values are
    the standard quantities needed to calculate
    the new quantity (besides rho, bsq which are
    always loaded).
    """
    list_of_raw_calculated_quantities = {
        "coulViscRatio":{"Qcoul", "Qvisc_e"},
        "coolViscRatio":{"Qcool", "Qvisc_e"},
        "coolCoulRatio":{"Qcool", "Qcoul"},
        "coolTotalRatio":{"Qcool", "Qvisc_e", "Qcoul"},
        "Hmean":{},
        "Qcool_corona":{"Qcool", "PRESS"},
        "Qcoul_corona":{"Qcoul", "PRESS"},
        "Qcoul_opt_thin":{"Qcoul"},
        "Qcool_opt_thin":{"Qcool"},
        "Qvisc_e_opt_thin":{"Qvisc_e"},
        "Qvisc_p_opt_thin":{"Qvisc_p"},
        "frac_above_target_e":{"Thetae"},
        "frac_above_target_p":{"Thetap"},
        "Qcool_hotPhase_e":{"Thetae", "Qcool"},
        "Qcool_hotPhase_p":{"Thetap", "Qcool"},
        "Qcoul_hotPhase_e":{"Thetae", "Qcoul"},
        "Qcoul_hotPhase_p":{"Thetap", "Qcoul"},
        # The below are taken from run_ebhlight_postprocess.py script,
        # sent by Michelle Athay.
        "Pgr":{"UU"},
        "vphir":{"ucon"}, "vrr":{"ucon"},
        "vphir_bl":{"ucon_bl"}, "vrr_bl":{"ucon_bl"},
        "mass_flux":{"ucon"},
        "surface_density":{},
        "optical_depth":{},
        "magnetic_flux":{"B1"},
        "H2":{},
        "Qmri_theta":{"bcon", "ucon", "UU"},
        "Qmri_phi":{"bcon", "ucon", "UU"},
    }
    return list_of_raw_calculated_quantities


def get_list_of_reduced_calculated_quantities():
    """
    Quantities that can be calculated from shell-averaged
    data of other quantities, either before or after
    time-averaging (taken care of in the
    calculate_reduced_quantity function).
    Key is the quantity name. Values are
    the standard quantities needed to calculate
    the new quantity.
    """
    list_of_reduced_calculated_quantities = {
        "H_over_R":{"H2"},
        "H_over_R0":{"H2", "Hmean"},
        "HT_over_R":{"PRESS", "RHO", "Thetap"},
        "coolCoronaPost":{"Qcool", "Qcool_corona"},
        "coulCoronaPost":{"Qcoul", "Qcoul_corona"},
        "coolOptThinPost":{"Qcool", "Qcool_opt_thin"},
        "coulOptThinPost":{"Qcoul", "Qcoul_opt_thin"},
        "viscEOptThinPost":{"Qvisc_e", "Qvisc_e_opt_thin"},
        "viscPOptThinPost":{"Qvisc_p", "Qvisc_p_opt_thin"},
        "hotPhaseFracPost_e":{"frac_above_target_e"},
        "hotPhaseFracPost_p":{"frac_above_target_p"},
        "QcoolHotPhaseFracPost_e":{"Qcool", "Qcool_hotPhase_e"},
        "QcoolHotPhaseFracPost_p":{"Qcool", "Qcool_hotPhase_p"},
        "QcoulHotPhaseFracPost_e":{"Qcoul", "Qcoul_hotPhase_e"},
        "QcoulHotPhaseFracPost_p":{"Qcoul", "Qcoul_hotPhase_p"},
        # "coulCoronaPost":{"Qcoul", "Qcoul_corona"},
        "coolCoulCoronaRatioPost":{"Qcool_corona", "Qcoul_corona"},
        "tinfall":{"ucon_bl1"},
        "tCool_e":{"ue", "Qcool", "Tcool"},
        "tCoulomb_e":{"ue", "Qcoul"},
        "tCoulomb_p":{"up", "Qcoul"},
        "theat":{"UU", "Qvisc_p"},
        "theat_p":{"up", "Qvisc_p"},
        "theat_e":{"ue", "Qvisc_e"},
        "coulombQuality":{"ue", "Qcoul"},
        "Te":{"Thetae"},
        "Tp":{"Thetap"},
        "coulViscRatioPost":{"Qcoul", "Qvisc_e"},
        "coolViscRatioPost":{"Qcool", "Qvisc_e"},
        "coolCoulRatioPost":{"Qcool", "Qcoul"},
        "coolTotalRatioPost":{"Qcool", "Qcoul", "Qvisc_e"},
        "ueRatio":{"UU", "ue"},
    }

    return list_of_reduced_calculated_quantities


def get_log_scales():
    log_scales = {
        "RHO":True,
        "Thetae":True, "Thetap":True,
        "Te":True, "Tp":True, "TpTe": True,
        "ue":True, "up":True,
        "beta":True, "sigma":True,
        "Qcoul": True, "Qvisc_e": True,
        "Qvisc_p": True,
        "KEL":True,
        "KTOT":True,
        "UU":True, "ueRatio":True,
        "coulViscRatio": True,
        "coolViscRatio": True,
        "coolCoulRatio": True,
        "coolTotalRatio": True,
        "coulViscRatioPost": True,
        "coolViscRatioPost": True,
        "coolTotalRatioPost": True,
        "coolCoulRatioPost": True,
        "coolCoronaPost": True,
        "coolCoulCoronaRatioPost": True,
        "coulCoronaPost": False,
        "Tel_test": True,
        "Qcool": True,
        "Qcool_corona": False,
        "Qcoul_corona": False,
        "Qcool_opt_thin": False,
        "Qcoul_opt_thin": False,
        "Qvisc_e_opt_thin": False,
        "Qvisc_p_opt_thin": False,
        "frac_above_target_e":True,
        "frac_above_target_p":True,
        "hotPhaseFracPost_e":True,
        "hotPhaseFracPost_p":True,
        "Qcool_hotPhase_e":True,
        "Qcool_hotPhase_p":True,
        "Qcoul_hotPhase_e":True,
        "Qcoul_hotPhase_p":True,
        "QcoulHotPhaseFracPost_e":True,
        "QcoulHotPhaseFracPost_p":True,
        "QcoolHotPhaseFracPost_e":True,
        "QcoolHotPhaseFracPost_p":True,
        "coolOptThinPost":True,
        "coulOptThinPost":True,
        "viscEOptThinPost":True,
        "viscPOptThinPost":True,
        "Tcool": True,
        "H2": True,
        "Hmean": False,
        "H_over_R": True,
        "H_over_R0": True,
        "HT_over_R": True,
        "Qmri_theta": True, "Qmri_phi": True,
        "tinfall": True, "tCool_e":True,
        "tCoulomb_p": True, "tCoulomb_e": True,
        "theat_p":True, "theat_e":True, "theat":True,
        "coulombQuality":True,
        "ucon_bl0": True, "ucon_bl1": True,
        "vrr": True, "vrr_bl": True,
        "vphir": True, "vphir_bl": True,
        "mass_flux": True,
        "surface_density": True,
        "magnetic_flux": True,
    }
    for quantity in get_list_of_all_quantities():
        if quantity not in log_scales:
            log_scales[quantity] = False
    return log_scales


def get_list_of_all_quantities():
    all_quantities = get_list_of_standard_quantities() + get_list_of_standard_vector_quantities() + list(get_list_of_raw_calculated_quantities().keys()) + list(get_list_of_reduced_calculated_quantities().keys())
    return all_quantities


def calculate_raw_quantity(quantity, header, geom, dfile):
    """
    Calculate raw quantities, given the quantity to calculate
    and other information contained in the header and geom.
    dfile is the dump file that contains the necessary raw data.
    """
    print("Calculating raw quantity " + quantity)
    gdet = geom['gdet']
    rho = dfile['RHO']
    bsq = dfile['bsq']
    # Take only locations where magnetization is less than 30.
    # sigma_cut = bsq/rho < 30.0
    # Take only locations where magnetization is less than 1.
    sigma_cut = bsq/rho < 1.0
    # Density-weighted.
    weight = rho*sigma_cut*gdet
    # Denominator of all shell averages: density in each shell.
    normalization = np.sum(np.sum(weight, axis=-1), axis=-1)
    if (normalization == 0.0).any():
        print("NOTE: there are no cells with sigma < 1.0!")
        print(normalization[:10])

    # Begin individual calculations
    if quantity == "coulViscRatio":
        data = dfile["Qcoul"]/dfile["Qvisc_e"]
    elif quantity == "coolViscRatio":
        data = dfile["Qcool"]/dfile["Qvisc_e"]
    elif quantity == "coolCoulRatio":
        data = dfile["Qcool"]/dfile["Qcoul"]
    elif quantity == "coolTotalRatio":
        data = dfile["Qcool"]/(dfile["Qvisc_e"] + dfile["Qcoul"])
    # Below was taken from run_ebhlight_postprocess.
    elif quantity == "Pgr":
        data = (header["gam"] - 1.0)*dfile["UU"]
    elif quantity == "rhorr":
        data = data["RHO"]
    elif quantity == "H2": # scaleheight^2 (second moment)
        # Note that geom[r] and geom[th] are in BL coords
        data = (geom['r']*np.cos(geom['th']))**2
    elif quantity == "Hmean": # scaleheight mean (first moment)
        # Note that geom[r] and geom[th] are in BL coords
        data = (geom['r']*np.cos(geom['th']))
    elif quantity == "Qcoul_corona": # fraction of Qcoul where beta < 1
        beta = 2.*(dfile['PRESS']/(bsq + 1.e-30))
        # Take only locations where beta is less than 1.
        beta_cut = beta < 1.0
        # print(sigma_cut.sum())
        # print(beta_cut.sum())
        # print(beta.shape)
        # print(beta_cut[50, 50, 0:10])
        # print(beta[50, 50, 0:10])
        # print(dfile["Qcoul"][50, 50, 0:10])
        data = dfile["Qcoul"]*beta_cut
        # print(data[50, 50, 0:10])
    elif quantity == "Qcool_corona": # fraction of Qcool where beta < 1
        beta = 2.*(dfile['PRESS']/(bsq + 1.e-30))
        # Take only locations where beta is less than 1.
        beta_cut = beta < 1.0
        data = dfile["Qcool"]*beta_cut
    elif "opt_thin" in quantity:
        print(quantity)
        # Calculate optical depth.
        # Average over hemispheres
        midplane_ind = int(weight.shape[1]/2)
        top_hemi = weight[:, :midplane_ind, :]
        bottom_hemi = weight[:, midplane_ind:, :]
        averaged_hemi = (top_hemi + bottom_hemi[:, ::-1, :])/2.

        # Remember that geom[X2] is the same for all r, phi, so just choose.
        x2 = geom['X2'][0, :midplane_ind, 0]
        mass_integral = np.abs(integrate.cumtrapz(averaged_hemi, x=x2, axis=1, initial=0))
        convert_to_cgs = header["M_unit"]/header["L_unit"]**2
        opacity = 0.4
        tau = mass_integral*convert_to_cgs*opacity
        # Stitch back together for full 3D slice
        tau = np.concatenate((tau, tau[:, ::-1, :]), axis=1)
        # Take only locations where tau is less than 1.
        tau_cut = tau < 1.0
        tau_cut = tau_cut*sigma_cut
        if quantity == "Qcoul_opt_thin": # fraction of Qcoul where tau < 1
            data = dfile["Qcoul"]*tau_cut
        elif quantity == "Qcool_opt_thin": # fraction of Qcool where tau < 1
            data = dfile["Qcool"]*tau_cut
        elif quantity == "Qvisc_e_opt_thin": # fraction of Qcool where tau < 1
            data = dfile["Qvisc_e"]*tau_cut
        elif quantity == "Qvisc_p_opt_thin": # fraction of Qcool where tau < 1
            data = dfile["Qvisc_p"]*tau_cut
    elif quantity == "frac_above_target_p": # fraction of gas with T>target
        temp = get_Tp(dfile["Thetap"])
        T_target = header["Tel_target"]*geom['r']**(-header["Tel_rslope"])
        # Take only locations where temperature is greater than target
        temp_cut = temp < T_target
        data = temp_cut
    elif quantity == "frac_above_target_e": # fraction of gas with T>target
        temp = get_Te(dfile["Thetae"])
        T_target = header["Tel_target"]*geom['r']**(-header["Tel_rslope"])
        # Take only locations where temperature is greater than target
        temp_cut = temp < T_target
        data = temp_cut
    elif quantity == "Qcoul_hotPhase_e": # fraction of Qcool in regions with T>Target
        temp = get_Te(dfile["Thetae"])
        T_target = header["Tel_target"]*geom['r']**(-header["Tel_rslope"])
        # Take only locations where temperature is greater than target
        temp_cut = temp < T_target
        data = dfile["Qcoul"]*temp_cut
    elif quantity == "Qcoul_hotPhase_p": # fraction of Qcool in regions with T>Target
        temp = get_Tp(dfile["Thetap"])
        T_target = header["Tel_target"]*geom['r']**(-header["Tel_rslope"])
        # Take only locations where temperature is greater than target
        temp_cut = temp < T_target
        data = dfile["Qcoul"]*temp_cut
    elif quantity == "Qcool_hotPhase_e": # fraction of Qcool in regions with T>Target
        temp = get_Te(dfile["Thetae"])
        T_target = header["Tel_target"]*geom['r']**(-header["Tel_rslope"])
        # Take only locations where temperature is greater than target
        temp_cut = temp < T_target
        data = dfile["Qcool"]*temp_cut
    elif quantity == "Qcool_hotPhase_p": # fraction of Qcool in regions with T>Target
        temp = get_Tp(dfile["Thetap"])
        T_target = header["Tel_target"]*geom['r']**(-header["Tel_rslope"])
        # Take only locations where temperature is greater than target
        temp_cut = temp < T_target
        data = dfile["Qcool"]*temp_cut
    # Three-velocities in MMKS!!!
    elif quantity == "vphir":
        data = dfile["ucon"][:, :, :, 3]/dfile["ucon"][:, :, :, 0]
    elif quantity == "vrr":
        data = dfile["ucon"][:, :, :, 1]/dfile["ucon"][:, :, :, 0]
    # Three-velocities in BL!!!
    elif quantity == "vphir_bl":
        data = dfile["ucon_bl"][:, :, :, 3]/dfile["ucon_bl"][:, :, :, 0]
    elif quantity == "vrr_bl":
        data = dfile["ucon_bl"][:, :, :, 1]/dfile["ucon_bl"][:, :, :, 0]
    # Surface density is different because it is integrated from the disk midplane
    # to the theta value. See Calculation Note "Optical Depth and Surface Density"
    elif quantity == "surface_density":
        midplane_ind = int(weight.shape[1]/2)
        top_hemi = weight[:, :midplane_ind, :]
        bottom_hemi = weight[:, midplane_ind:, :]

        # Remember that geom[X2] is the same for all r, phi, so just choose.
        x2 = geom['X2'][0, :, 0]
        top_integral = np.abs((integrate.cumtrapz(top_hemi[:, ::-1, :], x=(x2[:midplane_ind])[::-1], axis=1, initial=0))[:, ::-1, :])
        bottom_integral = (integrate.cumtrapz(bottom_hemi[:, :, :], x=x2[midplane_ind:], axis=1, initial=0))
        data = np.concatenate((top_integral, bottom_integral), axis=1)
    # Optical depth is different because it is integrated from the pole
    # to the theta value. See Calculation Note "Optical Depth and Surface Density".
    elif quantity == "optical_depth":
        # Average over hemispheres
        midplane_ind = int(weight.shape[1]/2)
        top_hemi = weight[:, :midplane_ind, :]
        bottom_hemi = weight[:, midplane_ind:, :]
        averaged_hemi = (top_hemi + bottom_hemi[:, ::-1, :])/2.

        # Remember that geom[X2] is the same for all r, phi, so just choose.
        x2 = geom['X2'][0, :midplane_ind, 0]
        mass_integral = np.abs(integrate.cumtrapz(averaged_hemi, x=x2, axis=1, initial=0))
        convert_to_cgs = header["M_unit"]/header["L_unit"]**2
        opacity = 0.4
        tau_data = mass_integral*convert_to_cgs*opacity
        # Stitch back together for full 3D slice
        data = np.concatenate((tau_data, tau_data[:, ::-1, :]), axis=1)
    elif quantity == "mass_flux":
        data = -(dfile["ucon"][:, :, :, 1]*weight)
    elif quantity == "magnetic_flux":
        B1 = dfile['B1']
        data = 0.5*(4.*np.pi)**0.5*np.abs(gdet*B1)
    # *Approximately* compute
    # number of cells resolving one MRI wavelength
    elif "Qmri" in quantity:
        dx2 = geom['X2'][0,1,0]-geom['X2'][0,0,0]
        if quantity == "Qmri_theta":
            direction = 2
        elif quantity == "Qmri_phi":
            direction = 3
        vaudir = np.abs(dfile["bcon"][:,:,:,direction])/np.sqrt(dfile["RHO"]+dfile["bsq"]+header["gam"]*dfile["UU"])
        omega = dfile["ucon"][:,:,:,3]/dfile["ucon"][:,:,:,0]+1e-15# dxdxp[3][3] needed here but = 1 in usual x3 = phi coords
        lambdamriudir = 2*np.pi * vaudir / omega
        if direction == 2:
            data = lambdamriudir/dx2
        elif direction == 3:
            if header["N3"] == 1:
                print("Cannot calculate Qmri_phi for 2D simulation")
                data = np.zeros(lambdamriudir.shape)
            else:
                dx3 = geom['X3'][0,0,1]-geom['X3'][0,0,0]
                data = lambdamriudir/dx3
    else:
        print(quantity + " calculation not implemented yet.")
    return data


def get_quantities_to_retrieve_raw(quantities):
    """
    Prune the quantities list to include exactly the ones that are needed,
    so that load_dump only calculates the ones that are
    needed and aren't there already.
    In particular, make sure that the raw calculated quantities have all the
    needed raw quantities.
    """
    q_to_retrieve = quantities.copy()
    q_reduced = get_list_of_raw_calculated_quantities()
    for q in quantities:
        # Standard and raw calculated quantities are already in q_retrieve,
        # so just need to check reduced calculated quantities
        if q in q_reduced:
            q_to_retrieve.remove(q)
            for q_calc in q_reduced[q]:
                if q_calc not in q_to_retrieve:
                    q_to_retrieve.append(q_calc)
    return q_to_retrieve


def get_quantities_to_retrieve(quantities):
    """
    Prune the quantities list to include exactly the ones that are needed,
    so that retrieving shell averages only calculates the ones that are
    needed and aren't there already.
    In particular, make sure that the reduced quantities have all the
    needed raw/raw calculated quantities.
    """
    q_to_retrieve = quantities.copy()
    q_reduced = get_list_of_reduced_calculated_quantities()
    for q in quantities:
        # Standard and raw calculated quantities are already in q_retrieve,
        # so just need to check reduced calculated quantities
        if q in q_reduced:
            q_to_retrieve.remove(q)
            for q_calc in q_reduced[q]:
                if q_calc not in q_to_retrieve:
                    q_to_retrieve.append(q_calc)
    return q_to_retrieve


def calculate_reduced_quantity(quantity, shell_avgs, r_vals, r_index=None, smooth=False, neighbors=10, sim_units=None, EH_ind=None, tavg=True):
    """
    Calculate reduced quantities, i.e. quantities that are
    computed from shell-averaged quantities.

    This will time average over the times passed to it if r_index is None.
    if r_index has a value, it will return the quantity at that index over time.
    """
    qs_needed = get_list_of_reduced_calculated_quantities()[quantity]
    for q in qs_needed:
        if q not in shell_avgs:
            print("Missing " + q + ", so skipping " + quantity)
            return None

    if quantity == "H_over_R" or quantity == "H_over_R0":
        if quantity == "H_over_R":
            shell_array = shell_avgs["H2"]
        elif quantity == "H_over_R0":
            # Subtract off the first moment of H/r:
            # variance = std^2 = <H^2> - <H>^2
            shell_array = shell_avgs["H2"] - shell_avgs["Hmean"]**2
        shell_array = np.sqrt(shell_array)
        if r_index:
            shell_data = shell_array[:, r_index]/r_vals[r_index]
        elif shell_array.ndim > 1 and tavg:
            shell_data = np.nanmean(shell_array, axis=0)/r_vals
        else:
            shell_data = shell_array/r_vals
    elif quantity == "coulCoronaPost":
        if r_index:
            shell_data1 = shell_avgs["Qcoul_corona"][:, r_index]
            shell_data2 = shell_avgs["Qcoul"][:, r_index]
        elif shell_avgs["Qcoul"].ndim > 1 and tavg:
            shell_data1 = np.nanmean(shell_avgs["Qcoul_corona"], axis=0)
            shell_data2 = np.nanmean(shell_avgs["Qcoul"], axis=0)
        else:
            shell_data1 = shell_avgs["Qcoul_corona"]
            shell_data2 = shell_avgs["Qcoul"]
        shell_data = shell_data1/shell_data2
    elif quantity == "coolCoronaPost":
        if r_index:
            shell_data1 = shell_avgs["Qcool_corona"][:, r_index]
            shell_data2 = shell_avgs["Qcool"][:, r_index]
        elif shell_avgs["Qcool"].ndim > 1 and tavg:
            shell_data1 = np.nanmean(shell_avgs["Qcool_corona"], axis=0)
            shell_data2 = np.nanmean(shell_avgs["Qcool"], axis=0)
        else:
            shell_data1 = shell_avgs["Qcool_corona"]
            shell_data2 = shell_avgs["Qcool"]
        shell_data = shell_data1/shell_data2
    elif quantity == "coolCoulCoronaRatioPost":
        if r_index:
            shell_data1 = shell_avgs["Qcool_corona"][:, r_index]
            shell_data2 = shell_avgs["Qcoul_corona"][:, r_index]
        elif shell_avgs["Qcool_corona"].ndim > 1 and tavg:
            shell_data1 = np.nanmean(shell_avgs["Qcool_corona"], axis=0)
            shell_data2 = np.nanmean(shell_avgs["Qcoul_corona"], axis=0)
        else:
            shell_data1 = shell_avgs["Qcool_corona"]
            shell_data2 = shell_avgs["Qcoul_corona"]
        shell_data = shell_data1/shell_data2
    elif quantity == "coulOptThinPost":
        if r_index:
            shell_data1 = shell_avgs["Qcoul_opt_thin"][:, r_index]
            shell_data2 = shell_avgs["Qcoul"][:, r_index]
        elif shell_avgs["Qcoul"].ndim > 1 and tavg:
            shell_data1 = np.nanmean(shell_avgs["Qcoul_opt_thin"], axis=0)
            shell_data2 = np.nanmean(shell_avgs["Qcoul"], axis=0)
        else:
            shell_data1 = shell_avgs["Qcoul_opt_thin"]
            shell_data2 = shell_avgs["Qcoul"]
        shell_data = shell_data1/shell_data2
    elif quantity == "coolOptThinPost":
        if r_index:
            shell_data1 = shell_avgs["Qcool_opt_thin"][:, r_index]
            shell_data2 = shell_avgs["Qcool"][:, r_index]
        elif shell_avgs["Qcool"].ndim > 1 and tavg:
            shell_data1 = np.nanmean(shell_avgs["Qcool_opt_thin"], axis=0)
            shell_data2 = np.nanmean(shell_avgs["Qcool"], axis=0)
        else:
            shell_data1 = shell_avgs["Qcool_opt_thin"]
            shell_data2 = shell_avgs["Qcool"]
        shell_data = shell_data1/shell_data2
    elif quantity == "viscEOptThinPost":
        if r_index:
            shell_data1 = shell_avgs["Qvisc_e_opt_thin"][:, r_index]
            shell_data2 = shell_avgs["Qvisc_e"][:, r_index]
        elif shell_avgs["Qvisc_e"].ndim > 1 and tavg:
            shell_data1 = np.nanmean(shell_avgs["Qvisc_e_opt_thin"], axis=0)
            shell_data2 = np.nanmean(shell_avgs["Qvisc_e"], axis=0)
        else:
            shell_data1 = shell_avgs["Qvisc_e_opt_thin"]
            shell_data2 = shell_avgs["Qvisc_e"]
        shell_data = shell_data1/shell_data2
    elif quantity == "viscPOptThinPost":
        if r_index:
            shell_data1 = shell_avgs["Qvisc_p_opt_thin"][:, r_index]
            shell_data2 = shell_avgs["Qvisc_p"][:, r_index]
        elif shell_avgs["Qvisc_p"].ndim > 1 and tavg:
            shell_data1 = np.nanmean(shell_avgs["Qvisc_p_opt_thin"], axis=0)
            shell_data2 = np.nanmean(shell_avgs["Qvisc_p"], axis=0)
        else:
            shell_data1 = shell_avgs["Qvisc_p_opt_thin"]
            shell_data2 = shell_avgs["Qvisc_p"]
        shell_data = shell_data1/shell_data2
    elif quantity == "coolTotalRatioPost":
        if r_index:
            shell_data1 = shell_avgs["Qcool"][:, r_index]
            shell_data2 = shell_avgs["Qcoul"][:, r_index] + shell_avgs["Qvisc_e"][:, r_index]
            if smooth:
                box = np.ones(neighbors)/neighbors
                shell_data1 = np.convolve(shell_data1, box, mode='same')
                shell_data2 = np.convolve(shell_data2, box, mode='same')
        elif shell_avgs["Qcool"].ndim > 1 and tavg:
            shell_data1 = np.nanmean(shell_avgs["Qcool"], axis=0)
            shell_data2 = np.nanmean(shell_avgs["Qvisc_e"] + shell_avgs["Qcoul"], axis=0)
        else:
            shell_data1 = shell_avgs["Qcool"]
            shell_data2 = shell_avgs["Qvisc_e"]
        shell_data = shell_data1/shell_data2
    elif quantity == "coulViscRatioPost":
        if r_index:
            shell_data1 = shell_avgs["Qcoul"][:, r_index]
            shell_data2 = shell_avgs["Qvisc_e"][:, r_index]
            if smooth:
                box = np.ones(neighbors)/neighbors
                shell_data1 = np.convolve(shell_data1, box, mode='same')
                shell_data2 = np.convolve(shell_data2, box, mode='same')
        elif shell_avgs["Qcoul"].ndim > 1 and tavg:
            shell_data1 = np.nanmean(shell_avgs["Qcoul"], axis=0)
            shell_data2 = np.nanmean(shell_avgs["Qvisc_e"], axis=0)
        else:
            shell_data1 = shell_avgs["Qcoul"]
            shell_data2 = shell_avgs["Qvisc_e"]
        shell_data = shell_data1/shell_data2
    elif quantity == "coolCoulRatioPost":
        if r_index:
            shell_data1 = shell_avgs["Qcool"][:, r_index]
            shell_data2 = shell_avgs["Qcoul"][:, r_index]
        elif shell_avgs["Qcool"].ndim > 1 and tavg:
            shell_data1 = np.nanmean(shell_avgs["Qcool"], axis=0)
            shell_data2 = np.nanmean(shell_avgs["Qcoul"], axis=0)
        else:
            shell_data1 = shell_avgs["Qcool"]
            shell_data2 = shell_avgs["Qcoul"]
        shell_data = shell_data1/shell_data2
    elif quantity == "coolViscRatioPost":
        if r_index:
            shell_data1 = shell_avgs["Qcool"][:, r_index]
            shell_data2 = shell_avgs["Qvisc_e"][:, r_index]
            if smooth:
                box = np.ones(neighbors)/neighbors
                shell_data1 = np.convolve(shell_data1, box, mode='same')
                shell_data2 = np.convolve(shell_data2, box, mode='same')
        elif shell_avgs["Qcool"].ndim > 1 and tavg:
            shell_data1 = np.nanmean(shell_avgs["Qcool"], axis=0)
            shell_data2 = np.nanmean(shell_avgs["Qvisc_e"], axis=0)
        else:
            shell_data1 = shell_avgs["Qcool"]
            shell_data2 = shell_avgs["Qvisc_e"]
        shell_data = shell_data1/shell_data2
    elif quantity == "hotPhaseFracPost_e":
        shell_array = shell_avgs["frac_above_target_e"]
        if r_index:
            shell_data = shell_array[:, r_index]
        elif shell_avgs["frac_above_target_e"].ndim > 1 and tavg:
            shell_data = np.nanmean(shell_array, axis=0)
        else:
            shell_data = shell_array
    elif quantity == "hotPhaseFracPost_p":
        shell_array = shell_avgs["frac_above_target_p"]
        if r_index:
            shell_data = shell_array[:, r_index]
        elif shell_avgs["frac_above_target_p"].ndim > 1 and tavg:
            shell_data = np.nanmean(shell_array, axis=0)
        else:
            shell_data = shell_array
    elif quantity == "QcoolHotPhaseFracPost_e":
        if r_index:
            shell_data1 = shell_avgs["Qcool_hotPhase_e"][:, r_index]
            shell_data2 = shell_avgs["Qcool"][:, r_index]
        elif shell_avgs["Qcool"].ndim > 1 and tavg:
            shell_data1 = np.nanmean(shell_avgs["Qcool_hotPhase_e"], axis=0)
            shell_data2 = np.nanmean(shell_avgs["Qcool"], axis=0)
        else:
            shell_data1 = shell_avgs["Qcool_hotPhase_e"]
            shell_data2 = shell_avgs["Qcool"]
        shell_data = shell_data1/shell_data2
    elif quantity == "QcoolHotPhaseFracPost_p":
        if r_index:
            shell_data1 = shell_avgs["Qcool_hotPhase_p"][:, r_index]
            shell_data2 = shell_avgs["Qcool"][:, r_index]
        elif shell_avgs["Qcool"].ndim > 1 and tavg:
            shell_data1 = np.nanmean(shell_avgs["Qcool_hotPhase_p"], axis=0)
            shell_data2 = np.nanmean(shell_avgs["Qcool"], axis=0)
        else:
            shell_data1 = shell_avgs["Qcool_hotPhase_p"]
            shell_data2 = shell_avgs["Qcool"]
        shell_data = shell_data1/shell_data2
    elif quantity == "QcoulHotPhaseFracPost_e":
        if r_index:
            shell_data1 = shell_avgs["Qcoul_hotPhase_e"][:, r_index]
            shell_data2 = shell_avgs["Qcoul"][:, r_index]
        elif shell_avgs["Qcoul"].ndim > 1 and tavg:
            shell_data1 = np.nanmean(shell_avgs["Qcoul_hotPhase_e"], axis=0)
            shell_data2 = np.nanmean(shell_avgs["Qcoul"], axis=0)
        else:
            shell_data1 = shell_avgs["Qcoul_hotPhase_e"]
            shell_data2 = shell_avgs["Qcoul"]
        shell_data = shell_data1/shell_data2
    elif quantity == "QcoulHotPhaseFracPost_p":
        if r_index:
            shell_data1 = shell_avgs["Qcoul_hotPhase_p"][:, r_index]
            shell_data2 = shell_avgs["Qcoul"][:, r_index]
        elif shell_avgs["Qcoul"].ndim > 1 and tavg:
            shell_data1 = np.nanmean(shell_avgs["Qcoul_hotPhase_p"], axis=0)
            shell_data2 = np.nanmean(shell_avgs["Qcoul"], axis=0)
        else:
            shell_data1 = shell_avgs["Qcoul_hotPhase_p"]
            shell_data2 = shell_avgs["Qcoul"]
        shell_data = shell_data1/shell_data2
    elif quantity == "Te":
        shell_array = shell_avgs["Thetae"]
        if r_index:
            shell_data = get_Te(shell_array)[:, r_index]
        elif shell_avgs["Thetae"].ndim > 1 and tavg:
            shell_data = np.nanmean(get_Te(shell_array), axis=0)
        else:
            shell_data = get_Te(shell_array)
    elif quantity == "Tp":
        shell_array = shell_avgs["Thetap"]
        if r_index:
            shell_data = get_Tp(shell_array)[:, r_index]
        elif shell_avgs["Thetap"].ndim > 1 and tavg:
            shell_data = np.nanmean(get_Tp(shell_array), axis=0)
        else:
            shell_data = get_Tp(shell_array)
    elif quantity == "ueRatio":
        if r_index:
            shell_data1 = shell_avgs["ue"][:, r_index]
            shell_data2 = shell_avgs["UU"][:, r_index]
        elif shell_avgs["UU"].ndim > 1 and tavg:
            shell_data1 = np.nanmean(shell_avgs["ue"], axis=0)
            shell_data2 = np.nanmean(shell_avgs["UU"], axis=0)
        else:
            shell_data1 = shell_avgs["ue"]
            shell_data2 = shell_avgs["UU"]
        shell_data = shell_data1/shell_data2
    elif quantity == "HT_over_R":
        P_cgs = shell_avgs['PRESS']*sim_units['U_unit']
        rho_cgs = shell_avgs['RHO']*sim_units['RHO_unit']
        cs_cgs = np.sqrt(P_cgs/rho_cgs/sim_units['gam'])
        r_cgs = r_vals*sim_units['L_unit']
        vK_cgs = np.sqrt(units["GNEWT"]*sim_units["Mbh"]/r_cgs)
        if r_index:
            shell_data1 = cs_cgs[:, r_index]
            shell_data2 = vK_cgs[:, r_index]
            shell_data = np.sqrt(2./np.pi)*shell_data1/shell_data2
        elif rho_cgs.ndim > 1 and tavg:
            # shell_data1 = np.nanmean(cs_cgs, axis=0)
            # shell_data2 = np.nanmean(vK_cgs, axis=0)
            # shell_data = np.sqrt(2./np.pi)*shell_data1/shell_data2
            shell_data = np.sqrt(2./np.pi)*np.nanmean(cs_cgs/vK_cgs, axis=0)
        else:
            shell_data = np.sqrt(2./np.pi)*cs_cgs/vK_cgs
    # -----------------------------------------------
    # NOTE: these timescales are all in the PROPER frame
    # -----------------------------------------------
    elif quantity == "tinfall":
        if EH_ind is not None:
            r_vals = r_vals[EH_ind:]
        else:
            EH_ind = 0
        uR_array = shell_avgs["ucon_bl1"]
        if r_index:
            # shell_data = get_Tp(shell_array)[:, r_index]
            print("Not implemented yet.")
        # First time-average, then integrate to get infall time
        elif shell_avgs["ucon_bl1"].ndim > 1 and tavg:
            uR_array = np.nanmean(uR_array, axis=0)
        uR_array = np.abs(uR_array)[EH_ind:]
        shell_data = np.hstack([0, integrate.cumtrapz(1.0/uR_array, r_vals)])
    elif quantity == "theat_e":
        # Average *before* dividing!
        ue = shell_avgs["ue"]
        Qvisc_e = shell_avgs["Qvisc_e"]
        # # Multiply by u^0 to convert from proper time to coordinate time
        # uT_array = shell_avgs["ucon_bl0"]
        # shell_array = shell_array*uT_array
        if r_index:
            ue = ue[:, r_index]
            Qvisc_e = Qvisc_e[:, r_index]
        elif ue.ndim > 1 and tavg:
            ue = np.nanmean(ue, axis=0)
            Qvisc_e = np.nanmean(Qvisc_e, axis=0)
        shell_data = ue/Qvisc_e
    elif quantity == "theat_p":
        # Average *before* dividing!
        up = shell_avgs["up"]
        Qvisc_p = shell_avgs["Qvisc_p"]
        # # Multiply by u^0 to convert from proper time to coordinate time
        # uT_array = shell_avgs["ucon_bl0"]
        # shell_array = shell_array*uT_array
        if r_index:
            up = up[:, r_index]
            Qvisc_p = Qvisc_p[:, r_index]
        elif up.ndim > 1 and tavg:
            up = np.nanmean(up, axis=0)
            Qvisc_p = np.nanmean(Qvisc_p, axis=0)
        shell_data = up/Qvisc_p
    elif quantity == "theat":
        # Average *before* dividing!
        ug = shell_avgs["UU"]
        Qvisc_p = shell_avgs["Qvisc_p"]
        # # Multiply by u^0 to convert from proper time to coordinate time
        # uT_array = shell_avgs["ucon_bl0"]
        # shell_array = shell_array*uT_array
        if r_index:
            ug = ug[:, r_index]
            Qvisc_p = Qvisc_p[:, r_index]
        elif ug.ndim > 1 and tavg:
            ug = np.nanmean(ug, axis=0)
            Qvisc_p = np.nanmean(Qvisc_p, axis=0)
        if smooth:
            box = np.ones(neighbors)/neighbors
            ug = np.convolve(ug, box, mode='same')
            Qvisc_p = np.convolve(Qvisc_p, box, mode='same')
        shell_data = ug/Qvisc_p
    elif quantity == "tCoulomb_e":
        # Average *before* dividing!
        # Important for when it starts fluctuating close to zero
        # Do not need units because we want the time in code units (rg/c)
        ue = shell_avgs["ue"]
        Qcoul = shell_avgs["Qcoul"]
        # # Multiply by u^0 to convert from proper time to coordinate time
        # uT_array = shell_avgs["ucon_bl0"]
        # shell_array = shell_array*uT_array
        if r_index:
            ue = ue[:, r_index]
            Qcoul = Qcoul[:, r_index]
            # shell_data = shell_array[:, r_index]
        elif shell_avgs["ue"].ndim > 1 and tavg:
            ue = np.nanmean(ue, axis=0)
            Qcoul = np.nanmean(Qcoul, axis=0)
            # shell_data = np.nanmean(shell_array, axis=0)
        # else:
            # shell_data = shell_array
            # ue = np.nanmean(ue, axis=0)
            # Qcoul = np.nanmean(Qcoul, axis=0)
        shell_data = ue/Qcoul
    elif quantity == "tCool_e":
        # Average *before* dividing!
        # Important for when it starts fluctuating close to zero
        # Do not need units because we want the time in code units (rg/c)
        ue = shell_avgs["ue"]
        Qcool = shell_avgs["Qcool"]
        # # Multiply by u^0 to convert from proper time to coordinate time
        # uT_array = shell_avgs["ucon_bl0"]
        # shell_array = shell_array*uT_array
        if r_index:
            ue = ue[:, r_index]
            Qcool = Qcool[:, r_index]
            # shell_data = shell_array[:, r_index]
        elif shell_avgs["ue"].ndim > 1 and tavg:
            ue = np.nanmean(ue, axis=0)
            Qcool = np.nanmean(Qcool, axis=0)
            # shell_data = np.nanmean(shell_array, axis=0)
        # else:
            # shell_data = shell_array
            # ue = np.nanmean(ue, axis=0)
            # Qcool = np.nanmean(Qcool, axis=0)
        shell_data = ue/Qcool
    elif quantity == "tCoulomb_p":
        # Average *before* dividing!
        # Important for when it starts fluctuating close to zero
        # Do not need units because we want the time in code units (rg/c)
        up = shell_avgs["up"]
        Qcoul = shell_avgs["Qcoul"]
        # # Multiply by u^0 to convert from proper time to coordinate time
        # uT_array = shell_avgs["ucon_bl0"]
        # shell_array = shell_array*uT_array
        if r_index:
            up = up[:, r_index]
            Qcoul = Qcoul[:, r_index]
            # shell_data = shell_array[:, r_index]
        elif shell_avgs["up"].ndim > 1 and tavg:
            up = np.nanmean(up, axis=0)
            Qcoul = np.nanmean(Qcoul, axis=0)
            # shell_data = np.nanmean(shell_array, axis=0)
        # else:
            # shell_data = shell_array
            # up = np.nanmean(up, axis=0)
            # Qcoul = np.nanmean(Qcoul, axis=0)
        shell_data = up/Qcoul
    elif quantity == "coulombQuality":
        ue = shell_avgs["ue"]
        Qcoul = shell_avgs["Qcoul"]
        # shell_array = ue/Qcoul/(2.e-3)
        if r_index:
            ue = ue[:, r_index]
            Qcoul = Qcoul[:, r_index]
            # shell_data = shell_array[:, r_index]
        elif shell_avgs["ue"].ndim > 1 and tavg:
            ue = np.nanmean(ue, axis=0)
            Qcoul = np.nanmean(Qcoul, axis=0)
            # shell_data = np.nanmean(shell_array, axis=0)
        # else:
            # shell_data = shell_array
        shell_data = ue/Qcoul/(2.e-3)
    else:
        print("Cannot calculate reduced quantity " + quantity)
    return shell_data


def times_dict_to_array(times_dict, tstart, tend):
    times_array = [times_dict[tstart]]
    for t in [*range(tstart + 1, tend + 1)]:
        times_array.append(times_dict[t])
    return times_array


def get_times_needed(dictionary, times_to_calculate, tstart, tend):
    """
    Called in calculate_* functions.
    Find which times need to be calculated (avoid re-calculating
    if the time and quantity is already in the dictionary).
    Need to do all this fanciness in this function because the retrieve
    function will not necessarily have access to raw data
    """
    all_times_needed = []
    for quantity in times_to_calculate:
        # Check for -1 in times_to_calculate array, add on full times
        # if necessary
        if times_to_calculate[quantity][-1] == -1:
            if quantity in dictionary:
                times_already_calculated = sorted(dictionary[quantity].keys())
                if len(times_already_calculated) == 0:
                    times_to_calculate[quantity] = [*range(tstart, tend)]
                else:
                    latest_time = times_already_calculated[-1]
                    # qtimes: to calculate, not involving -1
                    qtimes = times_to_calculate[quantity][:-1]
                    if latest_time != tend - 1:
                        t_to_add = [*range(times_already_calculated[-1], tend)]
                    else:
                        t_to_add = []
                    times_to_calculate[quantity] = qtimes + t_to_add
            else:
                times_to_calculate[quantity] = [*range(tstart, tend)]

        times_to_add = list(set(times_to_calculate[quantity]).difference(all_times_needed))
        all_times_needed = all_times_needed + times_to_add
    return (times_to_calculate, all_times_needed)


def get_list_of_dumps(raw_data_path):
    files = sorted(glob.glob(raw_data_path + "dump*.h5"))
    return files


def get_list_of_reduced_files(reduced_data_path):
    files = sorted(glob.glob(reduced_data_path + "t0*.p"))
    return files


def get_dump_num_from_path(dump_path):
    return int((dump_path.split("/")[-1]).strip("dump_").replace(".h5", ""))


def clean_reduced_quantities_list(quantities, header):
    if not header["COOLING"]:
        print("Removing COOLING quantities")
        for q in get_list_of_cooling_quantities():
            if q in quantities: quantities.remove(q)
    if not header["COULOMB"]:
        print("Removing COULOMB quantities")
        for q in get_list_of_coulomb_quantities():
            if q in quantities: quantities.remove(q)
    # # Vector quantities don't work well being shell averaged (yet),
    # # so just remove them for now.
    # for q in get_list_of_standard_vector_quantities():
        # if q in quantities:
            # quantities.remove(q)
    # Don't calculate Qmri_phi for 2D simulation
    if header["N3"] == 1 and "Qmri_phi" in quantities:
        quantities.remove("Qmri_phi")
    return quantities


def clean_raw_quantities_list(quantities, header):
    cleaned_qs = []
    # Get rid of quantities that would result in errors
    if not header["COOLING"]:
        for q in get_list_of_cooling_quantities():
            if q in quantities: quantities.remove(q)
    if not header["COULOMB"]:
        for q in get_list_of_coulomb_quantities():
            if q in quantities: quantities.remove(q)

    # Get rid of unknown quantities
    for q in quantities:
        if q in get_list_of_standard_quantities() or q in get_list_of_raw_calculated_quantities():
            cleaned_qs.append(q)
        elif q[:-1] in get_list_of_standard_vector_quantities():
            cleaned_qs.append(q[:-1])
        elif q not in get_list_of_reduced_calculated_quantities():
            print("Unknown quantity " + q)
    return cleaned_qs


def keplerian_freq(r_vals, spin):
    omega_k = 1.0/(r_vals**1.5 + spin)
    return omega_k


def get_Hr_from_targetT(targetT, rvals, gamma):
    """
    Assuming temperature is AT the target for protons/electrons.
    Assuming H = cs/Omega, see calculations note "Disk scale height".
    Note that targetT does not have to be constant.
    """
    constant_val = np.sqrt(units['KBOL']*gamma/(units['MP']*units['CL']**2))
    return constant_val*np.sqrt(targetT*rvals)/np.sqrt(2.0)


def check_same_targets(targets):
    """
    targets is a dictionary with keys corresponding to simulation names
    and values corresponding to the arrays I need to check for sameness.
    """
    to_plot = {}
    for s, t in targets.items():
        to_compare = list(to_plot.keys())
        for s1 in to_compare:
            if np.allclose(t, targets[s1]):
                to_plot[s] = False
                continue
        if s not in to_plot:
            to_plot[s] = True
    return to_plot


# def ISCO_freq(r_vals, spin):
    # return 1.0
#
#
# def get_dynamical_time(r_vals, rISCO, spin):
    # keplerian_times = 1.0/keplerian_freq(r_vals, spin)
    # ISCO_times = 1.0/ISCO_freq(r_vals, spin)
    # dynamical_times = np.where(r_vals > rISCO, keplerian_times, ISCO_times)
    # return dynamical_times
