# ------------------------------
#      Default file paths here
# ------------------------------
def get_default_filepaths(sim_name, setup='lia_nasa'):
    if setup == "lia_hp":
        # For simulations run on my laptop.
        # Presumably only tests, hence the tests folder.
        path_to_raw_data = "/mnt/c/Users/liaha/research/projects/ebhlight_code/ebhlight_ecool/prob/" + sim_name + "/dumps/"
        path_to_reduced_data = "/mnt/c/Users/liaha/research/projects/ebhlight_tests/reduced_data/" + sim_name + "/"
        path_to_figures = "/mnt/c/Users/liaha/research/projects/ebhlight_tests/figures/" + sim_name + "/"
    elif setup == "lia_dell":
        path_to_raw_data = "/mnt/c/Users/liaha/Downloads/" + sim_name + "/dumps/"
        path_to_reduced_data = "/mnt/c/Users/liaha/Downloads/" + sim_name + "/reduced_data/"
        path_to_figures = "/mnt/c/Users/liaha/Downloads/" + sim_name + "/figures/"
        # # For simulations run on my laptop.
    elif setup == "seagate_tests":
        path_to_raw_data = "/mnt/d/ebhlight_ecool_tests/raw_data/" + sim_name + "/dumps/"
        path_to_reduced_data = "/mnt/d/ebhlight_ecool_tests/reduced_data/" + sim_name + "/"
        path_to_figures = "/mnt/d/ebhlight_ecool_tests/figures/" + sim_name + "/"
    elif setup == "lia_lou":
        path_to_raw_data = "/u/ahankla/raw_data/" + sim_name + "/dumps/"
        path_to_reduced_data = "/u/ahankla/reduced_data/" + sim_name + "/"
        path_to_figures = "/home4/ahankla/figures/" + sim_name + "/"
    elif setup == "lia_nasa":
        path_to_raw_data = "/nobackup/ahankla/ebhlight_ecool/prob/" + sim_name + "/dumps/"
        # path_to_reduced_data = "/home4/ahankla/reduced_data/" + sim_name + "/"
        path_to_reduced_data = "/nobackup/ahankla/reduced_data/" + sim_name + "/"
        # path_to_figures = "/home4/ahankla/figures/" + sim_name + "/"
        path_to_figures = "/nobackup/ahankla/figures/" + sim_name + "/"
    path_to_properties = path_to_reduced_data + "properties.p"
    path_to_geom = path_to_reduced_data + "geom.p"
    path_to_xyz = path_to_reduced_data + "xyz.p"
    path_to_dX2 = path_to_reduced_data + "dX2.p"
    path_to_gdet = path_to_reduced_data + "gdet.p"
    return (path_to_raw_data, path_to_reduced_data,
            path_to_figures, path_to_properties,
            path_to_geom, path_to_xyz, path_to_dX2,
            path_to_gdet)
