import numpy as np
import os
import h5py as h5
import glob
import shutil
from ast import literal_eval
import csv
import pickle
import sys
sys.path.append('modules/')
from plotting_utils import *
from ebhlight_tools import *
import hdf5_to_dict as io
from default_filepaths import get_default_filepaths


# Functions that are common to both the post processor
# and the simulation plotter
class base_simulation_analyzer:
    def __init__(self, sim_name, setup="lia_nasa", **kwargs):
        self.geom = None
        self.xyz_geom = None
        self.dX2 = None
        self.gdet = None
        self.quiet = kwargs.get("quiet", False)
        self.sim_name = sim_name
        self.setup = setup
        default_paths = get_default_filepaths(sim_name, setup)
        path_to_raw_data = default_paths[0]
        path_to_reduced_data = default_paths[1]
        path_to_figures = default_paths[2]
        path_to_properties = default_paths[3]
        path_to_geom = default_paths[4]
        path_to_xyz = default_paths[5]
        path_to_dX2 = default_paths[6]
        path_to_gdet = default_paths[7]
        # ------------------------------
        # If paths are non-standard, set them here using kwargs
        # Otherwise, use the defaults from above
        # ------------------------------
        self.path_to_raw_data = kwargs.get("path_to_raw_data", path_to_raw_data)
        self.path_to_reduced_data = kwargs.get("path_to_reduced_data", path_to_reduced_data)
        self.path_to_figures = kwargs.get("path_to_figures", path_to_figures)
        self.path_to_properties = kwargs.get("path_to_properties", path_to_properties)
        self.path_to_geom = kwargs.get("path_to_geom", path_to_geom)
        self.path_to_xyz = kwargs.get("path_to_xyz", path_to_xyz)
        self.path_to_dX2 = kwargs.get("path_to_dX2", path_to_dX2)
        self.path_to_gdet = kwargs.get("path_to_gdet", path_to_gdet)

        self.get_list_of_quantities()
        self.get_labels()

        return

    def load_gdet(self):
        """
        All that's needed of geom for a lot of calculate_raw_quantity
        is gdet, so save as a separate file.
        """
        if not os.path.exists(self.path_to_gdet):
            self.load_geom()
            gdet = self.geom['gdet']
            with open(self.path_to_gdet, 'wb') as fh:
                pickle.dump(gdet, fh)
        else:
            gdet = pickle.load(open(self.path_to_gdet, 'rb'))
        self.gdet = gdet
        return

    def load_dtheta_geom(self):
        """
        All that's needed of geom for cumulative integrals is geom['X2']
        So save these as a separate file.
        """
        if not os.path.exists(self.path_to_dX2):
            self.load_geom()
            dX2 = self.geom['X2'][0, 1, 0] - self.geom['X2'][0, 0, 0]
            with open(self.path_to_dX2, 'wb') as fh:
                pickle.dump(dX2, fh)
        else:
            dX2 = pickle.load(open(self.path_to_dX2, 'rb'))
        self.dX2 = dX2
        return

    def load_xyz_geom(self):
        """
        All that's needed of geom for plotting is geom['x'] y and z.
        So save these as a separate file.
        """
        if not os.path.exists(self.path_to_xyz):
            self.load_geom()
            x = self.geom['x'].copy()
            y = self.geom['y'].copy()
            z = self.geom['z'].copy()
            xyz_geom = {'x':x, 'y':y, 'z':z}
            with open(self.path_to_xyz, 'wb') as fh:
                pickle.dump(xyz_geom, fh)
        else:
            xyz_geom = pickle.load(open(self.path_to_xyz, 'rb'))
        self.xyz_geom = xyz_geom
        return

    def load_geom(self):
        """
        Only load geom file when necessary because it's large.
        """
        self.geom = pickle.load(open(self.path_to_geom, 'rb'))
        return

    def load_properties(self, convert=True):
        """
        Mostly generic loading in properties. Except:
        for many simulations, had saved geom file within properties file. Now,
        want to load it from a separate file.

        So if geom is in the properties file, load it and save to new file.
        """
        properties = pickle.load(open(self.path_to_properties, 'rb'))
        self.header = properties['header']
        # self.diag = properties["diag"]
        self.gdet = properties["gdet"]
        self.r_grid = properties["r_grid"]
        self.theta_grid = properties["theta_grid"]
        self.has_radiation = properties["has_radiation"]
        self.has_electrons = properties["has_electrons"]
        self.has_cooling = properties["has_cooling"]
        self.rEH = properties["rEH"]
        self.rISCO = properties["rISCO"]

        # TODO: might need to check default Coulomb collision setting
        if "COULOMB" not in self.header.keys():
            self.header["COULOMB"] = False

        # -----------------------------
        # If geom in properties, convert to separate file,
        # DELETE it from properties, and re-save properties
        # -----------------------------
        if "geom" in properties:
            print("Saving geom to its own file: " + self.path_to_geom)
            geom = properties["geom"]
            with open(self.path_to_geom, 'wb') as fh:
                pickle.dump(geom, fh)
            del properties["geom"]
            with open(self.path_to_properties, 'wb') as fh:
                pickle.dump(properties, fh)
        return

    def update_path_to_figures(self, new_path):
        self.path_to_figures = new_path
        return

    def update_path_to_reduced_data(self, new_path):
        self.path_to_reduced_data = new_path
        return

    def update_path_to_raw_data(self, new_path):
        self.path_to_raw_data = new_path
        return

    def set_mbh(self, new_value):
        self.header["mbh"] = new_value
        self.header["Mbh"] = new_value*units["MSOLAR"]
        return

    def get_list_of_quantities(self):
        self.list_of_standard_quantities = get_list_of_standard_quantities()
        self.list_of_raw_calculated_quantities = get_list_of_raw_calculated_quantities()
        self.list_of_reduced_calculated_quantities = get_list_of_reduced_calculated_quantities()
        return

    def get_labels(self):
        self.labels = get_list_of_labels()
        return

    def update_dump_times_file(self, dump_num, dump_rgc, filetype):
        """
        Call in calculate_shell_averages_t, update the dump_time_conversion.txt file
        to include the latest information.
        """
        dump_times_path = self.path_to_reduced_data
        if filetype == "shells":
            dump_times_path += "shell_averages/"
        elif filetype == "slices":
            dump_times_path += "reduced_slices/"
        elif filetype == "integrals":
            dump_times_path += "radial_integrals/"
        elif filetype == "vert_integrals":
            dump_times_path += "vertical_integrals_midplane/"
        dump_times_path += "dump_time_conversion.txt"

        if os.path.exists(dump_times_path):
            dump_times = np.loadtxt(dump_times_path, skiprows=1)
            # print(dump_times)
            if dump_times.ndim == 1:
                dump_nums = np.array(dump_times[0])
                dump_rgcs = np.array(dump_times[1])
            else:
                dump_nums = dump_times[:, 0]
                dump_rgcs = dump_times[:, 1]
        else:
            dump_nums = np.array([])
            dump_rgcs = np.array([])
        if dump_num not in dump_nums:
            dump_nums = np.append(dump_nums, dump_num)
            dump_rgcs = np.append(dump_rgcs, dump_rgc)
            # Sort and re-dump
            sorted_inds = dump_nums.argsort()
            dump_nums = dump_nums[sorted_inds]
            dump_rgcs = dump_rgcs[sorted_inds]
            to_dump = np.transpose(np.vstack([dump_nums, dump_rgcs]))
            # np.savetxt(dump_times_path, np.vstack([dump_nums, dump_rgcs]))
            np.savetxt(dump_times_path, to_dump, header="Dump number, time in rg/c")
        return

    def update_times_quantities_file(self, dump_num, quantities, filetype):
        """
        Call in calculate_shell_averages_t, update the times_and_quantities.txt file
        to include the latest information.
        """
        q_times_path = self.path_to_reduced_data
        if filetype == "shells":
            q_times_path += "shell_averages/"
        elif filetype == "slices":
            q_times_path += "reduced_slices/"
        elif filetype == "integrals":
            q_times_path += "radial_integrals/"
        elif filetype == "vert_integrals":
            q_times_path += "vertical_integrals_midplane/"
        q_times_path += "times_and_quantities.txt"

        if os.path.exists(q_times_path):
            q_times = {}
            reader = csv.reader(open(q_times_path, "r"))
            for lines in reader:
                dump = int(lines[0])
                q_times[dump] = literal_eval(lines[1])
        else:
            q_times = {}

        if dump_num not in q_times.keys():
            q_times[dump_num] = quantities
        else:
            for q in quantities:
                if q not in q_times[dump_num]:
                    q_times[dump_num].append(q)
        q_times[dump_num] = list(set(q_times[dump_num]))
        w = csv.writer(open(q_times_path, "w"))
        for key, val in q_times.items():
            w.writerow([key, val])
        return

    def load_dump_times(self, filetype):
        dump_times_path = self.path_to_reduced_data
        if filetype == "shells":
            dump_times_path += "shell_averages/"
        elif filetype == "slices":
            dump_times_path += "reduced_slices/"
        elif filetype == "integrals":
            dump_times_path += "radial_integrals/"
        elif filetype == "vert_integrals":
            dump_times_path += "vertical_integrals_midplane/"
        dump_times_path += "dump_time_conversion.txt"
        if not os.path.exists(dump_times_path):
            print("No " + filetype + " dump times path found for " + self.sim_name + ", so need to calculate all reduced " + filetype)
            return None

        dump_times = np.loadtxt(dump_times_path, skiprows=1)
        if dump_times.ndim == 1:
            dump_nums = np.array(dump_times[0])
            dump_rgcs = np.array(dump_times[1])
        else:
            dump_nums = dump_times[:, 0]
            dump_rgcs = dump_times[:, 1]
        return dump_nums, dump_rgcs

    def retrieve_vertical_integrals(self, **kwargs):
        """
        This function will check for the presence of post-processed
        cumulative/vertical integrals (not weighted shell averages).
        It will look at the number
        of post-processed vertical integrals in the reduced_data folder
        and compare with dump_time_conversion.txt to make sure there
        are no gaps in the files. It should be run after all integrals
        have already been calculated.

        Do NOT pass quantities calculated after the shell average is already done
        (current example, coulViscRatioPost)

        Return dictionary: keys are the quantities. Values are 2D arrays with
        dimensions of number of times requested x radial bins
        """
        quantities = kwargs.get("vert_integral_quantities", get_list_of_vert_integral_quantities())

        tstart_rgc = kwargs.get("tstart_vert_integrals", 0) # rg/c
        tend_rgc = kwargs.get("tend_vert_integrals", -1) # rg/c

        # Load dump_times file
        dump_nums, dump_rgcs = self.load_dump_times("vert_integrals")

        # Take last value in dump_times file
        if tend_rgc == -1:
            tend_rgc = dump_rgcs[-1]
        if tend_rgc > dump_rgcs[-1]:
            print("ERROR: requested end time {:.2f} rg/c is greater than the last known time.".format(tend_rgc))
            print("You might want to re-reduce data for " + self.sim_name)
            print("Setting tend_rgc = last known time ({:.2f} rg/c) for now...".format(dump_rgcs[-1]))
            tend_rgc = dump_rgcs[-1]
        if tstart_rgc > tend_rgc:
            print("tstart was greater than tend. Resetting to tstart = tend - 50 rg/c.")
            tstart_rgc = tend_rgc - 50.

        # Identify starting/starting dumps
        tstart_dump = dump_nums[(np.abs(tstart_rgc - dump_rgcs)).argmin()]
        tend_dump = dump_nums[(np.abs(tend_rgc - dump_rgcs)).argmin()]
        self.check_for_missing_quantities(tstart_dump, tend_dump, quantities, "vert_integrals", **kwargs)

        # Convert to array format
        shell_avgs = self.create_shell_array(tstart_dump, tend_dump, quantities, "vert_integrals")
        return (tstart_rgc, tend_rgc, shell_avgs)

    def retrieve_radial_integrals(self, **kwargs):
        """
        This function will check for the presence of post-processed
        radial integrals (not weighted shell averages).
        It will look at the number
        of post-processed radial integrals in the reduced_data folder
        and compare with dump_time_conversion.txt to make sure there
        are no gaps in the files. It should be run after all shell averages
        have already been calculated.

        Do NOT pass quantities calculated after the shell average is already done
        (current example, coulViscRatioPost)

        Return dictionary: keys are the quantities. Values are 2D arrays with
        dimensions of number of times requested x radial bins
        """
        quantities = kwargs.get("integral_quantities", get_list_of_integral_quantities())

        tstart_rgc = kwargs.get("tstart_integrals", 0) # rg/c
        tend_rgc = kwargs.get("tend_integrals", -1) # rg/c

        # Load dump_times file
        dump_nums, dump_rgcs = self.load_dump_times("integrals")

        # Take last value in dump_times file
        if tend_rgc == -1:
            tend_rgc = dump_rgcs[-1]
        if tend_rgc > dump_rgcs[-1]:
            print("ERROR: requested end time {:.2f} rg/c is greater than the last known time.".format(tend_rgc))
            print("You might want to re-reduce data for " + self.sim_name)
            print("Setting tend_rgc = last known time ({:.2f} rg/c) for now...".format(dump_rgcs[-1]))
            tend_rgc = dump_rgcs[-1]
        if tstart_rgc > tend_rgc:
            print("tstart was greater than tend. Resetting to tstart = tend - 50 rg/c.")
            tstart_rgc = tend_rgc - 50.

        # Identify starting/starting dumps
        tstart_dump = dump_nums[(np.abs(tstart_rgc - dump_rgcs)).argmin()]
        tend_dump = dump_nums[(np.abs(tend_rgc - dump_rgcs)).argmin()]
        self.check_for_missing_quantities(tstart_dump, tend_dump, quantities, "integrals", **kwargs)

        # Convert to array format
        shell_avgs = self.create_shell_array(tstart_dump, tend_dump, quantities, "integrals")
        return (tstart_rgc, tend_rgc, shell_avgs)

    def check_for_time_averaged_shells(self, quantities, **kwargs):
        """
        This function will check for density-weighted shells
        that have been time-averaged over kwargs[tstart] to kwargs[tend].
        If it doesn't exist, call retrieve_shell_averages and save to file.
        If it does exist, load and return.
        """
        overwrite = kwargs.get("overwrite_timeAvg_shells", False)

        tstart_rgc = kwargs.get("tstart_shells", 0) # rg/c
        tend_rgc = kwargs.get("tend_shells", tstart_rgc) # rg/c

        # Load dump_times file
        dump_nums, dump_rgcs = self.load_dump_times("shells")

        # Take last value in dump_times file
        if tend_rgc == -1:
            tend_rgc = dump_rgcs[-1]
        if tend_rgc > dump_rgcs[-1]:
            print("ERROR: requested end time {:.2f} rg/c is greater than the last known time.".format(tend_rgc))
            print("You might want to re-reduce data for " + self.sim_name)
            print("Setting tend_rgc = last known time ({:.2f} rg/c) for now...".format(dump_rgcs[-1]))
            tend_rgc = dump_rgcs[-1]
        if tstart_rgc > tend_rgc:
            print("tstart was greater than tend. Resetting to tstart = tend - 50 rg/c.")
            tstart_rgc = tend_rgc - 50.

        print(quantities)
        clean_quantities = clean_reduced_quantities_list(quantities, self.header)
        print(clean_quantities)
        q_to_retrieve = get_quantities_to_retrieve(clean_quantities)
        # print("to retrieve: ")
        # print(q_to_retrieve)
        kwargs["shell_quantities"] = q_to_retrieve
        if tstart_rgc == tend_rgc:
            shell_data = self.retrieve_shell_averages(**kwargs)
            return shell_data

        # Path to time-averaged slice data
        shell_dir = self.path_to_reduced_data + "shell_averages/tAvgs/"
        shell_path = shell_dir + "tAvg{:.0f}-{:.0f}rgc.p".format(tstart_rgc, tend_rgc)

        if not os.path.exists(shell_dir): os.makedirs(shell_dir)
        if not os.path.exists(shell_path):
            reduced_dict = {}
        else:
            ts, te, reduced_dict = pickle.load(open(shell_path, 'rb'))

        if "vrr_bl" not in q_to_retrieve:
            q_to_retrieve.append("vrr_bl")
        for q in q_to_retrieve:
            if q not in reduced_dict:
                print("Retrieving quantities for time-averaged shells.")
                kwargs["shell_quantities"] = q_to_retrieve
                shell_data = self.retrieve_shell_averages(**kwargs)
                pickle.dump(shell_data, open(shell_path, 'wb'))
                return shell_data
        print("Loading time-averaged shells from reduced file.")
        return (ts, te, reduced_dict)

    def check_for_time_averaged_slice(self, quantity, **kwargs):
        """
        This function will check for post-processed vertical slices
        that have been time-averaged over kwargs[tstart] to kwargs[tend].
        If it doesn't exist, call retrieve_quantity_slice and save to file.
        If it does exist, load and return.
        """
        overwrite = kwargs.get("overwrite_timeAvg_slice", False)
        phi_ind = kwargs.get("phi_ind", "Avg")
        if isinstance(phi_ind, int):
            phi_ind = "{:d}".format(phi_ind)

        tstart_rgc = kwargs.get("tstart_slices", 0) # rg/c
        tend_rgc = kwargs.get("tend_slices", tstart_rgc) # rg/c

        # Load dump_times file
        dump_nums, dump_rgcs = self.load_dump_times("slices")

        # Take last value in dump_times file
        if tend_rgc == -1:
            tend_rgc = dump_rgcs[-1]
        if tend_rgc > dump_rgcs[-1]:
            print("ERROR: requested end time {:.2f} rg/c is greater than the last known time.".format(tend_rgc))
            print("You might want to re-reduce data for " + self.sim_name)
            print("Setting tend_rgc = last known time ({:.2f} rg/c) for now...".format(dump_rgcs[-1]))
            tend_rgc = dump_rgcs[-1]
        if tstart_rgc > tend_rgc:
            print("tstart was greater than tend. Resetting to tstart = tend - 50 rg/c.")
            tstart_rgc = tend_rgc - 50.

        if tstart_rgc == tend_rgc:
            slice_data = self.retrieve_quantity_slice(quantity, **kwargs)
            return slice_data

        # Indices of tstart and tend
        tstart_ind = (np.abs(tstart_rgc - dump_rgcs)).argmin()
        tend_ind = (np.abs(tend_rgc - dump_rgcs)).argmin()
        # Code values of tstart and tend
        tstart_dump_rgc = dump_rgcs[tstart_ind]
        tend_dump_rgc = dump_rgcs[tend_ind]

        print("tstart/tend: ({:.0f}, {:.0f}) rg/c, dump numbers: ({:d}, {:d})".format(tstart_dump_rgc, tend_dump_rgc, int(dump_nums[tstart_ind]), int(dump_nums[tend_ind])))
        # Path to time-averaged slice data
        slice_dir = self.path_to_reduced_data + "reduced_slices/tAvgs/"
        slice_path = slice_dir + "tAvg{:.0f}-{:.0f}rgc.h5".format(tstart_dump_rgc, tend_dump_rgc)

        to_calculate = False
        if not os.path.exists(slice_dir): os.makedirs(slice_dir)
        if not os.path.exists(slice_path):
            to_calculate = True
        reduced_file = h5.File(slice_path, 'a')
        if quantity not in reduced_file:
            to_calculate = True
            qgroup = reduced_file.create_group(quantity)
        else:
            qgroup = reduced_file[quantity]
            # Skipping condition: both "Avg" and phi_ind are in the group
            # and overwrite is False
            to_calculate = "Avg" not in qgroup.keys() or phi_ind not in qgroup.keys()
        to_calculate = (to_calculate or overwrite)

        if to_calculate:
            print("Check slices is calling retrieve quantity for " + quantity)
            slice_data = self.retrieve_quantity_slice(quantity, **kwargs)
            # Output to slice path
            if phi_ind in qgroup:
                qgroup[phi_ind][...] = slice_data
            else:
                qgroup.create_dataset(phi_ind, data=slice_data)
            print("Saved to " + slice_path)
        else:
            slice_data = qgroup[phi_ind][...]
        reduced_file.close()
        return slice_data

    def retrieve_quantity_slice(self, quantity, **kwargs):
        """
        This function will check for the presence of post-processed
        slices. It will look at the number
        of post-processed slices in the reduced_data folder
        and compare with dump_time_conversion.txt to make sure there
        are no gaps in the files. It should be run after all slices
        have already been calculated.

        Do NOT pass quantities calculated after the shell average is already done
        (current example, coulViscRatioPost)

        Returns a time-averaged 2D slice: either azimuthally-averaged or sliced.
        """
        if quantity in get_list_of_reduced_calculated_quantities():
            quantities = get_list_of_reduced_calculated_quantities()[quantity]
        else:
            quantities = [quantity]
        quantities = clean_reduced_quantities_list(quantities, self.header)
        phi_ind = kwargs.get("phi_ind", "Avg")
        if isinstance(phi_ind, int):
            phi_ind = "{:d}".format(phi_ind)

        tstart_rgc = kwargs.get("tstart_slices", 0) # rg/c
        tend_rgc = kwargs.get("tend_slices", tstart_rgc) # rg/c

        # Load dump_times file
        dump_nums, dump_rgcs = self.load_dump_times("slices")

        # Take last value in dump_times file
        if tend_rgc == -1:
            tend_rgc = dump_rgcs[-1]
        if tend_rgc > dump_rgcs[-1]:
            print("ERROR: requested end time {:.2f} rg/c is greater than the last known time.".format(tend_rgc))
            print("You might want to re-reduce data for " + self.sim_name)
            print("Setting tend_rgc = last known time ({:.2f} rg/c) for now...".format(dump_rgcs[-1]))
            tend_rgc = dump_rgcs[-1]
        if tstart_rgc > tend_rgc:
            print("tstart was greater than tend. Resetting to tstart = tend - 50 rg/c.")
            tstart_rgc = tend_rgc - 50.

        # Identify starting/starting dumps
        tstart_dump = int(dump_nums[(np.abs(tstart_rgc - dump_rgcs)).argmin()])
        tend_dump = int(dump_nums[(np.abs(tend_rgc - dump_rgcs)).argmin()])
        self.check_for_missing_quantities(tstart_dump, tend_dump, quantities, "slices", **kwargs)

        dump_array = np.arange(tstart_dump, tend_dump + 1)
        num_slices = len(dump_array)
        slice_numerator = None
        slice_denominator = None

        # Time average over tstart to tend
        if quantity == "H_over_R":
            print("H/r not calculated for slices.")
            return None
        elif quantity == "tinfall":
            print("tinfall not calculated for slices")
            return None
        for dump_num in dump_array:
            # Load slice
            slice_path = self.path_to_reduced_data + "reduced_slices/"
            slice_path += "slice_t{:08d}.h5".format(dump_num)
            reduced_file = h5.File(slice_path, 'r')
            # print(reduced_file.keys())

            denominator = None
            if quantity in reduced_file.keys():
                numerator = reduced_file[quantity][phi_ind][...]
            # Quantities that need: (sum numerator)/(sum denominator)
            elif quantity == "coolTotalRatioPost":
                numerator = reduced_file["Qcool"][phi_ind][...]
                denominator = reduced_file["Qcoul"][phi_ind][...]
                denominator += reduced_file["Qvisc_e"][phi_ind][...]
            elif quantity == "coulViscRatioPost":
                numerator = reduced_file["Qcoul"][phi_ind][...]
                denominator = reduced_file["Qvisc_e"][phi_ind][...]
            elif quantity == "coolCoulRatioPost":
                numerator = reduced_file["Qcool"][phi_ind][...]
                denominator = reduced_file["Qcoul"][phi_ind][...]
            elif quantity == "coolViscRatioPost":
                numerator = reduced_file["Qcool"][phi_ind][...]
                denominator = reduced_file["Qvisc_e"][phi_ind][...]
            # Timescales can be averaged before or after dividing
            elif quantity == "theat":
                numerator = reduced_file["ug"][phi_ind][...]
                denominator = reduced_file["Qvisc_p"][phi_ind][...]
            elif quantity == "tCoulomb":
                numerator = reduced_file["up"][phi_ind][...]
                denominator = reduced_file["Qcoul"][phi_ind][...]
            # Quantities that need sum (numerator/denominator):
            # Keep denominator None
            elif quantity == "Te":
                thetae = reduced_file["Thetae"][phi_ind][...]
                numerator = get_Te(thetae)
            elif quantity == "Tp":
                thetap = reduced_file["Thetap"][phi_ind][...]
                numerator = get_Tp(thetap)
            elif quantity == "coulombQuality":
                Dt = 2.e-3
                numerator = reduced_file["ue"][phi_ind][...]/reduced_file["Qcoul"][phi_ind][...]/Dt
            else:
                print(quantity + " has no numerator")

            # Now add to existing slices
            if slice_numerator is None:
                slice_numerator = numerator
            else:
                slice_numerator += numerator
            if slice_denominator is None:
                if denominator is not None:
                    slice_denominator = denominator
            else:
                slice_denominator += denominator
        if slice_denominator is None:
            time_averaged_slice = slice_numerator/num_slices
        else:
            time_averaged_slice = slice_numerator/slice_denominator
        return time_averaged_slice

    def retrieve_shell_averages(self, **kwargs):
        """
        This function will check for the presence of post-processed
        shell averages (density-weighted 1D radial profiles;
        see e.g. Dexter+ 2021 Eq. 1). It will look at the number
        of post-processed shell averages in the reduced_data folder
        and compare with dump_time_conversion.txt to make sure there
        are no gaps in the files. It should be run after all shell averages
        have already been calculated.

        Do NOT pass quantities calculated after the shell average is already done
        (current example, coulViscRatioPost)

        Return dictionary: keys are the quantities. Values are 2D arrays with
        dimensions of number of times requested x radial bins
        """
        quantities = kwargs.get("shell_quantities", self.list_of_raw_calculated_quantities)
        print(quantities)
        quantities = clean_reduced_quantities_list(quantities, self.header)

        print("in retrieve, find quantities of")
        print(quantities)
        over_time = kwargs.get("over_time", False)
        tstart_rgc = kwargs.get("tstart_shells", 0) # rg/c
        tend_rgc = kwargs.get("tend_shells", -1) # rg/c

        # Load dump_times file
        dump_nums, dump_rgcs = self.load_dump_times("shells")
        # Take last value in dump_times file
        if tend_rgc == -1:
            tend_rgc = dump_rgcs[-1]
        if tend_rgc > dump_rgcs[-1]:
            print("ERROR: requested end time {:.2f} rg/c is greater than the last known time.".format(tend_rgc))
            print("You might want to re-reduce data for " + self.sim_name)
            print("Setting tend_rgc = last known time ({:.2f} rg/c) for now...".format(dump_rgcs[-1]))
            tend_rgc = dump_rgcs[-1]
        if tstart_rgc > tend_rgc:
            print("tstart was greater than tend. Resetting to tstart = tend - 50 rg/c.")
            tstart_rgc = tend_rgc - 50.
        # Identify starting/starting dumps
        tstart_ind = (np.abs(tstart_rgc - dump_rgcs)).argmin()
        tend_ind = (np.abs(tend_rgc - dump_rgcs)).argmin()
        tstart_dump = dump_nums[(np.abs(tstart_rgc - dump_rgcs)).argmin()]
        tend_dump = dump_nums[(np.abs(tend_rgc - dump_rgcs)).argmin()]
        tstart_dump_rgc = dump_rgcs[tstart_ind]
        tend_dump_rgc = dump_rgcs[tend_ind]
        if np.abs(tstart_dump_rgc - tstart_rgc) > 50.0 and not over_time:
            print("WARNING: the actual start time for " + self.sim_name)
            print("will be {:.2f} instead of {:.2f}.".format(tstart_dump_rgc, tstart_rgc))
            print("Please change tstart to get actual data.")
            return (tstart_dump_rgc, tend_dump_rgc, {})
        # print("Entering check for missing...")
        self.check_for_missing_quantities(tstart_dump, tend_dump, quantities, "shells", **kwargs)

        # Convert to array format
        shell_avgs = self.create_shell_array(tstart_dump, tend_dump, quantities)
        return (tstart_rgc, tend_rgc, shell_avgs)

    def create_shell_array(self, tstart_dump, tend_dump, quantities, filetype="shells", dumps_to_calculate=None):
        """
        Called in retrieve_shell_quantities after making sure that no data is missing.
        Returns a dictionary with quantities as keys, each entry is a 2D array with
        dimensions of times x radial bins.
        """
        # Initialize dictionary to return
        shell_avgs = {}
        if dumps_to_calculate is None:
            tstart_dump = int(tstart_dump)
            tend_dump = int(tend_dump)
            shell_avgs["dumps"] = np.arange(tstart_dump, tend_dump + 1)
        else:
            shell_avgs["dumps"] = dumps_to_calculate

        missing_qs = []
        not_found = []
        for dump_num in shell_avgs["dumps"]:
            shell_avg_path = self.path_to_reduced_data
            if filetype == "shells":
                shell_avg_path += "shell_averages/"
            elif filetype == "slices":
                shell_avg_path += "reduced_slices/"
            elif filetype == "integrals":
                shell_avg_path += "radial_integrals/"
            elif filetype == "vert_integrals":
                shell_avg_path += "vertical_integrals_midplane/"
            shell_avg_path += "t{:08d}.p".format(int(dump_num))
            shell_dump = pickle.load(open(shell_avg_path, 'rb'))
            for q in quantities:
                if q not in shell_dump:
                    if q not in not_found:
                        not_found.append(q)
                    if q not in missing_qs:
                        missing_qs.append(q)
                    continue
                if q not in shell_avgs:
                    shell_avgs[q] = shell_dump[q]
                else:
                    shell_avgs[q] = np.vstack([shell_avgs[q], shell_dump[q]])
            if not_found:
                self.remove_quantities_at_t_from_qtimes(not_found, dump_num, "shells")
                not_found = []
        if missing_qs:
            print("ERROR: between dumps {:.0f} and {:.0f}.".format(tstart_dump, tend_dump))
            print("    quantities " + ", ".join(missing_qs))
            print("    were missing for simulation " + self.sim_name)
            print("    You should run postprocessing over these dump times.")
            print("    Skipping and removing from times_and_quantities...")

        return shell_avgs

    def remove_quantities_at_t_from_qtimes(self, quantities, time, filetype, **kwargs):
        """
        Sometimes we need to bulk remove all quantities at a certain time
        from the q times file because it was added by a mistake.
        """
        q_times_path = self.path_to_reduced_data
        if filetype == "shells":
            q_times_path += "shell_averages/"
        elif filetype == "integrals":
            q_times_path += "radial_integrals/"
        elif filetype == "slices":
            q_times_path += "reduced_slices/"
        q_times_path += "times_and_quantities.txt"
        if os.path.exists(q_times_path):
            q_times = {}
            reader = csv.reader(open(q_times_path, "r"))
            for lines in reader:
                dump_num = int(lines[0])
                q_times[dump_num] = literal_eval(lines[1])
        else:
            q_times = {}

        for q in quantities:
            if q in q_times[time]:
                q_times[time].remove(q)

        # Write new version
        w = csv.writer(open(q_times_path, "w"))
        for key, val in q_times.items():
            w.writerow([key, val])
        return

    def remove_time_from_qtimes(self, time, filetype, **kwargs):
        """
        Sometimes we need to bulk remove all quantities at a certain time
        from the q times file because it was added by a mistake.
        """
        q_times_path = self.path_to_reduced_data
        if filetype == "shells":
            q_times_path += "shell_averages/"
        elif filetype == "integrals":
            q_times_path += "radial_integrals/"
        elif filetype == "slices":
            q_times_path += "reduced_slices/"
        q_times_path += "times_and_quantities.txt"
        if os.path.exists(q_times_path):
            q_times = {}
            reader = csv.reader(open(q_times_path, "r"))
            for lines in reader:
                dump_num = int(lines[0])
                q_times[dump_num] = literal_eval(lines[1])
        else:
            q_times = {}

        new_q_times = q_times.copy()
        del new_q_times[time]

        # Write new version
        w = csv.writer(open(q_times_path, "w"))
        for key, val in new_q_times.items():
            w.writerow([key, val])
        return

    def remove_quantity_from_qtimes(self, quantity, filetype, **kwargs):
        """
        Sometimes we need to bulk remove a quantity from the q times file because
        it was added by a mistake.
        """
        q_times_path = self.path_to_reduced_data
        if filetype == "shells":
            q_times_path += "shell_averages/"
        elif filetype == "integrals":
            q_times_path += "radial_integrals/"
        elif filetype == "slices":
            q_times_path += "reduced_slices/"
        q_times_path += "times_and_quantities.txt"
        if os.path.exists(q_times_path):
            q_times = {}
            reader = csv.reader(open(q_times_path, "r"))
            for lines in reader:
                dump_num = int(lines[0])
                q_times[dump_num] = literal_eval(lines[1])
        else:
            q_times = {}

        new_q_times = q_times.copy()
        for time, quantities in q_times.items():
            if quantity in quantities:
                new_qs = quantities.copy()
                new_qs.remove(quantity)
                new_q_times[time] = new_qs

        # Write new version
        w = csv.writer(open(q_times_path, "w"))
        for key, val in new_q_times.items():
            w.writerow([key, val])
        return

    def check_for_missing_quantities(self, tstart_dump, tend_dump, quantities, filetype, **kwargs):
        """
        Check times_and_quantities file for any quantities that need
        to be calculated, and calculate them if possible (need raw data path).

        tstart_dump and tend_dump are the dump numbers, not dump paths.
        """
        quantities = list(set(quantities))
        reverse = kwargs.get("reverse", True)
        tstart_dump = int(tstart_dump)
        tend_dump = int(tend_dump)
        q_times_path = self.path_to_reduced_data
        if filetype == "shells":
            overwrite = kwargs.get("overwrite_shell_avgs", False)
            q_times_path += "shell_averages/"
        elif filetype == "vert_integrals":
            overwrite = kwargs.get("overwrite_vertical_integrals_midplane", False)
            q_times_path += "vertical_integrals_midplane/"
        elif filetype == "integrals":
            overwrite = kwargs.get("overwrite_radial_integrals", False)
            q_times_path += "radial_integrals/"
        elif filetype == "slices":
            overwrite = kwargs.get("overwrite_slices", False)
            q_times_path += "reduced_slices/"
        q_times_path += "times_and_quantities.txt"

        if os.path.exists(q_times_path):
            q_times = {}
            reader = csv.reader(open(q_times_path, "r"))
            for lines in reader:
                dump_num = int(lines[0])
                q_times[dump_num] = literal_eval(lines[1])
        else:
            q_times = {}
        dumps_to_calculate = {}
        dump_range = np.arange(tstart_dump, tend_dump + 1)
        if reverse:
            dump_range = dump_range[::-1]
        missing_qs = []
        for dump_num in dump_range:
            qs_to_check = quantities.copy()
            dump_q_to_calculate = []
            if overwrite or dump_num not in q_times.keys():
                print("{:d} not in q_times for sim ".format(dump_num) + self.sim_name)
                dump_q_to_calculate = quantities
            else:
                # First need to check for old case where would put ucov in times_and_quantities
                # but not ucov1, ucov0, etc.
                qs_to_remove = []
                for quantity in get_list_of_standard_vector_quantities():
                    if quantity in q_times[dump_num]:
                        qs_to_remove.append(quantity)
                        qdims = []
                        for k in np.arange(0, 4):
                            qdim = quantity + "{:d}".format(k)
                            qdims.append(qdim)
                            q_times[dump_num].append(qdim)
                        self.update_times_quantities_file(dump_num, qdims, filetype)
                        q_times[dump_num].remove(quantity)
                        if quantity in qs_to_check:
                            qs_to_check.remove(quantity)
                    # Now check if should add to qs to calculate
                    else:
                        all_there = True
                        for k in np.arange(0, 4):
                            qdim = quantity + "{:d}".format(k)
                            if qdim not in q_times[dump_num]:
                                all_there = False
                                continue
                        if all_there and quantity in qs_to_check:
                            qs_to_check.remove(quantity)
                if qs_to_remove:
                    print("Expanded 4-vectors at dump {:d}".format(dump_num))
                    self.remove_quantities_at_t_from_qtimes(qs_to_remove, dump_num, filetype)

                # Now actually loop through and check for quantities
                for q in qs_to_check:
                    if q not in q_times[dump_num]:
                        if q in get_list_of_all_quantities():
                            # dump_q_to_calculate = np.append(dump_q_to_calculate, q)
                            # missing_qs = np.append(missing_qs, q)
                            dump_q_to_calculate.append(q)
                            missing_qs.append(q)
                        if q[:-1] in get_list_of_all_quantities():
                            if q[:-1] not in q_times[dump_num]:
                                dump_q_to_calculate.append(q[:-1])
                                missing_qs.append(q[:-1])
                                # dump_q_to_calculate = np.append(dump_q_to_calculate, q[:-1])
                                # missing_qs = np.append(missing_qs, q[:-1])
            if len(dump_q_to_calculate) != 0:
                dumps_to_calculate[dump_num] = dump_q_to_calculate
        # Call calculate for everything in dumps_to_calculate
        if dumps_to_calculate:
            if not os.path.exists(self.path_to_raw_data):
                print("---------------------------------------")
                print("            ERROR")
                print("The plotter class for simulation " + self.sim_name)
                print("is trying to calculate " + filetype)
                print("which means death. We need the following variables and times: ")
                needed_qs = []
                print(dumps_to_calculate)
                for v in dumps_to_calculate.values():
                    if len(v) > 0:
                        for val in v:
                            if val not in needed_qs:
                                needed_qs.append(val)
                print(needed_qs)
                # print(dumps_to_calculate)
                # print("Exiting...")
            else:
                # print(dumps_to_calculate)
                # exit()
                for dump_num in dumps_to_calculate:
                    dump_num = int(dump_num)
                    dump_path = self.path_to_raw_data + "dump_{:08d}.h5".format(dump_num)
                    print(dump_num)
                    qs = dumps_to_calculate[dump_num]
                    if filetype == "shells":
                        self.calculate_dump_shell_averages(qs, dump_path, **kwargs)
                    elif filetype == "integrals":
                        self.calculate_dump_integrals(qs, dump_path, **kwargs)
                    elif filetype == "slices":
                        self.calculate_dump_slice_data(qs, dump_path, **kwargs)
        return

    def retrieve_diag_quantity(self, diag, q, Medd=True):
        """
        Convert diagonal quantities.
        """
        if "mdot" in q:
            if "M_unit" in self.header and Medd:
                # Mbh is already in cgs. mbh is mass in solar units.
                edd_factor = 0.1
                try:
                    Mass_g = self.header["Mbh"]
                except:
                    self.set_mbh(1.e8)
                    Mass_g = self.header["Mbh"]
                Ledd = 4.*np.pi*units["GNEWT"]*units["MP"]*units["CL"]*Mass_g/units["THOMSON"]
                MdotEdd = Ledd/(edd_factor*units["CL"]**2)
                Mdot_cgs = -diag[q]*self.header["M_unit"]/self.header["T_unit"]
                return Mdot_cgs/MdotEdd
            else:
                return -diag[q]
        elif q == "visc_efficiency" and "Qvisc" in diag:
            visc_data = diag["Qvisc"]
            mdot_data = -diag["mdot_eh"]
            return visc_data/mdot_data
        elif q == "cool_efficiency" and "Qcool" in diag:
            cool_data = diag["Qcool"]
            mdot_data = -diag["mdot_eh"]
            return cool_data/mdot_data
        elif q in diag:
            return diag[q]
        # print("Couldn't find quantity " + q)
        return None

    def convert_times_quantities(self, **kwargs):
        """
        In mid January 2023 (i.e. Jan. 18ish), I reorganized how
        the times_and_quantities file was stored, changing from
        pickle to txt file.
        To run the same analysis scripts with old data, I need a
        function to convert from the old dictionary-based method of
        storing all shell averages in a single file to dumping them
        in different files at each time step.

        Note we don't need to include slices because they are in
        a totally different format.
        """
        old_path = self.path_to_reduced_data + "shell_averages/times_and_quantities.p"
        old_path = kwargs.get("old_path", old_path)

        old_q_times = pickle.load(open(old_path, 'rb'))
        q_times_path = self.path_to_reduced_data + "shell_averages/times_and_quantities.txt"

        w = csv.writer(open(q_times_path, "w"))
        for key, val in old_q_times.items():
            w.writerow([key, val])

        print("Completed converting times and quantities file from simulation " + self.sim_name)
        return

    def convert_shell_avgs_storage(self, **kwargs):
        """
        In mid January 2023 (i.e. Jan. 12, 13ish), I reorganized how
        reduced data was calculated and stored.
        To run the same analysis scripts with old data, I need a
        function to convert from the old dictionary-based method of
        storing all shell averages in a single file to dumping them
        in different files at each time step.

        Note we don't need to include slices because they are in
        a totally different format.
        """
        old_path = self.path_to_reduced_data + "shell_averages.p"
        old_path = kwargs.get("old_path", old_path)

        old_shells = pickle.load(open(old_path, 'rb'))
        new_shells_path = self.path_to_reduced_data + "shell_averages/"
        if not os.path.exists(new_shells_path):
            os.makedirs(new_shells_path)

        list_of_dumps = old_shells["times"].keys()
        # Make dump_time_conversion.txt
        for dump_num in list_of_dumps:
            dump_rgc = old_shells["times"][dump_num]
            self.update_dump_times_file(dump_num, dump_rgc, "shells")

        # Make times_and_quantities.p
        q_list = list(old_shells.keys())
        q_list.remove('r_vals')
        q_list.remove('times')
        q_times = {}
        for dump_num in list_of_dumps:
            missing_list = []
            q_times[dump_num] = []
            for q in q_list:
                if dump_num in list(old_shells[q].keys()):
                    q_times[dump_num].append(q)
                else:
                    missing_list.append(q)
            if missing_list:
                print("Dump {:d} missing: ".format(dump_num) + ', '.join(missing_list))

        q_times_path = new_shells_path + "times_and_quantities.txt"
        w = csv.writer(open(q_times_path, "w"))
        for key, val in q_times.items():
            w.writerow([key, val])

        # Convert shell averages file into many smaller files
        for dump_num in list_of_dumps:
            new_shell_avgs = {}
            shell_avg_path = new_shells_path + "t{:08d}.p".format(dump_num)
            for q in q_list:
                if dump_num in list(old_shells[q].keys()):
                    new_shell_avgs[q] = old_shells[q][dump_num]
            # Save to pickle
            pickle.dump(new_shell_avgs, open(shell_avg_path, 'wb'))

        print("Completed converting shell averages from simulation " + self.sim_name)
        return

    def check_quantity_times(self, dump_num):
        q_times_path = self.path_to_reduced_data + "shell_averages/times_and_quantities.txt"
        q_times = {}
        reader = csv.reader(open(q_times_path, "r"))
        for lines in reader:
            dump = int(lines[0])
            q_times[dump] = literal_eval(lines[1])
        return q_times[dump_num]

    def get_list_of_cgs_quantities(self):
        """
        Quantities that need to be converted from code
        units to cgs when comparing across simulations.
        Key is the quantity name. Values are
        the conversion factor. To get the quantity in cgs,
        multiply the code value by the conversion factor.
        """
        list_of_cgs_quantities = {
            "Qcoul":self.header["U_unit"]/self.header["T_unit"],
            "Qvisc_e":self.header["U_unit"]/self.header["T_unit"],
            "Qcool":self.header["U_unit"]/self.header["T_unit"],
            "RHO":self.header["RHO_unit"],
        }
        return list_of_cgs_quantities

    def clean_times_and_quantities_file(self, filetype="shells"):
        """
        Explicitly open up every reduced data file and make sure that the keys
        in it are the same as that time entry in the times_and_quantities file.
        """
        q_times_path = self.path_to_reduced_data
        if filetype == "shells":
            q_times_path += "shell_averages/"
        elif filetype == "slices":
            q_times_path += "reduced_slices/"
        elif filetype == "integrals":
            q_times_path += "radial_integrals/"
        elif filetype == "vert_integrals":
            q_times_path += "vertical_integrals_midplane/"
        q_times_file = q_times_path + "times_and_quantities.txt"
        q_times_bak = q_times_file + "_bak"
        if not os.path.exists(q_times_bak):
            shutil.copyfile(q_times_file, q_times_bak)
        else:
            shutil.copyfile(q_times_file, q_times_bak + "2")

        if os.path.exists(q_times_file):
            q_times = {}
            reader = csv.reader(open(q_times_file, "r"))
            for lines in reader:
                dump = int(lines[0])
                q_times[dump] = literal_eval(lines[1])
        else:
            q_times = {}

        # Get the files in the reduced data path.
        files_list = get_list_of_reduced_files(q_times_path)
        list_of_dump_nums = []
        for f in files_list:
            dump_num = int((f.split("/")[-1].replace("t", "").replace(".p", "")))
            list_of_dump_nums.append(dump_num)

        for dump_num in list_of_dump_nums:
            if dump_num in q_times:
                q_times_qs = q_times[dump_num]
            else:
                q_times_qs = []
            # Load actual reduced data file
            reduced_data_path = q_times_path + "t{:08d}.p".format(dump_num)
            try:
                reduced_data = pickle.load(open(reduced_data_path, 'rb'))
                actual_qs = list(reduced_data.keys())
            except:
                print("ERROR: CORRUPTED FILE " + reduced_data_path)
                print(reduced_data_path + " corrupted.")
                actual_qs = []

            if sorted(q_times_qs) != sorted(actual_qs):
                print("Resetting dump {:d} q times according to file keys.".format(dump_num))
                actual_qs = list(set(sorted(actual_qs)))
                q_times[dump_num] = actual_qs

        # Print in order.
        dump_nums = sorted(list(q_times.keys()))
        w = csv.writer(open(q_times_file, "w"))
        for dump in dump_nums[::-1]:
            sorted_qs = sorted(set(list(q_times[dump])))
            w.writerow([dump, sorted_qs])
        return
