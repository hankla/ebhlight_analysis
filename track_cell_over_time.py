import matplotlib.pyplot as plt
import h5py as h5
import numpy as np
import pickle
import sys
sys.path.append('modules/')
import plotting_utils as pu
import ebhlight_tools as tools
import hdf5_to_dict as io

colorblind_colors = ["#2691db", "#ff7f0e", "#2e692c", "#723b7a", "#e41a1c"]

tit_str = "High Mdot (Mu25)\n"
prop_path = '/mnt/e/ebhlight_ecool_tests/reduced_data/torus_ecool_3DT1e9Mu25/properties.p'
properties = pickle.load(open(prop_path, 'rb'))
header = properties["header"]
sigma_cut = 1.

# trange = np.arange(1990, 2000)
trange = np.arange(1400, 1410)
fname_base = '/mnt/e/ebhlight_ecool_tests/raw_data/torus_ecool_3DT1e9Mu25/dumps/dump_'
r_vals = np.array(properties["geom"]['r'][:, 0, 0])
theta_vals = np.array(properties["geom"]['th'][0, :, 0])
phi_vals = np.array(properties["geom"]['phi'][0, 0, :])
ISCO_ind = (np.abs(r_vals - header["Risco"])).argmin()

fname0 = fname_base + "{:08d}.h5".format(trange[0])
dfile0 = io.load_dump(fname0, geom=properties['geom'])
sigma_mask0 = dfile0['sigma'] < sigma_cut
theta_masked0 = dfile0['Thetae']*sigma_mask0
inner_theta = theta_masked0[:ISCO_ind, :, :]
# Smallest non-zero values
min_to_max = sorted(inner_theta[np.nonzero(inner_theta)])
min_val = min_to_max[1]
min_ind = np.where(inner_theta == min_val)

r_min = r_vals[min_ind[0]][0]
theta_min = theta_vals[min_ind[1]][0]
phi_min = phi_vals[min_ind[2]][0]

# t1_min_evolution = [min_val]
# times = [dfile0['t']]
t1_min_evolution = []
times = []
markerfillstyle = []

plt.figure()
for t in trange:
    fname = fname_base + "{:08d}.h5".format(t)
    dfile = io.load_dump(fname, geom=properties['geom'])
    sigma_mask = dfile['sigma'] < sigma_cut
    theta_masked = dfile['Thetae']*sigma_mask
    inner_theta = theta_masked[:ISCO_ind, :, :]
    val_at_loc = theta_masked[min_ind][0]
    if val_at_loc == 0.0:
        val_at_loc = dfile['Thetae'][min_ind][0]
        fs = 'none'
    else:
        fs = 'full'
    t1_min_evolution.append(val_at_loc)
    times.append(dfile['t'])
    plt.plot(dfile['t'], val_at_loc, marker='o', fillstyle=fs, color='blue')

print(t1_min_evolution)
plt.xlabel(r'$tc/r_g$')
plt.yscale('log')
plt.ylabel(r'$\theta_e$')
tit_str += r'Minimum value at $r={:.2f}~r_g$, $\theta={:.2f}$, $\phi={:.2f}$'.format(r_min, theta_min, phi_min)
plt.title(tit_str)

plt.show()
