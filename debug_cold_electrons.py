import matplotlib.pyplot as plt
import h5py as h5
import numpy as np
import pickle
import sys
sys.path.append('modules/')
import plotting_utils as pu
import ebhlight_tools as tools
import hdf5_to_dict as io

colorblind_colors = ["#2691db", "#ff7f0e", "#2e692c", "#723b7a", "#e41a1c"]

# fname = '/mnt/e/ebhlight_ecool_tests/raw_data/torus_ecool_3DT1e9Mu21/dumps/dump_00001996.h5'
# fname2 = '/mnt/e/ebhlight_ecool_tests/raw_data/torus_ecool_3DT1e9Mu21/dumps/dump_00001991.h5'
# prop_path = '/mnt/e/ebhlight_ecool_tests/reduced_data/torus_ecool_3DT1e9Mu21/properties.p'
fname = '/mnt/e/ebhlight_ecool_tests/raw_data/torus_ecool_3DT1e9Mu25/dumps/dump_00001995.h5'
fname2 = '/mnt/e/ebhlight_ecool_tests/raw_data/torus_ecool_3DT1e9Mu25/dumps/dump_00001999.h5'
prop_path = '/mnt/e/ebhlight_ecool_tests/reduced_data/torus_ecool_3DT1e9Mu25/properties.p'
geom_path = '/mnt/e/ebhlight_ecool_tests/reduced_data/torus_ecool_3DT1e9Mu25/geom.p'
# fname = '/mnt/e/ebhlight_ecool_tests/raw_data/test_torus_ecool_2DT1e9Mu25/dumps/dump_00001400.h5'
# fname2 = '/mnt/e/ebhlight_ecool_tests/raw_data/test_torus_ecool_2DT1e9Mu25/dumps/dump_00001401.h5'
# prop_path = '/mnt/e/ebhlight_ecool_tests/reduced_data/test_torus_ecool_2DT1e9Mu25/properties.p'
# fname = '/mnt/e/ebhlight_ecool_tests/raw_data/torus_ecool_3DT1e9Mu25tc01/dumps/dump_00001550.h5'
# fname2 = '/mnt/e/ebhlight_ecool_tests/raw_data/torus_ecool_3DT1e9Mu25tc01/dumps/dump_00001555.h5'
# prop_path = '/mnt/e/ebhlight_ecool_tests/reduced_data/torus_ecool_3DT1e9Mu25tc01/properties.p'
hdr = io.load_hdr(fname)
geom = io.load_geom(hdr)
properties = pickle.load(open(prop_path, 'rb'))
d1 = io.load_dump(fname, geom=geom)
d2 = io.load_dump(fname2, geom=geom)
header = properties["header"]

print("Nans:")
print((np.isnan(d1['RHO']).any()))
print((np.isnan(d2['RHO']).any()))
print("Rho < 0:")
print((d1['RHO'] < 0.).any())
print((d2['RHO'] < 0.).any())
sigma_cut = 1.
sigma1mask = d1['sigma'] < sigma_cut
sigma2mask = d2['sigma'] < sigma_cut
Te1mask = d1['Te'] > header['Tel_target']
TeTp1mask = d1['Te'] > d1['Tp']
Qcoul1mask = d1['Qcoul'] < 0.0
double1mask = (d1['Qcoul'] < 0.0) & (d1['Te'] > header['Tel_target'])
thetae_target = tools.get_thetae(1.e9)
r_vals = np.array(geom['r'][:, 0, 0])
theta_vals = np.array(geom['th'][0, :, 0])
phi_vals = np.array(geom['phi'][0, 0, :])
x = geom['x'].copy()
y = geom['y'].copy()
z = geom['z'].copy()
if z.shape[2] > 1:
    x = pu.flatten_xz(x, header, flip=True)
    y = pu.flatten_xz(y, header, flip=True)
    z = pu.flatten_xz(z, header)
    rcyl = np.sqrt(x**2 + y**2)
    rcyl[np.where(x<0)] *= -1
else:
    rcyl = np.sqrt(x**2 + y**2)


# https://numpy.org/doc/1.21/user/tutorial-ma.html
# Exclude values with sigma > 1 and r < rISCO and r > 40rg, i.e.
# make those values false
wanted_values = ((d1['sigma'] < sigma_cut) &
                   (geom['r'] > header['Risco']) &
                   (geom['r'] < 40.0)
)
# False is zero
# These are all arrays of True or False
Te_mask = d1['Te']*wanted_values
Tp_mask = d1['Tp']*wanted_values
Te_colder = (d1['Te'] < d1['Tp'])*wanted_values
Tp_colder = (d1['Tp'] < d1['Te'])*wanted_values
Te_over_target = (d1['Te'] > header['Tel_target'])*wanted_values
Qcoul_heatE_mask = (d1['Qcoul'] > 0.0)*wanted_values
Qcoul_coolE_mask = (d1['Qcoul'] < 0.0)*wanted_values
Qcool_mask = (d1['Qcool'] > 0.0)*wanted_values
print("{:d} cells with colder electrons.".format(Te_colder.sum()))
print("{:d} cells with hotter electrons.".format(Tp_colder.sum()))
print("The norm: electrons are colder than protons and Coulomb collisions heat them")
good_Qcoul = Te_colder*Qcoul_heatE_mask
print("{:.0f} cells with colder electrons and Coulomb heating.".format(good_Qcoul.sum()))
print("The concerning: cases where electrons are colder than protons but Coulomb collisions cool them")
bad_Qcoul = Te_colder*Qcoul_coolE_mask
print("{:.0f} cells with colder electrons but Coulomb cooling.".format(bad_Qcoul.sum()))

print("A check: only electrons over the target temperature should cool.")
good_Qcool = Te_over_target*Qcool_mask
print(good_Qcool.sum())
print("Bad news: electrons under the target temperature that still cool due to cooling  function.")
bad_Qcool = (~Te_over_target)*Qcool_mask
print(bad_Qcool.sum())
print("How many cells where Qcool is triggered and it shouldn't be does Qcoul cool electrons?")
print("This could lead to runaway/artificial way overcooling of electrons")
print((bad_Qcool*Qcoul_coolE_mask).sum())


exit()


thetae_target = tools.get_thetae(1.e9)
midplane_ind = int(rcyl.shape[1]/2)
inner50_ind1 = (np.abs(rcyl[:, midplane_ind] + 50.0)).argmin()
inner50_ind2 = (np.abs(rcyl[:, midplane_ind] - 50.0)).argmin()
ISCO_ind1 = (np.abs(rcyl[:, midplane_ind] + header["Risco"])).argmin()
ISCO_ind2 = (np.abs(rcyl[:, midplane_ind] - header["Risco"])).argmin()
EH_ind1 = (np.abs(rcyl[:, midplane_ind] + header["Reh"])).argmin()
EH_ind2 = (np.abs(rcyl[:, midplane_ind] - header["Reh"])).argmin()

print(ISCO_ind1)
print(ISCO_ind2)

thetae1mask = d1['Thetae']*sigma1mask
thetae2mask = d2['Thetae']*sigma2mask
innerISCO_Thetae1 = thetae1mask[ISCO_ind1:ISCO_ind2, :, :]
innerISCO_Thetae2 = thetae2mask[ISCO_ind1:ISCO_ind2, :, :]
innerISCO_Thetae1unmask = d1['Thetae'][ISCO_ind1:ISCO_ind2, :, :]
innerISCO_Thetae2unmask = d2['Thetae'][ISCO_ind1:ISCO_ind2, :, :]
inner50_Thetae1 = thetae1mask[inner50_ind1:inner50_ind2, :]
inner50_Thetae2 = thetae2mask[inner50_ind1:inner50_ind2, :]

innerISCO_TpTe1 = d1['TpTe'][ISCO_ind1:ISCO_ind2, :, :]
innerISCO_TpTe2 = d2['TpTe'][ISCO_ind1:ISCO_ind2, :, :]
innerISCO_Qcoul1 = d1['Qcoul'][ISCO_ind1:ISCO_ind2, :, :]
innerISCO_Qcoul2 = d2['Qcoul'][ISCO_ind1:ISCO_ind2, :, :]
innerISCO_Te1 = d1['Te'][ISCO_ind1:ISCO_ind2, :, :]
innerISCO_Te2 = d2['Te'][ISCO_ind1:ISCO_ind2, :, :]
innerISCO_Tp1 = d1['Tp'][ISCO_ind1:ISCO_ind2, :, :]
innerISCO_Tp2 = d2['Tp'][ISCO_ind1:ISCO_ind2, :, :]
innerISCO_Thetae1 = d1['Thetae'][ISCO_ind1:ISCO_ind2, :, :]
innerISCO_Thetae2 = d2['Thetae'][ISCO_ind1:ISCO_ind2, :, :]
innerISCO_Thetap1 = d1['Thetap'][ISCO_ind1:ISCO_ind2, :, :]
innerISCO_Thetap2 = d2['Thetap'][ISCO_ind1:ISCO_ind2, :, :]
inner50_Te1 = d1['Te'][inner50_ind1:inner50_ind2, :, :]
inner50_Te2 = d2['Te'][inner50_ind1:inner50_ind2, :, :]
inner50_Tp1 = d1['Tp'][inner50_ind1:inner50_ind2, :, :]
inner50_Tp2 = d2['Tp'][inner50_ind1:inner50_ind2, :, :]
inner50_Thetae1 = d1['Thetae'][inner50_ind1:inner50_ind2, :, :]
inner50_Thetae2 = d2['Thetae'][inner50_ind1:inner50_ind2, :, :]
inner50_Thetap1 = d1['Thetap'][inner50_ind1:inner50_ind2, :, :]
inner50_Thetap2 = d2['Thetap'][inner50_ind1:inner50_ind2, :, :]
inner50_Qcoul1 = d1['Qcoul'][inner50_ind1:inner50_ind2, :, :]
inner50_Qcoul2 = d2['Qcoul'][inner50_ind1:inner50_ind2, :, :]
inner50_Qcool1 = d1['Qcool'][inner50_ind1:inner50_ind2, :, :]
inner50_Qcool2 = d2['Qcool'][inner50_ind1:inner50_ind2, :, :]
inner50_Qviscp1 = d1['Qvisc_p'][inner50_ind1:inner50_ind2, :, :]
inner50_Qviscp2 = d2['Qvisc_p'][inner50_ind1:inner50_ind2, :, :]
inner50_KEL1 = d1['KEL'][inner50_ind1:inner50_ind2, :, :]
inner50_KEL2 = d2['KEL'][inner50_ind1:inner50_ind2, :, :]

print(innerISCO_Qcoul1.shape)
Qcoul_min1 = np.min(innerISCO_Qcoul1)
Qcoul_min2 = np.min(innerISCO_Qcoul2)
Qcoul_max1 = np.max(innerISCO_Qcoul1)
Qcoul_max2 = np.max(innerISCO_Qcoul2)
min1_ind = np.where(innerISCO_Qcoul1 == Qcoul_min1)
min2_ind = np.where(innerISCO_Qcoul2 == Qcoul_min2)
# print("Min/max of Qcoul inside ISCO at slice 1: {:.2e} and {:.2e}".format(Qcoul_min1, Qcoul_max1))
# print("Min/max of Qcoul inside ISCO at slice 2: {:.2e} and {:.2e}".format(Qcoul_min2, Qcoul_max2))
# print("Te and Tp at Qcoul min within ISCO slice 1: {:.2e} and {:.2e}K".format(innerISCO_Te1[min1_ind][0], innerISCO_Tp1[min1_ind][0]))
# print("Te and Tp at Qcoul min within ISCO slice 2: {:.2e} and {:.2e}K".format(innerISCO_Te2[min2_ind][0], innerISCO_Tp2[min2_ind][0]))
# print("Thetae and Thetap at Qcoul min within ISCO slice 1: {:.2e} and {:.2e}".format(innerISCO_Thetae1[min1_ind][0], innerISCO_Thetap1[min1_ind][0]))
# print("Thetae and Thetap at Qcoul min within ISCO slice 2: {:.2e} and {:.2e}".format(innerISCO_Thetae2[min2_ind][0], innerISCO_Thetap2[min2_ind][0]))

Qcoul_min1 = np.min(inner50_Qcoul1)
Qcoul_min2 = np.min(inner50_Qcoul2)
Qcoul_max1 = np.max(inner50_Qcoul1)
Qcoul_max2 = np.max(inner50_Qcoul2)
min1_ind = np.where(inner50_Qcoul1 == Qcoul_min1)
min2_ind = np.where(inner50_Qcoul2 == Qcoul_min2)
max1_ind = np.where(inner50_Qcoul1 == Qcoul_max1)
max2_ind = np.where(inner50_Qcoul2 == Qcoul_max2)
# print(min1_ind)
# print("Min/max of Qcoul inside 50rg at slice 1: {:.2e} and {:.2e}".format(Qcoul_min1, Qcoul_max1))
# print("Min/max of Qcoul inside 50rg at slice 2: {:.2e} and {:.2e}".format(Qcoul_min2, Qcoul_max2))
# print("Te and Tp at Qcoul min within 50rg slice 1: {:.2e} and {:.2e}K".format(inner50_Te1[min1_ind][0], inner50_Tp1[min1_ind][0]))
# print("Te and Tp at Qcoul min within 50rg slice 2: {:.2e} and {:.2e}K".format(inner50_Te2[min2_ind][0], inner50_Tp2[min2_ind][0]))
# print("Te and Tp at Qcoul max within 50rg slice 1: {:.2e} and {:.2e}K".format(inner50_Te1[max1_ind][0], inner50_Tp1[max1_ind][0]))
# print("Te and Tp at Qcoul max within 50rg slice 2: {:.2e} and {:.2e}K".format(inner50_Te2[max2_ind][0], inner50_Tp2[max2_ind][0]))
# print("Thetae and Thetap at Qcoul min within 50rg slice 1: {:.2e} and {:.2e}".format(inner50_Thetae1[min1_ind][0], inner50_Thetap1[min1_ind][0]))
# print("Thetae and Thetap at Qcoul min within 50rg slice 2: {:.2e} and {:.2e}".format(inner50_Thetae2[min2_ind][0], inner50_Thetap2[min2_ind][0]))
# print("KEL at Qcoul min within 50rg slice 1: {:.2e}".format(inner50_KEL1[min1_ind][0]))
# print("KEL at Qcoul min within 50rg slice 2: {:.2e}".format(inner50_KEL2[min2_ind][0]))
# print("Qvisc_p at Qcoul min within 50rg slice 1: {:.2e}".format(inner50_Qviscp1[min1_ind][0]))
# print("Qvisc_p at Qcoul min within 50rg slice 2: {:.2e}".format(inner50_Qviscp2[min2_ind][0]))
# print("Qcool at Qcoul min within 50rg slice 1: {:.2e}".format(inner50_Qcool1[min1_ind][0]))
# print("Qcool at Qcoul min within 50rg slice 2: {:.2e}".format(inner50_Qcool2[min2_ind][0]))

r_min = r_vals[min1_ind[0]][0]
theta_min = theta_vals[min1_ind[1]][0]
phi_min = phi_vals[min1_ind[2]][0]
r_max = r_vals[max1_ind[0]][0]
theta_max = theta_vals[max1_ind[1]][0]
phi_max = phi_vals[max1_ind[2]][0]
print(r'Slice 1 Minimum value at $r={:.2f}~r_g$, $\theta={:.2f}$, $\phi={:.2f}$'.format(r_min, theta_min, phi_min))
print(r'Slice 1 Maximum value at $r={:.2f}~r_g$, $\theta={:.2f}$, $\phi={:.2f}$'.format(r_max, theta_max, phi_max))

# Smallest non-zero values within the ISCO
min1 = np.min(innerISCO_Thetae1[np.nonzero(innerISCO_Thetae1)])
min2 = np.min(innerISCO_Thetae2[np.nonzero(innerISCO_Thetae2)])
max1 = np.max(innerISCO_Thetae1[np.nonzero(innerISCO_Thetae1)])
max2 = np.max(innerISCO_Thetae2[np.nonzero(innerISCO_Thetae2)])
min1_ind = np.where(innerISCO_Thetae1 == min1)
min2_ind = np.where(innerISCO_Thetae2 == min2)
max1_ind = np.where(innerISCO_Thetae1 == max1)
max2_ind = np.where(innerISCO_Thetae2 == max2)
print(min1_ind)
print(min2_ind)
min1_phi_ind = min1_ind[2][0]
min2_phi_ind = min2_ind[2][0]

TpTe_min1 = innerISCO_TpTe1.min()
TpTe_min2 = innerISCO_TpTe2.min()
TpTe_max1 = innerISCO_TpTe1.max()
TpTe_max2 = innerISCO_TpTe2.max()
min1_ind = np.where(innerISCO_TpTe1 == TpTe_min1)
min2_ind = np.where(innerISCO_TpTe2 == TpTe_min2)
max1_ind = np.where(innerISCO_TpTe1 == TpTe_max1)
max2_ind = np.where(innerISCO_TpTe2 == TpTe_max2)
min1_phi_ind = min1_ind[2][0]
min2_phi_ind = min2_ind[2][0]
min1_phi_ind = max1_ind[2][0]
min2_phi_ind = max2_ind[2][0]

Tpmin = 1.e9; Tpmax = 1.e12
Temin = 1.e7; Temax = 1.e11
print("First slice's Tp/Te ranges from {:.2e} to {:.2e}".format(TpTe_min1, TpTe_max1))
print("Second slice's Tp/Te ranges from {:.2e} to {:.2e}".format(TpTe_min2, TpTe_max2))
print(np.allclose(d1['TpTe'], (d1['Tp']/d1['Te'])))
# # Get slices
# thetae1slice = pu.flatten_xz(thetae1mask, header, azi_avg=False, phi_ind=min1_phi_ind)
# thetae2slice = pu.flatten_xz(thetae2mask, header, azi_avg=False, phi_ind=min2_phi_ind)
# thetae1slice_unmask = pu.flatten_xz(d1['Thetae'], header, azi_avg=False, phi_ind=min1_phi_ind)
# thetae2slice_unmask = pu.flatten_xz(d2['Thetae'], header, azi_avg=False, phi_ind=min2_phi_ind)

if (d1['Qcool'] < 0.0).any():
    print("WARNING!! Slice 1 has negative Qcool!")
if (d2['Qcool'] < 0.0).any():
    print("WARNING!! Slice 2 has negative Qcool!")

if d1['sigma'].shape[2] > 1.:
    sigma_noAzi = pu.flatten_xz(d1['sigma'], header, azi_avg=False, phi_ind=min1_phi_ind)
    sigma_noAzi2 = pu.flatten_xz(d2['sigma'], header, azi_avg=False, phi_ind=min2_phi_ind)
else:
    sigma_noAzi = d1['sigma'][:, :, 0]
    sigma_noAzi2 = d2['sigma'][:, :, 0]

# Plot the minimum value slice
plt.figure()
ax = plt.gca()
# rcyl, z, mesh, sc = pu.plot_xz(ax, geom, d1['Thetae'] - thetae_target, header, azi_avg=False, cmap='RdBu_r', vmin=-0.1, vmax=0.1, phi_ind=min1_phi_ind)
# rcyl, z, mesh, sc = pu.plot_xz(ax, geom, d1['Te'], header, azi_avg=False, cmap='RdBu_r', vmin=Temin, vmax=Temax, phi_ind=min1_phi_ind)
rcyl, z, mesh, sc = pu.plot_xz(ax, geom, d1['RHO'], header, azi_avg=False, cmap=r'viridis', phi_ind=min1_phi_ind, vmin=1.e-5, vmax=1.)
# rcyl, z, mesh, sc = pu.plot_xz(ax, geom, d1['Qcoul'], header, azi_avg=False, cmap='RdBu_r', vmin=-1.e-6, vmax=1.e-6, xlim=50., ylim=50., phi_ind=min1_phi_ind)
# rcyl, z, mesh, sc = pu.plot_xz(ax, geom, d1['Qvisc_e'], header, azi_avg=False, cmap='RdBu_r', vmin=-1.e-2, vmax=1.e-2, phi_ind=min1_phi_ind)
# rcyl, z, mesh, sc = pu.plot_xz(ax, geom, d1['Qcool']*double1mask, header, azi_avg=False, cmap='viridis', vmin=1.e-7, vmax=1.e-3, phi_ind=min1_phi_ind)
# rcyl, z, mesh, sc = pu.plot_xz(ax, geom, -d1['Qcool']/d1['Qcoul']*double1mask, header, azi_avg=False, cmap='RdBu_r', vmin=1.e-1, vmax=1.e1, phi_ind=min1_phi_ind)
# rcyl, z, mesh, sc = pu.plot_xz(ax, geom, d1['TpTe'], properties["header"], azi_avg=False, cmap='RdBu_r', vmin=header["tptemin"], vmax=header["tptemax"], phi_ind=min1_phi_ind)
con_data1 = plt.contour(rcyl, z, sigma_noAzi, levels=np.array([1.0]), colors=sc)
# plt.colorbar(mesh, label=r"$\theta_e-\theta_{e, {\rm target}}$")
# plt.colorbar(mesh, label=r"$T_p/T_e$")
# plt.colorbar(mesh, label=r"$T_e$")
plt.colorbar(mesh, label=r"$\rho$")
# plt.colorbar(mesh, label=r"$Q_{\rm coul}$")
# plt.colorbar(mesh, label=r"$Q_{\rm visc}^e$")
# plt.colorbar(mesh, label=r"$Q_{\rm cool}$")
# plt.colorbar(mesh, label=r"$-Q_{\rm cool}/Q_{\rm coul}$")
plt.title(r'$t={:.2f}~r_g/c$, $\phi={:.2f}$'.format(d1['t'], phi_vals[min1_phi_ind]))
# plt.xlim([0., 5.])
# plt.ylim([-5., 5.])
xlim = 50; ylim = xlim
plt.xlim([0, xlim])
plt.ylim([-ylim, ylim])
plt.tight_layout()
# plt.show()

# sigma_cut_contours = {"t0":{}, "t1":{}}
# level_one_polygons = con_data1.allsegs[0]
# for n, poly in enumerate(level_one_polygons):
    # if len(poly) > 100:
        # sigma_cut_contours["t0"][n] = poly
#
fig, ax = plt.subplots()
# rcyl, z, mesh, sc = pu.plot_xz(ax, geom, d2['Thetae'] - thetae_target, properties["header"], azi_avg=False, cmap='RdBu_r', vmin=-0.1, vmax=0.1, phi_ind=min2_phi_ind)
# rcyl, z, mesh, sc = pu.plot_xz(ax, geom, d2['TpTe'], properties["header"], azi_avg=False, cmap='RdBu_r', vmin=header["tptemin"], vmax=header["tptemax"], phi_ind=min2_phi_ind)
# rcyl, z, mesh, sc = pu.plot_xz(ax, geom, d2['Qcoul'], header, azi_avg=False, cmap='RdBu_r', vmin=-1.e-2, vmax=1.e-2, phi_ind=min2_phi_ind)
rcyl, z, mesh, sc = pu.plot_xz(ax, geom, d2['RHO'], header, azi_avg=False, cmap=r'viridis', phi_ind=min2_phi_ind, vmin=1.e-5, vmax=1., xlim=xlim, ylim=ylim)
con_data = plt.contour(rcyl, z, sigma_noAzi2, levels=np.array([1.0]), colors=sc)
# plt.colorbar(mesh, label=r"$\theta_e-\theta_{e, {\rm target}}$")
# plt.colorbar(mesh, label=r"$T_p/T_e$")
plt.colorbar(mesh, label=r"$\rho$")
# plt.colorbar(mesh, label=r"$Q_{\rm coul}$")
plt.title(r'$t={:.2f}~r_g/c$, $\phi={:.2f}$'.format(d2['t'], phi_vals[min2_phi_ind]))
plt.tight_layout()

print(d2['RHO'].shape)
print(min2_phi_ind)
print(min1_phi_ind)

fig, ax = plt.subplots()
# rcyl, z, mesh, sc = pu.plot_xz(ax, geom, d2['Tp'], properties["header"], azi_avg=False, cmap='viridis', vmin=Tpmin, vmax=Tpmax, phi_ind=min2_phi_ind)
# rcyl, z, mesh, sc = pu.plot_xz(ax, geom, d2['TpTe'], properties["header"], azi_avg=False, cmap='RdBu_r', vmin=header["tptemin"], vmax=header["tptemax"], phi_ind=min2_phi_ind)
# rcyl, z, mesh, sc = pu.plot_xz(ax, geom, d2['Te'], properties["header"], azi_avg=False, cmap='RdBu_r', vmin=Temin, vmax=Temax, phi_ind=min2_phi_ind)
rcyl, z, mesh, sc = pu.plot_xz(ax, geom, d2['Te'], properties["header"], azi_avg=False, cmap='RdBu_r', vmin=Temin, vmax=Temax, phi_ind=40, xlim=xlim, ylim=ylim)

min2_phi_ind = 40
print(d2['Te'][:, :, min2_phi_ind].min())
print("{:.2e}".format(d2['Te'][:, :, min2_phi_ind].max()))
con_data = plt.contour(rcyl, z, sigma_noAzi2, levels=np.array([1.0]), colors=sc)
# plt.colorbar(mesh, label=r"$\theta_e-\theta_{e, {\rm target}}$")
plt.colorbar(mesh, label=r"$T_e$")
# plt.colorbar(mesh, label=r"$T_p$")
plt.title(r'$t={:.2f}~r_g/c$, $\phi={:.2f}$'.format(d2['t'], phi_vals[min2_phi_ind]))
# plt.xlim([0., 5.])
# plt.ylim([-5., 5.])
plt.tight_layout()
plt.show()
exit()
# level_one_polygons = con_data.allsegs[0]
# n = 0
# for poly in level_one_polygons:
    # if len(poly) > 100:
        # sigma_cut_contours["t1"][n] = poly
        # n += 1
#
# plt.close('all')
# plt.figure()
# ax = plt.gca()
# for t, dfile in enumerate([d1, d2]):
    # tstr = "t{:d}".format(t)
    # tkeys = list(sigma_cut_contours[tstr].keys())
    # plt.plot(sigma_cut_contours[tstr][tkeys[0]][:, 0], sigma_cut_contours[tstr][tkeys[0]][:, 1], color=colorblind_colors[t], label=r"$t={:.2f}~r_g/c$".format(dfile['t']))
    # plt.plot(sigma_cut_contours[tstr][tkeys[1]][:, 0], sigma_cut_contours[tstr][tkeys[1]][:, 1], color=colorblind_colors[t], label=None, ls='--')
# plt.legend()
# circle1 = plt.Circle((0,0), properties["header"]['Reh'],color='k', ls=':', fill=False);
# ax.add_artist(circle1)
# circle2 = plt.Circle((0,0), properties["header"]['Risco'],color='red', ls='--', fill=False);
# ax.add_artist(circle2)
# plt.xlabel('$x/M$')
# plt.ylabel('$z/M$')
# plt.xlim([-5, 5])
# plt.ylim([-5, 5])
# plt.show()

# ----------------------------------------------

print((innerISCO_Thetae1 < thetae_target).sum())
print((innerISCO_Thetae2 < thetae_target).sum())

xlims = [5.e-2, 10]
fig, axs = plt.subplots(1, 2, sharey=True, sharex=True, tight_layout=True)
n_bins = 20
bins = np.logspace(np.log10(xlims[0]), np.log10(xlims[1]), n_bins)
axs[0].hist(innerISCO_Thetae1.flatten(), bins=bins, log=True)
axs[1].hist(innerISCO_Thetae2.flatten(), bins=bins, log=True)
axs[0].axvline([thetae_target], color='black', ls='--')
axs[1].axvline([thetae_target], color='black', ls='--')
# ax.axvline([np.log10(thetae_target)], color='black', ls='--')
axs[0].set_yscale('log')
axs[0].set_xlim(xlims)
axs[0].set_xscale('log')
axs[0].set_ylim([1., None])
axs[0].set_ylabel("Counts")
axs[0].set_xlabel(r"$\theta_e$")
axs[1].set_xlabel(r"$\theta_e$")
axs[0].set_title(r'$t={:.2f}~r_g/c$'.format(d1['t']))
axs[1].set_title(r'$t={:.2f}~r_g/c$'.format(d2['t']))
plt.suptitle("Electron temperature within the ISCO")
# plt.figure(); ax = plt.gca()
# plt.hist(innerISCO_Thetae2, bins=20)
# ax.axhline([thetae_target], color='black', ls='--')
print(innerISCO_Thetae1.min())
print(innerISCO_Thetae2.min())

print("t1's smallest value of {:.3e} found at (i, j) = ({:d}, {:d})".format(min1, min1_ind[0][0], min1_ind[1][0]))
print("t2's smallest value of {:.3e} found at (i, j) = ({:d}, {:d})".format(min2, min2_ind[0][0], min2_ind[1][0]))

print("Value of t1 at t2's min location: {:.3e}".format(innerISCO_Thetae1[min2_ind][0]))
print("Value of t2 at t1's min location: {:.3e}".format(innerISCO_Thetae2[min1_ind][0]))

print("({:d}, {:d}) went from {:.3e} to {:.3e} from t1 to t2".format(min1_ind[0][0], min1_ind[1][0], min1, innerISCO_Thetae2[min1_ind][0]))
print("({:d}, {:d}) went from {:.3e} to {:.3e} from t1 to t2".format(min2_ind[0][0], min2_ind[1][0], innerISCO_Thetae1[min2_ind][0], min2))
# plt.show()
