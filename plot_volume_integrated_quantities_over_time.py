import numpy as np
import os
import matplotlib.lines as mlines
import matplotlib.pyplot as plt
import sys
sys.path.insert(0, '../')
sys.path.append('modules/')
from ebhlight_tools import *
import hdf5_to_dict as io
from compare_handler import comparison


def plot_volume_integrated_quantities_over_time_panel(com, **kwargs):
    """
    Plot panel figure comparing Mdot simulations.
    The first column is accretion rate, second is
    volume-integrated efficiencies of Qvisc and Qcool.
    Row 1: first accretion rate (Mu21)
    Row 2: next accretion rate (Mu25, etc.)
    """
    figdir = "/mnt/e/ebhlight_ecool_tests/figures/paper_figures/"
    figdir = kwargs.get("figdir", figdir)
    figname = "compare_volume_integrated_quantities_over_time.png"
    figname = kwargs.get("figname", figname)
    # ylimits = [[1.e-1, 1.e3], [1.e-5, 1.e3]]
    ylimits = [[1.e-9, 1.e-3], [1.e-5, 1.e0], [1, 320*256]]
    ylimits = kwargs.get("ylimits", ylimits)
    save_pdf = kwargs.get("save_pdf", False)
    dpi = kwargs.get("dpi", 100)

    quantity_sets = [["mdot_eh"], ["visc_efficiency", "cool_efficiency"], ["num_super"]]
    # quantity_sets = [["mdot_eh"], ["cool_efficiency"], ["num_super"]]
    col_names = [r"$\dot M/\dot M_{\rm Edd}$", r"Efficiency $Q/\dot Mc^2$", r"Supercooling instances $N_{\rm super}$"]
    row_names = [r"$\dot m_0\sim10^{-8}$", r"$\dot m\sim10^4\dot m_0$"]
    row_names = [r"Small Accretion Rate", "Large Accretion Rate"]
    all_quantities = [item for sublist in quantity_sets for item in sublist]
    mdot_values = []
    for sim in com.sim_names:
        sim_mdot_ind = sim.index("Mu")
        sim_mdot = sim[sim_mdot_ind:sim_mdot_ind + 4]
        if sim_mdot not in mdot_values:
            mdot_values.append(sim_mdot)
    nrow = len(mdot_values)
    ncol = len(quantity_sets)
    linestyles = ['-', '--']

    kwargs = {}
    kwargs["quiet"] = True

    width_to_height = 8.0/12.0
    fig_width = ncol*3.0 + 3.0
    fig_height = fig_width*width_to_height

    fig, axs = plt.subplots(nrow, ncol, sharex=True, figsize=(fig_width, fig_height), sharey='col')
    tit_str = com.title
    fig.suptitle(tit_str)
    # Plot data
    for row_num, mdot in enumerate(mdot_values):
        for sim_name in com.sim_names:
            if mdot not in sim_name:
                continue
            sim = com.simulations[sim_name]
            diag = io.load_diag(sim.path_to_reduced_data, sim.header)
            for col_num, quantities in enumerate(quantity_sets):
                # Set column titles
                if row_num == 0:
                    axs[row_num][col_num].set_title(col_names[col_num])
                cycle_quantities = quantities.copy()
                for j, q in enumerate(cycle_quantities):
                    diag_data = sim.retrieve_diag_quantity(diag, q)
                    if diag_data is not None:
                        axs[row_num][col_num].plot(diag['t'], diag_data,
                                                # label=labels[row_num][j],
                                                color=com.sim_colors[sim_name],
                                                ls=linestyles[j])
                # Set y-limits for each row
                axs[0][col_num].set_ylim(ylimits[col_num])
                # Set all to have log y scales
                axs[row_num, col_num].set_yscale('log')
                # Set xlabel/xlim for each
                if row_num == nrow - 1:
                    axs[row_num, col_num].set_xlabel(r"$tc/r_g$")
                if col_num == 0:
                    axs[row_num, col_num].set_ylabel(row_names[row_num])
    # Legend for isothermal vs. not
    mhandles = []
    mhandles.append(mlines.Line2D([], [],
                                    color=colorblind_colors[0],
                                    ls=linestyles[0],
                                    label="Regular Electrons"))
    if "isothermal" in category:
        label_str = "Isothermal electrons"
    else:
        label_str = "Coulomb uses $T_e=10^9$ K"
    mhandles.append(mlines.Line2D([], [],
                                    color=colorblind_colors[1],
                                    ls=linestyles[0],
                                    label=label_str)
    )
    axs[0][0].legend(handles=mhandles,
                      loc='upper right',
                      # bbox_to_anchor=(0, 0.),
                      frameon=False,
                      # ncol=2
    )
    # Legend for different quantities
    qhandles = []
    qhandles.append(mlines.Line2D([], [],
                                    color='k',
                                    ls=linestyles[0],
                                    label=r"$Q_{\rm visc}/\dot Mc^2$"))
    qhandles.append(mlines.Line2D([], [],
                                    color='k',
                                    ls=linestyles[1],
                                    label=r"$Q_{\rm cool}/\dot Mc^2$"))
    axs[0][1].legend(handles=qhandles,
                     loc='upper right',
                     frameon=False)
    plt.tight_layout()
    print("Saving figure " + figdir + figname)
    if not os.path.exists(figdir): os.makedirs(figdir)
    plt.savefig(figdir + figname, bbox_inches='tight', dpi=dpi)
    if save_pdf:
        if not os.path.exists(figdir + "pdfs/"): os.makedirs(figdir + "pdfs/")
        plt.savefig(figdir + "pdfs/" + figname.replace(".png", ".pdf"), bbox_inches='tight', dpi=dpi)
    return

if __name__ == '__main__':
    plt.style.use('lia.mplstyle')

    kwargs = {}
    setup = "seagate_tests"
    category = "compare_2D_fakeQcoul"
    # category = "compare_2D_isothermalE"
    com = comparison(category, setup)
    plot_volume_integrated_quantities_over_time_panel(com, **kwargs)
