import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate
from cartesian_analyzer import *
import sys;
sys.path.append('modules/')
import units
units = units.get_cgs()
from ebhlight_tools import *

class analytic_solution:
    def __init__(self, sim):
        # Use simulation to set initial values.
        # These values can be manually reset by calling
        # set_Te0, etc.
        self.sim = sim
        self.Te0 = sim.header["Te0"]
        self.Tp0 = sim.header["Tp0"]
        self.n0_code = sim.header["ne"]
        self.rho0_code = self.n0_code*units['MP']
        # Obtain relevant quantities
        self.gammae = sim.header["game"]
        self.gammap = sim.header["gamp"]
        self.gamma = sim.header["gam"]
        if sim.has_cooling:
            self.Te_target = sim.header["Tel_target"]
        try:
            self.q_constant = sim.header["q_constant"]
        except:
            self.q_constant = 0.5
        self.tf_code = sim.header["tf"]
        self.L_unit = sim.header["L_unit"]
        self.M_unit = sim.header["M_unit"]
        if sim.has_cooling:
            self.U_unit = sim.header["U_unit"]
            self.T_unit = sim.header["T_unit"]
            self.tcool_code = sim.header["tcool0"]
        else:
            rho_unit = self.M_unit/self.L_unit**3
            self.U_unit = rho_unit*units['CL']**2
            self.T_unit = self.L_unit/units['CL']

        # Conversion to cgs
        if sim.has_cooling:
            self.tcool_cgs = self.tcool_code*self.L_unit/units['CL']
        self.tf_cgs = self.tf_code*self.L_unit/units['CL']
        self.rho0_cgs = self.rho0_code*self.M_unit/self.L_unit**3
        self.n0_cgs = self.rho0_cgs/units['MP']

    def set_Tp0(self, Tp0):
        self.Tp0 = Tp0
        return

    def set_rho0_code(self, rho0):
        self.rho0_code = rho0
        self.rho0_cgs = rho0/self.L_unit**3
        return

    def set_rho0_cgs(self, rho0):
        self.rho0_cgs = rho0
        self.rho0_code = rho0*self.L_unit**3
        return

    def set_Te0(self, Te0):
        self.Te0 = Te0
        return

    def set_tcool_code(self, tc):
        self.tcool_code = tc
        self.tcool_cgs = self.tcool_code*self.L_unit/units['CL']
        return

    def set_tf_code(self, tf):
        self.tf_code = tf
        self.tf_cgs = self.tf_code*self.L_unit/units['CL']
        return

    def set_tf_cgs(self, tf):
        self.tf_cgs = tf
        self.tf_code = self.tf_cgs*units['CL']/self.L_unit
        return

    def coolingFunction(self, Te):
        # Following ebhlight implementation
        Y = Te/self.Te_target - 1.0
        Y = np.min([Y, 1000])
        ue_code = (self.n0_cgs*units['KBOL']/self.U_unit)*Te/(self.gammae - 1.0)
        L_code = 2.0*ue_code*Y**self.q_constant/self.tcool_code
        # due/dt = -L, since ue = n0 kB Te/(gammae - 1),
        # dTe/dt = -L*(gammae-1)/(n0 kB)
        Qcool_code = -L_code*(self.gammae - 1.0)/(units['KBOL']*self.n0_cgs/self.U_unit)
        # print("Y = {:.3e}".format(Y))
        # print("Y^q = {:.3e}".format(Y**self.q_constant))
        # print("ue = {:.3e}, due={:.3e}".format(ue, L*self.dt_cgs))
        # print("Qcool = {:.3e}, dTe={:.3e}".format(Qcool, Qcool*self.dt_cgs))
        # print("tcool = {:.3e}".format(self.tcool_cgs))
        # print("dt = {:.3e}".format(self.dt_cgs))

        if Y > 0 and Te > 0:
            return Qcool_code
        else:
            return 0.0

    def coulombHeating(self, Te, Tp, time):
        """
        Get Coulomb heating rate in cgs (erg/s/cm^3).
        Stepney & Guilbert 1983 eq. 15
        Note this is the heating rate for the electron energy Ee,
        where Ee = internal energy density (units of erg/cm^3).
        """
        if time == 0.0:
            return 0.0
        # Following ebhlight implementation
        # Need to handle cases where Thetap < 1e-2, Thetae < 1e-2, and both
        # Thetae and Thetap < 1e-2 separately due to Bessel functions exploding
        thetaCrit = 1.e-2
        thetae = get_thetae(Te)
        thetap = get_thetap(Tp)
        thetam = get_thetam(thetae, thetap)

        prefactor = 3.0/2.0*units['ME']/units['MP']
        prefactor *= units['COULOMBLOG']*units['CL']*units['KBOL']
        prefactor *= units['THOMSON']
        prefactor *= self.n0_cgs**2
        prefactor *= (Tp - Te)

        if (thetae < thetaCrit) and (thetap < thetaCrit):
            if thetam < 0:
                print("ERROR: thetam = {:.2f}, t={:.2e} ".format(thetam, time))
            if thetae < 0:
                print("ERROR: thetae = {:.2f}, t={:.2e} ".format(thetae, time))
            if thetap < 0:
                print("ERROR: thetap = {:.2f} ".format(thetap))
            term1 = np.sqrt(thetam/(np.pi*thetae*thetap/2.));
            term2 = np.sqrt(thetam/(np.pi*thetae*thetap/2.));
        elif (thetae < thetaCrit):
            term1 = np.exp(-1./thetap)/safeKn(2, 1./thetap)*np.sqrt(thetam/thetae)
            term2 = np.exp(-1./thetap)/safeKn(2, 1./thetap)*np.sqrt(thetam/thetae)
        elif (thetap < thetaCrit):
            term1 = np.exp(-1./thetae)/safeKn(2, 1./thetae)*np.sqrt(thetam/thetap)
            term2 = np.exp(-1./thetae)/safeKn(2, 1./thetae)*np.sqrt(thetam/thetap)
        else:
            term1 = safeKn(1, 1./thetam)/(safeKn(2, 1./thetae)*safeKn(2, 1./thetap))
            term2 = safeKn(0, 1./thetam)/(safeKn(2, 1./thetae)*safeKn(2, 1./thetap))

        term1 *= (2.0*(thetae + thetap)**2 + 1.0)/(thetae + thetap)
        term2 *= 2.0

        qC_cgs = prefactor*(term1 + term2)
        return qC_cgs

    def no_cooling(self, t, temperatures):
        Te, Tp = temperatures
        # Convert qC to code units
        qC_code = self.coulombHeating(Te, Tp, t)*self.T_unit/self.U_unit
        dTedt = (self.gammae - 1.0)/(units['KBOL']*self.n0_cgs/self.U_unit)*qC_code
        dTpdt = -(self.gammap - 1.0)/(units['KBOL']*self.n0_cgs/self.U_unit)*qC_code
        return [dTedt, dTpdt]

    def coupled_system(self, t, temperatures):
        Te, Tp = temperatures
        """
        Note that although internal energies ue and up change by the same magnitude
        qC, the temperature of electrons and protons will change by different amounts
        because electrons and protons have different heat capacities (game vs gamp).
        qCoul converts qC from internal energy to temperature.
        """
        # Convert qC to code units
        qC_code = self.coulombHeating(Te, Tp, t)*self.T_unit/self.U_unit
        # Convert to temperature instead of internal energy,
        # as in ebhlight's coulomb function.
        qCoule_code = qC_code*(self.gammae - 1.0)/(self.n0_cgs*units['KBOL']/self.U_unit)
        qCoulp_code = qC_code*(self.gammap - 1.0)/(self.n0_cgs*units['KBOL']/self.U_unit)
        # qCoule = (self.gammae - 1.0)/(kB_code*self.n0_code)*qC_code
        # qCoulp = (self.gammap - 1.0)/(kB_code*self.n0_code)*qC_code
        # print("t = {:.3e}".format(t))
        # print("Te = {:.3e}, Target = {:.3e}".format(Te, self.Te_target))
        # print("qCoul = {:.3e}, dTe={:.3e}".format(qCoul, qCoul*self.dt_cgs))
        qCool_code = self.coolingFunction(Te)
        dTedt_code = qCoule_code + qCool_code
        dTpdt_code = -qCoulp_code
        # print("dTedt = {:.3e}, dTe = {:.3e}".format(dTedt, dTedt*self.dt_cgs))
        return [dTedt_code, dTpdt_code]

    def get_analytic_solution(self):
        # Now solve
        dt_max_code = np.min([self.tcool_code*0.5, 50])
        initial_state = [self.Te0, self.Tp0]
        # dt_max_cgs = self.tcool_cgs*0.5
        # result = integrate.solve_ivp(self.coupled_system, [0.0, self.tf_cgs], initial_state, rtol=1e-8, first_step=dt_max_cgs, max_step=dt_max_cgs)
        # no_cooling_result = integrate.solve_ivp(self.no_cooling, [0.0, self.tf_cgs], initial_state)
        print(self.tf_code)
        print(dt_max_code)
        result_code = integrate.solve_ivp(self.coupled_system, [0.0, self.tf_code], initial_state, rtol=1e-8, first_step=dt_max_code, max_step=dt_max_code)
        no_cooling_result = integrate.solve_ivp(self.no_cooling, [0.0, self.tf_code], initial_state)
        return (result_code, no_cooling_result)

    def plot_analytic_solution(self):
        with_cooling, without_cooling = self.get_analytic_solution()
        Te_over_time = with_cooling.y[0]
        Tp_over_time = with_cooling.y[1]
        times_cool = with_cooling.t
        Te_no_cool = without_cooling.y[0]
        Tp_no_cool = without_cooling.y[1]
        times_no_cool = without_cooling.t
        marker = None
        plt.style.use('lia.mplstyle')
        plt.figure()
        plt.plot(times_cool, Tp_over_time, label="Protons", color="C0", marker=marker)
        plt.plot(times_cool, Te_over_time, label="Electrons", color="C1", marker=marker)
        plt.plot(times_no_cool, Tp_no_cool, label="No cooling Protons", ls='--', color="C0", marker=marker)
        plt.plot(times_no_cool, Te_no_cool, label="No cooling Electrons", ls='--', color="C1", marker=marker)
        plt.legend()
        plt.yscale('log')
        plt.xscale('log')
        plt.axhline(self.Te_target, ls=':', color='black')
        plt.ylabel("Temperature [K]")
        plt.xlabel(r"$tL/c$")
        plt.title(r"$t_{{\rm cool}}=$" + format_exp(self.tcool_cgs) + " s")
        plt.tight_layout()
        plt.show()


if __name__ == '__main__':
    setup = "lia_hp"
    sim_name = "test_gas_box_Te1e9Tp1e11"
    sim_name = "gas_box_template"
    kwargs = {}
    # kwargs["path_to_raw_data"] = "/mnt/c/Users/liaha/research/projects/ebhlight_code/ebhlight_ecool/prob/test_gas_box_Te1e9Tp1e11/dumps/"
    kwargs["path_to_raw_data"] = "/mnt/c/Users/liaha/research/projects/ebhlight_code/ebhlight_ecool/prob/gas_box_template/dumps/"
    plt.style.use('lia.mplstyle')
    gas_box = cartesian_analyzer(sim_name, setup, **kwargs)
    analytic_solver = analytic_solution(gas_box)

    # ----------------------------------
    # Set quantities manually. For testing,
    # not comparing to simulation
    Te0 = 1e8
    Tp0 = 1e10
    rho0 = 1e-4
    analytic_solver.set_Te0(Te0)
    analytic_solver.set_Tp0(Tp0)
    analytic_solver.set_rho0_code(rho0)
    # ----------------------------------

    # (times, with_cooling, without_cooling) = analytic_solver.get_analytic_solution()
    analytic_solver.set_tf_code(1e5)
    analytic_solver.set_tcool_code(1e3)
    analytic_solver.plot_analytic_solution()
