import numpy as np
from cycler import cycler
import os
import shutil
import matplotlib
import matplotlib.pyplot as plt
import sys;
sys.path.append('modules/')
from plotting_utils import *
from ebhlight_tools import *
import hdf5_to_dict as io
from default_filepaths import get_default_filepaths
from base_simulation_analyzer import base_simulation_analyzer
from compare_handler import comparison
from nice_plots import *


def plot_figure():
    """
    Specific kwargs for paper figure.
    """
    kwargs = {}
    # category = "test_3D_Mdot"
    category = "test_3D_MdotHigh"
    setup = "seagate_tests"
    com = comparison(category, setup)
    kwargs["tstart_rgc"] = 9000.0
    kwargs["tend_rgc"] = 10000.0
    # kwargs["ylimits"] = [[1.e-1, 1.e3], [1.e-5, 1.e3]]
    kwargs["ylimits"] = [[1.e-1, 1.e8], [1.e-5, 1.e3]]
    kwargs["figdir"] = "/mnt/e/ebhlight_ecool_tests/figures/paper_figures/"
    kwargs["figname"] = "compare_mdot_grid.png"
    kwargs["setup"] = setup
    kwargs["category"] = category
    plot_timescales_and_heating_panel(com, **kwargs)

if __name__ == '__main__':
    plt.style.use('lia.mplstyle')

    # ----------------------------------------------
    # Plot panel figure comparing Mdot simulations
    # Each column is a simulation with certain Mdot
    # Row 1: timescales. Row 2: Heating ratios.
    # ----------------------------------------------
    plot_figure()
